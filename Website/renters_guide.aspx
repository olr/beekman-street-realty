﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="renters_guide.aspx.cs" Inherits="renters_guide" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages guides">
        <div class="titles-global" style="margin-bottom: 20px;">
            rentals guide
        </div>
        <div class="text">
            <p>
                <span>Guide to Renting an Apartment in New York City</span></p>
            <p>
                If you are a renter in the New York City area, there are several important factors
                you need to be aware of in order to qualify and move in to your new apartment. You
                should become familiar with the rules of renting in order to make your apartment
                search go as smoothly and hassle-free as possible.
            </p>
            <p>
                <b>Be Prepared to Take Action Immediately</b></p>
            <p>
                Since apartments in New York City are often rented on the spot (this especially
                holds true in Manhattan), personal documentation is vital when viewing an apartment
                with your agent. The more information you have immediately available for the landlord,
                the more likely you are to attain the apartment. Such documentation includes:
            </p>
            <ol>
                <li>Personal ID (i.e. Driver’s license, passport etc.).</li>
                <li>A letter of employment/salary verification. </li>
                <li>Recent (current) pay stubs from the last two pay periods, to show proof of income.</li>
                <li>Bank account numbers (checking and savings) and any credit card numbers.</li>
                <li>Current bank statements.</li>
                <li>Contact information of previous landlords. Landlords like to see a rental history,
                    if applicable. </li>
                <li>Contact information of your accountant or attorney, if applicable. </li>
                <li>Contact information of any personal or professional references.</li>
                <li>Tax returns from the last two years. </li>
            </ol>
            <p>
                When you are applying for your apartment, make sure you have blank checks with you
                because you may need to give a deposit at the time you submit the application. The
                deposit amount will typically be the equivalent of the first month’s rent plus one
                month security deposit, although sometimes the landlord will require the last month’s
                rent as well. Therefore you should be prepared to pay up to three months’ rent upfront
                to the landlord. This excludes any registration, application, credit check, or broker
                fees. Contact one of our <a href="agents.aspx">listing agents</a> to find out the
                exact fees involved with using a broker. Fees will vary depending on the apartment.
            </p>
            <p>
                Most landlords require that new tenants have an annual income at least 40 times
                the annual rent. If you are a student, recent college graduate, or cannot accommodate
                this requirement, a guarantor will be required to co-sign the lease. As such, guarantors
                will also need to produce tax returns and/or bank statements as proof that they
                are qualified to be a co-signer on the lease. Many landlords also do not take guarantors
                that live out of the New York Metropolitan area. Make sure you have someone in the
                area willing to co-sign before applying for an apartment. Always ask about the fees
                involved before applying for the apartment or signing the lease. You may also want
                to find out what utilities are paid by the landlord, if any, and any pet policies.
            </p>
            <p>
                <span>Signing the Lease</span></p>
            <p>
                It is extremely important that you read your lease carefully. Leases should include
                the term, monthly rent amount, sublet policy and of course list any inhabitants
                of the apartment (i.e. all roommates). The lease should also outline due dates for
                paying rent and any late penalties. Additionally, it is extremely important to find
                out what happens at the end of the lease term. Some leases renew automatically while
                some do not. The agents at Beekman Street Realty will be happy to review your lease
                with you.
            </p>
            <p>
                If the apartment you are applying for is rent-stabilized (an agent will tell you),
                your lease will be different than if it is a free market apartment. The rent-stabilized
                lease will outline how the rent amount was determined, and describe the rights and
                obligations of both tenant and landlord under the <a href="http://www.dhcr.state.ny.us/ora/progs/oraprogs.htm#underrs" target="_blank">Rent Stabilization Law</a>. One of
                the main components of rent stabilization is the limit on rent increases from year
                to year. As of October 1, 2007, one-year leases signed are subject to a maximum
                of a 3% increase from year to year and two-year leases are subject to a maximum
                of a 5.75% increase. Free market apartments have no restrictions on how much the
                landlord may increase the rent from year to year.
            </p>
            <p>
                For more information on rent stabilization in New York City visit <a href="http://www.housingnyc.com" target="_blank">www.housingnyc.com</a>
                and <a href="http://www.dhcr.state.ny.us/ora/ora.htm" target="_blank">www.dhcr.state.ny.us/ora/ora.htm</a>.</p>
            <p><span>If Craigslist is Free, Why Use a Broker?</span></p>
            <p>The New York City real estate market is very complex and unlike any other market in the nation. Because of the high demand to live in New York City, apartments often rent within a few hours from the time they are listed. Most landlords prefer to list their units with brokers to avoid having to deal with the applications and credit check processes, as well as unqualified renters. Landlords use the brokers to pre-screen applicants, selecting only the best possible tenant for their apartment.  If you try to avoid using a broker, chances are you will become frustrated and waste time and energy. </p>
            <p><span>Choose & Use Your Broker:</span></p>
            <p>Many apartment hunters in New York City have a tendency of going to whichever real estate agent has the listing they like.  While this is a good starting point, it is not always wisest to follow the listings. Apartments often become rented before the listing is removed from the website, and as such, what you are interested in seeing may no longer be available.  </p>
            <p>Stick with one broker. Find an agent who you like and develop a relationship with that agent. As soon as a suitable new apartment listing is available that meets your criteria, you will have first priority.  You are hiring the broker for their services; why not give them the chance to work for you?  And when (or if) the time comes that you would like to move to a new apartment, you can return to your trusted broker instead of having to re-navigate the sea of New York City real estate agents. </p>
            <p>Find your Beekman Street Realty <a href="agents.aspx">rental agent</a> who will commit to you.</p>
        
        </div>
        <%--        <span>Renter's Guide</span> > <a href="standard_fund_requirements_rent.aspx">Standard
            Fund Requirements</a> > <a href="general_closing_procedures_rent.aspx">General Closing
                Procedures</a> > <a href="typical_questions_answered_rent.aspx">Typical Questions Answered</a>
        <div class="page-titles-guides">
            Renter's Guide
        </div><br />
        <p>
            It is a well known fact that the New York City Apartments Rental market moves fast.
            Our Rental Guide addresses the fundamental questions our clients have about finding
            an apartment. See sections on General Approval Requirements, Standard Fund Requirements,
            Typical Questions Answered, and Real Estate Terms Defined.
        </p>--%>
    </div>
</asp:Content>
