﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="typical_questions_answered.aspx.cs" Inherits="typical_questions_answered" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages guides">
        <a href="buyers_guide.aspx">Buyer's Guide</a> > <a href="standard_fund_requirements.aspx">
            Standard Fund Requirements</a> > <a href="general_closing_procedures.aspx">General Closing
                Procedures</a> > <span>Typical Questions Answered</span>
        <div class="page-titles-guides">
            Typical Questions Answered
        </div>
        <ul>
            <li><strong>What do I need to facilitate the buying process so I do not miss out on
                a great deal?</strong></li><br>
            <br>
            The New York City real estate market is extremely competitive. The most desirable
            and affordable properties can go to contract in a matter of days. The most important
            factors in getting a deal to closing include the following:<br>
            <br>
            Annual Income - Generally you can borrow up to about 2xs gross annual income. Monthly
            mortgage and maintenance payments should not exceed 1 weeks gross salary. If you
            have significant liquid assets you may be able to borrow more.<br>
            <br>
            Financial Statements - should be prepared by an accountant. It should lists net
            worth including assets, liabilities, salary, bonus, etc. Netmore Realty Group submits
            offers with financial validation. The most qualified buyers are usually the ones
            whose offers are accepted. Having a complete financial statement is very important
            when there is an apartment or property with multiple bids.<br>
            <br>
            Asset Valuation - A down payment alone does not qualify a buyer for a coop or a
            condo in Manhattan. Usually, a coop board or condo association wants guarantees
            for mandatory maintenance fees. They want assurances against unexpected loss of
            income. Many coops require liquid assets totaling 1 year's worth of maintenance
            and mortgage payments after closing. Also, some buildings require liquidity up to
            three years of combined costs. Ask your Netmore Realty Group Broker to assist you
            with specific requirements.<br>
            <br>
            <li><strong>What is the importance of a credit check and how does it fit into the buying
                process?</strong> </li>
            <br>
            <br>
            A credit check is generally performed by a mortgage broker and pertains to your
            credit history. Resolve disputed claims and have them removed immediately from your
            credit report. Keep all pertinent documentation which illustrates that the issue
            is resolved. Also, we recommend buyers choose a mortgage broker because mortgage
            brokers can save time and money. We work with various brokers throughout the New
            York Area; if you are interested ask your Netmore Realty Group broker for more information.<br>
            <br>
            <li><strong>Question: What does being pre-qualified mean?</strong></li><br>
            <br>
            Pre-qualified, also called pre-approved, is when a perspective buyer tells a lender
            income level, debt and credit information, so the lender can provide an estimated
            loan amount, based on these criteria. Being a pre-qualified buyer will reassure
            a seller that any offers made are bona fide and that a buyer can actually make the
            purchase. In fact, most offers submitted by Netmore Realty Group are often accompanied
            by a buyer's pre-qualification letter; this letter notify sellers that financing
            is an option.<br>
            <br>
            Ask your Netmore Realty Group broker to provide you with information about financing,
            estimated monthly mortgage payments, and any other necessary qualifications. Pre-approval
            letters make for strong offers because the lender has already pulled a credit report,
            checked debt/to/income ratio and done an analysis of your finances. Also, it is
            preferable to be pre-approved so that there will be no surprises when the credit
            report is received.<br>
            <br>
            <li><strong>Why select a real estate attorney?</strong></li><br>
            <br>
            The real estate market in New York City is a very dynamic market. Having a real
            estate attorney who specializes in the NYC market is very important. First, they
            know the intricacies of the laws and regulations. Second, they can respond quickly
            to any problems that arise and can therefore expedite the closing. Ask your Netmore
            Realty Group Broker to assist you in selecting a qualified real estate attorney.<br>
            <br>
            <li><strong>What is a closing?</strong></li><br>
            <br>
            A closing is where a buyer gives the seller money in exchange for ownership and
            title to a particular property. This bargained for exchange, or consideration, transfers
            ownership of the property. The seller also needs to sign different documents including
            a deed. The place of closing is normally at the bank attorney’s office. The parties
            present will be: seller, bank attorney, brokers, seller’s attorney, buyer’s attorney
            and title closer.<br>
            <br>
            <li><strong>What are the steps in the application process?</strong></li><br>
            <br>
            For mortgage applications and for coop or condo boards, buyers need to gather relevant
            financial documentation. Required documentation includes 2 months of most recent
            bank statements, brokerage statements and any other assets. In addition to current
            income verification, boards and banks require a minimum of 2 years of federal income
            tax returns. Self-employed buyers need to provide at least 3 years of federal tax
            returns and a letter from an accountant verifying income.<br>
            <br>
            <li><strong>When do I need to move?</strong></li><br>
            <br>
            Find a target date for moving. If looking to finance, expect to take about 3 months
            from purchase to close. Buyers, begin your search 4-6 months before prior to an
            estimate moving date.<br>
            <br>
            <li><strong>Question: Where do I want to live?</strong></li><br>
            <br>
            New York City is a city of diverse neighborhoods, all with their own unique appeal.
            Before beginning your search, decide what is important. For example, proximity to
            public transportation or proximity to your job. Are your preferences for a quiet
            residential neighborhood or a bustling hub of activity and nightlife? Try to be
            open to different areas as sometimes neighborhoods other than your first choice
            are more affordable and offer many of the same features. Visit our detailed neighborhood
            guide for information on all our neighborhoods. Also, ask your Netmore Realty Group
            broker to include any recommended, areas as you may not be aware of them and they
            may best suit your needs.<br>
            <br>
            <li><strong>What is the best way to win over sellers?</strong></li><br>
            <br>
            If you really want to buy a particular property that has a lot of interest, sell
            the seller on the fact that you are a qualified buyer. It doesn't matter whether
            you plan to buy a studio or a building, chances are that there is a limited number
            of properties available in your price range. In a competitive market attractive
            properties can be in contract before they appear in the classified ads.<br>
            <br>
            Brokers tend to notify serious buyers first. Make sure that you are ready to buy;
            have all your paperwork in order including financial documentation and pre-qualification
            for a mortgage. Communicate yours needs, budget, timetable, and neighborhood preferences.<br>
            <br>
            Make yourself available to view properties. By being flexible with viewing properties
            during the work week, you can avoid weekends shoppers, congested open houses and
            a lot of the best buildings tend to show properties between 10-3, M-F.<br>
            <br>
            <li><strong>How do I make the deal happen?</strong></li><br>
            <br>
            After finding the property you want, immediately make a verbal offer. Consult your
            Netmore Realty Group broker for the best ways to extend an offer to a seller. There
            are a lot of different reasons, other than money, that influence a seller in choosing
            the right buyer. If the verbal offer is accepted, have your broker get confirmation
            of the offer and acceptance in writing. Once the written offer is accepted and signed,
            your Netmore Realty Group broker prepares a transaction summary. This is sent to
            the seller's and to the buyer's attorneys for review. Using the signed offer and
            acceptance, the seller's attorney then prepares a contract of sale. After the buyer’s
            attorney reviews the contract, negotiates terms and evaluates the property's financial
            statements, the buyer then signs the contract and presents the down-payment check.
            Generally, down-payments range from 10% - 20% of the contract of sale. This money
            is often held in the selling attorney's escrow account to be sent to the seller's
            attorney. The seller then executes the contract.<br>
            <br>
            <i>Note*:</i> Verbal offers are not legally binding. In fact, in the New York City
            area an offer is legally binding when both the buyer and the seller sign a contract
            of sale. Sometimes sellers are tempted by higher offers. To avoid problems, be prepared
            and have your attorney move quickly through reviewing financials and completing
            the contract.<br>
        </ul>
    </div>
</asp:Content>
