﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="photopage.aspx.cs" Inherits="photopage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title></title>
   <%-- <style type="text/css">
        body
        {
            margin: 0;
            padding: 0;
        }
        td, th
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            color: #666666;
        }
        .Address
        {
            font-size: 16px;
            font-weight: bold;
        }
        .openHouse
        {
            color: red;
            font-weight: bold;
            text-transform: uppercase;
            font-size: 11px;
        }
        .openHouseInfo
        {
            color: #336633;
            font-weight: bold;
            font-size: 12px;
        }
        .style2
        {
            color: #555;
            font-weight: bold;
            font-size: 12px;
            text-align: left;
        }
        .style5
        {
            font-size: 36px;
        }
        .style6
        {
            font-size: 11px;
        }
        span.price
        {
            font-weight: bold;
            color: #000;
            font-size: 12px;
        }
        .style7
        {
            width: 60px;
        }
        .mainPhoto
        {
            width: 300px;
        }
        .photoList
        {
            margin-left: -10px;
            margin-top: 20px;
        }
        .content
        {
            width: 700px;
            overflow: hidden;
        }
    </style>--%>
    <!--[if IE]>
    <style type="text/css">
        .content { width: 700px; height:760px; overflow: hidden; }
    </style>
    <![endif]-->
</head>
<body>
    <div class="wrap">
        <div style="margin: -8px auto 0 auto; width: 700px;">
            <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" valign="top">
                        <% if (Model.GetMedia().Photos.Count >= 1)
                           { %>
                        <div style="page-break-before: always;">
                            <!-- header -->
                            <%=Html.RenderPartial("cntrls/Print/header", Model)%>
                            <!-- end header-->
                            <div class='displayAddress'>
                                <%=Model.GetDisplayAddress() %></div>
                            <%if (Model.GetDisplayAddress() != Model.Neighborhood)
                              {%>
                            <div class='neighborhood'>
                                <%=Model.Neighborhood%></div>
                            <%} %>
                            <div class='listingtype'>
                                <%=Model.Ownership %></div>
                            -<%if (Model.Price > 0)
                               { %><div class='price'>
                                                                                       Price:&nbsp;<%=Model.Price%></div>
                            <%}
                               else
                               {
                                   if (Model.Rent > 0)
                                   { %>
                            Rent:&nbsp;<%=Model.Rent%><%} if (Model.FurnishedRent > 0)
                                                                                       { %>
                            Furnished Rent:&nbsp;<%=Model.FurnishedRent%><%}
                                                                                   } %>
                            <!-- listing detail -->
                            <%=Html.RenderPartial("cntrls/Print/photos", Model)%>
                            <!-- end listing detail -->
                            <!-- footer -->
                            <%=Html.RenderPartial("cntrls/Print/footer", Model)%>
                            <!-- end footer -->
                        </div>
                        <% } %>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
