﻿<%@ Page Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="agent_detail.aspx.cs"
    Inherits="agent_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.expander.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="agent-detail-page clearfix">
        <div class="titles-global">
        </div>
        <div class="agent-info-container clearfix">
            <div class="left info-agent">
                <%=this.RenderPartial("cntrls/AgentDetails/AgentInfo", Model) %>
            </div>
            <div class="bio left">
                <%=this.RenderPartial("cntrls/AgentDetails/AgentBio", Model) %>
                <br />
                <% if (Model.Languages.Count > 0)
                   {%>
                <div>
                    <b><span style="color: #2A406D; font-weight:bold;">Languages Spoken:</span></b>&nbsp;&nbsp;&nbsp;
                    <%= Format_Language(Model.Languages)%>
                </div>
                <% }  %>
                <div>
                    <%= this.RenderPartial("cntrls/AgentDetails/AgentSocial",Model) %>
                </div>
            </div>
        </div>
        <olr:AgentListings runat="server" ID="AgentListings1" />
        <div class="clearfix">
            <div class="span-5">
            </div>
            <div class="span-10">
            </div>
            <div class="span-9 last">
            </div>
        </div>
    </div>
</asp:Content>
