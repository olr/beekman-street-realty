﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true"
    CodeFile="registration.aspx.cs" Inherits="registration" %>

<%@ Register TagPrefix="OLR" TagName="menu_cntrl" Src="~/cntrls/shared/menu.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
<form runat="server">
    <div class="customer_registration_wrapper registration-page" id="formPanel" runat="server">
    <div class="titles-global" style="margin-bottom:10px;">
            registration
        </div>
        <div style="padding: 20px 0 0 0px;" class="clearfix">
            <div class="column" style="padding-top:3px;">
                <span class="client_registration_title">I am looking to</span>
            </div>
            <div class="column">
                <asp:RadioButtonList CssClass="radio" ID="CustomerType" runat="server" RepeatDirection="Horizontal" CellPadding="3" RepeatColumns="4">
                    <asp:ListItem Text="Buy" Value="Buy" />
                    <asp:ListItem Text="Rent" Value="Rent" Selected="True" />
                </asp:RadioButtonList>
            </div>
        </div>
        <br>
        <div class="client_registration">
            <div class="client_registration_left left">
                  <div class="client_registration_title">
                    Desired neighborhoods
                </div>
                <div class="apartment_size_minimum">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="60px">
                                Enter
                            </td>
                            <td>
                                <asp:TextBox ID="Neighborhoods" runat="server" CssClass="register_textBox" />
                            </td>
                           <%-- <td>
                               &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>--%>
                        </tr>
                    </table>
                </div>
                <br>
                <div class="client_registration_title">
                    Apartment size minimum
                </div>
                <div class="apartment_size_minimum">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="60px">
                                Beds:
                            </td>
                            <td>
                                <asp:TextBox ID="Beds" runat="server" CssClass="register_textBox" style="margin-right:20px;" />
                            </td>
                            <td width="60px">
                                Baths:
                            </td>
                            <td>
                                <asp:TextBox ID="Baths" runat="server" CssClass="register_textBox" />
                            </td>
                        </tr>
                    </table>
                </div>
                <br>
                <div class="client_registration_title">
                    Price / Rent range
                </div>
                <div class="apartment_size_minimum">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="60px">
                                Min $:
                            </td>
                            <td>
                                <asp:TextBox ID="MinPrice" runat="server" CssClass="register_textBox" style="margin-right:20px;"/>
                            </td>
                            <td width="60px">
                                Max $:
                            </td>
                            <td>
                                <asp:TextBox ID="MaxPrice" runat="server" CssClass="register_textBox" />
                            </td>
                        </tr>
                    </table>
                </div>
                <br>
                <div class="client_registration_title">
                    Desired building amenities
                </div>
                <div class="desired_amenities">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <input type="checkbox" name="BuildingAmenities" value="Doorman" />Doorman
                            </td>
                            <td>
                                <input type="checkbox" name="BuildingAmenities" value="Fitness Facility"  />Fitness Facility
                            </td>
                            <td>
                                <input type="checkbox" name="BuildingAmenities" value="Parking" />Parking
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="BuildingAmenities" value="Deck"  />Deck
                            </td>
                            <td>
                                <input type="checkbox" name="BuildingAmenities" value="Garden/Courtyard"  />Garden / Courtyard
                            </td>
                            <td>
                                <input type="checkbox" name="BuildingAmenities" value="Deck"  />Pet Friendly
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" name="BuildingAmenities" value="Pool"  />Elevator
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
            <br>
                <div class="client_registration_title">
                    Additional comments
                </div>
                <div class="additional_comments">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <div style="width: 90px; float:left; margin-top:10px; font-size:10px;">
                                    Please provide any additional information that can help us find you the perfect
                                    apartment.
                                </div>
                            </td>
                            <td>
                                <asp:TextBox ID="Comments" runat="server" CssClass="register_textField" TextMode="MultiLine" Rows="2" Columns="20" style="height:130px; width:325px;"/>
                            </td>
                        </tr>
                    </table>
                </div>
                   </div>
            <div class="client_registration_right right">
                <div class="client_registration_title">
                    Contact information (*Required)
                </div>
                <div class="additional_comments">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="left" class="required" width="120px">
                                E-Mail Address: <asp:RequiredFieldValidator ControlToValidate="Email" ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" />
                            </td>
                            <td>
                                <asp:TextBox ID="Email" runat="server" CssClass="register_textInput" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="required" width="120px">
                                First Name: <asp:RequiredFieldValidator ControlToValidate="FirstName" ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" />
                            </td>
                            <td>
                                <asp:TextBox ID="FirstName" runat="server" CssClass="register_textInput" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="required" width="120px">
                                Last Name: <asp:RequiredFieldValidator ControlToValidate="LastName" ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" />
                            </td>
                            <td>
                                <asp:TextBox ID="LastName" runat="server" CssClass="register_textInput" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="required" width="120px">
                                Phone Number: <asp:RequiredFieldValidator ControlToValidate="Phone" ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" />
                            </td>
                            <td>
                                <asp:TextBox ID="Phone" runat="server" CssClass="register_textInput" />
                            </td>
                        </tr>
                    </table>
                </div>
                <br>
                <div class="client_registration_title">
                    Preferred contact method
                </div>
                <div class="additional_comments">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="190px">
                                <asp:RadioButton runat="server" GroupName="ContactMethod" ID="ContactByPhone" Text="by Phone" />
                            </td>
                            <td>
                                <asp:RadioButton runat="server" GroupName="ContactMethod" ID="ContactByEmail" Text="by Email" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="additional_comments" style="margin-top: 15px;">
                  <div class="button submit-button button-image right">
                    <asp:ImageButton ImageUrl="images/clear.png" ID="submitButton" runat="server" />
                    </div>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>

    <div id="messagePanel" runat="server" visible="false">
        <p style="padding: 30px;">
            Thank you for contacting us. If your matter is urgent please call 212-624-6714.
            <br /><br />
            One of our agents will be happy to assist you. We appreciate your inquiry.
        </p>
    </div>

</form>


</asp:Content>
