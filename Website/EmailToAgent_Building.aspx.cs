﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net.Mail;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Configuration;

public partial class EmailToAgent_Building : System.Web.UI.Page
{
    protected BrokerInfo Model = new BrokerInfo();
    protected Exception exception;
    protected BuildingInfo bi;

    protected override void OnInit(EventArgs e)
    {
        sendEmail.Click += new EventHandler(Send_Email);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            bi = GetBuilding();
            if (bi != null)
            {
                messageText.Text = "Please contact me about the following Building ID: " + bi.ID;
            }

            this.DataBind(true);
        }
    }

    BuildingInfo GetBuilding()
    {
        BuildingInfo b = null;
        string bid = Request.QueryString["bid"];
        if (!string.IsNullOrEmpty(bid))
            b = ServiceLocator.GetRepository().GetBuildingInfoByID(Convert.ToInt32(bid));
        return b;
    }

    void Send_Email(object sender, EventArgs e)
    {
        int id = fn.Get<int>(Request.QueryString["bid"]);
        Model.FirstName = ConfigurationManager.AppSettings["SiteName"];
        Model.Email = ConfigurationManager.AppSettings["defaultEmail"];
        //Model = ServiceLocator.GetRepository().GetBrokerByID(id);
        if (IsValid && Model != null && !string.IsNullOrEmpty(Model.Email))
        {
            StringBuilder body = new StringBuilder();

            body.AppendLine("<div class='overlay_container' style='background-color: #ffffff; color: #2D2D2D; font-family: Arial; font-size: 12px;line-height: 1.2em;'>");
            body.AppendFormat("<img src='http://{0}/images/PRINT-Logo.jpg' style='width:176px; border:none;' />", Request.Url.Host);
            body.AppendFormat("<div>{0}: {1} {2}</div>", Resources.Email.ToLabel, Model.FirstName, Model.LastName);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientNameEmailLabel, yourName.Text);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientEmailEmailLabel, yourEmail.Text);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientPhoneEmailLabel, yourPhone.Text);
            //body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.SubjectLabel, subjectEmail.Text);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientMessageEmailLabel, messageText.Text);
            if (!string.IsNullOrEmpty(Request.QueryString["bid"]))
                body.AppendFormat("<a href='http://{1}/buildingdetail.aspx?id={0}'>Click here to view this Building on your website.</a>", Request.QueryString["bid"], Request.Url.Host);
            body.AppendLine("</div>");

            MailMessage message = new MailMessage(yourEmail.Text, Model.Email);
            if (bi != null && bi.ID != 0)
                message.Subject = "Website lead from http://prestonny.com/ - Building ID#" + bi.ID;
            else
                message.Subject = "";
            message.Body = body.ToString();
            message.IsBodyHtml = true; 
            SmtpClient client = new SmtpClient();
            try
            {
                client.Send(message);
                formpanel.Visible = false;
                sentpanel.Visible = true;
            }
            catch (Exception ex)
            {
                exception = ex;
                formpanel.Visible = false;
                sentfailed.Visible = true;
            }
        }
    }

}
