﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;

public partial class search_commercial : BasePage<search_commercial, SearchOptions>
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Model = new SearchOptions();
        Model.PropertyTypes.Add(PropertyType.Commercial);

        if (string.Equals(Request.QueryString["type"], "rent"))
        {
            Model.PropertyTypes.Add(PropertyType.Rental);
        }
        else
        {
            Model.PropertyTypes.Add(PropertyType.Sale);
        }

        if (string.Equals(Request.QueryString["refine"], "true"))
        {
            SearchOptions options;
            if (TempStorage.TryGet<SearchOptions>("SearchOptions", out options))
                Model = options;
        }

    }
}