﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class press : System.Web.UI.Page
{
    protected bool ShowVowLink { get; set; }
    private bool lastPage(int pageIndex, int pageSize, int totalItems)
    {
        int numPages = (int)Math.Ceiling(totalItems / (double)pageSize);
        numPages = numPages > 0 ? numPages : 1;
        return pageIndex + 1 == numPages;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PagingOptions options = new PagingOptions();
            options.PageSize = 10;
            //Paging code start here
            if (Request.QueryString["page"] != null)
            {
                int page = fn.Get<int>(Request.QueryString["page"]);
                if (page > 0)
                    options.PageIndex = page - 1;
            }
            //end here
            RunSearch(options);
        }
    }
    void RunSearch(PagingOptions options)
    {
        int total = 0;
        CompanyNewsList data = ServiceLocator.GetRepository().GetCompanyPress(options, out total);
        if (data != null)
        {
            this.Controls.SetPagers(options.PageIndex + 1, options.PageSize, total);
        }
        this.Controls.DataBind<CompanyNewsList>(data);
        ShowVowLink = lastPage(options.PageIndex, options.PageSize, total);
    }
}
