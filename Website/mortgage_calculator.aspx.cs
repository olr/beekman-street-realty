﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class mortgage_calculator : System.Web.UI.Page
{
    protected ListingInfo Model = new ListingInfo();
    protected decimal[] mortgageRate;
    protected bool isCoopCondop;
    protected void Page_Load(object sender, EventArgs e)
    {
        isCoopCondop = false;
        mortgageRate = GetMortgageRateValue();
        if (Request.QueryString["id"] == null)
        {
            Model.Price = 0;
            Model.Maintenance = 0;
            Model.CommonCharges = 0;
            Model.FinancingAllowed = 0;
            Model.RETaxes = 0;
        }
        else if (!Page.IsPostBack)
        {
            Model = ServiceLocator.GetRepository().GetListingInfoByID(Request.QueryString["id"]);
            if (Model != null)
            {
                if (Model.Ownership.ToLower() == "coop" || Model.Ownership.ToLower() == "condop")
                    isCoopCondop = true;
                this.Controls.DataBind<ListingInfo>(Model);
                this.DataBind();
            }
        }
    }
    private decimal[] GetMortgageRateValue()
    {
        Dictionary<int, decimal> rates = MortgageRate.GetMortgageRateTable();
        decimal[] rs = new decimal[4];
        rs[0] = rates[1];//15 years
        rs[1] = rates[2];//30 years
        rs[2] = rates[3];//1 year ARM
        rs[3] = rates[4];//1/5 year ARM

        return rs;
    }
}
