﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="agents.aspx.cs" Inherits="agents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("img#nav5").attr("src", "images/menu/menu_selected_06.png");
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="agent-page static-pages">
    <div class="titles-global">
<%--        <img src="images/agents-title.png" />
--%>    </div>
        <div class="agent_text">
        </div>
        <!--agents search code -->
    <div class="agent-search-container clearfix"">
    
        <input type="text" class="text corners" value="Search for an Agent" id="agentSearch" name="q" autocomplete="off" />
<%--        <input type="image" src="images/search-home.png" class="button" value="Search" />
--%>    </div>

    
    <script type="text/javascript">
        var isProcessing;

        $(function () {
            $('#agentSearch').bind('keyup', runSearch);
            $('#agentSearch').bind('focus', function () { $(this).val(''); });
            $('#agentSearch').bind('blur', function () { $(this).val('Agent Name'); });
            isProcessing = false;
        });

        function runSearch() {
            if (!isProcessing) {
                isProcessing = true;
                var q = $('#agentSearch').val();
                if (q != "Agent Name") {
                    var json = JSON.stringify({ 'q': q });
                    $.ajax({
                        type: 'POST',
                        url: 'Service.svc/AgentSearch',
                        contentType: 'application/json',
                        dataType: 'json',
                        data: json,
                        processData: true,
                        success: handleReturnData
                    });
                }
            }
        }

        function handleReturnData(data) {
            $('.agent_result_container').html(data.AgentSearchResult.Content);
            $('ul.agent-results li:nth-child(3n)').addClass('no-right');
            isProcessing = false;
        }
    </script>

        <div class="agent_result_container">
            <%=Html.RenderPartial("cntrls/Lists/Agents", Model) %>
        </div>
    </div>


</asp:Content>
