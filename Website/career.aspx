﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="career.aspx.cs" Inherits="career" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages career clearfix">
        <div class="titles-global" style="margin-bottom: 20px;">
            Careers
        </div>
        <div class="text">
            <p>
                <span>Join the Beekman Street Realty Team </span>
            </p>
            <p>
                If you are an experienced and professional New York City licensed real estate agent,
                and you are looking to work for a cutting-edge real estate brokerage firm, Beekman
                Street Realty is looking for you!
            </p>
            <p>
                The Beekman Street Realty family is comprised of dedicated agents and we are always
                seeking new talent. Opportunities are available for successful and high-energy agents
                who want to upgrade their work environment and increase their earnings.
            </p>
            <p>
                Beekman Street Realty offers you:</p>
            <ul>
                <li> A collaborative and supportive team environment </li>
                <li> Excellent landlord relationships </li>
                <li> Access to our exclusive apartment listings database</li>
            </ul>
            <p>
                Become a part of the Beekman Street Realty team! Please email your resume and cover
                letter in confidence.</p>
        </div>
        <form id="form1" runat="server">
        <div class="career-form form clearfix">
            <div class="clearfix">
                <div class="column-left left relative">
                    <ul>
                        <li class="clearfix">Your Name
                            <asp:TextBox class="careers_form" ID="name" runat="server" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="name" ID="RequiredFieldValidator2"
                                runat="server" ErrorMessage="*Please enter your name." CssClass="error-message-text" />
                        </li>
                        <li class="clearfix">Email
                            <asp:TextBox class="careers_form" ID="Email" runat="server" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="Email" ID="RequiredFieldValidator1"
                                runat="server" ErrorMessage="*Please enter your email." CssClass="error-message-text" />
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="Email" ID="RegularExpressionValidator1"
                                runat="server" ErrorMessage="*Please enter a valid email address." CssClass="invalid-message-text-absolute"
                                ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$" />
                        </li>
                        <li class="clearfix">Daytime Phone
                            <asp:TextBox class="dphone" ID="dphone" runat="server" Width="200px"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="dphone" ID="RequiredFieldValidator3"
                                runat="server" ErrorMessage="*Please enter your daytime phone number." CssClass="error-message-text" />
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="dphone" EnableClientScript="true"
                                ID="RegularExpressionValidator2" runat="server" ErrorMessage="*Please enter a valid phone number."
                                CssClass="invalid-message-text" ValidationExpression="1?\s*\W?\s*([2-9][0-8][0-9])\s*\W?\s*([2-9][0-9]{2})\s*\W?\s*([0-9]{4})(\se?x?t?(\d*))?" />
                        </li>
                        <li class="clearfix">Evening Phone
                            <asp:TextBox class="ephone" ID="ephone" runat="server" Width="200px"></asp:TextBox>
                        </li>
                        <li class="clearfix">Current Occupation
                            <asp:TextBox class="careers_form" ID="occupation" runat="server" Width="200px"></asp:TextBox>
                        </li>
                        
                    </ul>
                </div>
                <div class="column-right right relative">
                    <ul>
                    <li class="clearfix">License Status
                            <asp:DropDownList ID="licence_status" class="careers_form" Width="206px" runat="server">
                                <asp:ListItem Value="None" Selected="True">I'm not licensed.</asp:ListItem>
                                <asp:ListItem Value="Licensed">I am licensed.</asp:ListItem>
                                <asp:ListItem Value="Expired">My license has expired.</asp:ListItem>
                                <asp:ListItem Value="Taking Course">I'm taking a course.</asp:ListItem>
                            </asp:DropDownList>
                        </li>
                        <li class="clearfix">Optional Message
                            <asp:TextBox TextMode="MultiLine" Width="380px" Height="92px" ID="message" class="careers_form"
                                runat="server" />
                        </li>
                    </ul>
                </div>
            </div>
            <div class="short-button button-image right cursor" style="margin-top: 5px;">
                                    <span class="font">submit</span>
                                    <asp:ImageButton ID="Submit" ImageUrl="images/clear.png" runat="server" OnClick="Submit_Click" />
                                </div>
        </div>
        <%--        <div style="width: 100%; height: 600px; overflow: hidden; margin-left: 3px;" class="career-page">
            <div style="margin-bottom: 12px; margin-top: 10px; position: relative;">
          
                <div id="formpanel" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                Your Name:
                            </td>
                            <td>
                                Email:
                            </td>
                        </tr>
                        <tr>
                            <td width="220px">
                                <asp:TextBox class="careers_form" ID="name" runat="server" Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="name" ID="RequiredFieldValidator2"
                                    runat="server" ErrorMessage="*Please enter your name." CssClass="error-message-text" />
                            </td>
                            <td width="210px">
                              
                                <asp:TextBox class="careers_form" ID="Email" runat="server" Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="Email" ID="RequiredFieldValidator1"
                                    runat="server" ErrorMessage="*Please enter your email." CssClass="error-message-text" />
                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="Email" ID="RegularExpressionValidator1"
                                    runat="server" ErrorMessage="*Please enter a valid email address." CssClass="invalid-message-text-absolute"
                                    ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Daytime Phone:
                            </td>
                            <td>
                                Evening Phone:
                            </td>
                        </tr>
                        <tr>
                            <td width="220px">
                                <asp:TextBox class="dphone" ID="dphone" runat="server" Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="dphone" ID="RequiredFieldValidator3"
                                    runat="server" ErrorMessage="*Please enter your daytime phone number." CssClass="error-message-text" />
                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="dphone" EnableClientScript="true"
                                    ID="RegularExpressionValidator2" runat="server" ErrorMessage="*Please enter a valid phone number."
                                    CssClass="invalid-message-text" ValidationExpression="1?\s*\W?\s*([2-9][0-8][0-9])\s*\W?\s*([2-9][0-9]{2})\s*\W?\s*([0-9]{4})(\se?x?t?(\d*))?" />
                            </td>
                            <td width="210px">
                                <asp:TextBox class="ephone" ID="ephone" runat="server" Width="200px" Style="margin-top: -8px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Current Occupation:
                            </td>
                            <td>
                                License Status:
                            </td>
                        </tr>
                        <tr>
                            <td width="220px">
                                <asp:TextBox class="careers_form" ID="occupation" runat="server" Width="200px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:DropDownList ID="licence_status" class="careers_form" Width="206px" runat="server">
                                    <asp:ListItem Value="None" Selected="True">I'm not licensed.</asp:ListItem>
                                    <asp:ListItem Value="Licensed">I am licensed.</asp:ListItem>
                                    <asp:ListItem Value="Expired">My license has expired.</asp:ListItem>
                                    <asp:ListItem Value="Taking Course">I'm taking a course.</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Optional Message
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="top">
                                <asp:TextBox TextMode="MultiLine" Width="415px" Height="150px" ID="message" class="careers_form"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="short-button button-image cursor" style="margin-top: 5px;">
                                    <span class="font">submit</span>
                                    <asp:ImageButton ID="Submit" ImageUrl="images/clear.png" runat="server" OnClick="Submit_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <p style="margin-top: 10px; font-size: 12px;">
                        *You may also contact us by calling or emailing directly.</p>
                </div>
                <div id="sentpanel" runat="server" visible="false">
                    <div class="footer">
                        <p>
                            Thank you. Your message has been sent, a representative will contact you shortly.</p>
                    </div>
                </div>
                <div id="sentfailed" runat="server" visible="false">
                    <div class="footer">
                        <p>
                            Sorry, your email sent failed.</p>
                        <p>
                            <%=exception.ToString() %></p>
                    </div>
                </div>
            </div>
        </div>
        --%>
        </form>
    </div>
</asp:Content>
