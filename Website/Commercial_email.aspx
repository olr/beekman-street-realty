﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Popup.master" CodeFile="Commercial_email.aspx.cs" Inherits="email_agent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Email Agent</title>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $('input, textarea').bind('focus', function () {
                $(this).siblings('label').hide();
            });
            $('input, textarea').bind('blur', function () {
                if ($(this).val().length == 0)
                    $(this).siblings('label').show();
            });
            $('input, textarea').each(function (i) {
                if ($(this).val().length == 0)
                    $(this).siblings('label').show();
                else
                    $(this).siblings('label').hide();
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1 class="title">Email Us</h1>
    <form id="Form1" runat="server">
        <div class="w_popup_body">
            <div id="formpanel" runat="server">
                <h2>To: Netmorerealty.com</h2>
                <fieldset class="form">
                    <div class="section">
                        <label>Your Name</label>
                        <asp:TextBox ID="yourName" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="yourName" runat="server" ErrorMessage="*" />
                    </div>
                    <div class="section">
                        <label>Your Email</label>
                        <asp:TextBox ID="yourEmail" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="yourEmail" runat="server" ErrorMessage="*" />
                         <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="yourEmail" ID="RegularExpressionValidator1" runat="server" ErrorMessage="*" ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$" />
                    </div>
                    <div class="section">
                        <label>Your Phone Number</label>
                        <asp:TextBox ID="yourPhone" runat="server" />
                    </div>
                    <div style="margin-top: 10px;" class="contact_titles">
       <b> Your desired property type</b>
    </div>
    <div>
        <table>
            <tr>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="PropertyType" value="Apartment Buildings" />
                    Apartment Buildings
                </td>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="PropertyType" value="Commercial Buildings" />
                    Commercial Buildings
                </td>
            </tr>
            <tr>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="PropertyType" value="Commercial Condo/Co-op" />
                    Commercial Condo/Co-op
                </td>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="PropertyType" value=" Development/Conversion Sites" />
                    Development/Conversion Sites
                </td>
            </tr>
            <tr>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="PropertyType" value="Industrial" />
                    Industrial
                </td>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="PropertyType" value="Mixed-Use User Buildings" />
                    Mixed-Use User Buildings
                </td>
            </tr>
            <tr>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="PropertyType" value="Mixed-Use Investment Buildings" />
                    Mixed-Use Investment Buildings
                </td>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="PropertyType" value="Townhouses" />
                    Townhouses
                </td>
            </tr>
            <tr>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="PropertyType" value="Live Plus Income Buildings" />
                    Live Plus Income Buildings
                </td>
            </tr>
        </table>
    </div>
                    <div class="section">
                        <label>Subject</label>
                        <asp:TextBox ID="subjectEmail" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="subjectEmail" runat="server" ErrorMessage="*" />
                    </div>
                    <div class="section">
                        <label>Your Message</label>
                        <asp:TextBox ID="messageText" runat="server" CssClass="textBox" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </fieldset>
                <div class="buttons">
                    <asp:Button ID="sendEmail" runat="server" Text="Send Email" />
                </div>
                <div class="footer">
                    You will receive an email confirmation that your form was received, and a representative will be in contact with you soon.
                </div>
            </div>
            <div id="sentpanel" runat="server" visible="false">
                <div class="footer">
                    <p>Thank You!</p>
                    <p>You will receive an email confirmation that your form was received. A representative will be in contact with you soon.</p>
                </div>
            </div>

        </div>
    </form>
</asp:Content>
