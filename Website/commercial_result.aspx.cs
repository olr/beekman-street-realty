﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OLRData;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class commercial_result : BasePage<commercial_result, SearchOptions>
{
    protected int total;
    protected override void OnInit(EventArgs e)
    {
        this.Controls.InitSortables(SortClicked);
        base.OnInit(e);
    }

    void SortClicked(object sender, CommandEventArgs e)
    {
        SearchOptions opts;
        if (TempStorage.TryGet<SearchOptions>("SearchOptions", out opts))
        {
            opts.OrderBy = fn.StringToEnum<SortOrder>(e.CommandName);
            opts.IsDescending = ((SortDirection)e.CommandArgument).Equals(SortDirection.Descending);
            opts.PageIndex = 0;
            RunSearch(opts);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            total = 0;
            Model = new SearchOptions();
            Model.PropertyTypes.Add(PropertyType.Commercial);
            if (Model != null)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["sort"]))
                {
                    ReSort(Model, Request.QueryString["sort"]);
                    return;
                }
                if (Request.QueryString["page"] != null)
                {
                    int page = fn.Get<int>(Request.QueryString["page"]);
                    if (page > 0)
                        Model.PageIndex = page - 1;
                }
                RunSearch(Model);
            }
            else
                Response.Redirect("~/search.aspx");

        }
    }

    void ReSort(SearchOptions options, string sortOrder)
    {
        var lastSortOrder = options.OrderBy;
        switch (sortOrder)
        {
            case "price":
                options.OrderBy = SortOrder.Price;
                break;
            case "size":
                options.OrderBy = SortOrder.Size;
                break;
            case "location":
                options.OrderBy = SortOrder.Address;
                break;
        }
        if (lastSortOrder.Equals(options.OrderBy))
            //reverse sort order
            options.IsDescending = !options.IsDescending;
        options.PageIndex = 0;
        TempStorage.Add("SearchOptions", options);
        Response.Redirect("~/commercial_result.aspx");
    }

    void RunSearch(SearchOptions options)
    {
        if (options != null)
        {
            total = 0;
            Listings listings = null;
            if (options.PropertyTypes.Contains(PropertyType.Commercial))
                listings = ServiceLocator.GetComercialSearchService().RunSearch(options, out total);
            else
                listings = ServiceLocator.GetSearchService().RunSearch(options, out total);
            if (listings != null)
            {
                this.Controls.SetPagers(options.PageIndex + 1, options.PageSize, total);

                this.Controls.DataBind<Listings>(listings);
                listings.CreatePagingSession(); //save paging info

            }
            Model = options;
            TempStorage.Add("SearchOptions", options);
            this.Controls.DataBind<SearchOptions>(options);
            ShowVowLink = lastPage(options.PageIndex, options.PageSize, total);
        }
    }

    protected bool ShowVowLink
    {
        get;
        set;
    }
    private bool lastPage(int pageIndex, int pageSize, int totalItems)
    {
        int numPages = (int)Math.Ceiling(totalItems / (double)pageSize);
        numPages = numPages > 0 ? numPages : 1;
        return pageIndex + 1 == numPages;
    }
}
