﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="our_company.aspx.cs" Inherits="our_company" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages clearfix;">
        <div class="photo left">
            <img src="images/our_company.jpg" />
        </div>
        <div class="text right">
            <b>NETM<span>O</span>RE REALTY GROUP</b> <span>"We give you more."</span><br />
            <p>
                <b>Netm<span>o</span>re Realty</b> is a Real Estate brokerage firm specializing in sales and rentals
                in Manhattan. We, the brokers and agents associated with Netmore Realty, specialize
                in the sale of cooperatives, condominiums and townhouses. We have extensive experience
                and knowledge in the intricacies of Manhattan real estate.
            </p>
            <p>
                We are highly trained professionals, who live and work in Manhattan. Our policy
                is to give individual attention and service to both buyers and sellers in our community.
                Our reputation for excellence, exceptional service and integrity are well established.
                Our many contacts with mortgage brokers, the legal community, and other real estate
                service providers, have been instrumental in assisting our customers and clients
                from around the globe make this often stressful process, as easy as possible.
            </p>
            <p>
                We often work as a team. By staying tuned to the market conditions, we have been
                instrumental in meeting the needs of our sellers and our buyers. We are able to
                get the optimal prices for both our sellers and buyers. We are noted for the quality
                of our service, our professionalism, and our dedicated attention to detail to the
                individual needs of both our clients and customers.
            </p>
        </div>
    </div>
</asp:Content>
