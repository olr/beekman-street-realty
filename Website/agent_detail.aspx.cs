﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class agent_detail : BasePage<agent_detail, BrokerInfo>
{
    protected override void OnInit(EventArgs e)
    {
        int id = fn.Get<int>(Request.QueryString["agentid"]);
        
        if (id > 0)
        {
            BrokerInfo b = ServiceLocator.GetRepository().GetBrokerByID(id);
            if (b != null)
            {
                Model = b;
                this.Controls.DataBind<BrokerInfo>(b);
            }
        }
        if (!string.IsNullOrEmpty(Request.QueryString["alias"]))
        {
            BrokerInfo b = ServiceLocator.GetRepository().GetBrokerByAlias(Request.QueryString["alias"]);
            if (b != null)
            {
                Model = b;
                this.Controls.DataBind<BrokerInfo>(b);
            }
        }

        base.OnInit(e);
    }
    public string Format_Language(List<string> items)
    {
        string val = "";

        foreach (string x in items)
        {

            val += " " + x;

        }
        val = val.Remove(val.Length - 1, 1);
        return val;
    }
}
