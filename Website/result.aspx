﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="result.aspx.cs" Inherits="result" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id='NoFee' class="no-fee-text" runat="server" visible="false">
    <div class="titles-global" style="margin-bottom: 20px;">
        No Fee Specials
    </div>
            <p>
                <span>Browse Beekman Street Realty No Fee Apartment Listings</span></p>
            <p>
                Beekman Street Realty is proud to offer our clients apartments for rent with no
                brokerage fees. Browse our no fee specials and if you see something you like call
                us today at 212-624-6714. Please check back often as our no fee listings are regularly
                updated.
            </p>
        </div>
    <div class="result-page">
       
        <div class="clearfix">
            <%=this.RenderPartial("cntrls/Search/RefineSearch", Model)%>
        </div>
        <olr:OLRPageNav ID="PageNav1" runat="server" UsePostback="false" PrevImageUrl="images/icons/prev.png"
            NextImageUrl="images/icons/next.png" />
        <olr:ShortForm runat="server" ID="ShortForm1" NoResultMessage="There are currently no listings that match your criteria." />
        <olr:OLRPageNav ID="PageNav2" runat="server" UsePostback="false" PrevImageUrl="images/icons/prev.png"
            NextImageUrl="images/icons/next.png" />
    </div>
</asp:Content>
