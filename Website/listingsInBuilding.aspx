﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="listingsInBuilding.aspx.cs" Inherits="listingsInBuilding" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" Runat="Server">
 <script type="text/javascript">
        $(function () {
            $("img#nav4").attr("src", "images/menu/menu_selected_05.png");
            <% if(pType=="sale"){ %>
                $('input#saleRadio').attr("checked","true");
            <%} else{ %>
                $('input#rentRadio').attr("checked","true");
            <%} %>
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
<div class="search-container clearfix">
        <div class="titles-global">
            <img src="images/openhouses-title.png" />
        </div>
        <form action="open_house.aspx" method="get">
        <div class="refine_search_container">
            <div class="refine_search_title">
                Listing Type</div>
            <div class="refine_search_text">
                Sales</div>
            <div style="margin: 3px 5px 0 0;">
                <a href="listingsInBuilding.aspx?type=sale&buildingid=<%=buildingID %>">
                    <input id="saleRadio" name="propertyType" type="radio" value="sale" />
                </a>
            </div>
            <div class="refine_search_text">
                <span>Rentals</span></div>
            <div style="margin: 3px 5px 0 0;">
                <a href="listingsInBuilding.aspx?type=rent&buildingid=<%=buildingID %>">
                    <input id="rentRadio" name="propertyType" type="radio" value="rent" />
                </a>
            </div>
           
            <div class="clear">
            </div>
        </div>
        </form>
    
    <div class="result-container">
        <olr:OLRPageNav ID="PageNav1" runat="server" UsePostback="false" PrevImageUrl="images/icons/arrow_left.png"
            NextImageUrl="images/icons/arrow_right.png" />
        <olr:ShortForm runat="server" ID="ShortForm1" NoResultMessage="There are currently no open houses scheduled." />
        <olr:OLRPageNav ID="PageNav2" runat="server" UsePostback="false" PrevImageUrl="images/icons/arrow_left.png"
            NextImageUrl="images/icons/arrow_right.png" />
    </div></div>
</asp:Content>

