﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="corporate_legal_disclaimer.aspx.cs" Inherits="corporate_legal_disclaimer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages clearfix">
        <div class="titles-global" style="margin-bottom: 20px;">
            Corporate Legal Disclaimer
        </div>
        <div class="text clearfix">
            <p>
                We take great care to make sure that our photos and pricing are as accurate as possible.
                However, because of the fast paced nature of the real estate market, we are not
                responsible in the event of an erroneous photograph or mispriced listing. Occasionally
                landlords change prices suddenly and without notice, and listings can become available
                or unavailable more quickly than such changes can appear on our website. Furthermore,
                please contact the office or agent listed next to the property you are interested
                in and we will try to accommodate your needs.</p>
        </div>
    </div>
</asp:Content>
