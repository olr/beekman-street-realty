﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.master" AutoEventWireup="true" CodeFile="mortgage_calculator.aspx.cs" Inherits="mortgage_calculator" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/pca.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
       
	        var price = document.CalcForm.txtPrice;
            price.value = <%=string.Format("{0:0}",Model.Price)%>;
            
	        var maintcc = document.CalcForm.txtMaintCC;
	        maintcc.value = <%=string.Format("{0:0}",Model.Maintenance>0?Model.Maintenance:Model.CommonCharges)%>;
           
	        var financing = document.CalcForm.txtDown;
	        financing.value = "<%=string.Format("{0:0}%", Model.FinancingAllowed>0?100-Model.FinancingAllowed: 10)%>";

	        var taxes = document.CalcForm.txtTaxes;
	        taxes.value = "<%=string.Format("{0:0}",Model.RETaxes)%>";

	        CalcPayment(document.CalcForm);
            var False=false;
            var True=true;
            if(<%=isCoopCondop %>)
                $('input#retaxes').attr('readonly','readonly');

                $('#mortgage_selector').change(function(){
                    if($(this).val()==15)
                    $('#rate_value').attr('value','<%=mortgageRate[0] %>%');
                    else if($(this).val()==30)
                    $('#rate_value').attr('value','<%=mortgageRate[1] %>%');
                    else if($(this).val()==1)
                    $('#rate_value').attr('value','<%=mortgageRate[2] %>%');
                    else if($(this).val()==0.2)
                    $('#rate_value').attr('value','<%=mortgageRate[3] %>%');
                })
        });
    </script>
      <!--[if IE]> <link href="css/IE.css" rel="stylesheet" type="text/css"> <![endif]-->

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
 <div style="padding:0px;">
    <h1 class="title">Mortgage Calculator</h1>
    <%if (Model.ListingID != null)
      { %>
    <h2 id="address">
        <%=Model.GetDisplayAddress()%></h2>
    <h2 id="listingid">
        Listing ID:
        <%=Model.ListingAgentID%></h2>
        <%} %>
    <div class="w_popup_body">
    <form name="CalcForm" method="post" action="javascript:CalcPayment(document.CalcForm)">
        <table border="0" cellpadding="4" cellspacing="0" style="margin-top:10px; width:310px;">
            <tr>
                <td class="text">
                    Purchase Price:</td>
                <td>
                    <input type="text" name="txtPrice" size="20" class="Field" value="0" style="text-align: right; background-color:#FFF;" /></td>
            </tr>
            <tr style="display:none">
                <td class="text">
                    US Tax Bracket:</td>
                <td>
                    <select name="txtTaxBracket" class="Field">
                        <option value="25%">25%</option>
                        <option value="28%">28%</option>
                        <option value="33%">33%</option>
                        <option value="35%">35%</option>
                        <option value="" selected="selected">N/A</option>
                    </select>
                    </td>
            </tr>
            <tr>
                <td class="text">
                    Down Payment:</td>
                <td width="142">
                    <input type="text" name="txtDown" size="20" class="Field" value="0%" style="background-color:#FFF;"/></td>
            </tr>
            <tr>
                <td class="text">
                    Mortgage Interest Rate:</td>
                <td width="142">
                    <input type="text" id="rate_value" name="txtMortgageInterestRate" size="20" class="Field" value="<%=mortgageRate[1] %>"%" style="background-color:#FFF;" /></td>
            </tr>
            <tr>
                <td class="text">
                    Loan Mortgage Term:</td>
                <td width="142">
                    <select name="txtMortgageLength" id="mortgage_selector" class="Field mortgage-dropdown">
                        <option value="1">1 yr ARM</option>
                        <option value="0.2">1/5 yr ARM</option>
                        <option value="15">15 yrs</option>
                        <option value="30" selected="selected">30 yrs</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="text">
                    Mortgage Amount:</td>
                <td width="142">
                    <input type="text" name="txtMortgage" size="20" class="FxField" style="text-align: right; background-color:#FFF;"  readonly="readonly" /></td>
            </tr>
            <tr>
                <td class="text">
                    <strong>Monthly Mortgage:</strong></td>
                <td width="142">
                    <input type="text" name="txtMonthlyMortgage" size="20" class="FxField" style="text-align: right; background-color:#FFF;" readonly="readonly" /></td>
            </tr>
            <tr>
                <td class="text">
                    CC/Maintenance:</td>
                <td width="142">
                    <input type="text" name="txtMaintCC" size="20" value="0" class="Field" style="text-align: right; background-color:#FFF;" /></td>
            </tr>
            <tr style="display:none">
                <td class="text">
                    % Deductible:</td>
                <td width="142">
                    <input type="text" name="txtDeductionPercent" size="5" value="" class="FxField" style="text-align: right; background-color:#FFF;" /></td>
            </tr>
            <tr>
                <td class="text">
                    RE Taxes:</td>
                <td width="142">
                    <input type="text" name="txtTaxes" size="20" value="" class="Field" style="text-align: right; background-color:#FFF;" /></td>
            </tr>
            <tr style="display:none">
                <td class="text">
                </td>
                <td width="142">
                </td>
            </tr>
            <tr>
                <td class="text">
                    Monthly Payment:</td>
                <td>
                    <input type="text" name="txtMOP" size="20" class="FxField" readonly="readonly" style="text-align: right; background-color:#FFF;" /></td>
            </tr>
            <tr style="display:none">
                <td class="text">
                    Monthly Tax Savings:</td>
                <td>
                    <input type="text" name="txtMonthlyAfterTax" size="20" class="FxField" readonly="readonly" style="text-align: right; background-color:#FFF;" /></td>
            </tr>
            <tr>
                <td class="text">
                    &nbsp;</td>
                <td>
                    <input type="submit" name="calculate" value="Calculate" class="button-alt" size="30" /></td>
            </tr>
        </table>
        <div style="padding:10px; border:2px solid #C7CED6;display:none;">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="text">
                    <b class="title2">Net Monthly Cost of Ownership</b></td>
            </tr>
            <tr>
                <td>
                    <span class="text"><span class="txtbig">
                        <input type="text" name="txtNetCost" size="20" class="FxFieldbig" readonly="readonly" />
                    </span></span>
                </td>
            </tr>
            <tr>
                <td><input type="submit" name="calculate" value="calculate" class="button11px" size="30" id="Submit1" /></td>
            </tr>
        </table>
        </div>
    </form>
    </div>
    </div>
</asp:Content>

