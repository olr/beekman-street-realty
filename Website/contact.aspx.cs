﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Configuration;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class contact : System.Web.UI.Page
{
    protected CompanyInfo company;
    protected void Page_Load(object sender, EventArgs e)
    {
        string id = ConfigurationManager.AppSettings["CompanyID"];
        company = ServiceLocator.GetRepository().GetCompanyInfoByID(Convert.ToInt32(id));
    }
    protected string showPhone()
    {
        if (!string.IsNullOrEmpty(company.Phone))
            return "O: " + company.Phone;
        else
            return string.Empty;
    }
    protected string showFax()
    {
        if (!string.IsNullOrEmpty(company.Fax))
            return "F: " + company.Fax;
        else
            return string.Empty;
    }
    protected string showEmail()
    {
        if (!string.IsNullOrEmpty(company.CompanyEmail))
            return company.CompanyEmail;
        else
            return string.Empty;
    }
}