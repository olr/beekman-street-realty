﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="standard_fund_requirements.aspx.cs" Inherits="standard_fund_requirements" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages guides">
        <a href="buyers_guide.aspx">Buyer's Guide</a> > <span>Standard Fund Requirements</span> > <a href="general_closing_procedures.aspx">
            General Closing Procedures</a> > <a href="typical_questions_answered.aspx">Typical Questions Answered</a>
        <div class="page-titles-guides">
            Standard Fund Requirements
        </div>
        <br />
        <ul>
            <strong>The Budget</strong>: Location, size, amenities, building type, school locations
            and nearness to public transportation are crucial to determining what a property
            will cost. We suggest that buyers review financing options to determine what amount
            of money they can put toward a deposit and then determine what they can afford for
            a monthly mortgage payment. Some people may want to see if they pre-qualify for
            mortgage status in order to know what budget they will be working within.<br>
            <br>
            <p>
            </p>
            <strong>Type of Property</strong>: The next thing to determine would be the type
            of property you'd like to buy. Would you like an apartment, condominium, coop or
            commercial property?<br>
            <br>
            <strong>Apartment Purchase</strong>: For apartment purchases there is no approval
            process, besides normal financial inquiries. Buyers are entitled to have complete
            control over any renovations they wish to make. Maintenance costs are the sole responsibility
            an owner and with this the owner gets total control over maintenance and repairs.
            Transferring deeds or reselling an apartment do not require approval. The size of
            a property can vary according to a budget and location of the property.<br>
            <br>
            <strong>Condominium Purchase</strong>: Condo owners buy the apartment and also own
            a percentage of the common building areas, i.e. entrance areas, hallways and recreational
            areas etc. The condominium is considered real property; because of this fact, owner's
            have the right to do use the property any way he/she sees fit. Owners are subject
            to pay property taxes which can either be escrowed into a part of a monthly mortgage
            payment or which are paid annually as an expense. Monthly maintenance fees are generally
            paid to the building's condominium association.<br>
            <br>
            <strong>Coop Purchase </strong>: Buying a coop is tricky, but Netmore Realty Group's
            team of qualified agents can help. When purchasing a cooperative apartment the purchaser
            is really purchasing shares in a cooperation. The shares correspond to the units
            in which you live. coop fees include a monthly mortgage payment and maintenance
            fees for the building. Maintenance fees are sometimes high but both mortgage payments
            and maintenance fees generally result in higher tax deductibles.<br>
            <br>
        </ul>
        </p>
        <p>
            <ul>
                <strong>Estimated Fees:</strong><br>
                <br>
                <strong><i>Cooperative Apartment:</i><br>
                    For Seller:</strong> <strong>Broker Fees:</strong> Are typically 6%. Fees vary
                according to property size and amount of marketing<br>
                <strong>Attorney Fees:</strong> Approximately $1,500<br>
                <strong>Managing Agent:</strong> $450<br>
                <strong>Flip Tax:</strong> Consult Managing Agent<br>
                <strong>Stock Transfer Tax:</strong> $.05 per share<br>
                <strong>Move Out Deposit Fee:</strong> $1,000<br>
                <strong>New York City Transfer Tax:</strong> 1.00% of price up to $500,000 1.425%
                of entire price when it exceeds $500,000<br>
                <strong>New York State Transfer Tax:</strong> 0.4% (.004) of price<br>
                <strong>Payoff Bank Attorney:</strong> If seller has mortgage $30<br>
                <strong>UCC-3 Filing Fee:</strong> If seller has mortgage $350<br>
                <strong>For Purchaser:</strong><br>
                <strong>Own Attorney Fee: Approximately $1500</strong><br>
                <strong>Bank Fees/Application/Credit/Appraisal/Bank Attorney/Miscellaneous Fees:</strong>
                $1,600<br>
                <strong>Short-Term Interest:</strong> One month max*<br>
                <strong>Move-In Deposit:</strong> $1000<br>
                <strong>Managing Agent or Co-op Attorney Fee:</strong> $600<br>
                <strong>Lien Search:</strong> $300<br>
                <strong>Maintenance Adjustments:</strong> One month tax<br>
                <strong>Mansion Tax:</strong> 1% of entire price when price exceeds $1,000,000<br>
                <i>*Prorated for month of closing</i><br>
            </ul>
        </p>
        <p>
            <ul>
                <strong><i>Condominium/Townhouse:</i><br>
                    For Seller:</strong> <strong>Broker Fees:</strong> Are typically 6%. Fees vary
                according to property size and amount of marketing<br>
                <strong>Attorney Fees:</strong> Approximately $1,500<br>
                <strong>Managing Agent:</strong> $450<br>
                <strong>Move Out Deposit Fee:</strong> $1,000<br>
                <strong>New York City Transfer Tax:</strong> 1.00% of price up to $500,000 1.425%
                of entire price when it exceeds $500,000<br>
                <strong>New York State Transfer Tax:</strong> 0.4% (.004) of price<br>
                <strong>Miscellaneous Title Company Fees:</strong> If seller has mortgage $200<br>
                <strong>For Purchaser:</strong><br>
                </strong> Approximately $1,500<br>
                <strong>Bank Fees:</strong> Points 2%<br>
                <strong>Application/Credit/Appraisal/Bank Attorney/Miscellaneous Fees:</strong>
                $1,600<br>
                <strong>Short-Term Interest:</strong> One month max*<br>
                <strong>Real Estate Tax:</strong> Escrows 2-6 months<br>
                <strong>Recording Fee:</strong> $200<br>
                <strong>Mortgage Tax:</strong> 1.75% of mortgage on loans under $500,000 or 1.875%
                of entire amount on loans over $500,000<br>
                <strong>Fee Title Insurance:</strong> Approximately $450 per $100,000<br>
                <strong>Mortgage Title Insurance:</strong> Approximately $200 per $100,000<br>
                <strong>Miscellaneous Title Charges:</strong> $300<br>
                <strong>Managing Agents Fee:</strong> $250<br>
                <strong>Adjustments:</strong><br>
                <ul>
                    <strong><i>Common charges:</i></strong> One month max*<br>
                    <strong><i>Real Estate Taxes:</i></strong> 1 to 6 months<br>
                    <strong><i>Mansion Tax:</i></strong> 1% of entire price when price exceeds $1,000,000<br>
                </ul>
                <i>*Prorated for month of closing<br>
                    *Expect to pay eight months Real Estate Taxes. These taxes are combined between
                    seller as an adjustment and escrow established by lender.<br>
                    *Note: When purchasing condos from a sponsor, the purchaser will be required to
                    pay New York City and New York State Transfer Taxes; see above for amounts.<br>
                    *Also, buyers are required to pay sponsor’s attorney’s fee; this fee is approximately
                    $1,000.</i><br>
            </ul>
        </p>
        <p>
            <ul>
                <strong><i>Multi-Family Residential/Commercial Property</i><br>
                    For Seller:</strong><br>
                <strong>Broker Fees:</strong> Are typically 6%. Fees vary according to property
                size and amount of marketing<br>
                <strong>Attorney Fees:</strong> Approximately $5,000<br>
                <strong>New York City Transfer Tax:</strong> 1.425% of price up to $500,000 2.625%
                of entire price when it exceeds $500,000<br>
                <strong>New York State Transfer Tax:</strong> 0.4% (.004) of price<br>
                <strong>Payoff Bank Attorney:</strong> If Seller has mortgage - $350 Miscellaneous
                $200 Transfer Security Deposit T.S.D.<br>
                <strong>For Purchaser:</strong><br>
                <strong>Own Attorney Fee:</strong> Approximately $5,000<br>
                Bank Fees: Points 2%><br>
                <strong>Application/Credit/Appraisal/Bank Attorney/Miscellaneous Fees:</strong>
                $5,000-$10,000<br>
                <strong>Short-Term Interest:</strong> One month max*<br>
                <strong>Mortgage Tax:</strong> 2.75% of entire amount on loans over $500,000<br>
                <strong>Real Estate Tax:</strong> Escrows 2-6 months<br>
                <strong>Fee Title Insurance:</strong> Approximately $450 per $100,000<br>
                <strong>Mortgage Title Insurance:</strong> Approximately $200 per $100,000<br>
                <strong>Miscellaneous Title Charges:</strong> $1,000<br>
                <strong>Adjustments:</strong> Rents* T.B.D. Real Estate Taxes 1-6 months *Prorated
                for month of closing<br>
                <br>
            </ul>
        </p>
    </div>
</asp:Content>
