﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="print_friendly2.aspx.cs"
    Inherits="print_friendly2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/site.css" rel="stylesheet" type="text/css" />
</head>
<body style="margin: 0;">
    <div class="print-wrapper2">
        <div class="container">
            <div class="logo">
            </div>
            <div class="listing-id">
                <div class="heavy">Listing ID: 536792</div>
                <div>212.555.5555</div>
            </div>
            <div class="photo-row clearfix">
                <div class="print-listing-photo-left">
                </div>
                <div class="print-listing-photo-col-right">
                    <div class="photos">
                        <img src="images/print-listing-no-pic.png" />
                    </div>
                    <div class="photos">
                        <img src="images/print-listing-no-pic.png" />
                    </div>
                </div>
            </div>
            <div class="description-container">
                <div class="title">
                    Aliquam turpis dui, imperdiet nec posuere quis, imperdiet non tellus.
                </div>
                <div class="description">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque facilisis
                    magna id eros placerat suscipit. Mauris vehicula sodales pellentesque. Aenean ante
                    massa, convallis nec mattis scelerisque, adipiscing sed magna. Maecenas id mauris
                    ipsum, quis venenatis est. Class aptent taciti sociosqu ad litora torquent per conubia
                    nostra, per inceptos himenaeos. Nunc id quam leo, at laoreet odio. Sed tincidunt
                    vehicula justo ut vehicula. Sed eu purus vitae velit laoreet consequat egestas sed
                    ligula. Nulla facilisi. Sed iaculis nunc ut velit mattis et malesuada nunc bibendum.
                    Ut nec justo et risus lacinia commodo. Nunc non libero at est tempor lacinia a et
                    lorem. Aliquam dignissim pharetra nulla at elementum. Fusce mattis metus aliquet
                    nisi laoreet iaculis. Vestibulum semper suscipit hendrerit. Etiam in purus odio.
                    Curabitur nulla enim, malesuada eget euismod id, lobortis vel ante. Fusce laoreet
                    convallis pellentesque.
                </div>
            </div>
            <div class="two-col-container clearfix">
                <div class="general-details">
                    <ul>
                        <li class="address">5555 5th Ave. #5555B</li>
                        <li>Cross Streets: 5th avenue & 55th Street</li>
                        <li>Midtown West</li>
                        <li>Bedrooms: 1</li>
                        <li>Bathrooms: 1</li>
                        <li>Approx. SqFt: 692</li>
                        <li>Price: $749,000</li>
                        <li>Maint/CC: $650</li>
                        <li>Taxes (Per Month): $125</li>
                    </ul>
                </div>
                <div class="agent-card">
                    <div class="agent-container clearfix">
                <div class="agent-thumb left">
                    <a href="agent_detail.aspx?agentid=53081">
                        <img class="agent_thumb_image" src='http://www.olr.com/Pictures/AgentPhoto/agent_photo_53081.jpg' style="height: 147px;
                                width: 105px;" alt="" />
                    </a>

                </div>
                <div class="left">
                <div class="agent-info">
                    <div>
                        <a class="agent-name" href="agent_detail.aspx?agentid=53081">Mike A. Malul</a>
                    </div>
                    <div class="agent-title">
                       president/broker
                    </div>

                    <div class="agentphone" style="display:block">
                        O: 212-953-1099 [ext 102]
                    </div>
                    <div class="agentphone" style="display:block">
                        M: 646-339-5040
                    </div>
                </div>
                <div class="agent-tools">
                    <div>
                        <a class="email-me" href="email_agent.aspx?id=53081">

                            Email Me
                        </a>
                    </div>
                    <div>
                        <a class="vcard" target="_blank" href="vcard.aspx?id=53081">
                            Download Contact
                        </a>
                    </div>
                    <div>
                        <a class="mylistings" href="agent_detail.aspx?agentid=53081">

                            View My Listings
                        </a>
                    </div>
                </div>
            </div>
            </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
