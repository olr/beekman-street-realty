﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Web.UI.WebControls;

public partial class agentSearch : BasePage<agentSearch,Brokers>
{
    protected override void OnInit(EventArgs e)
    {
        AgentSearchOptions options = new AgentSearchOptions();
        options.OrderBy = SortOrder.FirstName;

        int total;
        var agents = ServiceLocator.GetRepository()
                                   .GetBrokers(options, out total);
        if (agents != null)
        {
            Model = new Brokers();
            foreach (var a in agents)
                Model.Add(a);
        }
        base.OnInit(e);
        SiteHelper.CreateAgentLastNameXml();
        SiteHelper.CreateAgentFirstNameXml();
    }
}