﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="career_form.aspx.cs" Inherits="career_form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.formatnumber.js"></script>

    <title></title>
    <link href="css/site.css" rel="stylesheet" type="text/css" />
</head>
<body style="background: #f9f9f9;">
    <form id="form1" runat="server">
    <div style="width: 430px; height: 600px; overflow: hidden; margin-left: 3px;" class="career-page">
        <div class="header" style="background: url(images/popup-header-background.png) repeat-y;
            background-position: center; padding: 12px 0;">
            <img src="images/logo.png" alt="" style="height: 50px; margin-left: 12px;" />
        </div>
        <div style="margin-bottom: 12px; margin-top: 10px; position:relative;">
            555 5th Ave,<br>
            New York, NY10555<br>
            T: 212-624-6714<br>
            E-mail: info@beekmanstreetrealty.com

            <br />
            <br />
            <div id="formpanel" runat="server">
            <table width="430px">
                <tr>
                    <td>
                        Your Name:
                    </td>
                    <td>
                        Email:
                    </td>
                </tr>
                <tr>
                    <td width="220px">
                        <%--<input type="text" class="careers_form" name="name" />--%>
                        <asp:TextBox class="careers_form" ID="name" runat="server" Width="200px"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="name" ID="RequiredFieldValidator2"
                            runat="server" ErrorMessage="*Please enter your name." CssClass="error-message-text" />
                    </td>
                    <td width="210px">
                        <%--<input type="text" class="careers_form" name="email" />--%>
                        <asp:TextBox class="careers_form" ID="Email" runat="server" Width="200px"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="Email" ID="RequiredFieldValidator1"
                            runat="server" ErrorMessage="*Please enter your email." CssClass="error-message-text" />
                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="Email" ID="RegularExpressionValidator1"
                            runat="server" ErrorMessage="*Please enter a valid email address." CssClass="invalid-message-text-absolute" ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Daytime Phone:
                    </td>
                    <td>
                        Evening Phone:
                    </td>
                </tr>
                <tr>
                    <td width="220px">
                        <%--<input type="text" class="careers_form" name="dphone" />--%>
                        <asp:TextBox class="dphone" ID="dphone" runat="server" Width="200px"></asp:TextBox>
                        <asp:RequiredFieldValidator ControlToValidate="dphone" ID="RequiredFieldValidator3"
                            runat="server" ErrorMessage="*Please enter your daytime phone number." CssClass="error-message-text" />
                             <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="dphone"
                    EnableClientScript="true" ID="RegularExpressionValidator2" runat="server" ErrorMessage="*Please enter a valid phone number." CssClass="invalid-message-text"
                    ValidationExpression="1?\s*\W?\s*([2-9][0-8][0-9])\s*\W?\s*([2-9][0-9]{2})\s*\W?\s*([0-9]{4})(\se?x?t?(\d*))?" />
                    </td>
                    <td width="210px">
                        <%-- <input type="text" class="careers_form" name="ephone" />--%>
                        <asp:TextBox class="ephone" ID="ephone" runat="server" Width="200px" style="margin-top:-8px;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Current Occupation:
                    </td>
                    <td>
                        License Status:
                    </td>
                </tr>
                <tr>
                    <td width="220px">
                        <%--<input type="text" class="careers_form" name="occupation" />--%>
                        <asp:TextBox class="careers_form" ID="occupation" runat="server" Width="200px"></asp:TextBox>
                    </td>
                    <td>
                        <%--<select name="licence_status" class="careers_form" style="width: 146px;">
                        <option value="None">None</option>
                        <option value="Taking Course">Taking Course</option>
                        <option value="Licensed">Licensed</option>
                        <option value="Expired">Expired</option>
                    </select>--%>
                        <asp:DropDownList ID="licence_status" class="careers_form" Width="206px" runat="server">
                            <asp:ListItem Value="None" Selected="True">I'm not licensed.</asp:ListItem>
                            <asp:ListItem Value="Licensed">I am licensed.</asp:ListItem>
                            <asp:ListItem Value="Expired">My license has expired.</asp:ListItem>
                            <asp:ListItem Value="Taking Course">I'm taking a course.</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Optional Message
                    </td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">
                        <%--<input type="text" class="careers_form" name="message" style="width: 340px; height:150px; vertical-align:top;" />--%>
                        <asp:TextBox TextMode="MultiLine" Width="415px" Height="150px" ID="message" class="careers_form"
                            runat="server" />
                       <%-- <asp:RequiredFieldValidator ControlToValidate="message" ID="RequiredFieldValidator4"
                            runat="server" ErrorMessage="*" />--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--<input type="image" src="images/submit.png" />--%>
                        <div class="submit-button button-image cursor" style="margin-top: 5px;">
                            <asp:ImageButton ID="Submit" ImageUrl="images/clear.png" runat="server" OnClick="Submit_Click" />
                        </div>
                    </td>
                    <%--  <td style="text-align:right;">
                    <a href="companyVcard.aspx">
                    <img src="images/number.png" style="margin-right:47px;" />
                    </a>
                </td>--%>
                </tr>
            </table>
            <p style="margin-top:10px; font-size:12px;">*You may also contact us by calling or emailing directly.</p>
            </div>
              <div id="sentpanel" runat="server" visible="false">
            <div class="footer">
                <p>
                    Thank you. Your message has been sent, a representative will contact you shortly.</p>
            </div>
        </div>
        <div id="sentfailed" runat="server" visible="false">
            <div class="footer">
                <p>
                    Sorry, your email sent failed.</p>
                <p>
                    <%=exception.ToString() %></p>
            </div>
        </div>
        </div>
    </div>
    </form>
</body>
</html>
