﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true"
    CodeFile="search_brooklyn.aspx.cs" Inherits="search_brooklyn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/search.js" type="text/javascript"></script>
    <script src="js/json2.js" type="text/javascript"></script>
    <script src="js/jquery.ajaxform.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        var searchCount = function () {
            $('#searchForm').submitForm({
                url: 'Service.svc/SearchCount',
                success: function (data) {
                    //console.log(data.SearchCountResult.Content);
                    $('div#vow').html(data.SearchCountResult.Content);
                }
            });
        };

        $(function () {
            $('form input').bind('change', searchCount);
            $('form select').bind('change', searchCount);
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="search-container brooklyn-search-container clearfix">
        <div class="titles-global">
            Search
        </div>
        <form method="post" action="result.aspx" id="searchForm">
        <div class="map-view-container corners right">
            <div class="search-type-toggle right">
                <%=Html.RadioButton("PropertyType", Model.PropertyTypes.Contains(PropertyType.Sale), new { value = "Sale" })%><span>Sales</span>
                <%=Html.RadioButton("PropertyType", Model.PropertyTypes.Contains(PropertyType.Rental), new { value = "Rental" })%><span>Rentals</span>
            </div>
            <%--        <%if (Model.PropertyTypes.Contains(PropertyType.Rental))
      { %>
    <input type="hidden" name="propertyType" value="Rental" />
    <%}
      else
      { %>
   <input type="hidden" name="propertyType" value="Sale" />
   <%} %>--%>
   <input type="hidden" name="Borough" value="184" />
            <%=Html.RenderPartial("cntrls/Search/Map_Brooklyn_Micro", Model)%>
        </div>
        <div class="criteria left">
            <div class="location-container corners">
                <div class="page-titles">
                    Location</div>
                <%=Html.RenderPartial("cntrls/Search/Neighborhood_Brooklyn_Micro", Model)%>
                <div class="section clearfix">
                    <div id="dynamic-ownership" class="ownership">
                        <%=Html.RenderPartial("cntrls/Search/Ownership_Brooklyn", Model) %>
                    </div>
                </div>
            </div>
            <div class="clearfix" style="width: 99%;">
                <div class="feature-container left corners">
                    <div class="page-titles">
                        Apartment Size</div>
                    <%=Html.RenderPartial("cntrls/Search/ApartmentSize", Model)%>
                </div>
                <div class="feature-container right corners">
                    <div id="dynamic-price-range">
                        <%=Html.RenderPartial("cntrls/Search/PriceRange", Model)%>
                    </div>
                </div>
            </div>
            <div class="location-container corners clearfix">
                <div class="bldg-features span-25">
                    <%=Html.RenderPartial("cntrls/Search/BuildingFeatures", Model)%>
                </div>
                <div>&nbsp;</div>
                <div class="page-titles">
                    Apartment Features</div>
                <div id="dynamic-nofee" class="bldg-features span-25">
                    <%=Html.RenderPartial("cntrls/Search/NoFee", Model)%>
                </div>
                <div id="dynamic-aptfeatures" class="apt-features span-25">
                    <%=Html.RenderPartial("cntrls/Search/ApartmentFeatures_Brooklyn", Model) %>
                </div>
                <div id="vow" class="vow-counter"></div>
                <div class="search-button button button-image">
                    <input type="image" class="button" src="images/clear.png" value="Search" />
                </div>
            </div>
        </div>
        </form>
        
    </div>
</asp:Content>
