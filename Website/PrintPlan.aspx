﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.master" AutoEventWireup="true" CodeFile="PrintPlan.aspx.cs" Inherits="PrintPlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server" >
<style type="text/css">
    body{padding:5px;}
</style>
<script type="text/javascript">

</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<div style="width:411px; height:111px; overflow:hidden;">
<h1 id="H1" class="title">
        <%=l.GetDisplayAddress() %> - 
        Listing ID:
        <%=l.ListingAgentID %></h1>
<h2 >Would you like to include the following?</h2>
    

<form method="get" target="_blank" action="print.aspx">
    <div style="padding-top:12px;">
    <input type="hidden" name="id" value="<%=Request.QueryString["id"]%>" />
    <%if (hasPhoto)
      { %>
    <input type="checkbox" name="photo" value="1" /> Photos
    <%} if (hasFloorPlan)
      {%>
    <input type="checkbox" name="fp" value="1" /> Floor Plan
    <%} %>
    <input type="hidden" name="currency" value="<%=currency %>" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   <div class="print-icon button-image right" style="margin:-5px 20px 0 0;">
    <input type="image" src="images/clear.png" value="Print"/>
    </div>
    </div>
</form>
</div>
</asp:Content>

