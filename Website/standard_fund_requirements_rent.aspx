﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="standard_fund_requirements_rent.aspx.cs" Inherits="standard_fund_requirements_rent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="static-pages guides">
        <a href="renters_guide.aspx">Renter's Guide</a> > <span>Standard Fund Requirements</span> > <a href="general_closing_procedures_rent.aspx">
            General Closing Procedures</a> > <a href="typical_questions_answered_rent.aspx">Typical Questions Answered</a>
        <div class="page-titles-guides">
            Standard Fund Requirements
        </div>
        <br />
        <ul>
	<li>Approximate costs of Application Process: (credit reports/application fee) - Both applicants and guarantors are 	required to submit credit reports, no exceptions. </li>
		<li>Credit reports and application fees range from $25 to $100 depending on landlord/management company.</li>
	<li>Lease signing requirements: Once all completed application materials have been submitted, Netmore Realty Group can usually 	get our clients approved for their apartment or rental property that same day. </li>
		<li>Almost all applications are approved within 1-2 business days. </li>

		<li>Renter must have completed all application materials. This includes reference letters and supporting           
             	documentation. </li>
	<li>Approved clients are expected to sign lease immediately and provide necessary funds. Lease signings take place 	within   24 hours of application approval.</li>
 		<li>Renters sign their lease and provide necessary funds regardless of when the lease starts. </li>
	<ul>
	<li>Funds: Renters generally provide 1st month's rent and equivalent of 1 month's rent as a security deposit. </li>
		<li>This money is fully refundable in case a client gets rejected. </li>

		<li>When paying the deposit ask for a receipt that states, the money will be refunded if you are rejected. </li>
		<li>When signing a lease, renters should have funds for rent, security and broker fee (if any). </li>
			<li>Money should be in a certified form Bank Checks: Certified checks or Money orders. </li>
			<li>Personal checks are not accepted. </li>
			</ul>
	<li>Miscellaneous Fees:</li>

 		<ul>
			<li>Sometimes landlords/management companies require new tenants to pay a move in deposit.</li>
			<li>This fee can range from $250 - $600. </li>
			<li>Move in fees are usually refundable as long as there is no damage done to a building while 				moving. </li>
		<li>Some landlords/management companies require renters insurance. </li>
		<li>When renting in a condominium or in a co-op, renters should expect processing fees, move in fees     
             	and other miscellaneous fees sometimes in total of $250 - $1100 in non-refundable fees. </li>

	Be sure to ask your Netmore Realty Group broker if there are additional fees; especially if renting a Condo or Co-Op. 
	Fees for pets or for temporary walls are also common. </ul>
	
	
	<p><strong>The Budget:</strong> For obvious reasons, a budget is an important factor in determining what apartment or rental property 	a renter can afford. The price of a property depends on a variety of factors - size, location, condition, features, 	neighborhood amenities, schools among others. It is key to determine a budget at the beginning of the application 	process to have an understanding of what you can afford. </p>
	<li>A general price guideline for apartments:</li>
		<ul>
		<li>Studios range from   $1,800 to $2,500 per month</li>
		<li>One bedrooms range from   $2,200 to $3,700 per month</li>

		<li>Two bedrooms range from   $3,500 to $6,500 per month </li>
		<li>Three bedrooms range from   $4,500 to $10,000 per month.</li>
		</ul>







</ul>


        </div>
</asp:Content>

