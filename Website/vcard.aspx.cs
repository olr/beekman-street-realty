﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using BrokerTemplate.Web;
using BrokerTemplate.Core;

public partial class vcard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            int id = fn.Get<int>(Request.QueryString["id"]);

            var data = ServiceLocator.GetRepository().GetBrokerByID(id);
            if (data != null && data.ID > 0)
            {
                Response.BufferOutput = true;
                Response.Clear();
                Response.ContentType = "text/x-vcard";
                Response.AddHeader("Content-Disposition", "inline; filename=contact.vcf");
                Response.AddHeader("Content-Description", "vCard");
                Response.Write(GenVCard(data));
                Response.End();
            }
        }

    }

    private string GenVCard(BrokerInfo c)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("BEGIN:VCARD");
        sb.AppendLine("VERSION:2.1");
        sb.AppendLine(string.Format("N;LANGUAGE=en-us:{0};{1}", c.LastName, c.FirstName));
        sb.AppendLine(string.Format("FN:{0} {1}", c.FirstName, c.LastName));
        sb.AppendLine(string.Format("TITLE:{0}", c.Title));
        sb.AppendLine(string.Format("TEL;CELL;VOICE:{0}", c.MobilePhone));
        sb.AppendLine(string.Format("TEL;WORK;VOICE:{0}", c.WorkPhone));

        var company = new Repository().GetCompanyInfoByID(c.CompanyID);
        if (company != null)
        {
            sb.AppendLine(string.Format("ORG: {0}", company.CompanyName));
            sb.AppendLine(string.Format("LABEL;WORK;PREF;ENCODING=QUOTED-PRINTABLE:{0} =0A{1} {2} {3}", company.Address, company.City, company.State, company.ZipCode));
        }

        sb.AppendLine(string.Format("EMAIL;PREF;INTERNET: {0}", c.Email));
        sb.AppendLine(string.Format("REV:{0}{1}{2}T050100Z", DateTime.Today.Year, DateTime.Now.Month, DateTime.Now.Day));
        sb.AppendLine("END:VCARD");
        return sb.ToString();
    }

}