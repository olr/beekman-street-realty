﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Configuration;

public partial class list_with_us : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void buttom1_Click(object sender, ImageClickEventArgs e)
    {
        string message = "";
        message += "Contact Information<br/>";
        message += "<br/>Client Name: " + FirstName.Text+" "+LastName.Text;
        message += "<br/>Client Phone: " + phone.Text;

        if (owner.Checked)
            message += "<br/>I'm an:  Owner" ;
        if (Developer.Checked)
            message += "<br/>I'm a:  Developer" ;
        if (PropertyManager.Checked)
            message += "<br/>I'm a: Property Manager" ;
        if (Other.Checked)
            message += "<br/>I'm an: Other";

        message += "<br/>--------------------------------------";

        if (Sale.Checked)
            message += "<br/>This Apartment is for: Sale"  ;
        if (Rent.Checked)
            message += "<br/>This Apartment is for: Rent " ;
        if (SaleAndRent.Checked)
            message += "<br/>This Apartment is for: Sale and Rent" ;

        message += "<br/>Property Information<br/>";

        message += "<br/>Building/ Street: " + Building_street.Text;
        message += "<br/>City/ State: " + CityState.Text;
        message += "<br/>Zip Code: " + ZipCode.Text;
        message += "<br/>Apartment: " + Apartment.Text;
        message += "<br/>Region: " + Region.SelectedValue;
        message += "<br/>Neighborhood: " + Neighborhood.SelectedValue;
        message += "<br/>Size: " + bedrooms.SelectedValue;
        message += "<br/>Bathrooms: " + bathrooms.SelectedValue;
        message += "<br/>Asking Price: " + price.Text;
        message += "<br/>Building Amenities:";
        if (Elevator_CheckBox.Checked)
            message += "Elevator, ";
        if (Doorman_CheckBox.Checked)
            message += "Doorman, ";
        if (Pet_Friendly_CheckBox.Checked)
            message += "Pet Friendly, ";
        if (Gym_CheckBox.Checked)
            message += "Gym, ";
        if (Laundry_CheckBox.Checked)
            message += "Laundry, ";
        if (Outdoor_Space_CheckBox.Checked)
            message += "Outdoor Space";
        if (sqft.Text.Length > 0)
            message += "<br/>Square Footage: " + sqft.Text;

        message += "<br/>Apartment Amenities:";
        if (Terrace_Balcony.Checked)
            message += "Terrace / Balcony, ";
        if (Washer_Dryer.Checked)
            message += "Washer / Dryer, ";
        if (Dishwasher.Checked)
            message += "Dishwasher ";

        if (Furnished.Checked)
          message += "<br/>Furnished";
        if (sqft.Text.Length > 0)
            message += "<br/>Square Footage: " + sqft.Text;

        if (Furnished.Checked)
            message += "<br/>Furnished";
        if (comments.Text.Length > 0)
            message += "<br/>Additional Comments:<br/>" + comments.Text;

        SmtpClient client = new SmtpClient();

        MailMessage x = new MailMessage();
        string toEmail = ConfigurationManager.AppSettings["defaultEmail"];
        x.To.Add(toEmail); 
        x.From = new MailAddress("noreply@olr.com");
        x.Subject = "List With Us";
        x.IsBodyHtml = true;
        x.Body = message;
        try
        {
            client.Send(x);
            sent_ok.Visible = true;
          content_Area.Visible = false;
        }

        catch
        {
            content_Area.Visible = false;
           sent_ok.Visible = true;
        }
    }
}