﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="vow_popup.aspx.cs" Inherits="vow_popup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/site.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="vow-pop-up">
        <div class="vow-title">
            V.I.P Search
        </div>
        <div class="vow-info">
            <p>
                <strong>Base 2.0 VIP Search</strong> is the only place you need. No matter what you
                are looking for, you can find it here. All you need to do is complete a simple registration
                on the next page to unlock the door to every listing New York City has to offer.
                So click here and have fun looking!</p>
            <p>
                (If you prefer, contact us and we will do the searching for you. Just drop us a
                line of what you are looking for and we will find it for you.)</p>
        </div>
        <div class="register-button button-image">
            <a href="VOW.aspx" target="_parent">
                <img src="images/clear.png" />
            </a>
        </div>
    </div>
</body>
</html>
