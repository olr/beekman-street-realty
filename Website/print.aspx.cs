﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Text.RegularExpressions;

public partial class print : BasePage<print, ListingInfo>
{
    protected bool isPhotoPrint, isFloorplanPrint;
    protected void Page_Load(object sender, EventArgs e)
    {
        Model = this.GetListing() ?? new ListingInfo();

       
        if (Request.QueryString["photo"]=="1")
            isPhotoPrint = true;
        else
            isPhotoPrint = false;
        if (Request.QueryString["fp"]=="1")
            isFloorplanPrint = true;
        else
            isFloorplanPrint = false;
        
    }

   
}