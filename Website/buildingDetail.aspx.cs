﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using OLRData;
public partial class buildingDetail : BasePage<buildingDetail, BuildingInfo>
{
    protected FeaturedBuilding fb;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] == null)
            Response.Redirect("search.aspx");
            Model = ServiceLocator.GetRepository().GetBuildingInfoByID(Convert.ToInt32(Request.QueryString["id"]));
            fb = ServiceLocator.GetRepository().GetFeaturedBuildingInfoByID(Convert.ToInt32(Request.QueryString["id"]));
            if (Model != null)
                this.Controls.DataBind<BuildingInfo>(Model);
            else
                Response.Redirect("~/default.aspx");
    }
}