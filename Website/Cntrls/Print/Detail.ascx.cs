﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Text.RegularExpressions;
using System.Xml.Linq;

public partial class cntrls_Print_Detail : BaseControl<cntrls_Print_Detail, ListingInfo>, IRenderable<ListingInfo>
{
    private static Dictionary<string,double> rateTable = ExchangeRateCalulator.GetRateTable();
    public void DataBind(ListingInfo data)
    {
        Model = data ?? new ListingInfo();
        string currency = HttpContext.Current.Request.QueryString["currency"];
        if (string.IsNullOrEmpty(currency))
            currency = "USD";
       
        double rate = rateTable[currency];

        lblTitleAddress.Text = string.Format("{0}", Model.GetDisplayAddress());
        Neighborhood_Page1.Text = Model.Neighborhood;

        if (!string.IsNullOrWhiteSpace(Model.CrossStreet1) && !string.IsNullOrWhiteSpace(Model.CrossStreet2))
            Neighborhood_Page1.Text += string.Format(" [{1} & {2}]", Model.Neighborhood, Model.CrossStreet1, Model.CrossStreet2);

        ListingID_LabelPage1.Text = "Listing ID: " + Model.ListingID;

        //detail

        rptPrices.DataSource = Model.GetFinancialDetailForPrint(rate,currency);
        rptPrices.DataBind();

        rptDescriptions.DataSource = Model.GetListingDetailForPrint();
        rptDescriptions.DataBind();

        Building_detailsRepeater.DataSource = Model.GetBuildingDetailForPrint();
        Building_detailsRepeater.DataBind();


        imgMainPhoto.ImageUrl = Model.GetDisplayPhoto();
        var media = Model.GetMedia();
        if (media.Photos.Count > 1)
            imgMainPhoto2.ImageUrl = media.Photos[1].MediaUrl;
        else
            imgMainPhoto2.Visible = false;

        litFeatures.Text = Model.AptFeatures.ToString();
        lblFeatureTitle.Visible = !string.IsNullOrWhiteSpace(litFeatures.Text);

        PetPolicy.Text = Model.BuildPetPolicy();
        PetPolicyPlaceHolder.Visible = !string.IsNullOrEmpty(PetPolicy.Text);

        litBuildingDesc.Text = Model.BldgAmenities.ToString()
                                   .Replace("Pets Allowed,", string.Empty)
                                   .Replace("No Dogs,", string.Empty)
                                   .Replace("No Cats,", string.Empty)
                                   .Replace("No Pets,", string.Empty)
                                   .Replace("Prewar,",string.Empty)
                                   .Replace("Postwar,",string.Empty).Trim();
        litBuildingDesc.Text = Regex.Replace(litBuildingDesc.Text, @",+\s?$", string.Empty);
        lblBuildingDescTitle.Text = "Building Features";
        lblBuildingDescTitle.Visible = !string.IsNullOrWhiteSpace(litBuildingDesc.Text);

        lblDescriptTitle.Text = "Listing Description";
        litDescription.Text = Model.GetDescriptionForPrintPage();
        lblDescriptTitle.Visible = !string.IsNullOrWhiteSpace(litDescription.Text);

    }
}