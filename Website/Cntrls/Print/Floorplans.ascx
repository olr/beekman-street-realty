﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Floorplans.ascx.cs" Inherits="cntrls_Print_floorplans" %>
<%-- <div style="text-align: right; margin-top: 5px;">
                            <input type="image" src="images/clear.png" id="print_button" value="Print" class="print-icon" />
                        </div>--%>
<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td colspan="2">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" width="80%" style="padding-bottom: 15px;">
                        <asp:Label ID="lblTitleAddress" runat="server" CssClass="Address style1 style5" /><br />
                        <asp:Label ID="Neighborhood_Page1" runat="server"></asp:Label>
                    </td>
                    <td style="padding-top: 5px;" align="right" valign="top" width="20%">
                        <asp:Label ID="ListingID_LabelPage1" runat="server" CssClass="style6"></asp:Label><br />
                        <input type="image" src="images/clear.png" id="print_button" value="Print" class="print-icon" />
                    </td>
                    
                </tr>
            </table>
        </td>
    </tr>
    <tr runat="server" id="zooming">
        <td align="center">
            <asp:HyperLink class="zoom" ID="ZoomIn" runat="server">Zoom In <img src="images/icons/zoom-in.png" alt="zoom in" /></asp:HyperLink>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink class="zoom" ID="ZoomOut" runat="server">Zoom Out <img src="images/icons/zoom-out.png" alt="zoom out" /></asp:HyperLink>
        </td>
        </tr>
        <tr runat="server">
        <td align="center">
        <br />
            <asp:Image ID="Floorplan" ImageUrl="" runat="server" style="max-width:700px; max-height:750px;"></asp:Image>
        </td>
    </tr>
    <asp:ListView runat="server" ID="list">
        <LayoutTemplate>
            <div class="galleryContainer">
                <ul type="none" class="tabular clearfix gallery" style="width: 90%; padding:0;">
                    <li runat="server" id="itemPlaceHolder" />
                </ul>
            </div>
        </LayoutTemplate>
        <ItemTemplate>
            <li>
                <%# DisplayFloorplan(Container.DataItem as Floorplan) %>
            </li>
        </ItemTemplate>
    </asp:ListView>
   
</table>
