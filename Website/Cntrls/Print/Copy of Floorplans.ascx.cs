﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;


public partial class cntrls_Print_floorplans : BaseControl<cntrls_Print_floorplans, ListingInfo>, IRenderable<ListingInfo>
{
    protected List<Floorplan> floorplans = new List<Floorplan>();
    protected int count;
    public void DataBind(ListingInfo data)
    {
        Model = data ?? new ListingInfo();

        if (Model != null)
        {
            Media media = Model.GetMedia();

            if (media.Floorplans != null && media.Floorplans.Count > 0)
            {
                //List<Floorplan> floorplans = media.Floorplans.ToList();

                floorplans = (from m in media.Floorplans
                              where !m.MediaUrl.ToLower().Contains(".pdf")
                              select m).ToList();
                count = floorplans.Count;
           }
        }
    }
}