﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_Print_photos : BaseControl<cntrls_Print_photos, ListingInfo>, IRenderable<ListingInfo>
{
    protected int maxHeight = 300;

    public void DataBind(ListingInfo data)
    {
        Model = data ?? new ListingInfo();

        photoList.ItemDataBound += new DataListItemEventHandler(photoList_ItemDataBound);

        if (Model != null)
        {
            Media media = Model.GetMedia();

            if (media.Photos != null && media.Photos.Count > 1)
            {
                List<Photo> photos = media.Photos.ToList();
                photos.RemoveAt(0);
                //if (media.Floorplans.Count > 0)
                  //  photos.Add(new Photo { MediaUrl = media.Floorplans[0].MediaUrl });

                if (photos.Count > 8)
                    photos = photos.GetRange(0, 8);

                int count = photos.Count;

                if (count <= 2)
                {
                    maxHeight = 300;
                    photoList.RepeatColumns = 1;
                }
                else if (count <= 4)
                {
                    maxHeight = 250;
                    photoList.RepeatColumns = 2;
                }
                else if (count <= 6)
                {
                    maxHeight = 180;
                    photoList.RepeatColumns = 2;
                }
                else
                {
                    maxHeight = 150;
                    photoList.RepeatColumns = 3;
                }

                photoList.DataSource = photos;
                photoList.DataBind();
            }
        }

    }

    void photoList_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Image img = e.Item.FindControl("image") as Image;
            if (img != null)
            {
                img.ImageUrl = ((Photo)e.Item.DataItem).MediaUrl;
                img.Height = maxHeight;
            }
        }
    }
}