﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BrokerInfo.ascx.cs" Inherits="Cntrls_Print_BrokerInfo" %>
<asp:ListView runat="server" ID="contacts">
    <LayoutTemplate>
        <div class="listing-contacts">
            <asp:Literal runat="server" ID="itemPlaceHolder" />
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <div class="contact clearfix">
            <div class="agent-thumbnail left">
            <a href="<%# (Container.DataItem as BrokerInfo).GetDetailLink() %>" class="green">
                <img id="Img1" runat="server" src='<%# Eval("PhotoUrl") %>' alt="" class="agent-photo" />
            </a>
            </div>
            <div class="agent-info-container left" style="margin-left:15px;">
            <div class="agent-name">
                <a href="<%# (Container.DataItem as BrokerInfo).GetDetailLink() %>" class="green">
                    <%# Eval("FirstName") %> <%# Eval("LastName") %>
                </a>
            </div>
            <div class="agent-title">
                    <%# Eval("Title") %> 
            </div>
            <div class="agent-phone" <%# Html.DisplayStyle(Eval("WorkPhone","{0}").Length>1) %>>
                <span class="label">O:</span> <%# Eval("WorkPhone") %>
            </div>
            <div class="agent-phone" <%# Html.DisplayStyle(Eval("MobilePhone", "{0}").Length > 1)%>>
                <span class="label">M:</span> <%# Eval("MobilePhone")%>
            </div>
            
            <div class="agent-actions">
                <div>
                    <a class="email-me" href="<%# (Container.DataItem as BrokerInfo).GetEmailMeLink(Model) %>">
                        <%# (Container.DataItem as BrokerInfo).Email%>
                    </a>
                </div>
                
            </div>
        </div>
        </div>
    </ItemTemplate>
    <ItemSeparatorTemplate>
        <hr />
    </ItemSeparatorTemplate>
</asp:ListView>
