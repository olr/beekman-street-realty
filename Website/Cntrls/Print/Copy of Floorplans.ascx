﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Copy of Floorplans.ascx.cs" Inherits="cntrls_Print_floorplans" %>
  <div id="listingID">
  Listing ID: <%=Model.ListingID %>
  </div>
  <div id="address">
    <%=Model.GetDisplayAddress() %>
    <%if (Model.GetDisplayAddress() != Model.Neighborhood)
      {%>
    -  <%=Model.Neighborhood %>
    <%} %>
  </div>
    <% foreach (var fp in floorplans)
       { %>
        <div style="page-break-before: always;"> 
            <div class="content">
                <img src="<%=fp.MediaUrl%>" alt="" class="fp-image" style="max-height:760px" />
            </div>
        </div>
    <% } %>
    