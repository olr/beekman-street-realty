﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Footer.ascx.cs" Inherits="cntrls_Print_footer" %>
<div class="footer" style="margin-top: 20px; margin-bottom:10px;">
<div style="padding-left: 10px; width: 100%; border: 0px; padding: 0px;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="left" style="padding-right:10px;width:35px;">
                <asp:Image ID="imgAgent1" Height="55" runat="server" />
            </td>
            <td align="left" valign="top">
                <strong>
                    <asp:Label ID="lblA1Name" runat="server" CssClass="style1 style6"></asp:Label>
                </strong>
                <br />
                <asp:Literal ID="litA1OPhone" runat="server" />
                <asp:Literal ID="litA1MPhone" runat="server" />
                <asp:Literal ID="litA1Email" runat="server" />
            </td>
            <td align="left" style="padding-right:10px;width:35px;padding-left:20px;">
                <asp:Image ID="imgAgent2" Height="55" runat="server" Visible="false" />
            </td>
            <td align="left" valign="top">
                <strong>
                    <asp:Label ID="lblA2Name" runat="server" CssClass="style1 style6"></asp:Label>
                </strong>
                <br />
                <asp:Literal ID="litA2OPhone" runat="server" />
                <asp:Literal ID="litA2MPhone" runat="server" />
                <asp:Literal ID="litA2Email" runat="server" />
            </td>
        </tr>
    </table>
</div>
<div style="border-bottom: 1px solid #ccc; margin: 10px 0 10px 0;"></div>
<div style="text-align: left;" class="disclaimer">
    All information furnished herein is from sources deemed reliable.  No representation is made by Broker nor is any to be implied as to the accuracy thereof and all information is submitted subject to errors, omissions, change of price, prior sale or lease, or withdrawal without notice.  All dimensions and square footage are approximate and should be confirmed by customer.  For exact dimensions and square footage, please hire a professional architect or engineer.
</div>
                                
</div>
