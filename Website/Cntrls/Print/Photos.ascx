﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Photos.ascx.cs" Inherits="cntrls_Print_photos" %>

<div class="content">

<asp:DataList runat="server" ID="photoList" RepeatDirection="Horizontal" CellPadding="10" CssClass="photoList">
    <ItemTemplate>
        <asp:Image runat="server" ID="image" />
    </ItemTemplate>
</asp:DataList>


</div>