﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class Cntrls_Print_BrokerInfo : BaseControl<Cntrls_Print_BrokerInfo, ListingInfo>, IRenderable<ListingInfo>
{
    public void DataBind(ListingInfo _data)
    {
        if (_data != null)
        {
            Model = _data;
            var brokers = Model.GetBrokers();
            if (brokers.Count > 0)
            {
                contacts.DataSource = brokers;
                contacts.DataBind();
            }
        }
    }
}