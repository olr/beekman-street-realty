﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Detail.ascx.cs" Inherits="cntrls_Print_Detail" %>
<div class="content">

<table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td colspan="2">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" width="80%" style="padding-bottom:15px;">
                        <asp:Label ID="lblTitleAddress" runat="server" CssClass="Address style1 style5" /><br />
                        <asp:Label ID="Neighborhood_Page1" runat="server"></asp:Label>
                    </td>
                    <td style="padding-top: 5px;" align="right" valign="top" width="20%">
                        <asp:Label ID="ListingID_LabelPage1"  runat="server" CssClass="style6"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <table align="left" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="368" align="center" valign="top">
                        <asp:Repeater ID="rptPrices" runat="server" EnableViewState="false">
                            <ItemTemplate>
                                <table width="368" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="left" valign="top">
                                            <table width="368" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="50%" align="left">
                                                        <%# Eval("Key")%>
                                                    </td>
                                                    <td width="50%" align="right">
                                                        <%# Eval("Value")%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                        <br />
                        <table width="368" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblRptDescrTitle" Text="Listing Details" CssClass="style2" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Repeater ID="rptDescriptions" runat="server" EnableViewState="false">
                                        <ItemTemplate>
                                            <table width="368" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <%--<div style="padding: 2px 0 2px 70px">--%>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td width="50%" align="left">
                                                                    <%# Eval("Key")%>
                                                                </td>
                                                                <td width="50%" align="right">
                                                                    <%# Eval("Value")%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <br />
                                    <asp:Label ID="Building_label" Text="Building Details" CssClass="style2" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Repeater ID="Building_detailsRepeater" runat="server">
                                        <ItemTemplate>
                                            <table width="368" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <table width="368" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td width="50%" align="left">
                                                                    <%# Eval("Key")%>
                                                                </td>
                                                                <td width="50%" align="right">
                                                                    <%# Eval("Value")%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                        </table>
                        
                        <div style="text-align:left;margin-top:20px;">
                            <p style="margin-top: 10px;">
                                <asp:Label ID="lblFeatureTitle" Text="Apartment Features" CssClass="style2" runat="server"></asp:Label>
                                <br />
                                <asp:Literal ID="litFeatures" runat="server"></asp:Literal>
                            </p>
                            <p style="margin-top: 10px;">
                                <asp:Label ID="lblBuildingDescTitle" CssClass="style2" runat="server"></asp:Label>
                                <br />
                                <asp:Literal ID="litBuildingDesc" runat="server"></asp:Literal>
                            </p>
                         <asp:PlaceHolder runat="server" ID="PetPolicyPlaceHolder" Visible="false">
                            <p style="margin-top: 10px;">
                                <span class="style2">Pet Policy</span>
                                <br />
                                <asp:Literal runat="server" ID="PetPolicy" />
                            </p>
                            </asp:PlaceHolder>
                        </div>
                        
                    </td>
                </tr>
            </table>
        </td>
        <td width="300px" align="center" valign="top">
            <table style="width: 300px;" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td style="width: 300px; height: 200px;" align="center">
                        <asp:Image ID="imgMainPhoto" runat="server" CssClass="mainPhoto" />
                    </td>
                </tr>
                <tr style="display:none;">
                    <td style="width: 300px; height: 200px;" align="center">
                        <asp:Image Height="200" ID="imgMainPhoto2" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table style="width: 100%; border: 0px;" cellspacing="0" cellpadding="0">
    <tr>
        <td style="text-align: justify; /*height: 250px;*/ vertical-align: top;">
            <div style="padding-top:20px;">
                <asp:Label ID="lblDescriptTitle" CssClass="style2" runat="server"></asp:Label>
                <asp:Literal ID="litDescriptionBr" runat="server" Text="<br />"></asp:Literal>
                <asp:Label ID="litDescription" runat="server"></asp:Label>
                <br /><br />
                <asp:Label ID="litNotes" runat="server"></asp:Label>
            </div>
        </td>
    </tr>
</table>

</div>
