﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;


public partial class cntrls_Print_floorplans : BaseControl<cntrls_Print_floorplans, ListingInfo>, IRenderable<ListingInfo>
{
    protected List<Floorplan> floorplans = new List<Floorplan>();
    protected int count;
    public void DataBind(ListingInfo data)
    {
        Model = data ?? new ListingInfo();
        var media = new Media();
        lblTitleAddress.Text = string.Format("{0}", Model.GetDisplayAddress());
        if (lblTitleAddress.Text != Model.Neighborhood)
            Neighborhood_Page1.Text = Model.Neighborhood;
        else
            Neighborhood_Page1.Text = "";

        if (!string.IsNullOrWhiteSpace(Model.CrossStreet1) && !string.IsNullOrWhiteSpace(Model.CrossStreet2))
            Neighborhood_Page1.Text += string.Format("[{1} & {2}]", Model.Neighborhood, Model.CrossStreet1, Model.CrossStreet2);

        ListingID_LabelPage1.Text = "Listing ID: " + Model.ListingID;
        if (Model != null)
        {
            media = Model.GetMedia();
            list.DataSource = media.Floorplans;
            list.DataBind();
            Session["the_model"] = Model;
        }
        if (media != null && media.Floorplans != null && media.Floorplans.Count == 1 || HttpContext.Current.Request.QueryString["url"] != null)
        {
            list.Visible = false;
            string tiffile = "";
            if (string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["url"]))
            {
                tiffile = media.Floorplans[0].MediaUrl;
            }
            else
            {
                tiffile = HttpContext.Current.Request.QueryString["url"];

            }
            int width = 675;
            if (HttpContext.Current.Request.QueryString["w"] != null)
                width = Convert.ToInt16(HttpContext.Current.Request.QueryString["w"]);
            if (width < 400) width = 675;
            int width_in = width + 200 < 1800 ? width + 200 : width;
            int width_out = width - 200 > 0 ? width - 200 : width;
            Floorplan.ImageUrl = "http://broker.olr.com/broker/myolr/thumb.axd?url=" + tiffile + "&w=" + width.ToString();
            string request = HttpContext.Current.Request.Url.OriginalString;
            if (request.Contains("floorplan.aspx"))
            {
                ZoomOut.NavigateUrl = string.Format("floorplan.aspx?id={0}&url={1}&w={2}", Model.ListingID, tiffile, width_out.ToString());
                ZoomIn.NavigateUrl = string.Format("floorplan.aspx?id={0}&url={1}&w={2}", Model.ListingID, tiffile, width_in.ToString());
            }
            else
            {
                string id = HttpContext.Current.Request.QueryString["id"];
                string currency = HttpContext.Current.Request.QueryString["currency"];
                string head = HttpContext.Current.Request.Path;
                if (request.Contains("url"))
                {
                    ZoomOut.NavigateUrl = head + "?id=" + id + "&fp=1&currency=" + currency + string.Format("&url={1}&w={2}", Model.ListingID, tiffile, width_out.ToString());
                    ZoomIn.NavigateUrl = head + "?id=" + id + "&fp=1&currency=" + currency + string.Format("&url={1}&w={2}", Model.ListingID, tiffile, width_in.ToString());
                }
                else
                {
                    ZoomOut.NavigateUrl = request + string.Format("&url={1}&w={2}", Model.ListingID, tiffile, width_out.ToString());
                    ZoomIn.NavigateUrl = request + string.Format("&url={1}&w={2}", Model.ListingID, tiffile, width_in.ToString());
                }

            }
        }
        try
        {
            if (media != null && media.Floorplans != null && media.Floorplans.Count > 1 || media.Floorplans[0].MediaUrl.Contains(".pdf"))
            {
                zooming.Visible = false;
                list.Visible = true;
            }
        }
        catch { }
        try
        {
            Model = (ListingInfo)Session["the_model"];
        }
        catch { }
    }
    protected string DisplayFloorplan(Floorplan f)
    {
        StringBuilder sb = new StringBuilder();
        if (f.MediaUrl.EndsWith(".pdf"))
        {
            sb.AppendFormat(@"<br /><br /><a href=""{0}"" target=""_blank"">Download Floor Plan</a>", f.MediaUrl);
            sb.AppendFormat(@"<embed src=""{0}"" width=""550"" height=""420"" />", f.MediaUrl);
        }
        else
            sb.AppendFormat(@"<img src=""{0}"" alt="""" />", f.MediaUrl);
        return sb.ToString();
    }
}