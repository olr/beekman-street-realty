﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="cntrls_Print_header" %>
<div class="header">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" bgcolor="#FFFFFF">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="text-align: left; height:70px;">
                            <img src="images/logo.png" alt="" style="height:53px; padding-left:5px;" />
                        </td>
                        <td align="right" valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <%--<td valign="top">
                                        &nbsp;
                                    </td>--%>
                                    <td align="right" valign="top">
                                        <span class="style1 style6">
                                            <strong><asp:Literal ID="lblAgentName" runat="server" /></strong>
                                        </span>
                                        <br />
                                        <asp:Literal ID="litOPhone" runat="server"></asp:Literal>
                                        <asp:Literal ID="litEmail" runat="server"></asp:Literal>
                                        <asp:Literal ID="Date_Time1" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div style="border-bottom: 1px solid #ccc;">&nbsp;</div>
</div>
