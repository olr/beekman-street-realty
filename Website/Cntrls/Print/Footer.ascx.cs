﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_Print_footer : BaseControl<cntrls_Print_footer, ListingInfo>, IRenderable<ListingInfo>
{
    public void DataBind(ListingInfo data)
    {
        Model = data ?? new ListingInfo();
        //footer
        var brokers = Model.GetBrokers();

        imgAgent1.Visible = false;
        imgAgent2.Visible = false;

        if (brokers.Count > 0)
        {
            imgAgent1.Visible = true;
            imgAgent1.ImageUrl = brokers[0].PhotoUrl;
            lblA1Name.Text = brokers[0].FirstName + " " + brokers[0].LastName;
            if (!string.IsNullOrWhiteSpace(brokers[0].WorkPhone))
                litA1OPhone.Text = "O: " + brokers[0].WorkPhone + "<br />";
            if (!string.IsNullOrWhiteSpace(brokers[0].MobilePhone))
                litA1MPhone.Text = "M: " + brokers[0].MobilePhone + "<br />";
            if (!string.IsNullOrWhiteSpace(brokers[0].Email))
                litA1Email.Text = brokers[0].Email;

        }

        if (brokers.Count > 1)
        {
            imgAgent2.Visible = true;
            imgAgent2.ImageUrl = brokers[1].PhotoUrl;
            lblA2Name.Text = brokers[1].FirstName + " " + brokers[1].LastName;
            if (!string.IsNullOrWhiteSpace(brokers[1].WorkPhone))
                litA2OPhone.Text = "O: " + brokers[1].WorkPhone + "<br />";
            if (!string.IsNullOrWhiteSpace(brokers[1].MobilePhone))
                litA2MPhone.Text = "M: " + brokers[1].MobilePhone + "<br />";
            if (!string.IsNullOrWhiteSpace(brokers[1].Email))
                litA2Email.Text = brokers[1].Email;
        }
    }
}