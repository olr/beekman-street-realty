﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;

public partial class cntrls_Print_header : BaseControl<cntrls_Print_header, ListingInfo>, IRenderable<ListingInfo>
{
    public void DataBind(ListingInfo data)
    {
        Model = data ?? new ListingInfo();
    }
}