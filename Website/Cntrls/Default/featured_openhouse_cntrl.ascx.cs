﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
public partial class Cntrls_CustomCntrl_featured_openhouse_cntrl : System.Web.UI.UserControl,IRenderable<Listings>
{
    protected Listings ls;
    protected ListingInfo salelisting;
    protected ListingInfo rentlisting;
    //protected void Page_Load(object sender, EventArgs e)
    //{

    //}

    public void DataBind(Listings data)
    {
        foreach (var listing in data)
        {
            if (listing.Price > 0)
                salelisting = listing;
            else
                rentlisting = listing;
        }
    }
}