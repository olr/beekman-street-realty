﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="sale_cntrl.ascx.cs" Inherits="cntrls_default_sale_cntrl" %>
<div class="title">
    Featured Sale
</div>
<div class="image left">
    <a href="<%=Model.GetDetailLink()%>">
        <img class="preload" src="<%= Model.GetDisplayPhoto() %>" /></a>
</div>
<div class="home-info left">
    <div class="header">
        <%=Model.GetListingBedroom()%>&nbsp;&nbsp;|&nbsp;<%=Model.GetPriceRentText(false) %>
    </div>
    <div class="neighborhood">
        <%=Model.GetArea() %>
    </div>
    <div class="description">
        <%= Model.GetDescriptionForDefalutPage()%>
    </div>
    <div class="button view-button button-image">
        <a href="<%=Model.GetDetailLink()%>">
            <img src="images/clear.png" /></a>
    </div>
</div>
