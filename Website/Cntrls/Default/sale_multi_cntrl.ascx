﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="sale_multi_cntrl.ascx.cs" Inherits="Cntrls_Default_sale_multi_cntrl" %>
<script type="text/javascript">
    Cufon.replace('span.font');
    </script>
    <%--<div class="title">
Featured Sale
</div>--%>
<div class="image left">
    <a href="<%=saleListing.GetDetailLink()%>">
        <img class="preload" src="<%= saleListing.GetDisplayPhoto() %>" /></a>
</div>
<div class="home-featured-button right">
   <%-- <div id="rent-pre" class="prev-button button-image left">
        <a href="#">
            <img src="images/clear.png" /></a></div>
    <div id="rent-next" class="next-button button-image left">
        <a href="#">
            <img src="images/clear.png" /></a></div>--%>
            </div>
<div class="home-info left">
    <div class="header">
        <a href="<%=saleListing.GetDetailLink()%>">
            <%=saleListing.GetListingBedroom()%>&nbsp;&nbsp;|&nbsp;<%=saleListing.GetPriceRentText(false)%>
        </a>
    </div>
    <div class="neighborhood">
        <a href="<%=saleListing.GetDetailLink()%>">
            <%=saleListing.GetArea()%>
        </a>
    </div>
    <div class="description">
        <%= saleListing.GetDescriptionForDefalutPage()%>
    </div>
    <div class="button short-button button-image">
        <a href="<%=saleListing.GetDetailLink()%>">
            <span class="font">View</span></a>
    </div>
</div>
