﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="featured_openhouse.ascx.cs"
    Inherits="Cntrls_Default_featured_openhouse" %>
<script type="text/javascript">
    $(function () {
        var True = true;
        var False = false;
        if(<%=showSale %>&&!<%=showRent %>)
        {
            $('div.openhouseSale').attr('style', 'display:block');
                $('div.openhouseRental').attr('style', 'display:none');
        }
        else if(!<%=showSale %>&&<%=showRent %>)
        {
            $('div.openhouseRental').attr('style', 'display:block');
                $('div.openhouseSale').attr('style', 'display:none');
        }
        else if(!<%=showSale %>&&!<%=showRent %>)
        {
            $('div.openhouseRental').attr('style', 'display:block');
                $('div.openhouseSale').attr('style', 'display:none');
        }

        $('input[name="openhouses"]').change(function () {
            if ($('input[name="openhouses"]:checked').val() == 'fp_sale') {
                console.log('sale');
                $('div.openhouseSale').attr('style', 'display:block');
                $('div.openhouseRental').attr('style', 'display:none');
            }
            else {
                console.log('rent');
                $('div.openhouseRental').attr('style', 'display:block');
                $('div.openhouseSale').attr('style', 'display:none');
            }
        });
    })
</script>
<div class='openhouseSale' style='display: none;'>
    <a href="<%=saleList.GetDetailLink()%>">
        <div class="view-banner">
        </div>
    </a>
</div>
<div class='openhouseRental'>
    <a href="<%=rentalList.GetDetailLink()%>">
        <div class="view-banner">
        </div>
    </a>
</div>
<div class="image left">
    <div class='openhouseSale' style='display: none;'>
        <a href="<%=saleList.GetDetailLink()%>">
            <img class="preload" src="<%= saleList.GetDisplayPhoto() %>" /></a>
    </div>
    <div class='openhouseRental'>
        <a href="<%=rentalList.GetDetailLink()%>">
            <img class="preload" src="<%= rentalList.GetDisplayPhoto() %>" /></a>
    </div>
    <%----%>
</div>
<div class="home-info right">
    <div class='openhouseSale' style='display: none;'>
        <a href="<%=saleList.GetDetailLink()%>">
            <%--<div class="title">
                Featured Open House
            </div>--%>
        </a>
    </div>
    <div class='openhouseRental'>
      <%--  <a href="<%=rentalList.GetDetailLink()%>">
            <div class="title">
                Featured Open House
            </div>
        </a>--%>
    </div>
    <%if (showSale && showRent)
      { %>
    <input type="radio" name="openhouses" id="fp_sale" value='fp_sale' />
    Sales
    <input type="radio" name="openhouses" id="fp_rental" value='fp_rental' checked="checked" />
    Rentals
    <%} if (saleList != null)
      {%>
    <div class='openhouseSale' style='display: none;'>
        <div class="building-info">
            <a href="<%=saleList.GetDetailLink()%>">
               <b> <%=saleList.Bedrooms%>
                Bedroom |
                <%=saleList.Neighborhood%>
                | $<%=SiteHelper.GetFormatPrice(saleList.Price)%> </b>
            </a>
        </div>
        <br>
        <a href="<%=saleList.GetDetailLink()%>">
            <%=saleList.BuildOpenHouses(true)%>
        </a>
    </div>
    <%} if (rentalList != null)
      { %>
    <div class='openhouseRental'>
        <div class="building-info">
            <a href="<%=rentalList.GetDetailLink()%>">
              <b>  <%=rentalList.Bathrooms %>
                Bedroom |
                <%=rentalList.Neighborhood %>
                | $<%=SiteHelper.GetFormatPrice(rentalList.Rent != 0 ? rentalList.Rent : rentalList.FurnishedRent)%> </b></a>
        </div>
        <br>
        <a href="<%=rentalList.GetDetailLink()%>">
            <%=rentalList.BuildOpenHouses(true) %>
        </a>
    </div>
    <%} %>
</div>
