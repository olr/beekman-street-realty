﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="rental_multi_cntrl.ascx.cs"
    Inherits="cntrls_default_rental_multi_cntrl" %>
    <script type="text/javascript">
        Cufon.replace('span.font');
    </script>
<%--<div class="title">
    Featured Rental
</div>--%>
<div class="image left">
    <a href="<%=rentListing.GetDetailLink()%>">
        <img class="preload" src="<%= rentListing.GetDisplayPhoto() %>" /></a>
</div>
<div class="home-featured-button right">
    <%--<div id="rent-pre" class="prev-button button-image left">
        <a href="#">
            <img src="images/clear.png" /></a></div>
    <div id="rent-next" class="next-button button-image left">
        <a href="#">
            <img src="images/clear.png" /></a></div>--%>
            </div>
<div class="home-info left">
    <div class="header">
        <a href="<%=rentListing.GetDetailLink()%>">
            <%=rentListing.GetListingBedroom()%>&nbsp;&nbsp;|&nbsp;<%=rentListing.GetPriceRentText(false)%>
        </a>
    </div>
    <div class="neighborhood">
        <a href="<%=rentListing.GetDetailLink()%>">
            <%=rentListing.GetArea()%>
        </a>
    </div>
    <div class="description">
        <%= rentListing.GetDescriptionForDefalutPage()%>
    </div>
    <div class="button short-button button-image">
        <a href="<%=rentListing.GetDetailLink()%>">
         <span class="font">View</span></a>
    </div>
</div>
