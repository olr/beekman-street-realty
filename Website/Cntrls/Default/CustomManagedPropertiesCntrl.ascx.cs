﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using OLRData;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class Cntrls_CustomCntrls_CustomManagedPropertiesCntrl : System.Web.UI.UserControl, IRenderable<Buildings>
{
    protected Buildings bi;
    Dictionary<int, int> ListingCounts = null;
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void DataBind(Buildings data)
    {
        bi = data;
        if (bi != null && bi.Count != 0)
            ListingCounts = SiteHelper.GetListingCount(bi);
        list.DataSource = bi;
        list.DataBind();
    }
    private static string removeJScript(string ori)
    {
        string result = Regex.Replace(ori, "<script.*?</script>", "");
        return result;
    }
   
    protected string DisplayStyle(BuildingInfo b)
    {
        if (ListingCounts != null && ListingCounts[b.ID] != null)
            return ListingCounts[b.ID] > 0 ? "block;" : "none";
        return "none;";
    }
}