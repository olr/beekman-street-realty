﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Web;
using BrokerTemplate.Core;

public partial class Cntrls_Default_featured_openhouse : System.Web.UI.UserControl, IRenderable<FeaturedPropertyGroup>
{
    protected ListingInfo saleList;
    protected ListingInfo rentalList;
    protected bool showSale;
    protected bool showRent;
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void DataBind(FeaturedPropertyGroup data)
    {
        if (data != null)
        {
            saleList = SiteHelper.GetRandomFeaturedListing(true); //data.GetListing(PropertyType.Sale);
            if (saleList != null)
                showSale = string.IsNullOrEmpty(saleList.ListingID) ? false : true;
            else
                showSale = false;
            rentalList = SiteHelper.GetRandomFeaturedListing(false);//data.GetListing(PropertyType.Rental);
            if (rentalList != null)
                showRent = string.IsNullOrEmpty(rentalList.ListingID) ? false : true;
            else
                showRent = false;
        }
       
    }
}