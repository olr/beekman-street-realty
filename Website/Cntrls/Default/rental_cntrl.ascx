﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="rental_cntrl.ascx.cs"
    Inherits="cntrls_default_rental_cntrl" %>
<div class="title">
    Featured Rental
</div>
<div class="image left">
    <a href="<%=Model.GetDetailLink()%>">
        <img class="preload" src="<%= Model.GetDisplayPhoto() %>" /></a>
</div>
<div class="home-info left">
    <div class="header">
        <%=Model.GetListingBedroom()%>&nbsp;&nbsp;|&nbsp;<%=Model.GetPriceRentText(false) %>
    </div>
    <div class="neighborhood">
        <%=Model.GetArea() %>
    </div>
    <div class="description">
        <%= Model.GetDescriptionForDefalutPage()%>
    </div>
    <div class="button view-button button-image">
        <a href="<%=Model.GetDetailLink()%>">
            <img src="images/clear.png" /></a>
    </div>
</div>
