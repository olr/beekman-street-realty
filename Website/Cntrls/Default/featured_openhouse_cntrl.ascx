﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="featured_openhouse_cntrl.ascx.cs"
    Inherits="Cntrls_CustomCntrl_featured_openhouse_cntrl" %>
    <script type="text/javascript">
        $(function () {
            $('input#saleRadio').click(function () {
                $('div.sale').attr('style', 'display:block;');
                $('div.rent').attr('style', 'display:none;');
            })
            $('input#rentRadio').click(function () {
                $('div.rent').attr('style', 'display:block;');
                $('div.sale').attr('style', 'display:none;');
            })
        })
    </script>
<form id="Form1" runat="server">
<div class="title">
    Featured Open House
</div>

<div class="radio right">
    <div class="left">
      <input type="radio" name="propertytype" value="sale" id="saleRadio" checked="checked" /> Sales
      <input type="radio" name="propertytype" value="rent" id="rentRadio" /> Rentals
    </div>
</div>
<br/>
<br/>
<div class="sale">
<div class="image left">
    <a href="<%=salelisting.GetDetailLink() %>">
        <img src="<%=salelisting.GetDisplayPhoto()%>" /></a>
</div>
<div class="right-info right">
    <div class="description right">
        <b>
            <a href="<%=salelisting.GetDetailLink() %>">
                <%=salelisting.GetListingBedroom()%>&nbsp;&nbsp;|&nbsp;<%=salelisting.GetPriceRentText(false)%>
            </a>    
        </b>
        <br/>
        <i>
            <a href="<%=salelisting.GetDetailLink() %>">
                <%=salelisting.GetArea()%>
            </a>    
        </i>
    </div>
    <div class="date">
       <a href="<%=salelisting.GetDetailLink() %>">
            <%=salelisting.GetOpenHouseString(false) %>
        </a>
    </div>
    <div class="button view-button button-image right">
        <a href="<%=salelisting.GetDetailLink() %>">
            <img src="images/clear.png" /></a>
    </div>
</div>
</div>
<div class="rent" style='display:none;'>
<div class="image left">
    <a href="<%=rentlisting.GetDetailLink() %>">
        <img src="<%=rentlisting.GetDisplayPhoto()%>" /></a>
</div>
<div class="right-info right">
    <div class="description right">
        <b>
            <a href="<%=rentlisting.GetDetailLink() %>">
                <%=rentlisting.GetListingBedroom()%>&nbsp;&nbsp;|&nbsp;<%=rentlisting.GetPriceRentText(false)%>
            </a>    
        </b>
        <br/>
        <i>
            <a href="<%=rentlisting.GetDetailLink() %>">
                <%=rentlisting.GetArea()%>
            </a>     
        </i>
    </div>
    <div class="date">
        <a href="<%=rentlisting.GetDetailLink() %>">
            <%=rentlisting.GetOpenHouseString(false)%>
        </a>
    </div>
    <div class="button view-button button-image right">
        <a href="<%=rentlisting.GetDetailLink() %>">
            <img src="images/clear.png" /></a>
    </div>
</div>
</div>
</form>
