﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_default_sale_cntrl : BaseControl<cntrls_default_sale_cntrl, ListingInfo>, IRenderable<ListingInfo>
{
    public void DataBind(ListingInfo data)
    {
        Model = data ?? new ListingInfo();
    }
    public string GetBedroomTag()
    {
        int beds = Model.Bedrooms;
        if (beds == 1)
            return " Bedroom";
        else
            return " Bedroom";
    }
}