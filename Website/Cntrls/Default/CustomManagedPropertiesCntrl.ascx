﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomManagedPropertiesCntrl.ascx.cs"
    Inherits="Cntrls_CustomCntrls_CustomManagedPropertiesCntrl" %>
<asp:ListView runat="server" ID="list">
    <LayoutTemplate>
        <ul class="tabular clearfix newdevelopment">
            <li id="itemPlaceHolder" runat="server" />
        </ul>
    </LayoutTemplate>
    <ItemTemplate>
        <li>
            <div class="image left">
           
                <a href="buildingdetail.aspx?id=<%# (Container.DataItem as BuildingInfo).ID%>">
                    <img src="<%# (Container.DataItem as BuildingInfo).GetBuildingPhotos()%>" alt="" />
                </a>
            </div>
            <p>
                <em><a href="buildingdetail.aspx?id=<%# (Container.DataItem as BuildingInfo).ID%>">
                     <%# (Container.DataItem as BuildingInfo).BuildingName%></a></em>
                <br>
                  <%# (Container.DataItem as BuildingInfo).Address%><br />
                <%# (Container.DataItem as BuildingInfo).Neighborhood%><br />
              <div class="nav-background button-image" style="margin-top:8px;display:<%# DisplayStyle(Container.DataItem as BuildingInfo)%>"><a href="buildingdetail.aspx?id=<%# (Container.DataItem as BuildingInfo).ID%>">
              <span>VIEW LISTINGS</span>
              </a></div>
            </p>
        </li>
    </ItemTemplate>
</asp:ListView>
