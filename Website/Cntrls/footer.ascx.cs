﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class Cntrls_footer : System.Web.UI.UserControl
{
    protected CompanyInfo company;
    protected string facebook;
    protected string linkedin;
    protected string twitter;
    protected void Page_Load(object sender, EventArgs e)
    {
        string id=ConfigurationManager.AppSettings["CompanyID"];
        facebook=ConfigurationManager.AppSettings["facebook"];
        linkedin=ConfigurationManager.AppSettings["linkedin"];
        twitter=ConfigurationManager.AppSettings["twitter"];
        company = ServiceLocator.GetRepository().GetCompanyInfoByID(Convert.ToInt32(id));
    }
    protected string showRegisterYear()
    {
        return ConfigurationManager.AppSettings["register"];
    }
    protected string showInfo()
    { 
        StringBuilder result=new StringBuilder(string.Empty);
        if (!string.IsNullOrEmpty(company.Phone))
            result.Append("P: " + company.Phone);
        else if (!string.IsNullOrEmpty(company.Fax))
        {
            if (!string.IsNullOrEmpty(company.Phone))
                result.Append(" | ");
            result.Append("F: " + company.Fax);
        }
        else if (!string.IsNullOrEmpty(company.CompanyEmail))
        {
            if (!string.IsNullOrEmpty(company.Phone) || !string.IsNullOrEmpty(company.Fax))
                result.Append(" | ");
            result.Append(company.CompanyEmail);
        }
        return result.ToString();
    }
}