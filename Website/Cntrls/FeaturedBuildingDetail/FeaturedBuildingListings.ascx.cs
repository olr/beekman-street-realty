﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Web;
using BrokerTemplate.Core;

public partial class Cntrls_FeaturedBuildingDetail_FeaturedBuildingListings : BaseControl<Cntrls_FeaturedBuildingDetail_FeaturedBuildingListings, BuildingInfo>, IRenderable<BuildingInfo>
{
    SearchOptions soption = new SearchOptions();
    protected override void OnInit(EventArgs e)
    {
        this.Controls.InitSortables(SortClicked);
        base.OnInit(e);
    }

    void SortClicked(object sender, CommandEventArgs e)
    {
        SearchOptions opts = GetSearchOptions();
        opts.OrderBy = fn.StringToEnum<SortOrder>(e.CommandName);
        opts.IsDescending = ((SortDirection)e.CommandArgument).Equals(SortDirection.Descending);
        opts.PageIndex = 0;
        RunSearch(opts);
    }
    SearchOptions GetSearchOptions()
    {
        SearchOptions options;
        int pageIndex = fn.Get<int>(HttpContext.Current.Request.QueryString["page"]);
        if (TempStorage.TryGet<SearchOptions>("SearchOptions", out options))
        {
            options.IsDescending = true;
           
            if (pageIndex > 0)
                options.PageIndex = pageIndex - 1;
            options.BuildingID = Model.ID;
            options.PropertyTypes.Add(PropertyType.Sale);
            options.PropertyTypes.Add(PropertyType.Rental);
        }
        else
        {
            options = new SearchOptions();
            options.IsDescending = true;
            if (pageIndex > 0)
                options.PageIndex = pageIndex - 1;
            options.BuildingID = Model.ID;
            options.PropertyTypes.Add(PropertyType.Sale);
            options.PropertyTypes.Add(PropertyType.Rental);
        }
        return options;
    }

    void ReSort(SearchOptions options, string sortOrder)
    {
        var lastSortOrder = options.OrderBy;
        switch (sortOrder)
        {
            case "price": options.OrderBy = SortOrder.Price; break;
            case "size": options.OrderBy = SortOrder.Size; break;
            case "location": options.OrderBy = SortOrder.Address; break;
        }
        if (lastSortOrder.Equals(options.OrderBy))
            //reverse sort order
            options.IsDescending = !options.IsDescending;
        options.PageIndex = 0;
        TempStorage.Add("SearchOptions", options);
        Response.Redirect("~/buildingdetail.aspx");
    }

    public void DataBind(BuildingInfo data)
    {
        if (data != null)
        {
            Model = data ?? new BuildingInfo();
            SearchOptions options = GetSearchOptions();
            RunSearch(options);
        }
    }

    void RunSearch(SearchOptions options)
    {
        if (options != null)
        {
            int _total = 0;
            Listings listings = null;
            if (options.PropertyTypes.Contains(PropertyType.Commercial))
                listings = ServiceLocator.GetComercialSearchService().RunSearch(options, out _total);
            else
                listings = ServiceLocator.GetSearchService().RunSearch(options, out _total);
            if (listings != null)
            {
                this.Controls.SetPagers(options.PageIndex + 1, options.PageSize, _total);
                if (listings.Count > 0)
                {
                    listings.CreatePagingSession();
                }
                this.Controls.DataBind<Listings>(listings);

            }
            soption = options;
            TempStorage.Add("SearchOptions", options);
            // this.Controls.DataBind<SearchOptions>(options);
            ShowVowLink = lastPage(options.PageIndex, options.PageSize, _total);
        }
    }

    protected bool ShowVowLink { get; set; }
    private bool lastPage(int pageIndex, int pageSize, int totalItems)
    {
        int numPages = (int)Math.Ceiling(totalItems / (double)pageSize);
        numPages = numPages > 0 ? numPages : 1;
        return pageIndex + 1 == numPages;
    }

}