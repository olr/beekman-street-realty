﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="footer.ascx.cs" Inherits="Cntrls_footer" %>
<div id="footer">
    <div class="container clearfix relative">
        <div class="clearfix">
            <div class="social left clearfix">
                <div class="facebook-icon button-image left">
                    <a href="<%=facebook %>" target="_blank">
                        <img src="images/clear.png" />
                    </a>
                </div>
                <div class="linkedin-icon button-image left">
                    <a href="<%=linkedin%>" target="_blank">
                        <img src="images/clear.png" />
                    </a>
                </div>
                <div class="twitter-icon button-image left">
                    <a href="<%=twitter%>" target="_blank">
                        <img src="images/clear.png" />
                    </a>
                </div>
                <div class="button-image cursor left">
                    <span class='st_sharethis'>
                        <img src="images/share.png" style="height: 20px; width: 22px;" />
                    </span>
                </div>
                <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
            </div>
            <div class="footer-info left">
            <ul>
                <li class="first"><a href="search.aspx?type=rent">Rentals</a></li>|
                <li><a href="search.aspx">Sales</a></li>|
                <li><a href="open_house.aspx?type=rent">Open Houses</a></li>|
                <li><a href="search.aspx?nofee=1">No Fee Specials</a></li>|
                <li><a href="about.aspx">Our Company</a></li>|
                <li><a href="testimonial.aspx">Testimonials</a></li>|
                <li><a href="career.aspx">Careers</a></li>|
                <li><a href="contact.aspx">Contact Us</a></li>|
                <br />
                <li class="first"><a href="relocation.aspx">Corporate Relocation</a></li>|
                <li><a href="neighborhood_guide.aspx">Neighborhood Information</a></li>|
                <li><a href="renters_guide.aspx">Rentals Guide</a></li>|
                <li><a href="mortgage_calculator.aspx" class="calclink2">Mortgage Calculator</a></li>|
                <li><a class="listwithus-pop" href="list_with_us.aspx">List With Us </a></li>
                |
                <br />
                <li><a href="agents.aspx">Our Agents </a></li>
                |
                <li class="first"><a href="contact.aspx">Contact Us </a></li>
                |
                <li><a href="corporate_legal_disclaimer.aspx">Corporate Legal Disclaimer</a></li>
                |
                <li><a href="terms_of_use.aspx">Terms and Conditions of Use</a></li>
                |
                <li class=""><a  href="dos_update_modal.aspx" class="brokersProcedure cboxElement" data-toggle="modal" data-target="#brokersProcedure">Broker's Procedures</a></li>
                |
                <li class=""><a target="_blank" href="https://dos.ny.gov/system/files/documents/2021/08/fairhousingnotice.pdf" style="display: inline-flex; align-items: center;">Fair Housing &nbsp;<img style="height: 15px;" src="images/FAIR-HOUSING.png" alt="" /></a></li>
    
            </ul>
        </div>
        <div class="olr-branding-new right button-image">
                <a target="_blank" href="http://www.olrdigital.com">
                    <img src="images/clear.png" /></a>
            </div>
        </div>

        <div class="clearfix">
            <div class="rebny-logo left"></div>
        <div class="address-info left">
            &copy; <span id="year"></span> Beekman Street Realty | 70 West 93rd Street, Suite 2. New York, NY 10025.
 | 212-624-6714 | info@beekmanstreetrealty.com
        </div>   
        <div class="affiliates right">
            </div>  
        </div>
       
        
        
        <%--        <div class="right clearfix">
        <div class="social right clearfix">
            <div class="facebook-icon button-image left">
                <a href="<%=facebook %>" target="_blank">
                    <img src="images/clear.png" />
                </a>
            </div>
            <div class="linkedin-icon button-image left">
                <a href="<%=linkedin%>" target="_blank">
                    <img src="images/clear.png" />
                </a>
            </div>
            <div class="twitter-icon button-image left">
                <a href="<%=twitter%>" target="_blank">
                    <img src="images/clear.png" />
                </a>
            </div>
        </div>
            <div class="affiliates-small left">
            </div>
            <div class="olr-branding-new left button-image">
                <a href="http://www.olrdigital.com">
                    <img src="images/clear.png" /></a>
            </div>
        </div>
        --%>
    </div>
</div>
