﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

/// <summary>
/// Summary description for RecentTransactions
/// </summary>
public partial class cntrls_Lists_RecentTransactions : ListingsControl
{
    public override void DataBind(Listings data)
    {
        if (data != null)
        {
            list.DataSource = data;
            list.DataBind();
        }
    }

    protected void SortData(object sender, EventArgs e)
    {
        base.SortData(sender as LinkButton, this.list);
    }
}