﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Web;
using BrokerTemplate.Core;

public partial class cntrls_Lists_ShortFormNoContact : ListingsControl
{
    public override void DataBind(Listings data)
    {
        if (data != null)
        {
            list.DataSource = data;
            list.DataBind();
        }
    }

    protected void SortData(object sender, EventArgs e)
    {
        base.SortData(sender as LinkButton, this.list);
    }
}