﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CompanyPress.ascx.cs"
    Inherits="Cntrls_Lists_CompanyPress" %>

<script type="text/javascript">
    $(function () {
        $('a.disable').removeAttr('href'); //fadeTo("fast", .5).
    });

</script>
<asp:ListView ID="list" runat="server">
    <LayoutTemplate>
        <div class="press-body">
            <asp:Literal runat="server" ID="itemPlaceHolder" />
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <div class="press-result-container clearfix">
            <div class="press-logo left">
                <%--<img src="images/photo_details_thumb.png" />--%>
                                <img src="<%#Eval("ThumbnailUrl")%>" alt="<%# Eval("Source").ToString().Trim(' ')%>" title="<%# Eval("Source").ToString().Trim(' ')%>"/>
                
            </div>
            
            <div class="press-column-right right">
            <div class="press-date">
                <%# Eval("FileDate", "{0:MMMM dd, yyyy}") %>
            </div>                
                <div class="press-launch left">
                    <a target="_blank"  class='<%#HasFile(Container.DataItem as CompanyNewsInfo)?"":"disable"%>' href="<%# (Container.DataItem as CompanyNewsInfo).FileUrl %>">
                        <img src="images/document.png" alt="press_icon" />
                    </a>
                </div>
                <div class="press-headline">
                    <a target="_blank"  class='<%#HasFile(Container.DataItem as CompanyNewsInfo)?"":"disable"%>' href="<%# (Container.DataItem as CompanyNewsInfo).FileUrl %>">
                        <%# Eval("Title") %>
                    </a>
                </div>                
                <div class="press-snipit">
                    <%# Eval("Description")%></div>
                <br><div class="span-6">
                    Source:&nbsp;&nbsp;<%# Eval("Source") %></div>
            </div>
        </div>
    </ItemTemplate>
    <EmptyDataTemplate>
        <div class="no-results">
            <p>
                There is no press available.
            </p>
        </div>
    </EmptyDataTemplate>
</asp:ListView>
