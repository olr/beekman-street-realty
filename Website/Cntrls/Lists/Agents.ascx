﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Agents.ascx.cs" Inherits="cntrls_Lists_Agents" %>

<script type="text/javascript">
    $(function () {
        $("a.email-me").colorbox({ iframe: true, innerWidth: 380, innerHeight: 490 });
    });
</script>

<asp:ListView runat="server" ID="list">
    <LayoutTemplate>
        <ul class="agent-results clearfix">
            <li runat="server" id="itemPlaceHolder"/>
        </ul>
        <br />
    </LayoutTemplate>
    <ItemTemplate>
        <li class="corners">
            <div class="agent-container clearfix">
                <div class="agent-thumb left">
                    <a href="<%# (Container.DataItem as BrokerInfo).GetDetailLink() %>">
                        <img class="agent_thumb_image" src='<%# Eval("PhotoUrl") %>' style="height: 147px;
                                width: 105px;" alt="" />
                    </a>
                </div>
                <div class="left">
                <div class="agent-info">
                    <div>
                        <a class="agent-name" href="<%# (Container.DataItem as BrokerInfo).GetDetailLink() %>"><%#Eval("FirstName") %> <%# Eval("LastName") %></a>
                    </div>
                    <div class="agent-title">
                       <%# (Container.DataItem as BrokerInfo).GetTitle()%>
                    </div>
                    <div class="agentphone" <%# Html.DisplayStyle(!string.IsNullOrEmpty((Container.DataItem as BrokerInfo).WorkPhone)) %>>
                        O: <%# Eval("WorkPhone", "{0}") %>
                    </div>
                    <div class="agentphone" <%# Html.DisplayStyle(!string.IsNullOrEmpty((Container.DataItem as BrokerInfo).MobilePhone)) %>>
                        M: <%# Eval("MobilePhone", "{0}") %>
                    </div>
                </div>
                <div class="agent-tools">
                    <div>
                        <a class="email-me" href="<%# (Container.DataItem as BrokerInfo).GetEmailMeLink() %>">
                            Email Me
                        </a>
                    </div>
                    <div>
                        <a class="vcard" target="_blank" href="<%# (Container.DataItem as BrokerInfo).GetVCardLink() %>">
                            Download Contact
                        </a>
                    </div>
                    <div>
                        <a class="mylistings" href="<%# (Container.DataItem as BrokerInfo).GetDetailLink() %>">
                            View My Listings
                        </a>
                    </div>
                </div>
            </div>
            </div>
        </li>
    </ItemTemplate>
</asp:ListView>