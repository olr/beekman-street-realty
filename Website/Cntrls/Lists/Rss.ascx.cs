﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using RssToolkit.Rss;

public partial class cntrls_Lists_Rss : System.Web.UI.UserControl, IRenderable<string>
{
    public void DataBind(string url)
    {
        list.DataSource = GetRSSFeed(url);
        list.DataBind();
    }

    private List<RssItem> GetRSSFeed(string url)
    {
        List<RssItem> items = new List<RssItem>();
        RssDocument rss = null;
        try
        {
            rss = RssDocument.Load(new Uri(url));
            foreach (var item in rss.Channel.Items.Take(5).ToList())
            {
                item.Title = fn.Truncate(item.Title, 50, true, true);
                items.Add(item);
            }
        }
        catch
        {
        }
        return items;
    }
}