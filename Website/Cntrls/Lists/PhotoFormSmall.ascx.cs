﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_Lists_PhotoFormSmall : ListingsControl
{
    public bool DoNotShowLink { get; set; }

    public override void DataBind(Listings data)
    {
        if (data != null)
        {
            list.DataSource = data;
            list.DataBind();
        }
    }

    protected void SortData(object sender, EventArgs e)
    {
        base.SortData(sender as LinkButton, this.list);
    }

    protected override string BuildListingInfo(ListingInfo l)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendFormat(@"<span class=""titlegreen"">{0}</span><br />", l.GetPriceRentText(true));

        if (l.SquareFootage > 0)
            sb.AppendFormat("ASF. {0:#,###}", l.SquareFootage);

        if (!string.IsNullOrEmpty(l.Ownership))
        {
            if (l.SquareFootage>0)
                sb.AppendFormat(" | ");
            sb.AppendFormat("{0}", l.Ownership);
        }

        return sb.ToString();
    }

    private int counter = 0;
    protected override string BuildClassName()
    {
        counter++;
        string className = "middle";
        if (counter % 4 == 1)
            className = "first";
        else if (counter % 4 == 0)
            className = "last";
        return className;
    }

    protected string BuildDisplayPhoto(ListingInfo l)
    {
        StringBuilder sb = new StringBuilder();
        if (!DoNotShowLink)
            sb.AppendFormat(@"<a href=""{0}"">", l.GetDetailLink());
        sb.AppendFormat(@"<img src=""{0}"" alt="""" />", l.GetDisplayPhoto());
        if (!DoNotShowLink)
            sb.Append("</a>");
        return sb.ToString();
    }

}