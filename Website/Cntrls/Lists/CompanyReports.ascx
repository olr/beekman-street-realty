﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CompanyReports.ascx.cs" Inherits="cntrls_Lists_CompanyReports" %>
<script type="text/javascript">
    $(function () {
        $('a.disable').removeAttr('href'); //fadeTo("fast", .5).
    });
</script>
<asp:ListView ID="list" runat="server">
    <LayoutTemplate>
        <div class="press-body" style="margin-bottom:30px;">
            <asp:Literal runat="server" ID="itemPlaceHolder" />
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <div class="market-row clearfix">
            <div class="column">
                <a target="_blank"  class='<%#HasFile(Container.DataItem as CompanyNewsInfo)?"":"disable"%>' href="<%# (Container.DataItem as CompanyNewsInfo).FileUrl %>"><img src="images/icons/document.png" alt="" /></a>
            </div>
            <div class="column market-title">
                <a target="_blank"  class='<%#HasFile(Container.DataItem as CompanyNewsInfo)?"":"disable"%>' href="<%# (Container.DataItem as CompanyNewsInfo).FileUrl %>">
                Leslie J. Garfield & Co. Townhouse & Building Report - <%# Eval("Title") %></a>
            </div>
        </div>
    </ItemTemplate>
    <EmptyDataTemplate>
        <p>
            Market report data not available.
        </p>
    </EmptyDataTemplate>
</asp:ListView>
