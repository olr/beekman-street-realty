﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Web;
using BrokerTemplate.Core;

public partial class Cntrls_Lists_CompanyTestimonial : System.Web.UI.UserControl, IRenderable<CompanyNewsList>
{
    public void DataBind(CompanyNewsList data)
    {
        if (data != null)
        {
            testimonialList.DataSource = data;
            testimonialList.DataBind();
        }
    }
}