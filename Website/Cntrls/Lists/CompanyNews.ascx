﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CompanyNews.ascx.cs" Inherits="cntrls_Lists_CompanyNews" %>

<asp:ListView ID="list" runat="server">
    <LayoutTemplate>
        <div class="press-header clearfix">
            <div class="header span-6"><b>SOURCE</b></div>
            <div class="header span-11"><b>TITLE</b></div>
            <div class="header span-5"><b>DATE</b></div>
            <div class="header column last"><b>VIEW</b></div>
        </div>
        <hr />
        <div class="press-body">
            <asp:Literal runat="server" ID="itemPlaceHolder" />
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <div class="press-row clearfix">
            <div class="span-6"><%# Eval("Source") %></div>
            <div class="span-11"><%# Eval("Title") %></div>
            <div class="span-5"><%# Eval("FileDate", "{0:MMMM dd, yyyy}") %></div>
            <div class="column last">
                <a target="_blank" href="<%# (Container.DataItem as CompanyNewsInfo).FileUrl %>">
                    <img src="images/document.png" alt="" />
                </a>
            </div>
        </div>
    </ItemTemplate>
    <EmptyDataTemplate>
        <p>
            Press data not available.
        </p>
    </EmptyDataTemplate>
</asp:ListView>
