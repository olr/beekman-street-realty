﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_Lists_PhotoForm : ListingsControl
{
    public override void DataBind(Listings data)
    {
        if (data != null)
        {
            list.DataSource = data;
            list.DataBind();
        }
    }

    protected void SortData(object sender, EventArgs e)
    {
        base.SortData(sender as LinkButton, this.list);
    }
}