﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_Lists_Agents : BaseControl<cntrls_Lists_Agents, Brokers>, IRenderable<Brokers>
{
    public void DataBind(Brokers data)
    {
        if (data != null)
        {
            Model = OrderForNetmore(data);
            foreach (BrokerInfo b in Model)
            {
                if (string.IsNullOrEmpty(b.PhotoUrl))
                    b.PhotoUrl = "img/agent_thumb.png";
                b.PhotoUrl = b.PhotoUrl.Replace("~/", string.Empty);
            }
            list.DataSource = Model;
            list.DataBind();
        }
    }
    private Brokers OrderForNetmore(Brokers b)
    {
        Brokers brokers = new Brokers();
        foreach (BrokerInfo broker in b)
        {
            if (broker.Email == "mike@netmorerealty.com")
            {
                brokers.Add(broker);
                break;
            }
        }
        foreach (BrokerInfo broker in b)
        {
            if (broker.Email == "danny@netmorerealty.com")
            {
                brokers.Add(broker);
                break;
            }
        }
        foreach (BrokerInfo broker in b)
        {
            if (broker.Email == "ido@netmorerealty.com")
            {
                brokers.Add(broker);
                break;
            }
        }
        
        foreach (BrokerInfo broker in b)
        {
            if (broker.Email != "danny@netmorerealty.com" && broker.Email != "mike@netmorerealty.com"&&broker.Email != "ido@netmorerealty.com")
            brokers.Add(broker);
        }
        return brokers;
    }
}