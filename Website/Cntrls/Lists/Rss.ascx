﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Rss.ascx.cs" Inherits="cntrls_Lists_Rss" %>
<div class="neighborhood_feed_list">
    <asp:ListView runat="server" ID="list">
        <LayoutTemplate>
            <asp:Literal runat="server" ID="itemPlaceHolder" />
        </LayoutTemplate>
        <ItemTemplate>
            <p><a target="_blank" href='<%# Eval("link") %>'><%# Eval("title") %></a></p>
        </ItemTemplate>
    </asp:ListView>
</div>
