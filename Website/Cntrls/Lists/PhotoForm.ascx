﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PhotoForm.ascx.cs" Inherits="cntrls_Lists_PhotoForm" %>
<script type="text/javascript">
    $(document).ready(function () {
        $("a.email-me").colorbox({ iframe: true, innerWidth: 380, innerHeight: 490 });
    });
</script>
<asp:ListView runat="server" ID="list">
    <LayoutTemplate>
        <ul class="listings clearfix tabular">
            <li id="itemPlaceHolder" runat="server"></li>
        </ul>
    </LayoutTemplate>
    <ItemTemplate>
        <li class="listing <%# BuildClassName() %>">
            <div class="listing-photo">
                <a href="<%# (Container.DataItem as ListingInfo).GetDetailLink() %>">
                    <img src="<%# (Container.DataItem as ListingInfo).GetDisplayPhoto() %>" alt="" />
                </a>
            </div>
            <div class="listing-address"><%# (Container.DataItem as ListingInfo).GetDisplayAddress() %></div>
            <div class="listing-info"><%# BuildListingInfo(Container.DataItem as ListingInfo)%></div>
            <div class="openhouse-info"><%# GetOpenHouses(Container.DataItem as ListingInfo) %></div>
            <div class="contact-info"><%# BuildContact(Container.DataItem as ListingInfo)%></div>
        </li>
    </ItemTemplate>
    <EmptyDataTemplate>
        <p class="no-results">
            <%=Resources.SearchResultListings.NoResultsText%>
        </p>

        <div id="vowtemplate"><a href="vow.aspx"><img alt="" src="images/vowljg.jpg" /></a></div>

    </EmptyDataTemplate>
</asp:ListView>