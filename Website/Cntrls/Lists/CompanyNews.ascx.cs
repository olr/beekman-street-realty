﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;

public partial class cntrls_Lists_CompanyNews : System.Web.UI.UserControl, IRenderable<CompanyNewsList>
{
    public void DataBind(CompanyNewsList data)
    {
        if (data != null)
        {
            list.DataSource = data;
            list.DataBind();
        }
    }

}