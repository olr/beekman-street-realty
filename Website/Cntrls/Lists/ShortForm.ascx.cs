﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Web;
using BrokerTemplate.Core;
using System.Text;

public partial class cntrls_Lists_ShortForm : ListingsControl
{
    private string _NoResultMessage = Resources.SearchResultListings.NoResultsText;
    public string NoResultMessage
    {
        get { return _NoResultMessage; }
        set { _NoResultMessage = value; }
    }
    protected bool isAgentPage = false;
    protected int agentid;
    public bool DoNotShowLink { get; set; }
    protected Listings l;
    protected string rentpriceLable;
    public override void DataBind(Listings data)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["agentid"]))
        {
            isAgentPage = true;
            agentid = Convert.ToInt32(Request.QueryString["agentid"]);
        }
        if (data != null)
        {
            list.DataSource = data;
            list.DataBind();
            if (data.Count != 0)
                rentpriceLable = BuildPriceRentLabel(data[0]);
        }
    }

    protected void SortData(object sender, EventArgs e)
    {
        base.SortData(sender as LinkButton, this.list);
    }

    protected string BuildDisplayPhoto(ListingInfo l)
    {
        StringBuilder sb = new StringBuilder();
        if (!DoNotShowLink)
            sb.AppendFormat(@"<a href=""{0}"">", l.GetDetailLink());
        sb.AppendFormat(@"<img src=""{0}"" alt="""" />", l.GetDisplayPhoto());
        if (!DoNotShowLink)
            sb.Append("</a>");
        return sb.ToString();
    }
}