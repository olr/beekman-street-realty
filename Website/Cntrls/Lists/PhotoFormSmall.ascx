﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PhotoFormSmall.ascx.cs" Inherits="cntrls_Lists_PhotoFormSmall" %>
<asp:ListView runat="server" ID="list">
    <LayoutTemplate>
        <ul class="listings clearfix tabular">
            <li id="itemPlaceHolder" runat="server"></li>
        </ul>
    </LayoutTemplate>
    <ItemTemplate>
        <li class="listing <%# BuildClassName() %>">
            <div class="listing-photo">
                <%# BuildDisplayPhoto(Container.DataItem as ListingInfo)%>
            </div>
            <div class="listing-address"><%# (Container.DataItem as ListingInfo).GetDisplayAddress() %></div>
            <div class="listing-info"><%# BuildListingInfo(Container.DataItem as ListingInfo)%></div>
            <div style="display:none;" class="openhouse-info"><%# GetOpenHouses(Container.DataItem as ListingInfo) %></div>
        </li>
    </ItemTemplate>
</asp:ListView>