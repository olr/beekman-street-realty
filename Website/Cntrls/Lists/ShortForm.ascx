﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ShortForm.ascx.cs" Inherits="cntrls_Lists_ShortForm" %>
<script type="text/javascript">
    $(document).ready(function () {
        $("a.email-me").colorbox({ iframe: true, innerWidth: 380, innerHeight: 490 });
        $('a.photolink').colorbox({ iframe: true, innerWidth: 600, innerHeight: 500 });
        $('a.fplink').colorbox({ iframe: true, innerWidth: 600, innerHeight: 500 });
        $('a.emailfriend').colorbox({ iframe: true, innerWidth: 400, innerHeight: 400 });
        $('.tip').tipTip({ defaultPosition: "left" });
        <%if(rentpriceLable=="Rent:") {%>
        $('.financials').text('Rent');
        <%} else {%>
        $('.financials').text('Price');
        <%} %>
    });
</script>
<form runat="server">
<asp:ListView ID="list" runat="server">
    <LayoutTemplate>
        <div class="result-container withcontact ">
            <div class="result-header corners clearfix">
                <div class="column-photo">
                    &nbsp;</div>
                <div class="column-location">
                    <asp:LinkButton ID="LocationLinkButton" runat="server" OnClick="SortData" CommandArgument="Neighborhood"
                        Text="<%$ Resources:SearchResultListings, LocationLabel %>" />
                    <asp:Image ID="NeighborhoodArrow" runat="server" ImageUrl="~/images/icons/down.png"
                        Visible="False" />
                </div>
                 <div class="column-size">
                    <asp:LinkButton ID="SizeLinkButton" runat="server" OnClick="SortData" CommandArgument="Beds"
                        Text="<%$ Resources:SearchResultListings, SizeLabel %>" />
                    <asp:Image ID="SizeArrow" runat="server" ImageUrl="~/images/icons/down.png" Visible="False" />
                </div>
                <div class="column-amenities">
                    Building
                </div>
                <div class="column-price">
                    <asp:LinkButton ID="PriceLinkButton" runat="server" OnClick="SortData" CommandArgument="Price" class="financials"
                        Text="<%$ Resources:SearchResultListings, FinancialsLabel %>" />
                    <asp:Image ID="PriceRentArrow" runat="server" ImageUrl="~/images/icons/down.png" />
                </div>
               
                
                <div class="column-contact">
                    <span>Contact</span>
                </div>
            </div>
            <asp:Literal runat="server" ID="itemPlaceHolder" />
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <div class="result-body corners clearfix <%#OddEvenString%>">
            <div class="column-photo">
                <div class="photo-thumb">
                    <div class="photo-thumb-border">
                        <a href="<%# (Container.DataItem as ListingInfo).GetDetailLink() %>">
                            <img class="preload" src="<%# (Container.DataItem as ListingInfo).GetDisplayPhoto() %>"
                                alt="" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="column-location">
                <div class="result_address_title">
                    <a href="<%# (Container.DataItem as ListingInfo).GetDetailLink() %>">
                        <%# (Container.DataItem as ListingInfo).GetDisplayAddress() %>
                    </a>
                </div>
                <div>
                    <%# (Container.DataItem as ListingInfo).GetNeiborhoodForShortForm()%></div>
                <div>
                    Listing ID:
                    <%# Eval("ListingID") %></div>
                    <div class="open-house-container">
                <%# GetOpenHouses(Container.DataItem as ListingInfo)%>
            </div>
            </div>
             <div class="column-size">
                <div>
                    <%# BuildSize(Container.DataItem as ListingInfo)%></div>
                <div>
                    <%# BuildBathroom(Container.DataItem as ListingInfo)%></div>
                
                 </div> 
            <div class="column-amenities">
                <div>
                    <%# (Container.DataItem as ListingInfo).Ownership %></div>
                <div>
                    <%# (Container.DataItem as ListingInfo).BuildingType %></div>
                <div>
                    <%# BuildServiceLevel(Container.DataItem as ListingInfo)%></div>
            </div>
            <div class="column-price">
                <dl class="price-info">
                    <dt class="result_price_title">
                        <%# BuildPriceRentLabel(Container.DataItem as ListingInfo) %></dt>
                    <dd class="result_price_title">
                        <%# BuildPriceRent(Container.DataItem as ListingInfo,true)%>
                        <%# BuildIsNoFee(Container.DataItem as ListingInfo)%>
                    </dd>
                    <dt>
                        <%# BuildMaintCCLabel(Container.DataItem as ListingInfo)%></dt>
                    <dd>
                        <%# BuildMaintCC(Container.DataItem as ListingInfo)%></dd>
                    <dt>
                        <%# BuildRETaxesLabel(Container.DataItem as ListingInfo)%></dt>
                    <dd>
                        <%# BuildRETaxes(Container.DataItem as ListingInfo)%></dd>
                        <dt>
                        <%# BuildStatusLable(Container.DataItem as ListingInfo)%></dt>
                        <dd><%# BuildStatus(Container.DataItem as ListingInfo)%></dd>
                </dl>
            </div>
            <div class="column-contact">
                <%# BuildContact(Container.DataItem as ListingInfo,isAgentPage,agentid)%>
            </div>
        </div>
    </ItemTemplate>
    <EmptyDataTemplate>
        <p class="no-results">
            <%=NoResultMessage%>
        </p>
    </EmptyDataTemplate>
</asp:ListView>
</form>
