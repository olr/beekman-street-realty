﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FeaturedBuildings.ascx.cs" Inherits="Cntrls_Lists_FeaturedBuildings" %>
<asp:ListView runat="server" ID="list">
    <LayoutTemplate>
        <ul class="tabular clearfix newdevelopment">
            <li id="itemPlaceHolder" runat="server" />
        </ul>
    </LayoutTemplate>
    <ItemTemplate>
        <li>
            <div class="top-condos corners left">
                <div class="top-condos-container clearfix">
                    <div class="image">
                        <a href="buildingdetail.aspx?id=<%# (Container.DataItem as BuildingInfo).ID%>">
                            <img src="<%# (Container.DataItem as BuildingInfo).GetBuildingPhotos()%>" alt="" />
                        </a>
                    </div>
                    <div class="building-info left">
                        <p>
                           <a href="buildingdetail.aspx?id=<%# (Container.DataItem as BuildingInfo).ID%>">
                                <%# Eval("BuildingName") %>
                                <br>
                               <span class="featured-address"> <%# Eval("Address") %></span></a></p>
                        
                        <p>
                            <%# Eval("Neighborhood") %>
                            <br>
                           [<%# Eval("CrossStreet1") %>
                            &
                            <%# Eval("CrossStreet2") %>]</p>
                            
                        <div class="view-listings-button button-image"  style="margin-top:8px;display:<%# DisplayStyle(Container.DataItem as BuildingInfo)%>">
                            <a href="buildingdetail.aspx?id=<%# (Container.DataItem as BuildingInfo).ID%>">
                                <img src="images/clear.png" alt="" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
          
        </li>
    </ItemTemplate>
</asp:ListView>
