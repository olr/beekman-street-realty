﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecentTransactions.ascx.cs" Inherits="cntrls_Lists_RecentTransactions" %>
<asp:ListView ID="list" runat="server">
    
    <LayoutTemplate>
  
        <table width="100%" border="0" align="center" cellpadding="0" class="recent-transactions-header corners" cellspacing="5" class="recentTransaction" >
            <tr class="result_header">
                <%--<th align="center" valign="top" style="width: 25%; padding-left:60px;">Address</th>--%>
                <th align="center" valign="top" style="width: 25%; padding-left:65px;">Neighborhood</th>
                <th align="center" valign="top" style="width: 45%; padding-left:45px;">Ownership</th>
                <th align="center" valign="top" style="width: 25%; padding-left:62px;"><table><tr><td style='width:46px; font-weight:bold;'>Rooms&nbsp;&nbsp;</td><td style='width:40px; font-weight:bold;'>Beds&nbsp;</td><td style='width:30px; font-weight:bold;'>Baths&nbsp;</td></td></table></th>
            </tr>
            <tr id="itemPlaceholder" runat="server" />
        </table>
        
    </LayoutTemplate>

    <ItemTemplate>
        <tr class="<%#OddEvenString%>">
            <%--<td style="color:#666666; padding-left:60px;"><%# Eval("Address") %></td>--%>
            <td style="color:#666666; padding-left:65px;"><%# (Container.DataItem as ListingInfo).GetArea() %></td>
            <td style="color:#666666; padding-left:45px;"><%# (Container.DataItem as ListingInfo).Ownership %></td>
            <td style="color:#666666; padding-left:55px;"><%# BuildRoomCount(Container.DataItem as ListingInfo)%></td>
        </tr>
    </ItemTemplate>
</asp:ListView>
