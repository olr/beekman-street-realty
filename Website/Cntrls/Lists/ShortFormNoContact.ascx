﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ShortFormNoContact.ascx.cs" Inherits="cntrls_Lists_ShortFormNoContact" %>
<script type="text/javascript">
    $(document).ready(function () {
        $('a.photolink').colorbox({ iframe: true, innerWidth: 600, innerHeight: 500 });
        $('a.fplink').colorbox({ iframe: true, innerWidth: 600, innerHeight: 500 });
    });
</script>

<asp:ListView ID="list" runat="server">
    <LayoutTemplate>
    <div class="result_container nocontact">
        <div class="result-header clearfix">
            <div class="column-photo">&nbsp;</div>
            <div class="column-location">
                <asp:LinkButton ID="LocationLinkButton" runat="server" OnClick="SortData" CommandArgument="Neighborhood" Text="<%$ Resources:SearchResultListings, LocationLabel %>"></asp:LinkButton>
                <asp:Image ID="NeighborhoodArrow" runat="server" ImageUrl="~/images/sort_arrow.png" Visible="False" />
            </div>
            <div class="column-size">
                <asp:LinkButton ID="SizeLinkButton" runat="server" OnClick="SortData" CommandArgument="Beds" Text="<%$ Resources:SearchResultListings, SizeLabel %>"></asp:LinkButton>
                <asp:Image ID="SizeArrow" runat="server" ImageUrl="~/images/sort_arrow.png" Visible="False" />
            </div>
            <div class="column-amenities">
                Building
            </div>
            <div class="column-price">
                <asp:LinkButton ID="PriceLinkButton" runat="server" OnClick="SortData" CommandArgument="Price" Text="<%$ Resources:SearchResultListings, FinancialsLabel %>"></asp:LinkButton>
                <asp:Image ID="PriceRentArrow" runat="server" ImageUrl="~/images/sort_arrow.png" />
            </div>
        </div>
        <asp:Literal runat="server" ID="itemPlaceHolder" />
    </div>
    </LayoutTemplate>
    <ItemTemplate>
        <div class="result_body clearfix <%#OddEvenString%>">
            <div class="column-photo">
                <div class="photo-thumb">
                    <a href="<%# (Container.DataItem as ListingInfo).GetDetailLink() %>">
                        <img class="preload" src="<%# (Container.DataItem as ListingInfo).GetDisplayPhoto() %>" alt="" />
                    </a>
                </div>
            </div>
            <div class="column-location">
                <div class="result_address_title">
                    <a href="<%# (Container.DataItem as ListingInfo).GetDetailLink() %>">
                        <%# (Container.DataItem as ListingInfo).GetDisplayAddress() %>
                    </a>
                </div>
                <div><%# Eval("Neighborhood") %></div>
                <div>Listing ID: #<%# Eval("ListingID") %></div>
            </div>
            <div class="column-size">
                <div><%# BuildSize(Container.DataItem as ListingInfo)%></div>
                <div><%# BuildBathroom(Container.DataItem as ListingInfo)%></div>
                <div><%# BuildSquareFootage(Container.DataItem as ListingInfo)%></div>
            </div>
            <div class="column-amenities">
                <div><%# (Container.DataItem as ListingInfo).Ownership %></div>
                <div><%# (Container.DataItem as ListingInfo).BuildingType %></div>
                <div><%# (Container.DataItem as ListingInfo).ServiceLevel %></div>
            </div>
            <div class="column-price">
                <dl class="clearfix price-info">
                    <dt class="result_price_title"><%# BuildPriceRentLabel(Container.DataItem as ListingInfo) %></dt>
                    <dd class="result_price_title">
                        <%# BuildPriceRent(Container.DataItem as ListingInfo,true)%>
                        <%# BuildIsNoFee(Container.DataItem as ListingInfo)%>
                    </dd>
                    <dt><%# BuildMaintCCLabel(Container.DataItem as ListingInfo)%></dt>
                    <dd><%# BuildMaintCC(Container.DataItem as ListingInfo)%></dd>
                    <dt><%# BuildRETaxesLabel(Container.DataItem as ListingInfo)%></dt>
                    <dd><%# BuildRETaxes(Container.DataItem as ListingInfo)%></dd>
                </dl>
            </div>
            <div class="column-openhouse">
                <%# GetOpenHouses(Container.DataItem as ListingInfo)%>
            </div>
            <div class="column-tools">
                <%# BuildToolLinks(Container.DataItem as ListingInfo)%>
            </div>
        </div>
    </ItemTemplate>
</asp:ListView>
