﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;

public partial class Cntrls_Lists_FeaturedbuildingsToResult : System.Web.UI.UserControl, IRenderable<Buildings>
{
    public void DataBind(Buildings data)
    {
        list.DataSource = data;
        list.DataBind();
    }
}