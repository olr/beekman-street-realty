﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CompanyTestimonial.ascx.cs" Inherits="Cntrls_Lists_CompanyTestimonial" %>

<asp:ListView runat="server" ID="testimonialList">
    <LayoutTemplate>
    <asp:literal runat="server" ID="itemPlaceHolder"></asp:literal> 
    </LayoutTemplate>
    <ItemTemplate>
    <div class="testimonial-container clearfix">
    <div class="date"><%#(Container.DataItem as CompanyNewsInfo).FileDate.ToShortDateString() %></div>
    <div class="name"><%#(Container.DataItem as CompanyNewsInfo).Source%></div>
    <div class="description"><%#(Container.DataItem as CompanyNewsInfo).Description %></div>
</div>
    </ItemTemplate>
</asp:ListView>

