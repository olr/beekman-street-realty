﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class Cntrls_Lists_CompanyPress : BaseControl<Cntrls_Lists_CompanyPress, CompanyNewsList>, IRenderable<CompanyNewsList>
{
    protected CompanyNewsList Press = new CompanyNewsList();
    public void DataBind(CompanyNewsList data)
    {
        if (data != null)
        {
            Model = data;
            list.DataSource = Model;
            list.DataBind();
        }
    }
    protected bool HasFile(CompanyNewsInfo info)
    {
        if (string.IsNullOrEmpty(info.FileUrl))
            return false;
        else
            return true;
    }
}