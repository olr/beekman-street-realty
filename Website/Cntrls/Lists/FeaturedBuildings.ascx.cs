﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;

public partial class Cntrls_Lists_FeaturedBuildings : System.Web.UI.UserControl, IRenderable<Buildings>
{
    Dictionary<int, int> ListingCounts = null;

    public void DataBind(Buildings data)
    {
        if (data != null)
            ListingCounts = BrokerTemplate.Web.SiteHelper.GetListingCount(data);

        list.DataSource = data;
        list.DataBind();
    }
    protected string ShowBuildingName(BuildingInfo b)
    {
        string name = b.BuildingName;
        if (b.BuildingName.EndsWith(", The"))
            name = "The " + name.Replace(", The", string.Empty);
        return name;
    }
    protected bool HidePhoto(BuildingInfo b)
    {
        return b.HidePhoto;
    }
    protected string DisplayStyle(BuildingInfo b)
    {
        if (ListingCounts != null && ListingCounts[b.ID] != null)
            return ListingCounts[b.ID] > 0 ? "block;" : "none";
        return "none;";
    }
}