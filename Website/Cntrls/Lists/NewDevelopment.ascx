﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewDevelopment.ascx.cs" Inherits="cntrls_Lists_NewDevelopment" %>

<asp:ListView runat="server" ID="list">
    <LayoutTemplate>
        <ul class="tabular clearfix newdevelopment">
            <li id="itemPlaceHolder" runat="server" />
        </ul>
    </LayoutTemplate>
    <ItemTemplate>
        <li>
            <div>
                <a href="<%# (Container.DataItem as BuildingInfo).GetNewDevelopmentLink() %>">
                    <img src="<%# Eval("PhotoUrl") %>" alt="" />
                </a>
            </div>
            <div class="info clearfix">
                <div class="buildingName column-fifty"><%# Eval("BuildingName") %></div>
                <div class="buildingName column-fifty"><%# Eval("Neighborhood") %></div>
                <div class="buildingName column-fifty"><%# Eval("Address") %></div>
                <div class="buildingName column-fifty"><%# Eval("CrossStreet1") %> & <%# Eval("CrossStreet2") %></div>
            </div>
            <div>
                <a href="<%# (Container.DataItem as BuildingInfo).GetNewDevelopmentLink() %>">
                    <img src="images/more_information.png" alt="" />
                </a>
            </div>
        </li>
    </ItemTemplate>
</asp:ListView>