﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FeaturedbuildingsToResult.ascx.cs" Inherits="Cntrls_Lists_FeaturedbuildingsToResult" %>
<asp:ListView runat="server" ID="list">
    <LayoutTemplate>
        <ul class="tabular clearfix newdevelopment">
            <li id="itemPlaceHolder" runat="server" />
        </ul>
    </LayoutTemplate>
    <ItemTemplate>
        <li>
            <div class="top-condos left">
                <div class="top-condos-container clearfix">
                    <div class="image left">
                        <a href="listingsInBuilding.aspx?buildingid=<%# (Container.DataItem as BuildingInfo).ID%>">
                            <img src="<%# Eval("PhotoUrl") %>" alt="" />
                        </a>
                    </div>
                    <div class="building-info left">
                        <p>
                            <span><a href="listingsInBuilding.aspx?buildingid=<%# (Container.DataItem as BuildingInfo).ID%>">
                                <%# Eval("BuildingName") %>
                                <br>
                                <%# Eval("Address") %></a><span></p>
                        <br>
                        <p>
                            <%# Eval("Neighborhood") %>, Cross Streets:
                            <br>
                            <%# Eval("CrossStreet1") %>
                            &
                            <%# Eval("CrossStreet2") %></p>
                        <div class="more-info-button">
                            <a href="listingsInBuilding.aspx?buildingid=<%# (Container.DataItem as BuildingInfo).ID%>">
                                <img src="images/more_information.png" alt="" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ItemTemplate>
</asp:ListView>