﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApartmentFeatures.ascx.cs"
    Inherits="cntrls_Search_ApartmentFeatures" %>
<ul class="apartmentfeatures clearfix">
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Washer/Dryer", Model.ApartmentFeatures.Contains(ApartmentFeature.WasherDryer), new { @value = "WasherDryer" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Fireplace", Model.ApartmentFeatures.Contains(ApartmentFeature.Fireplace), new { @value = "Fireplace" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Outdoor Space", Model.ApartmentFeatures.Contains(ApartmentFeature.OutdoorSpace), new { @value = "OutdoorSpace" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "High Ceilings [9'+]", Model.ApartmentFeatures.Contains(ApartmentFeature.HighCeilings), new { @value = "HighCeilings" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Loft", Model.ApartmentFeatures.Contains(ApartmentFeature.Loft), new { @value = "Loft" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Chef's Kitchen", Model.ApartmentFeatures.Contains(ApartmentFeature.ChefsKitchen), new { @value = "ChefsKitchen" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "City View", Model.ApartmentFeatures.Contains(ApartmentFeature.CityView), new { @value = "CityView" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Central AC", Model.ApartmentFeatures.Contains(ApartmentFeature.CentralAir), new { @value = "CentralAir" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Park View", Model.ApartmentFeatures.Contains(ApartmentFeature.ParkView), new { @value = "ParkView" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Dining Room", Model.ApartmentFeatures.Contains(ApartmentFeature.DiningRoom), new { @value = "DiningRoom" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "River View", Model.ApartmentFeatures.Contains(ApartmentFeature.RiverView), new {@id="waterview", @value = "RiverView", @class="waterview" })%></li>
    <li class="nowrap">
        <%=Html.CheckBox("ApartmentFeatures", "Stainless Steel Appls", Model.ApartmentFeatures.Contains(ApartmentFeature.StainlessSteelAppliances), new { @value = "StainlessSteelAppliances" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Duplex/Triplex", Model.ApartmentFeatures.Contains(ApartmentFeature.Duplex) || Model.ApartmentFeatures.Contains(ApartmentFeature.Triplex) || Model.ApartmentFeatures.Contains(ApartmentFeature.Quadruplex), new { @value = "1,2,4096" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "New Appliances", Model.ApartmentFeatures.Contains(ApartmentFeature.NewAppliances), new { @value = "NewAppliances" })%></li>

<%--    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Washer/Dryer", Model.ApartmentFeatures.Contains(ApartmentFeature.WasherDryer), new { @value = "WasherDryer" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Fireplace", Model.ApartmentFeatures.Contains(ApartmentFeature.Fireplace), new { @value = "Fireplace" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Outdoor Space", Model.ApartmentFeatures.Contains(ApartmentFeature.OutdoorSpace), new { @value = "OutdoorSpace" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "High Ceilings [9'+]", Model.ApartmentFeatures.Contains(ApartmentFeature.HighCeilings), new { @value = "HighCeilings" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Loft", Model.ApartmentFeatures.Contains(ApartmentFeature.Loft), new { @value = "Loft" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Chef's Kitchen", Model.ApartmentFeatures.Contains(ApartmentFeature.ChefsKitchen), new { @value = "ChefsKitchen" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "New Appliances", Model.ApartmentFeatures.Contains(ApartmentFeature.NewAppliances), new { @value = "NewAppliances" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Central AC", Model.ApartmentFeatures.Contains(ApartmentFeature.CentralAir), new { @value = "CentralAir" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "City View", Model.ApartmentFeatures.Contains(ApartmentFeature.CityView), new { @value = "CityView" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Dining Room", Model.ApartmentFeatures.Contains(ApartmentFeature.DiningRoom), new { @value = "DiningRoom" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Park View", Model.ApartmentFeatures.Contains(ApartmentFeature.ParkView), new { @value = "ParkView" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Duplex/Triplex", Model.ApartmentFeatures.Contains(ApartmentFeature.Duplex) || Model.ApartmentFeatures.Contains(ApartmentFeature.Triplex) || Model.ApartmentFeatures.Contains(ApartmentFeature.Quadruplex), new { @value = "1,2,4096" })%></li>
   <li>
        <%=Html.CheckBox("ApartmentFeatures", "River View", Model.ApartmentFeatures.Contains(ApartmentFeature.RiverView), new {@id="waterview", @value = "RiverView", @class="waterview" })%></li>
    <li class="nowrap">
        <%=Html.CheckBox("ApartmentFeatures", "Stainless Steel Appls", Model.ApartmentFeatures.Contains(ApartmentFeature.StainlessSteelAppliances), new { @value = "StainlessSteelAppliances" })%></li>
--%></ul>
