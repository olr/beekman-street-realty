﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Neighborhood_Manhattan_Medium.ascx.cs" Inherits="cntrls_Search_Neighborhood_Manhattan_Medium" %>
<script type="text/javascript">
    $(function () {
        $('.all-checkbox').click(function () {
            var IsChecked = $(this).attr('checked');
            $('input.manhattan').attr('checked', IsChecked);
        });
        $('input.manhattan').click(function () {
            var IsChecked = $(this).attr('checked');
            if ($('.all-checkbox').attr('checked'))
                $('.all-checkbox').attr('checked', IsChecked);
            else {
                var allChecked = true;
                $('input.manhattan').each(function () {
                    if (!$(this).attr('checked'))
                        allChecked = false;
                });
                if (allChecked)
                    $('.all-checkbox').attr('checked', IsChecked);
            }
        });
    });
   
</script>
<div class="section">
   <div style="margin-bottom:8px;" class="all-checkbox-title">
    <%=Html.CheckBox("Borough", "All Manhattan", Model.Boroughs.Contains(Borough.Manhattan), new { @value =(int) Borough.Manhattan, @class = "all-checkbox" })%>
    </div>
    <div class="clearfix">
        <div class="column neighborhood-col">
            <ul class="neighborhoods">
                <li><%=Html.CheckBox("Neighborhood", "Financial District", Model.Neighborhoods.Contains(Manhattan.FinancialDistrict, Manhattan.BatteryParkCity), new { @value = "3,12", @class = "financial-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Tribeca", Model.Neighborhoods.Contains(Manhattan.Tribeca), new { @value = (int)Manhattan.Tribeca, @class = "tribeca-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Soho", Model.Neighborhoods.Contains(Manhattan.Soho, Manhattan.LittleItaly), new { @value = "20,27", @class = "soho-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Lower East Side", Model.Neighborhoods.Contains(Manhattan.LowerEastSide, Manhattan.Chinatown), new { @value = "19,6", @class = "les-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "East Village", Model.Neighborhoods.Contains(Manhattan.E_GreenwichVillage), new { @value = "324", @class = "eastvillage-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Greenwich Village", Model.Neighborhoods.Contains(Manhattan.GreenwichVillage, Manhattan.W_GreenwichVillage, Manhattan.Noho), new { @value = "325,15,24", @class = "greenwichvillage-checkbox manhattan" })%></li>
            </ul>
        </div>
        <div class="column neighborhood-col">
            <ul class="neighborhoods">
                <li><%=Html.CheckBox("Neighborhood", "Gramercy Park", Model.Neighborhoods.Contains(Manhattan.GramercyPark, Manhattan.PeterCooperVillage, Manhattan.StuyvesantTown), new { @value = "14,42,41", @class = "gramercy-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Chelsea", Model.Neighborhoods.Contains(Manhattan.Chelsea, Manhattan.FlatironDistrict), new { @value = "9,13", @class = "chelsea-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Murray Hill", Model.Neighborhoods.Contains(Manhattan.MurrayHill, Manhattan.KipsBay), new { @value = "22,18", @class = "murrayhill-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Midtown East", Model.Neighborhoods.Contains(Manhattan.MidtownEast, Manhattan.RooseveltIsland, Manhattan.TudorCity, Manhattan.TurtleBay), new { @value = "21,26,29,30", @class = "midtowneast-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Sutton Place", Model.Neighborhoods.Contains(Manhattan.SuttonPlace, Manhattan.BeekmanPlace), new { @value = "2,28", @class = "suttonplace-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Clinton", Model.Neighborhoods.Contains(Manhattan.Clinton), new { @value = (int)Manhattan.Clinton, @class = "clinton-checkbox manhattan" })%></li>
            </ul>
        </div>
        <div class="column neighborhood-col">
            <ul class="neighborhoods">
                <li><%=Html.CheckBox("Neighborhood", "Midtown West", Model.Neighborhoods.Contains(Manhattan.MidtownWest, Manhattan.CentralParkSouth), new { @value = "7,23", @class = "midtownwest-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Upper East Side", Model.Neighborhoods.Contains(Manhattan.UpperEastSide, Manhattan.CarnegieHill), new { @value = "32,4", @class = "uppereastside-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Upper West Side", Model.Neighborhoods.Contains(Manhattan.UpperWestSide, Manhattan.CentralParkWest, Manhattan.MorningsideHeights), new { @value = "8,17,33", @class = "upperwestside-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Harlem", Model.Neighborhoods.Contains(Manhattan.WestHarlem, Manhattan.EastHarlem, Manhattan.MtMorrisPark, Manhattan.HamiltonHeights), new { @value = "10,34,321,58", @class = "harlem-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Upper Manhattan", Model.Neighborhoods.Contains(Manhattan.HudsonHeights, Manhattan.Inwood, Manhattan.WashingtonHeights), new { @value = "59,128,43", @class = "uppermanhattan-checkbox manhattan" })%></li>
            </ul>
        </div>
        <div class="column neighborhood-col">
            <ul class="neighborhoods">
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226;</li>
                <li><%=Html.CheckBox("Borough", "All Brooklyn", Model.Boroughs.Contains(Borough.Brooklyn), new { @value = (int)Borough.Brooklyn, @class = "brooklyn-checkbox" })%></li>
                <li><%=Html.CheckBox("Borough", "All Queens", Model.Boroughs.Contains(Borough.Queens), new { @value = (int)Borough.Queens, @class = "queens-checkbox" })%></li>
                <li><%=Html.CheckBox("Borough", "All Bronx", Model.Boroughs.Contains(Borough.Bronx), new { @value = (int)Borough.Bronx, @class = "bronx-checkbox" })%></li>

                <li><%=Html.CheckBox("Borough", "Westchester", Model.Boroughs.Contains(Borough.OutOfTown), new { @value = "0", @class = "Out-of-town-checkbox" })%></li>
            </ul>
        </div>
    </div>
</div>