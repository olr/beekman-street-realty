﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RefineSearch.ascx.cs" Inherits="cntrls_RefineSearch" %>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.hovermap.js"></script>
<script type="text/javascript">
    Cufon.replace('#moreoptions');
</script>
<script type="text/javascript">
    function toggleAdvance() {
        $('#more-search-options').toggle();
        if ($('#more-search-options').is(':visible'))
            //$('#moreoptions').html('<img src="images/fewer-options.png" />');
            $('#moreoptions').text('Fewer Options');
        else
            //$('#moreoptions').html('<img src="images/more-options.png" />');
            $('#moreoptions').text('More Options');
        Cufon.replace('#moreoptions');
    }
</script>

<div class="refine-search-container corners">
    <div class="padding-container">
        <form id="searchForm" method="post" action="result.aspx">
        <input type="hidden" name="PropertyType" value="<%=Model.PropertyTypes.Contains(PropertyType.Rental)?"Rental":"Sale"%>" />    
        <div class="clearfix refine-search-options">
            <div class="column refine-label">
                Refine Search
            </div>
            <div class="column search-size">
                <%=this.RenderPartial("cntrls/Search/ApartmentSize", Model)%>
            </div>
            <div class="column search-price">
                <%=Html.DropDownList("MinPrice", SiteHelper.GetMinPriceList(Model), Model.MinPrice.ToString(), new { @class="column minprice" } )%>    
                <%=Html.DropDownList("MaxPrice", SiteHelper.GetMaxPriceList(Model), Model.MaxPrice.ToString(), new { @class = "column maxprice" })%>    
            </div>
                        <div class="column more-button more-options-button medium-button button-image">
                <a id="moreoptions" href="javascript:toggleAdvance()">More Options<%--<img src="images/more-options.png" />--%></a>
            </div>
            <div class="column refine-search-button short-button button-image"><span class="font">Refine</span>
                <input id="submitSearch" class="gobutton" type="image" src="images/clear.png" value="Refine Search" />
            </div>

        </div>
        <div id="more-search-options" style="display:none;">
            <div class="neighborhood neighborhood_space">
            <br>
                <div class="page-titles">Location</div>
                <%=this.RenderPartial("cntrls/Search/Neighborhood_Manhattan_Medium", Model)%>
                <div class="ownership">
                    <%=this.RenderPartial("cntrls/Search/Ownership", Model)%>
                </div>
            </div>
            <div class="horizontal_spacer">
            </div>
            <div class="clearfix">
                <div class="apt-features span-7">
                                    <div class="page-titles">
                    Apartment Features</div>
                     <div id="dynamic-nofee" class="bldg-features span-25">
                    <%=Html.RenderPartial("cntrls/Search/NoFee", Model)%>
                </div>
                    <%=this.RenderPartial("cntrls/Search/ApartmentFeatures", Model)%>
                </div>
                <div class="bldg-features span-7">
                   
                    <%=this.RenderPartial("cntrls/Search/BuildingFeatures", Model)%>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>