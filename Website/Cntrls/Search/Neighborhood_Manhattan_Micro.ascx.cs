﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;

public partial class cntrls_Search_Neighborhood_Manhattan_Micro : BaseControl<cntrls_Search_Neighborhood_Manhattan_Micro, SearchOptions>,
    IRenderable<SearchOptions>
{
    public void DataBind(SearchOptions data)
    {
        Model = data;
    }
}