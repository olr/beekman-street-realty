﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;

public partial class cntrls_Search_CommercialForm : BaseControl<cntrls_Search_CommercialForm, SearchOptions>, IRenderable<SearchOptions>
{
    public void DataBind(SearchOptions data)
    {
        Model = data ?? new SearchOptions();
    }
}