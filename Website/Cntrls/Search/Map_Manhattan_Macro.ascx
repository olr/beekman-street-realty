﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Map_Manhattan_Macro.ascx.cs" Inherits="cntrls_Search_Map" %>

<script src="js/jquery.tooltip.js" type="text/javascript"></script>
<script src="js/jquery.metadata.js" type="text/javascript"></script>
<script src="js/jquery.hovermap.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    $(function () {
        $('.map-container').hovermap({ width: 254, height: 507, backgroundImage: 'images/map-macro.png' });
        $('.map-container map *').tooltip({ positionLeft: true, track: true });
    });
</script>
<div class="map-container">
        <img src="images/map-macro/clear.png" border="0" width="549" height="486" usemap="#manhattan" alt="" class="map" />
        <map name="manhattan" id="manhattan-map">
        <area href="#" id="downtown" shape="poly" alt="Downtown"
                coords="45,357,78,371,76,376,90,387,87,391,108,407,115,423,105,469,78,473,62,478,34,497,20,485,26,455,31,450,37,429,32,424,43,358" 
                class="{hoverImage:'images/map-macro/downtown.png',selectedImage:'images/map/downtown-selected.png',left:19,top:352}" />
        <area href="#" id="midtownwest" shape="poly" alt="Midtown West"
                coords="69,311,107,329,85,376,75,375,78,370,46,352,68,310" 
                class="{hoverImage:'images/map-macro/midtownwest.png',selectedImage:'images/map/midtownwest-selected.png',left:43,top:305}" />
        <area href="#" id="midtowneast" shape="poly" alt="Midtown East"
                coords="110,335,134,346,138,366,160,328,171,310,177,309,155,343,140,366,132,349,112,387,111,401,102,401,91,394,94,386,86,379,109,336" 
                class="{hoverImage:'images/map-macro/midtowneast.png',selectedImage:'images/map/midtowneast-selected.png',left:82,top:304}" />
        <area href="#" id="upper-west-side" shape="poly" alt="Upper West Side"
                coords="133,187,147,203,142,211,144,216,90,315,69,309,129,188"
                class="{hoverImage:'images/map-macro/uws.png',selectedImage:'images/map/uws-selected.png',left:67,top:180}" />
        <area href="#" id="upper-east-side" shape="poly" alt="Upper East Side"
               coords="114,328,137,342,170,296,169,285,164,278,157,271,156,266,155,264,158,246,155,245,114,324"
                class="{hoverImage:'images/map-macro/ues.png',selectedImage:'images/map/ues-selected.png',left:107,top:240}" />
        <area href="#" id="upper-manhattan" shape="poly" alt="Upper Manhattan"
                coords="189,56,187,49,195,32,207,31,206,42,218,40,224,39,229,53,213,76,208,71,208,78,183,139,184,177,185,211,194,224,197,245,180,260,178,269,171,276,159,270,162,264,156,259,163,247,143,232,147,217,144,212,152,207,137,180,137,170,148,163,153,150,160,118,158,106,165,100,187,57"
                class="{hoverImage:'images/map-macro/uppermanhattan.png',selectedImage:'images/map/uppermanhattan-selected.png',left:135,top:29}" />
        <area href="#" id="brooklyn" shape="poly" alt="Brooklyn"
               coords="58,506,67,492,82,484,102,482,115,497,122,493,120,476,123,461,138,434,135,412,142,401,161,396,177,407,193,422,205,425,212,448,199,461,200,469,202,464,205,457,215,452,253,474,251,511,59,505" 
               class="{hoverImage:'images/map-macro/brooklyn.png',selectedImage:'images/map/brooklyn-selected.png',left:34,top:393}" />
        <area href="#" id="queens" shape="poly" alt="Queens"
               coords="253,471,218,453,213,440,221,435,214,434,203,421,187,419,182,404,152,391,135,395,136,387,166,333,175,320,187,310,183,306,186,293,193,291,199,296,223,274,236,258,243,259,253,261,253,469" 
               class="{hoverImage:'images/map-macro/queens.png',selectedImage:'images/map/queens-selected.png',left:134,top:256}" />
        <area href="#" id="bronx" shape="poly" alt="Bronx"
               coords="224,4,206,24,221,33,234,41,234,56,225,78,210,95,195,129,188,162,192,176,192,202,192,211,199,226,211,227,218,231,233,241,252,218,251,4,224,4" 
               class="{hoverImage:'images/map-macro/bronx.png',selectedImage:'images/map/bronx-selected.png',left:189,top:0}" />
        </map>
</div>

