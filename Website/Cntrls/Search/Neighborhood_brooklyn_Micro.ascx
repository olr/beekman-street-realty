﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Neighborhood_brooklyn_Micro.ascx.cs"
    Inherits="cntrls_Search_Neighborhood" %>
<script type="text/javascript">
    $(function () {
        $('.brooklyn-all-checkbox').removeAttr('checked');
        $('.brooklyn-all-checkbox').click(function () {
            var IsChecked = $(this).attr('checked');
            $('input.brooklyn').attr('checked', IsChecked);
        }); //waterview

        $('.brooklyn').click(function () {
            var IsChecked = $(this).attr('checked');
            if ($('.brooklyn-all-checkbox').attr('checked')) {
                $('.brooklyn-all-checkbox').attr('checked', IsChecked);
            }
            else {
                var allChecked = true;
                $('input.brooklyn').each(function () {
                    if (!$(this).attr('checked'))
                        allChecked = false;
                });
                if (allChecked)
                    $('.brooklyn-all-checkbox').attr('checked', IsChecked);
            }
            if (IsChecked)
                $("input[value='RiverView']").next().text("Water View");
            else
                $("input[value='RiverView']").next().text("River View");
        });
    });
</script>
<div class="section">
    <%=Html.CheckBox("Borough", "All Brooklyn", Model.Boroughs.Contains(Borough.Brooklyn), new { @value = Neighborhood.Brooklyn, @class = "brooklyn-all-checkbox all-checkbox" })%>
    <div class="clearfix">
        <div class="column quarter">
            <ul class="neighborhoods">
                <li>
                    <%=Html.CheckBox("Neighborhood", "Bath Beach", (Model.Neighborhoods.Contains(Brooklyn.BathBeach) ), new { @value = (int)Brooklyn.BathBeach, @class = "bathbeach-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Bay Ridge", Model.Neighborhoods.Contains(Brooklyn.BayRidge), new { @value = (int)Brooklyn.BayRidge, @class = "bayridge-checkbox West brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Bedford-Stuyvesant ", Model.Neighborhoods.Contains(Brooklyn.BedfordStuyvesant), new { @value = (int)Brooklyn.BedfordStuyvesant, @class = "bedfordstuyvesant-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Bensonhurst ", Model.Neighborhoods.Contains(Brooklyn.Bensonhurst), new { @value = (int)Brooklyn.Bensonhurst, @class = "bensonhurst-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Bergen Beach ", Model.Neighborhoods.Contains(Brooklyn.BergenBeach), new { @value = (int)Brooklyn.BergenBeach, @class = "bergenbeach-checkbox East brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Boerum Hill ", Model.Neighborhoods.Contains(Brooklyn.BoerumHill), new { @value = (int)Brooklyn.BoerumHill, @class = "boerumhill-checkbox North brooklyn" })%></li>
                 <li>
                    <%=Html.CheckBox("Neighborhood", "Borough Park ", Model.Neighborhoods.Contains(Brooklyn.BoroughPark), new { @value = (int)Brooklyn.BoroughPark, @class = "boroughpark-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Brighton Beach ", Model.Neighborhoods.Contains(Brooklyn.BrightonBeach), new { @value = (int)Brooklyn.BrightonBeach, @class = "brightonbeach-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Brooklyn Heights", Model.Neighborhoods.Contains(Brooklyn.BrooklynHeights), new { @value = (int)Brooklyn.BrooklynHeights, @class = "brooklynheights-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Brownsville", Model.Neighborhoods.Contains(Brooklyn.Brownsville), new { @value = (int)Brooklyn.Brownsville, @class = "brownsville-checkbox East brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Bushwick", Model.Neighborhoods.Contains(Brooklyn.Bushwick), new { @value = (int)Brooklyn.Bushwick, @class = "bushwick-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Carroll Gardens", Model.Neighborhoods.Contains(Brooklyn.CarrollGardens), new { @value = (int)Brooklyn.CarrollGardens, @class = "carrollgardens-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "City Line", Model.Neighborhoods.Contains(Brooklyn.CityLine), new { @value = (int)Brooklyn.CityLine, @class = "cityline-checkbox East brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Clinton Hill", Model.Neighborhoods.Contains(Brooklyn.ClintonHill), new { @value = (int)Brooklyn.ClintonHill, @class = "clintonhill-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Cobble Hill", Model.Neighborhoods.Contains(Brooklyn.CobbleHill), new { @value = (int)Brooklyn.CobbleHill, @class = "cobblehill-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Coney Island", Model.Neighborhoods.Contains(Brooklyn.ConeyIsland), new { @value = (int)Brooklyn.ConeyIsland, @class = "coneyisland-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Crown Heights", Model.Neighborhoods.Contains(Brooklyn.CrownHeights), new { @value = (int)Brooklyn.CrownHeights, @class = "crownheights-checkbox East brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Cypress Hill", Model.Neighborhoods.Contains(Brooklyn.CypressHills), new { @value = (int)Brooklyn.CypressHills, @class = "cypresshill-checkbox East brooklyn" })%></li>
                
                <li>
                    <%=Html.CheckBox("Neighborhood", "Downtown Brooklyn", Model.Neighborhoods.Contains(Brooklyn.Downtown), new { @value = (int)Brooklyn.Downtown, @class = "downtownbrooklyn-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Dumbo", Model.Neighborhoods.Contains(Brooklyn.Dumbo), new { @value = (int)Brooklyn.Dumbo, @class = "dumbo-checkbox North brooklyn" })%></li>
            </ul>
        </div>
        <div class="column quarter">
            <ul class="neighborhoods">
                <li>
                    <%=Html.CheckBox("Neighborhood", "Dyker Heights", Model.Neighborhoods.Contains(Brooklyn.DykerHeights), new { @value = (int)Brooklyn.DykerHeights, @class = "dykerheights-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "East Flatbush", Model.Neighborhoods.Contains(Brooklyn.EastFlatbush), new { @value = (int)Brooklyn.EastFlatbush, @class = "eastflatbush-checkbox Central brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "East New Yrok", Model.Neighborhoods.Contains(Brooklyn.EastNewYork), new { @value = (int)Brooklyn.EastNewYork, @class = "eastnewyork-checkbox East brooklyn" })%></li>
                
                <li>
                    <%=Html.CheckBox("Neighborhood", "East Williamsburg", Model.Neighborhoods.Contains(Brooklyn.EastWilliamsburg), new { @value = (int)Brooklyn.EastWilliamsburg, @class = "eastwilliamsburg-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Farragut", Model.Neighborhoods.Contains(Brooklyn.Farragut), new { @value = (int)Brooklyn.Farragut, @class = "farragut-checkbox Central brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Flatbush", Model.Neighborhoods.Contains(Brooklyn.Flatbush), new { @value = (int)Brooklyn.Flatbush, @class = "flatbush-checkbox East brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Flatlands", Model.Neighborhoods.Contains(Brooklyn.Flatlands), new { @value = (int)Brooklyn.Flatlands, @class = "flatlands-checkbox East brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Fort Greene ", Model.Neighborhoods.Contains(Brooklyn.FortGreene), new { @value = (int)Brooklyn.FortGreene, @class = "fortgreene-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Fort Hamilton", Model.Neighborhoods.Contains(Brooklyn.FortHamilton), new { @value = (int)Brooklyn.FortHamilton, @class = "forthamilton-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Georgetown", Model.Neighborhoods.Contains(Brooklyn.Georgetown), new { @value = (int)Brooklyn.Georgetown, @class = "georgetown-checkbox East brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Gerritsen Beach", Model.Neighborhoods.Contains(Brooklyn.GerritsenBeach), new { @value = (int)Brooklyn.GerritsenBeach, @class = "gerritsenbeach-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Gowanus", Model.Neighborhoods.Contains(Brooklyn.Gowanus), new { @value = (int)Brooklyn.Gowanus, @class = "gowanus-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Gravesend", Model.Neighborhoods.Contains(Brooklyn.Gravesend), new { @value = (int)Brooklyn.Gravesend, @class = "gravesend-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Greenpoint", Model.Neighborhoods.Contains(Brooklyn.Greenpoint), new { @value = (int)Brooklyn.Greenpoint, @class = "greenpoint-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Highland Park", Model.Neighborhoods.Contains(Brooklyn.HighlandPark), new { @value = (int)Brooklyn.HighlandPark, @class = "highlandpark-checkbox East brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Homecrest", Model.Neighborhoods.Contains(Brooklyn.Homecrest), new { @value = (int)Brooklyn.Homecrest, @class = "homecrest-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Kensington", Model.Neighborhoods.Contains(Brooklyn.Kensington), new { @value = (int)Brooklyn.Kensington, @class = "kensington-checkbox West brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Lefferts Gardens", Model.Neighborhoods.Contains(Brooklyn.ProspectLeffertsGardens), new { @value = (int)Brooklyn.ProspectLeffertsGardens, @class = "leffertsgardens-checkbox Central brooklyn" })%></li>
                
                <li>
                    <%=Html.CheckBox("Neighborhood", "Madison", Model.Neighborhoods.Contains(Brooklyn.Madison), new { @value = (int)Brooklyn.Madison, @class = "madison-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Manhattan Beach", Model.Neighborhoods.Contains(Brooklyn.ManhattanBeach), new { @value = (int)Brooklyn.ManhattanBeach, @class = "manhattanbeach-checkbox South brooklyn" })%></li>
                
            </ul>
        </div>
        <div class="column last">
            <% //need 27% because "Prospect-Lefferts Grdn" does not fit @ 25% %>
            <ul class="neighborhoods">
                <li>
                    <%=Html.CheckBox("Neighborhood", "Marine Park", Model.Neighborhoods.Contains(Brooklyn.MarinePark), new { @value = (int)Brooklyn.MarinePark, @class = "marinepark-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Midwood", Model.Neighborhoods.Contains(Brooklyn.Midwood), new { @value = (int)Brooklyn.Midwood, @class = "midwood-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Mill Basin", Model.Neighborhoods.Contains(Brooklyn.MillBasin), new { @value = (int)Brooklyn.MillBasin, @class = "millbasin-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "New Lots", Model.Neighborhoods.Contains(Brooklyn.NewLots), new { @value = (int)Brooklyn.NewLots, @class = "newlots-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Ocean Hill", Model.Neighborhoods.Contains(Brooklyn.OceanHill), new { @value = (int)Brooklyn.OceanHill, @class = "oceanhill-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Old Mill Basin", Model.Neighborhoods.Contains(Brooklyn.OldMillBasin), new { @value = (int)Brooklyn.OldMillBasin, @class = "oldmillbasin-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Paedergat Basin", Model.Neighborhoods.Contains(Brooklyn.PaerdegatBasin), new { @value = (int)Brooklyn.PaerdegatBasin, @class = "paedergatbasin-checkbox East brooklyn" })%></li>
                
                <li>
                    <%=Html.CheckBox("Neighborhood", "Park Slope", Model.Neighborhoods.Contains(Brooklyn.ParkSlope), new { @value = (int)Brooklyn.ParkSlope, @class = "parkslope-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Prospect Heights ", Model.Neighborhoods.Contains(Brooklyn.ProspectHeights), new { @value = (int)Brooklyn.ProspectHeights, @class = "prospectheights-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Red Hook", Model.Neighborhoods.Contains(Brooklyn.RedHook), new { @value = (int)Brooklyn.RedHook, @class = "redhook-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Remsen Village", Model.Neighborhoods.Contains(Brooklyn.RemsenVillage), new { @value = (int)Brooklyn.RemsenVillage, @class = "remsenvillage-checkbox Central brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Sea Gate", Model.Neighborhoods.Contains(Brooklyn.SeaGate), new { @value = (int)Brooklyn.SeaGate, @class = "seagate-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Sheepshead Bay", Model.Neighborhoods.Contains(Brooklyn.SheepsheadBay), new { @value = (int)Brooklyn.SheepsheadBay, @class = "sheepsheadbay-checkbox South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Spring Creek", Model.Neighborhoods.Contains(Brooklyn.SpringCreek), new { @value = (int)Brooklyn.SpringCreek, @class = "springcreek-checkbox East brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Starrett City", Model.Neighborhoods.Contains(Brooklyn.StarrettCity), new { @value = (int)Brooklyn.StarrettCity, @class = "starrettcity-checkbox East brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Sunset Park", Model.Neighborhoods.Contains(Brooklyn.SunsetPark), new { @value = (int)Brooklyn.SunsetPark, @class = "sunsetpark-checkbox West brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Williamsburg", Model.Neighborhoods.Contains(Brooklyn.Williamsburg), new { @value = (int)Brooklyn.Williamsburg, @class = "williamsburg-checkbox North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Windsor Terrace", Model.Neighborhoods.Contains(Brooklyn.WindsorTerrace), new { @value = (int)Brooklyn.WindsorTerrace, @class = "windsorterrace-checkbox West brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Canarsie", Model.Neighborhoods.Contains(Brooklyn.Canarsie), new { @value = (int)Brooklyn.Canarsie, @class = "canarsie-checkbox East brooklyn" })%></li>
               <%-- <li>
                    <%=Html.CheckBox("Neighborhood", "Other Brooklyn", Model.Neighborhoods.Contains(Neighborhood.GarfieldOtherBrooklyn), new { @value = Neighborhood.GarfieldOtherBrooklyn, @class = "Other brooklyn" })%></li>--%>
            </ul>
        </div>
    </div>
</div>
