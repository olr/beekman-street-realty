﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class Cntrls_Search_Neighborhood_Brooklyn_Macro : BaseControl<Cntrls_Search_Neighborhood_Brooklyn_Macro, SearchOptions>, IRenderable<SearchOptions>
{
    protected SearchOptions Model;
    public void DataBind(SearchOptions data)
    {
        Model = data ?? new SearchOptions();
    }
}