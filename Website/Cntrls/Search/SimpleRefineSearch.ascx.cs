﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class Cntrls_Search_SimpleRefineSearch : BaseControl<Cntrls_Search_SimpleRefineSearch, string>, IRenderable<string>
{
    protected SearchOptions options;
    protected string type;
    protected string neighborhoods;
    public SearchOptions getSearchOptions()
    {
        SearchOptions op = new SearchOptions();
        var xml = XElement.Load(Server.MapPath("~/App_Data/NeighborhoodData.xml"));
        var node = xml.Elements("neighborhood").Where(x => x.Attribute("name").Value == Model).FirstOrDefault();
        neighborhoods = node.Attribute("value").Value;
        //Response.Write(value);
        op = new SearchOptions();
        
        if (Request.QueryString["searchtype"] == "rent" || Request.QueryString["searchtype"] == "rental")
        {
            op.PropertyTypes.Add(PropertyType.Rental);
            type = "rent";
        }
        else
        {
            op.PropertyTypes.Add(PropertyType.Sale);
            type = "sale";
        }
        return op;
    }
    public void DataBind(string data)
    {
        if (data != null)
        {
            Model = data;
            options = getSearchOptions();
            //this.Controls.DataBind<SearchOptions>(options);

        }
    }
}