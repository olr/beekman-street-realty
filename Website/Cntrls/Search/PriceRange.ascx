﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PriceRange.ascx.cs" Inherits="cntrls_Search_PriceRange" %>
 <script type="text/javascript">
     Cufon.replace('.page-titles');
    </script>
<% if (Model.PropertyTypes.Contains(PropertyType.Sale))
   { %>
<div class="page-titles">Price Range</div>
<% } else { %>
<div class="page-titles">Rent Range</div>
<% } %>

<div class="clearfix">
    <%=Html.DropDownList("MinPrice", SiteHelper.GetMinPriceList(Model), Model.MinPrice.ToString(), new { @class="column minprice" } )%>    
    <%=Html.DropDownList("MaxPrice", SiteHelper.GetMaxPriceList(Model), Model.MaxPrice.ToString(), new { @class = "column maxprice" })%>    
</div>
