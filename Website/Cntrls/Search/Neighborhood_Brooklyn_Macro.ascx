﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Neighborhood_Brooklyn_Macro.ascx.cs" Inherits="Cntrls_Search_Neighborhood_Brooklyn_Macro" %>
<script type="text/javascript">
    $(function () {
        $('.brooklyn-all-checkbox').click(function () {
            var IsChecked = $(this).attr('checked');
            $('input.brooklyn').attr('checked', IsChecked);
        }); //waterview

        $('.brooklyn').click(function () {
            var IsChecked = $(this).attr('checked');
            if ($('.brooklyn-all-checkbox').attr('checked')) {
                $('.brooklyn-all-checkbox').attr('checked', IsChecked);
            }
            else {
                var allChecked = true;
                $('input.brooklyn').each(function () {
                    if (!$(this).attr('checked'))
                        allChecked = false;
                });
                if (allChecked)
                    $('.brooklyn-all-checkbox').attr('checked', IsChecked);
            }
            if (IsChecked)
                $("input[value='RiverView']").next().text("Water View");
            else
                $("input[value='RiverView']").next().text("River View");
        });
    });
</script>
<div class="section">
    <%=Html.CheckBox("Borough", "All Brooklyn",Model.Boroughs.Contains(Borough.Brooklyn), new { @value = Borough.Brooklyn, @class = "brooklyn-all-checkbox all-checkbox" })%>
    <div class="clearfix">
        <div class="column quarter">
            <ul class="neighborhoods">
                <li>
                    <%=Html.CheckBox("Neighborhood", "North", Model.Neighborhoods.Contains(Neighborhood.North), new { @value = Neighborhood.North, @class = "north-checkbox brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "North West", Model.Neighborhoods.Contains(Neighborhood.NorthWest), new { @value = Neighborhood.NorthWest, @class = "northwest-checkbox brooklyn" })%></li>
                    <li>
                    <%=Html.CheckBox("Neighborhood", "West", Model.Neighborhoods.Contains(Neighborhood.West), new { @value = Neighborhood.West, @class = "west-checkbox brooklyn" })%></li>
                    <li>
                    <%=Html.CheckBox("Neighborhood", "South West", Model.Neighborhoods.Contains(Neighborhood.SouthWest), new { @value = Neighborhood.SouthWest, @class = "southwest-checkbox brooklyn" })%></li>
                    <li>
                    <%=Html.CheckBox("Neighborhood", "South", Model.Neighborhoods.Contains(Neighborhood.South), new { @value = Neighborhood.South, @class = "south-checkbox brooklyn" })%></li>
                    <li>
                    <%=Html.CheckBox("Neighborhood", "Central", Model.Neighborhoods.Contains(Neighborhood.Central), new { @value = Neighborhood.Central, @class = "central-checkbox brooklyn" })%></li>
                    <li>
                    <%=Html.CheckBox("Neighborhood", "East", Model.Neighborhoods.Contains(Neighborhood.East), new { @value = Neighborhood.East, @class = "east-checkbox brooklyn" })%></li>
                    <li>
                    <%=Html.CheckBox("Neighborhood", "South East", Model.Neighborhoods.Contains(Neighborhood.SouthEast), new { @value = Neighborhood.SouthEast, @class = "southeast-checkbox brooklyn" })%></li>
            </ul>
        </div>
<%--        <div class="column quarter">
            <ul class="neighborhoods">
                <li>
                    <%=Html.CheckBox("Neighborhood", "Crown Heights", Model.Neighborhoods.Contains(Brooklyn.CrownHeights), new { @value = (int)Brooklyn.CrownHeights, @class = "East brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Ditmas Park", Model.Neighborhoods.Contains(Brooklyn.DitmasPark), new { @value = (int)Brooklyn.DitmasPark, @class = "South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Downtown Brooklyn", Model.Neighborhoods.Contains(Brooklyn.Downtown), new { @value = (int)Brooklyn.Downtown, @class = "North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Dumbo/Vinegar Hill", Model.Neighborhoods.Contains(Brooklyn.Dumbo, Brooklyn.VinegarHill), new { @value = string.Join(",", new int[] { (int)Brooklyn.Dumbo, (int)Brooklyn.VinegarHill }), @class = "North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "East Williamsburg", Model.Neighborhoods.Contains(Brooklyn.EastWilliamsburg), new { @value = (int)Brooklyn.EastWilliamsburg, @class = "North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Flatbush", Model.Neighborhoods.Contains(Brooklyn.Flatbush), new { @value = (int)Brooklyn.Flatbush, @class = "East brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Fort Greene ", Model.Neighborhoods.Contains(Brooklyn.FortGreene), new { @value = (int)Brooklyn.FortGreene, @class = "North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Gowanus", Model.Neighborhoods.Contains(Brooklyn.Gowanus), new { @value = (int)Brooklyn.Gowanus, @class = "North brooklyn" })%></li>
            </ul>
        </div>
        <div class="column last" style="width: 27%;">
            <ul class="neighborhoods">
                <li>
                    <%=Html.CheckBox("Neighborhood", "Greenpoint", Model.Neighborhoods.Contains(Brooklyn.Greenpoint), new { @value = (int)Brooklyn.Greenpoint, @class = "North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Greenwood", Model.Neighborhoods.Contains(Brooklyn.GreenwoodHeights), new { @value = (int)Brooklyn.GreenwoodHeights, @class = "West brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Kensington", Model.Neighborhoods.Contains(Brooklyn.Kensington), new { @value = (int)Brooklyn.Kensington, @class = "West brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Midwood", Model.Neighborhoods.Contains(Brooklyn.Midwood), new { @value = (int)Brooklyn.Midwood, @class = "South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Park Slope", Model.Neighborhoods.Contains(Brooklyn.ParkSlope), new { @value = (int)Brooklyn.ParkSlope, @class = "North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Prospect Heights ", Model.Neighborhoods.Contains(Brooklyn.ProspectHeights), new { @value = (int)Brooklyn.ProspectHeights, @class = "North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Prospect Park South ", Model.Neighborhoods.Contains(Brooklyn.ProspectParkSouth), new { @value = (int)Brooklyn.ProspectParkSouth, @class = "South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Prospect-Lefferts Grdn", Model.Neighborhoods.Contains(Brooklyn.ProspectLeffertsGardens), new { @value = (int)Brooklyn.ProspectLeffertsGardens, @class = "East brooklyn" })%></li>
            </ul>
        </div>
        <div class="column last" style="width: 22%;">
            <ul class="neighborhoods">
                <li>
                    <%=Html.CheckBox("Neighborhood", "Red Hook", Model.Neighborhoods.Contains(Brooklyn.RedHook), new { @value = (int)Brooklyn.RedHook, @class = "North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Sheepshead Bay", Model.Neighborhoods.Contains(Brooklyn.SheepsheadBay), new { @value = (int)Brooklyn.SheepsheadBay, @class = "South brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Sunset Park", Model.Neighborhoods.Contains(Brooklyn.SunsetPark), new { @value = (int)Brooklyn.SunsetPark, @class = "West brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Williamsburg", Model.Neighborhoods.Contains(Brooklyn.Williamsburg), new { @value = (int)Brooklyn.Williamsburg, @class = "North brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Windsor Terrace", Model.Neighborhoods.Contains(Brooklyn.WindsorTerrace), new { @value = (int)Brooklyn.WindsorTerrace, @class = "West brooklyn" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Other Brooklyn", Model.Neighborhoods.Contains(Neighborhood.GarfieldOtherBrooklyn), new { @value = Neighborhood.GarfieldOtherBrooklyn, @class = "Other brooklyn" })%></li>
            </ul>
        </div>--%>
    </div>
</div>

