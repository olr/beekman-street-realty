﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Neighborhood_Queens_Macro.ascx.cs" Inherits="cntrls_Search_Neighborhood_Queens_Macro" %>
<script type="text/javascript">
    $(function () {
        $('.all-checkbox').click(function () {
            var IsChecked = $(this).attr('checked');
            $('input.manhattan').attr('checked', IsChecked);
        });
        $('input.manhattan').click(function () {
            var IsChecked = $(this).attr('checked');
            if ($('.all-checkbox').attr('checked'))
                $('.all-checkbox').attr('checked', IsChecked);
            else {
                var allChecked = true;
                $('input.manhattan').each(function () {
                    if (!$(this).attr('checked'))
                        allChecked = false;
                });
                if (allChecked)
                    $('.all-checkbox').attr('checked', IsChecked);
            }
        });
    });
   
</script>
<div class="section">
   <div style="margin-bottom:8px;">
    <%=Html.CheckBox("Borough", "All Queens", Model.Boroughs.Contains(Borough.Manhattan), new { @value = Neighborhood.Manhattan, @class = "all-checkbox" })%>
    </div>
    <div class="clearfix">
        <div class="column neighborhood-col">
            <ul class="neighborhoods">
                <li><%=Html.CheckBox("Neighborhood", "Area 1", Model.Neighborhoods.Contains(Manhattan.FinancialDistrict, Manhattan.BatteryParkCity), new { @value = "3,12", @class = "area1-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 2", Model.Neighborhoods.Contains(Manhattan.Tribeca), new { @value =  (int)Manhattan.Tribeca, @class = "area2-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 3", Model.Neighborhoods.Contains(Manhattan.Soho, Manhattan.LittleItaly), new { @value = "20,27", @class = "area3-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 4", Model.Neighborhoods.Contains(Manhattan.LowerEastSide, Manhattan.Chinatown), new { @value = "19,6", @class = "area4-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 5", Model.Neighborhoods.Contains(Manhattan.E_GreenwichVillage), new { @value = "324", @class = "area5-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 6", Model.Neighborhoods.Contains(Manhattan.GreenwichVillage, Manhattan.W_GreenwichVillage, Manhattan.Noho), new { @value = "325,15,24", @class = "area6-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 7", Model.Neighborhoods.Contains(Manhattan.GramercyPark, Manhattan.PeterCooperVillage, Manhattan.StuyvesantTown), new { @value = "14,42,41", @class = "area7-checkbox manhattan" })%></li>                
                <li><%=Html.CheckBox("Neighborhood", "Area 8", Model.Neighborhoods.Contains(Manhattan.SuttonPlace, Manhattan.BeekmanPlace), new { @value = "2,28", @class = "area8-checkbox manhattan" })%></li>
            </ul>
        </div>
        <div class="column neighborhood-col">
            <ul class="neighborhoods">
                <li><%=Html.CheckBox("Neighborhood", "Area 9", Model.Neighborhoods.Contains(Manhattan.MurrayHill, Manhattan.KipsBay), new { @value = "22,18", @class = "area9-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 10", Model.Neighborhoods.Contains(Manhattan.MidtownEast, Manhattan.RooseveltIsland, Manhattan.TudorCity, Manhattan.TurtleBay), new { @value = "21,26,29,30", @class = "area10-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 11", Model.Neighborhoods.Contains(Manhattan.Clinton), new { @value = (int)Manhattan.Clinton, @class = "area11-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 12", Model.Neighborhoods.Contains(Manhattan.MidtownWest, Manhattan.CentralParkSouth), new { @value = "7,23", @class = "area12-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 13", Model.Neighborhoods.Contains(Manhattan.UpperEastSide, Manhattan.CarnegieHill), new { @value = "32,4", @class = "area13-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 14", Model.Neighborhoods.Contains(Manhattan.UpperWestSide, Manhattan.CentralParkWest, Manhattan.MorningsideHeights), new { @value = "8,17,33", @class = "area14-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 15", Model.Neighborhoods.Contains(Manhattan.UpperWestSide, Manhattan.CentralParkWest, Manhattan.MorningsideHeights), new { @value = "8,17,33", @class = "area15-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 16", Model.Neighborhoods.Contains(Manhattan.UpperWestSide, Manhattan.CentralParkWest, Manhattan.MorningsideHeights), new { @value = "8,17,33", @class = "area16-checkbox manhattan" })%></li>
            </ul>
        </div>
        <div class="column neighborhood-col">
            <ul class="neighborhoods">
                <li><%=Html.CheckBox("Neighborhood", "Area 17", Model.Neighborhoods.Contains(Manhattan.UpperWestSide, Manhattan.CentralParkWest, Manhattan.MorningsideHeights), new { @value = "8,17,33", @class = "area17-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 18", Model.Neighborhoods.Contains(Manhattan.UpperWestSide, Manhattan.CentralParkWest, Manhattan.MorningsideHeights), new { @value = "8,17,33", @class = "area18-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 19", Model.Neighborhoods.Contains(Manhattan.UpperWestSide, Manhattan.CentralParkWest, Manhattan.MorningsideHeights), new { @value = "8,17,33", @class = "area19-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 20", Model.Neighborhoods.Contains(Manhattan.UpperWestSide, Manhattan.CentralParkWest, Manhattan.MorningsideHeights), new { @value = "8,17,33", @class = "area20-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Area 21", Model.Neighborhoods.Contains(Manhattan.UpperWestSide, Manhattan.CentralParkWest, Manhattan.MorningsideHeights), new { @value = "8,17,33", @class = "area21-checkbox manhattan" })%></li>

                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226;</li>
                <li><%=Html.CheckBox("Neighborhood", "Other Queens", Model.Boroughs.Contains(Borough.Queens), new
{
    @value = "177,158,191,176,189,142,192,193,151,178,196,174,197,162,149,198,181,175,140,131,143,171,153,132,150,194,137,195,46,199,166,167," +
                    "200,135,157,164,165,201,148,172,130,145,202,163,188,138,169,183,203,204,187,205,206,136,129,155,170,180,159,147,207,47,179,182,208,133,144,134,209,152,184,154,146,156,210,211,161,160,186,185,173,45,212,168,141,139,44,190,36",
                                                                                                  @class = "queens-checkbox"
})%></li>

                <%--<li><%=Html.CheckBox("Borough", "Out of Town", Model.Boroughs.Contains(Borough.OutOfTown), new { @value = "-100", @class = "Out-of-town-checkbox" })%></li>--%>
            </ul>
        </div>
    </div>
</div>