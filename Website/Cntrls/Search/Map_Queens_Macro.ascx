﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Map_Queens_Macro.ascx.cs" Inherits="cntrls_Search_Map_Queens_Macro" %>

<script src="js/jquery.tooltip.js" type="text/javascript"></script>
<script src="js/jquery.metadata.js" type="text/javascript"></script>
<script src="js/jquery.hovermap.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    $(function () {
        $('.map-container').hovermap({ width: 435, height: 563, backgroundImage: 'images/queens_macromap/map.png' });
        $('.map-container map *').tooltip({ positionLeft: true, track: true });
    });
</script>
<div class="map-container">
        <img src="images/clear.png" border="0" width="435" height="563" usemap="#queens" alt="" class="map" />
        <map name="queens" id="queens-map">
        <area href="#" id="area1" shape="poly" alt="Area 1"
                coords="83,83,86,74,102,57,106,55,116,64,114,60,116,58,125,66,122,70,114,74,114,78,116,78,117,74,122,73,130,80,124,84,130,93,135,93,136,97,138,102,128,105,120,121,121,129,104,130,102,134,97,132,88,136,85,133,76,133,67,138,63,138,45,127,51,110,63,99,67,96,60,90,65,85,65,81,69,82,76,84,78,82" 
                class="{hoverImage:'images/queens_macromap/area1.png',selectedImage:'images/queens_macromap/area1.png',left:44,top:56}" />
        <area href="#" id="area2" shape="poly" alt="Area 2"
                coords="42,128,61,139,67,141,76,136,83,135,88,139,97,135,101,137,105,133,123,132,137,166,121,167,101,190,93,192,87,187,80,187,75,181,68,181,63,178,59,170,41,159,33,159,28,162,24,161,25,155,30,153,28,149,31,143,34,144,35,136" 
                class="{hoverImage:'images/queens_macromap/area2.png',selectedImage:'images/queens_macromap/area2.png',left:24,top:128}" />
        <area href="#" id="area3" shape="poly" alt="Area 3"
                coords="128,85,130,83,135,89,143,89,145,81,161,75,187,91,182,100,177,100,171,94,168,95,160,92,154,93,141,101,137,96,137,91,130,89" 
                class="{hoverImage:'images/queens_macromap/area3.png',selectedImage:'images/queens_macromap/area3.png',left:127,top:75}" />
        <area href="#" id="area4" shape="poly" alt="Area 4"
                coords="122,123,131,107,139,105,153,97,161,96,169,98,185,115,194,118,194,130,176,136,131,141" 
                class="{hoverImage:'images/queens_macromap/area4.png',selectedImage:'images/queens_macromap/area4.png',left:122,top:96}" />
        <area href="#" id="area5" shape="poly" alt="Area 5"
                coords="82,191,86,190,89,194,100,194,117,176,120,170,136,170,138,173,139,179,150,182,159,179,163,182,164,186,174,200,181,208,182,222,187,231,185,233,182,229,169,233,165,240,168,254,140,271,135,268,129,269,124,272,122,281,120,282,119,280,120,276,115,271,115,265,122,253,123,248,86,219,84,210,83,206,81,208,76,208,79,204,85,205,86,202" 
                class="{hoverImage:'images/queens_macromap/area5.png',selectedImage:'images/queens_macromap/area5.png',left:77,top:169}" />
        <area href="#" id="area6" shape="poly" alt="Area 6"
                coords="133,145,170,140,194,133,196,134,191,139,198,158,153,178,144,178,143,175" 
                class="{hoverImage:'images/queens_macromap/area6.png',selectedImage:'images/queens_macromap/area6.png',left:132,top:133}" />
        <area href="#" id="area7" shape="poly" alt="Area 7"
                coords="164,177,204,160,204,167,218,194,226,203,232,204,233,207,227,206,207,221,199,220,193,226,192,229,189,229,184,224,183,205,165,179" 
                class="{hoverImage:'images/queens_macromap/area7.png',selectedImage:'images/queens_macromap/area7.png',left:164,top:160}" />
        <area href="#" id="area8" shape="poly" alt="Area 8"
                coords="196,121,210,107,214,111,210,112,204,117,206,125,210,126,211,129,215,132,220,138,216,153,220,162,218,172,231,192,233,201,229,202,222,196,208,166,207,157,202,158,194,140,198,135" 
                class="{hoverImage:'images/queens_macromap/area8.png',selectedImage:'images/queens_macromap/area8.png',left:193,top:108}" />
        <area href="#" id="area9" shape="poly" alt="Area 9"
                coords="200,73,192,73,186,69,185,64,190,62,193,57,192,52,193,48,197,51,202,50,204,48,208,49,213,43,218,53,217,57,227,60,225,55,232,50,231,45,235,44,243,38,252,43,255,42,266,45,272,44,282,49,280,54,288,57,304,55,307,48,304,45,312,45,315,54,319,61,313,62,319,69,310,80,286,90,286,128,282,132,283,146,279,153,280,157,245,158,238,157,231,161,228,151,220,150,222,138,216,129,213,125,209,123,208,118,211,115,215,115,215,106,208,104,200,99,199,90" 
                class="{hoverImage:'images/queens_macromap/area9.png',selectedImage:'images/queens_macromap/area9.png',left:185,top:37}" />
        <area href="#" id="area10" shape="poly" alt="Area 10"
                coords="289,91,312,82,321,73,327,83,327,86,344,106,346,101,343,94,347,91,345,85,347,77,351,73,403,116,396,121,394,126,385,136,385,143,352,176,343,183,337,178,332,175,332,170,334,156,325,144,314,149,309,149,283,158,282,156,282,149,284,144,285,136,287,130,289,128" 
                class="{hoverImage:'images/queens_macromap/area10.png',selectedImage:'images/queens_macromap/area10.png',left:281,top:73}" />
        <area href="#" id="area11" shape="poly" alt="Area 11"
                coords="220,153,225,154,226,160,229,165,239,159,283,161,307,153,311,153,323,148,325,151,328,153,331,159,329,171,329,176,334,179,343,186,336,195,316,205,299,211,291,211,251,228,245,216,236,207,235,193,222,175,222,165" 
                class="{hoverImage:'images/queens_macromap/area11.png',selectedImage:'images/queens_macromap/area11.png',left:219,top:148}" />
        <area href="#" id="area12" shape="poly" alt="Area 12"
                coords="168,245,170,237,181,233,185,236,194,232,195,228,200,224,205,224,218,217,219,219,215,222,216,228,202,238,196,235,192,236,191,241,171,245" 
                class="{hoverImage:'images/queens_macromap/area12.png',selectedImage:'images/queens_macromap/area12.png',left:168,top:216}" />
        <area href="#" id="area13" shape="poly" alt="Area 13"
                coords="171,250,193,244,193,240,197,240,198,241,217,232,219,223,223,218,222,216,228,211,234,210,249,230,256,252,191,278,181,278,175,273,171,253" 
                class="{hoverImage:'images/queens_macromap/area13.png',selectedImage:'images/queens_macromap/area13.png',left:170,top:209}" />
        <area href="#" id="area14" shape="poly" alt="Area 14"
                coords="181,281,190,281,257,256,272,288,272,306,266,307,263,315,260,315,258,310,253,311,233,310,226,307,230,323,232,334,229,335,226,325,223,321,223,324,226,332,225,335,222,335,218,325,216,313,214,319,216,332,217,335,219,345,209,351,206,343,202,341,201,335,189,324,180,327,175,324,185,321,190,317,189,296,182,290" 
                class="{hoverImage:'images/queens_macromap/area14.png',selectedImage:'images/queens_macromap/area14.png',left:176,top:255}" />
        <area href="#" id="area15" shape="poly" alt="Area 15"
                coords="253,231,292,214,298,214,337,198,346,214,360,250,342,283,342,301,338,303,326,301,276,303,275,289,253,233" 
                class="{hoverImage:'images/queens_macromap/area15.png',selectedImage:'images/queens_macromap/area15.png',left:252,top:198}" />
        <area href="#" id="area16" shape="poly" alt="Area 16"
                coords="387,143,388,137,396,128,396,125,402,120,408,119,424,133,426,157,416,179,382,192,386,216,387,280,383,289,384,313,387,322,389,330,383,336,377,336,365,342,362,355,361,351,349,339,347,340,327,325,323,324,299,310,275,307,322,304,339,305,344,303,345,288,363,252,341,196,347,189,345,185,352,182" 
                class="{hoverImage:'images/queens_macromap/area16.png',selectedImage:'images/queens_macromap/area16.png',left:276,top:119}" />
        <area href="#" id="area17" shape="poly" alt="Area 17"
                coords="236,338,230,314,255,314,257,317,264,318,267,310,296,312,320,328,327,327,354,352,353,365,335,374,324,384,323,391,319,392,320,385,296,373,289,365,243,338,242,331,241,325,245,317,239,318,236,321" 
                class="{hoverImage:'images/queens_macromap/area17.png',selectedImage:'images/queens_macromap/area17.png',left:229,top:309}" />
        <area href="#" id="area18" shape="poly" alt="Area 18"
                coords="71,524,78,540,51,540,60,533,65,526,68,524" 
                class="{hoverImage:'images/queens_macromap/area18.png',selectedImage:'images/queens_macromap/area18.png',left:51,top:523}" />
        <area href="#" id="area19" shape="poly" alt="Area 19"
                coords="76,523,81,515,92,515,99,509,104,511,104,530,94,532,81,538" 
                class="{hoverImage:'images/queens_macromap/area19.png',selectedImage:'images/queens_macromap/area19.png',left:75,top:509}" />
        <area href="#" id="area20" shape="poly" alt="Area 20"
                coords="109,511,114,512,125,508,130,513,133,499,140,500,142,504,134,508,133,511,144,505,141,500,153,500,174,488,176,490,171,493,177,503,129,525,116,525,109,528,107,512" 
                class="{hoverImage:'images/queens_macromap/area20.png',selectedImage:'images/queens_macromap/area20.png',left:107,top:488}" />
        <area href="#" id="area21" shape="poly" alt="Area 21"
                coords="180,490,176,487,194,473,213,473,230,469,234,466,243,464,260,453,268,443,271,438,275,440,284,438,292,432,288,441,289,449,292,444,293,441,301,432,301,427,306,422,306,418,314,418,315,421,314,427,310,434,304,438,304,442,308,443,312,437,316,439,325,441,322,433,321,424,324,418,316,412,325,410,331,416,332,419,335,420,339,417,345,419,355,415,363,426,369,431,369,446,353,448,349,455,312,454,290,460,284,461,214,487,200,495,181,503,175,495,176,489" 
                class="{hoverImage:'images/queens_macromap/area21.png',selectedImage:'images/queens_macromap/area21.png',left:175,top:410}" />
        </map>
</div>

