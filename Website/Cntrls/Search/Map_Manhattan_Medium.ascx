﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Map_Manhattan_Medium.ascx.cs" Inherits="cntrls_Search_Map_Manhattan_Medium" %>

<script src="js/jquery.metadata.js" type="text/javascript"></script>
<script src="js/jquery.hovermap.js" type="text/javascript"></script>
<script src="js/jquery.tooltip.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    $(function () {
        $('.map-container').hovermap({ width: 254, height: 507, backgroundImage: 'images/map.png' });
        $('.map-container map *').tooltip({ positionLeft: true, track: true });
    });
</script>
<div class="map-container">
        <img src="images/map/clear.png" border="0" width="254" height="507" usemap="#manhattan" alt="" class="map" />
        <map name="manhattan" id="manhattan-map">
        <area href="#" id="financial" shape="poly" alt="Financial District" coords="26,450,21,483,34,497,57,481,51,474,30,466,31,452" class="{hoverImage:'images/map/financial.png',selectedImage:'images/map/financial.png',left:21,top:451}" />
        <area href="#" id="tribeca" shape="poly" alt="Tribeca" coords="35,439,52,453,43,469,32,463" class="{hoverImage:'images/map/tribeca.png',selectedImage:'images/map/tribeca.png',left:29,top:437}" />
        <area href="#" id="soho" shape="poly" alt="Soho" coords="35,436,58,455,66,455,70,442,54,433,37,430" class="{hoverImage:'images/map/soho.png',selectedImage:'images/map/soho.png',left:30,top:428}" />
        <area href="#" id="les" shape="poly" alt="Lower East Side" coords="46,469,54,456,67,459,73,442,110,453,103,470,60,479" class="{hoverImage:'images/map/les.png',selectedImage:'images/map/les.png',left:45,top:442}" />
        <area href="#" id="eastvillage" shape="poly" alt="East Village" coords="85,416,75,439,110,451,115,433,116,423,113,421,108,429" class="{hoverImage:'images/map/evillage.png',selectedImage:'images/map/evillage.png',left:73,top:412}" />
        <area href="#" id="greenwichvillage" shape="poly" alt="Greenwich Village" coords="39,393,82,416,73,438,55,430,36,426" class="{hoverImage:'images/map/gvillage.png',selectedImage:'images/map/gvillage.png',left:30,top:390}" />
        <area href="#" id="gramercy" shape="poly" alt="Gramercy Park" coords="87,396,79,411,108,426,112,418,110,409" class="{hoverImage:'images/map/gramercy.png',selectedImage:'images/map/gramercy.png',left:78,top:393}" />
        <area href="#" id="chelsea" shape="poly" alt="Chelsea" coords="44,356,77,370,76,375,82,379,83,381,89,385,76,410,40,392" class="{hoverImage:'images/map/chelsea.png',selectedImage:'images/map/chelsea.png',left:34,top:351}" />
        <area href="#" id="suttonplace" shape="poly" alt="Sutton Place" coords="131,343,126,352,130,355,136,347" class="{hoverImage:'images/map/suttonplace.png',selectedImage:'images/map/suttonplace.png',left:124,top:340}" />
        <area href="#" id="murrayhill" shape="poly" alt="Murray Hill" coords="95,362,86,379,93,384,89,393,111,405,112,389,117,378,111,375,111,370" class="{hoverImage:'images/map/murrayhill.png',selectedImage:'images/map/murrayhill.png',left:84,top:359}" />
        <area href="#" id="midtowneast" shape="poly" alt="Midtown East" coords="119,375,113,373,113,368,97,359,109,333,129,342,123,352,129,358,145,347,158,328,165,314,176,307,172,316,158,338,148,351,139,363,135,366,142,354,139,352,140,349" class="{hoverImage:'images/map/midtowneast.png',selectedImage:'images/map/midtowneast.png',left:93,top:305}" />
        <area href="#" id="clinton" shape="poly" alt="Clinton" coords="68,310,91,322,68,363,45,352" class="{hoverImage:'images/map/clinton.png',selectedImage:'images/map/clinton.png',left:39,top:304}" />
        <area href="#" id="midtownwest" shape="poly" alt="Midtown West" coords="93,322,108,331,83,376,79,373,80,368,71,364" class="{hoverImage:'images/map/midtownwest.png',selectedImage:'images/map/midtownwest.png',left:70,top:320}" />
        <area href="#" id="uppereastside" shape="poly" alt="Upper East Side" coords="157,242,112,329,138,343,171,296,167,286,168,279,155,272,158,265,152,262,161,244" class="{hoverImage:'images/map/ues.png',selectedImage:'images/map/ues.png',left:107,top:241}" />
        <area href="#" id="upperwestside" shape="poly" alt="Upper West Side" coords="133,184,117,211,70,307,92,318,145,216,142,213,147,202" class="{hoverImage:'images/map/uws.png',selectedImage:'images/map/uws.png',left:67,top:181}" />
        <area href="#" id="uppermanhattan" shape="poly" alt="Upper Manhattan" coords="204,31,197,30,187,48,187,55,167,93,159,101,160,116,154,140,183,158,182,141,218,67,229,46,224,39" class="{hoverImage:'images/map/uppermanhattan.png',selectedImage:'images/map/uppermanhattan.png',left:154,top:29}" />
        <area href="#" id="harlem" shape="poly" alt="Harlem" coords="152,143,184,160,185,214,194,224,195,245,183,256,179,268,170,276,159,271,161,264,155,259,164,243,142,230,149,215,145,211,152,201,137,179,138,174,136,171,142,163,144,165" class="{hoverImage:'images/map/harlem.png',selectedImage:'images/map/harlem.png',left:134,top:142}" />
        
        <area href="#" id="brooklyn" shape="poly" alt="Brooklyn" coords="64,504,68,491,100,484,109,496,119,485,133,439,136,425,134,411,148,396,175,407,180,420,199,426,212,448,218,458,252,476,243,504,245,504" class="{hoverImage:'images/map/brooklyn.png',selectedImage:'images/map/brooklyn.png',left:33,top:393}" />
        <area href="#" id="queens" shape="poly" alt="Queens" coords="223,434,251,442,252,264,238,258,198,295,189,294,182,303,186,312,175,321,141,376,134,394,165,393,182,403,185,411,200,418,212,430" class="{hoverImage:'images/map/queens.png',selectedImage:'images/map/queens.png',left:133,top:257}" />
        <area href="#" id="bronx" shape="poly" alt="Bronx" coords="224,4,206,24,221,33,234,41,234,56,225,78,210,95,195,129,188,162,192,176,192,202,192,211,199,226,211,227,218,231,233,241,252,218,251,4,224,4" class="{hoverImage:'images/map/bronx.png',selectedImage:'images/map/bronx.png',left:189,top:0}" />
        
        </map>
</div>