﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Map_Brooklyn_Macro.ascx.cs" Inherits="Cntrls_Search_Map_Brooklyn_Macro" %>

<script src="js/jquery.tooltip.js" type="text/javascript"></script>
<script src="js/jquery.metadata.js" type="text/javascript"></script>
<script src="js/jquery.hovermap.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    $(function () {
        $('.map-container').hovermap({ width: 439, height: 563, backgroundImage: 'images/map-brooklyn-macro/map.png' });
        $('.map-container map *').tooltip({ positionLeft: true, track: true });
    });
</script>
<div class="map-container">
        <img src="images/map-macro/clear.png" border="0" width="549" height="486" usemap="#brooklyn" alt="" class="map" />
        <map name="brooklyn" id="brooklyn-map">
        <area href="#" id="north" shape="poly" alt="North"
                coords="172,96,170,77,172,68,182,61,193,62,208,66,215,72,217,81,224,87,234,90,242,92,243,97,248,112,242,116,234,124,232,134,236,141,239,138,238,131,238,128,246,127,250,138,299,177,285,197,286,205,293,214,291,223,296,228,265,243,266,228,192,223,179,218,172,175,174,170,157,147,156,128,163,107,171,100" 
                class="{hoverImage:'images/map-brooklyn-macro/north.png',selectedImage:'images/map-brooklyn-macro/north.png',left:156,top:60}" />
        <area href="#" id="northwest" shape="poly" alt="Northwest"
                coords="94,184,95,170,99,168,100,156,110,154,126,150,136,149,131,160,132,172,168,175,177,218,169,216,163,220,168,239,154,237,141,210,132,202"
                class="{hoverImage:'images/map-brooklyn-macro/northwest.png',selectedImage:'images/map-brooklyn-macro/northwest.png',left:93,top:149}" />
        <area href="#" id="west" shape="poly" alt="West"
                coords="75,204,76,208,92,187,137,210,153,239,134,270,147,283,148,298,133,303,133,289,116,262,104,255,96,246,88,253,75,249,70,258,64,262,56,259,54,245,56,248,61,255,66,254,70,250,67,244,57,237,53,221" 
                class="{hoverImage:'images/map-brooklyn-macro/west.png',selectedImage:'images/map-brooklyn-macro/west.png',left:51,top:186}" />
        <area href="#" id="southwest" shape="poly" alt="Southwest"
               coords="124,280,115,268,97,254,74,267,40,301,42,307,35,316,29,316,22,317,16,324,23,328,8,371,11,402,20,409,33,401,57,416,54,426,69,434,87,450,118,414,144,410,140,364,137,360,132,310,115,317,89,296,95,276,114,275"
                class="{hoverImage:'images/map-brooklyn-macro/southwest.png',selectedImage:'images/map-brooklyn-macro/southwest.png',left:2,top:252}" />
        <area href="#" id="south" shape="poly" alt="South"
               coords="91,453,118,419,157,413,163,420,205,398,213,434,232,436,243,450,258,472,246,473,235,462,237,455,231,450,233,469,244,479,232,483,199,479,197,484,211,488,230,488,235,501,178,507,133,513,91,514,75,505,65,500,70,492,82,486,115,495,117,490,105,483"
                class="{hoverImage:'images/map-brooklyn-macro/south.png',selectedImage:'images/map-brooklyn-macro/south.png',left:60,top:396}" />
        <area href="#" id="southeast" shape="poly" alt="Southeast"
                coords="206,353,254,350,265,358,301,377,308,383,305,392,310,399,315,399,320,407,315,412,315,416,301,422,295,419,276,428,265,421,264,409,267,402,265,396,251,408,254,415,249,420,245,417,227,431,217,432,209,399,215,384,221,376,222,373"
                class="{hoverImage:'images/map-brooklyn-macro/southeast.png',selectedImage:'images/map-brooklyn-macro/southeast.png',left:204,top:348}" />
        <area href="#" id="east" shape="poly" alt="East"
               coords="250,258,304,226,309,210,318,211,323,215,362,193,367,209,362,214,371,217,380,244,385,245,388,268,365,272,374,288,352,306,342,323,320,303,317,303,328,321,345,341,330,359,317,364,311,374,298,369,308,355,289,327,264,348,285,332,305,354,293,364,266,348,264,354,258,323,274,306,281,295"
               class="{hoverImage:'images/map-brooklyn-macro/east.png',selectedImage:'images/map-brooklyn-macro/east.png',left:246,top:191}" />
        <area href="#" id="central" shape="poly" alt="Central"
               coords="168,221,189,227,263,229,261,246,246,257,278,296,259,314,257,319,257,340,256,345,191,348,180,354,180,357,202,354,218,372,215,379,207,392,162,416,156,409,148,410,136,309,174,288,175,264,171,238"
               class="{hoverImage:'images/map-brooklyn-macro/central.png',selectedImage:'images/map-brooklyn-macro/central.png',left:135,top:219}" />
        </map>
</div>

