﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Neighborhood_Manhattan_Macro.ascx.cs" Inherits="cntrls_Search_Neighborhood_Manhattan_Macro" %>
<script type="text/javascript">
    $(function () {
        $('.manhattan-all-checkbox').click(function () {
            var IsChecked = $(this).attr('checked');
            $('input.manhattan').attr('checked', IsChecked);
        });
//        $('.manhattan-all-checkbox').siblings('label').click(function () {
//            var IsChecked = $('.manhattan-all-checkbox').attr('checked');
//            $('input.manhattan').attr('checked', IsChecked);
//        });
    });
</script>
<div class="section">
    <%=Html.CheckBox("Borough", "All Manhattan", Model.Boroughs.Contains(Borough.Manhattan), new { @value = Neighborhood.Manhattan, @class = "manhattan-all-checkbox all-checkbox manhattan" })%>
    <div class="clearfix">
        <div class="column">
            <ul class="neighborhoods-macro">
                <li style="margin-right:65px;"><%=Html.CheckBox("Neighborhood", "Downtown", Model.Neighborhoods.Contains(Neighborhood.Downtown), new { @value = Neighborhood.Downtown, @class = "downtown-checkbox manhattan" })%></li>
                <li style="margin-right:55px;"><%=Html.CheckBox("Neighborhood", "Midtown West", Model.Neighborhoods.Contains(Neighborhood.MidtownWest), new { @value = Neighborhood.MidtownWest, @class = "midtownwest-checkbox manhattan" })%></li>
                <li style="margin-right:50px;"><%=Html.CheckBox("Neighborhood", "Midtown East", Model.Neighborhoods.Contains(Neighborhood.MidtownEast), new { @value = Neighborhood.MidtownEast, @class = "midtowneast-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "Upper East Side", Model.Neighborhoods.Contains(Neighborhood.UpperEast), new { @value = Neighborhood.UpperEast, @class = "upper-east-side-checkbox manhattan" })%></li>
               <br>
                <li style="margin-right:36px;"><%=Html.CheckBox("Neighborhood", "Upper West Side", Model.Neighborhoods.Contains(Neighborhood.UpperWest), new { @value = Neighborhood.UpperWest, @class = "upper-west-side-checkbox manhattan" })%></li>
                <li style="margin-right:32px;"><%=Html.CheckBox("Neighborhood", "Upper Manhattan", Model.Neighborhoods.Contains(Neighborhood.UpperManhattan), new { @value = Neighborhood.UpperManhattan, @class = "upper-manhattan-checkbox manhattan" })%></li>
                <li style="margin-right:67px;"><%=Html.CheckBox("Neighborhood", "All Brooklyn", Model.Neighborhoods.Contains(Neighborhood.UpperManhattan), new { @value = Neighborhood.UpperManhattan, @class = "brooklyn-checkbox manhattan" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "All Queens", Model.Neighborhoods.Contains(Neighborhood.UpperManhattan), new { @value = Neighborhood.UpperManhattan, @class = "queens-checkbox manhattan" })%></li>
            </ul>
        </div>
    </div>
</div>

