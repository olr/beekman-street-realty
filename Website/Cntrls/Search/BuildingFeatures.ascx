﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BuildingFeatures.ascx.cs" Inherits="cntrls_Search_BuildingFeatures" %>

<div class="page-titles">Building Features</div>

<ul class="buildingfeatures clearfix">
    <li><%=Html.CheckBox("BuildingFeatures", "Attended Lobby", Model.BuildingFeatures.Contains(BuildingFeature.AttendedLobby), new { @value = "AttendedLobby" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Fitness Facility", Model.BuildingFeatures.Contains(BuildingFeature.FitnessFacility), new { @value = "FitnessFacility" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Elevator", Model.BuildingFeatures.Contains(BuildingFeature.WheelChairAccess), new { @value = "WheelChairAccess" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Garden/Courtyard", Model.BuildingFeatures.Contains(BuildingFeature.GardenCourtyard), new { @value = "GardenCourtyard" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "New Development", Model.BuildingFeatures.Contains(BuildingFeature.NewDevelopment), new { @value = "NewDevelopment" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Laundry Room", Model.BuildingFeatures.Contains(BuildingFeature.LaundryRoom), new { @value = "LaundryRoom" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Post-War", Model.BuildingFeatures.Contains(BuildingFeature.PostWar), new { @value = "PostWar" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "On-Site Parking", Model.BuildingFeatures.Contains(BuildingFeature.OnsiteParking), new { @value = "OnsiteParking" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Pre-War", Model.BuildingFeatures.Contains(BuildingFeature.PreWar), new { @value = "PreWar" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Pool", Model.BuildingFeatures.Contains(BuildingFeature.SwimmingPool), new { @value = "SwimmingPool" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Townhouse", Model.BuildingFeatures.Contains(BuildingFeature.TownhouseApt), new { @value = "TownhouseApt" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Roof Deck", Model.BuildingFeatures.Contains(BuildingFeature.RoofDeck), new { @value = "RoofDeck" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Pet Friendly", Model.BuildingFeatures.Contains(BuildingFeature.PetFriendly), new { @value = "PetFriendly" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Storage", Model.BuildingFeatures.Contains(BuildingFeature.Storage), new { @value = "Storage" })%></li>

<%--    <li><%=Html.CheckBox("BuildingFeatures", "Attended Lobby", Model.BuildingFeatures.Contains(BuildingFeature.AttendedLobby), new { @value = "AttendedLobby" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Fitness Facility", Model.BuildingFeatures.Contains(BuildingFeature.FitnessFacility), new { @value = "FitnessFacility" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Elevator", Model.BuildingFeatures.Contains(BuildingFeature.WheelChairAccess), new { @value = "WheelChairAccess" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Garden/Courtyard", Model.BuildingFeatures.Contains(BuildingFeature.GardenCourtyard), new { @value = "GardenCourtyard" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "New Development", Model.BuildingFeatures.Contains(BuildingFeature.NewDevelopment), new { @value = "NewDevelopment" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Laundry Room", Model.BuildingFeatures.Contains(BuildingFeature.LaundryRoom), new { @value = "LaundryRoom" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Pet Friendly", Model.BuildingFeatures.Contains(BuildingFeature.PetFriendly), new { @value = "PetFriendly" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "On-Site Parking", Model.BuildingFeatures.Contains(BuildingFeature.OnsiteParking), new { @value = "OnsiteParking" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Post-War", Model.BuildingFeatures.Contains(BuildingFeature.PostWar), new { @value = "PostWar" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Pool", Model.BuildingFeatures.Contains(BuildingFeature.SwimmingPool), new { @value = "SwimmingPool" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Pre-War", Model.BuildingFeatures.Contains(BuildingFeature.PreWar), new { @value = "PreWar" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Roof Deck", Model.BuildingFeatures.Contains(BuildingFeature.RoofDeck), new { @value = "RoofDeck" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Townhouse", Model.BuildingFeatures.Contains(BuildingFeature.TownhouseApt), new { @value = "TownhouseApt" })%></li>
    <li><%=Html.CheckBox("BuildingFeatures", "Storage", Model.BuildingFeatures.Contains(BuildingFeature.Storage), new { @value = "Storage" })%></li>
--%></ul>
