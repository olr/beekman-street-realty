﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApartmentSize.ascx.cs" Inherits="cntrls_Search_ApartmentSize" %>



<div class="clearfix ">
    <%=Html.DropDownList("MinBedrooms", SiteHelper.LoadDictionary("Bedrooms.xml"),"Minimum Bedrooms", new { @class = "column minbeds" })%>    
    <%=Html.DropDownList("MinBaths", SiteHelper.LoadDictionary("Bathrooms.xml"), Model.MinBathroom.ToString(), new { @class = "column minbaths" })%>    
</div>
