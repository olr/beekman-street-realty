﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CommercialForm.ascx.cs" Inherits="cntrls_Search_CommercialForm" %>

<div class="property-types">
    <div class="page_titles">Property Type</div>
    <div class="title_underline"></div>
    <ul>
        <li><input type="checkbox" id="AllPropertyTypes" value="all" /> <label>All Types</label></li>
        <% if (Model.PropertyTypes.Contains(PropertyType.Sale)) { %>
        <li><%=Html.CheckBox("OwnershipTypes", "Commercial", Model.OwnershipTypes.Contains(Ownership.Commercial), new { @value = "Commercial", @class = "property-type" })%></li>
        <li><%=Html.CheckBox("OwnershipTypes", "Mixed Use", Model.OwnershipTypes.Contains(Ownership.MixedUse), new { @value = "MixedUse", @class = "property-type" })%></li>
        <% } else { %>
        <li><%=Html.CheckBox("CLPropertyTypes", "Office", Model.CLPropertyType.Contains(CLPropertyType.Office), new { @value = "Office", @class = "property-type" })%></li>
        <li><%=Html.CheckBox("CLPropertyTypes", "Retail", Model.CLPropertyType.Contains(CLPropertyType.Retail), new { @value = "Retail", @class = "property-type" })%></li>
        <li><%=Html.CheckBox("CLPropertyTypes", "Other", Model.CLPropertyType.Contains(CLPropertyType.CommercialBuildings),
        new { @value = "MixedUseBuildings,VacantLand,Industrial,ResidentialBuildings,DevelopmentConversionSites,CommercialBuildings,Hotel,Residential,MixedUse", @class = "property-type" })%></li>
        <% } %>
    </ul>
</div>

