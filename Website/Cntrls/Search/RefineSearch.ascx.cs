﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;

public partial class cntrls_RefineSearch : BaseControl<cntrls_RefineSearch, SearchOptions>, IRenderable<SearchOptions>
{
   public void DataBind(SearchOptions data)
    {
        if (data != null)
        {
            Model = data;
            this.Controls.DataBind<SearchOptions>(Model);
        }
    }
}