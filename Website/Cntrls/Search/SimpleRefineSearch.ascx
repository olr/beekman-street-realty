﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SimpleRefineSearch.ascx.cs"
    Inherits="Cntrls_Search_SimpleRefineSearch" %>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jquery.hovermap.js"></script>

<div class="refine-search-container corners">
    <div class="padding-container">
     <div  class="salerent left">
        
            Sale &nbsp;
            <input id="saleRadio" name="propertyType" type="radio" value="sale" />
          Rental &nbsp;
            <input id="rentalRadio" name="propertyType" type="radio" value="rental" />
        </div>
        <form id="searchForm" method="post" action="result.aspx">
        <input type="hidden" name="PropertyType" value="<%=options.PropertyTypes.Contains(PropertyType.Rental)?"Rental":"Sale"%>" />
        <div class="clearfix refine-search-options">
            <div class="column refine-label">
                Refine Search
            </div>
            <div class="column search-size">
                <%=this.RenderPartial("cntrls/Search/ApartmentSize", options)%>
            </div>
            <div class="column search-price">
                <%=Html.DropDownList("MinPrice", SiteHelper.GetMinPriceList(options), options.MinPrice.ToString(), new { @class = "column minprice" })%>
                <%=Html.DropDownList("MaxPrice", SiteHelper.GetMaxPriceList(options), options.MaxPrice.ToString(), new { @class = "column maxprice" })%>
            </div>
            <div class="column refine-search-button button-image">
                <input id="submitSearch" class="gobutton" type="image" src="images/clear.png" value="Refine Search" />
            </div>
            </div>
            <div id="more-search-options" style="display: none;">
                <div class="neighborhood neighborhood_space">
                    <br />
                </div>
                <div class="horizontal_spacer">
                </div>
            </div>
            <input type="hidden" name="neighborhood" value="<%=neighborhoods %>" />
        </form>
    </div>
</div>
