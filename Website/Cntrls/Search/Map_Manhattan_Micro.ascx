﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Map_Manhattan_Micro.ascx.cs" Inherits="cntrls_Search_Map_Manhattan_Micro" %>

<script src="js/jquery.tooltip.js" type="text/javascript"></script>
<script src="js/jquery.metadata.js" type="text/javascript"></script>
<script src="js/jquery.hovermap.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    $(function () {
        $('.map-container').hovermap({ width: 254, height: 507, backgroundImage: 'images/manhattan_micromap/map.png' });
        $('.map-container map *').tooltip({ positionLeft: true, track: true });
    });
</script>
<div class="map-container">
        <img src="images/clear.png" border="0" width="549" height="486" usemap="#manhattan" alt="" class="map" />
        <map name="manhattan" id="manhattan-map">
        <area href="#" id="batteryparkcity" shape="poly" alt="Battery Park City"
                coords="26,463,25,454,28,451,31,452,24,488,21,484,24,463" 
                class="{hoverImage:'images/manhattan_micromap/battery-park-city.png',selectedImage:'images/manhattan_micromap/battery-park-city.png',left:22,top:451}" />
       <area href="#" id="carnegiehill" shape="poly" alt="Carnegie Hill"
                coords="156,262,158,266,149,281,141,277,148,260" 
                class="{hoverImage:'images/manhattan_micromap/carnegie-hill.png',selectedImage:'images/manhattan_micromap/carnegie-hill.png',left:140,top:260}" />
       <area href="#" id="chelsea" shape="poly" alt="Chelsea"
                coords="44,355,76,370,60,400,40,391,38,369,43,355" 
                class="{hoverImage:'images/manhattan_micromap/chelsea.png',selectedImage:'images/manhattan_micromap/chelsea.png',left:40,top:355}" />
       <area href="#" id="chinatown" shape="poly" alt="Chinatown"
                coords="54,455,56,458,64,458,71,467,63,467,56,475,50,470,45,470,52,458" 
                class="{hoverImage:'images/manhattan_micromap/chinatown.png',selectedImage:'images/manhattan_micromap/chinatown.png',left:46,top:456}" />
       <area href="#" id="clinton" shape="poly" alt="Clinton"
                coords="68,310,90,321,67,362,46,352,47,343,53,336,66,311" 
                class="{hoverImage:'images/manhattan_micromap/clinton.png',selectedImage:'images/manhattan_micromap/clinton.png',left:45,top:309}" />
       <area href="#" id="eastharlem" shape="poly" alt="East Harlem"
                coords="186,190,184,212,194,224,195,247,188,253,182,261,179,267,171,276,161,270,160,261,157,257,163,243,160,238,165,228,172,231,179,211,175,209" 
                class="{hoverImage:'images/manhattan_micromap/east-harlem.png',selectedImage:'images/manhattan_micromap/east-harlem.png',left:156,top:191}" />
       <area href="#" id="eastvillage" shape="poly" alt="East Village"
                coords="84,418,108,430,114,421,115,432,111,451,75,439,81,420" 
                class="{hoverImage:'images/manhattan_micromap/east-village.png',selectedImage:'images/manhattan_micromap/east-village.png',left:75,top:417}" />
       <area href="#" id="financial" shape="poly" alt="Financial District"
                coords="30,467,45,473,51,473,58,481,36,496,28,495,27,489" 
                class="{hoverImage:'images/manhattan_micromap/financial-district.png',selectedImage:'images/manhattan_micromap/financial-district.png',left:26,top:467}" />
       <area href="#" id="flatirondistrict" shape="poly" alt="Flatiron District"
                coords="78,376,89,384,77,409,64,402,77,375" 
                class="{hoverImage:'images/manhattan_micromap/flatiron-district.png',selectedImage:'images/manhattan_micromap/flatiron-district.png',left:64,top:376}" />
       <area href="#" id="gramercypark" shape="poly" alt="Gramercy Park"
                coords="87,396,100,403,93,418,79,411,85,397" 
                class="{hoverImage:'images/manhattan_micromap/gramercy-park.png',selectedImage:'images/manhattan_micromap/gramercy-park.png',left:80,top:396}" />
       <area href="#" id="greenwichvillage" shape="poly" alt="Greenwich Village"
                coords="60,405,81,416,72,438,52,428,46,428" 
                class="{hoverImage:'images/manhattan_micromap/greenwich-village.png',selectedImage:'images/manhattan_micromap/greenwich-village.png',left:48,top:405}" />
       <area href="#" id="hamiltonheights" shape="poly" alt="Hamilton Heights"
                coords="154,144,173,153,158,181,136,171,140,163,144,161" 
                class="{hoverImage:'images/manhattan_micromap/hamilton-heights.png',selectedImage:'images/manhattan_micromap/hamilton-heights.png',left:135,top:144}" />
       <area href="#" id="harlem" shape="poly" alt="Harlem"
                coords="175,156,184,159,186,181,172,207,163,202,153,217,162,224,155,237,143,230,148,215,146,209,150,201,135,181,138,177,158,186" 
                class="{hoverImage:'images/manhattan_micromap/harlem.png',selectedImage:'images/manhattan_micromap/harlem.png',left:135,top:155}" />
       <area href="#" id="hudsonheights" shape="poly" alt="Hudson Heights"
                coords="174,82,184,86,176,101,165,97,173,84" 
                class="{hoverImage:'images/manhattan_micromap/hudson-heights.png',selectedImage:'images/manhattan_micromap/hudson-heights.png',left:165,top:82}" />
       <area href="#" id="inwood" shape="poly" alt="Inwood"
                coords="188,51,189,43,196,30,205,29,207,38,211,41,218,36,229,38,229,47,222,64,212,77,188,53" 
                class="{hoverImage:'images/manhattan_micromap/inwood.png',selectedImage:'images/manhattan_micromap/inwood.png',left:187,top:30}" />
       <area href="#" id="kipsbay" shape="poly" alt="Kips Bay"
                coords="113,385,112,401,107,402,89,394,92,387,95,387,100,382,101,383,104,380" 
                class="{hoverImage:'images/manhattan_micromap/kips-bay.png',selectedImage:'images/manhattan_micromap/kips-bay.png',left:89,top:381}" />
       <area href="#" id="littleitaly" shape="poly" alt="Little Italy"
                coords="63,447,69,448,65,455,60,455,64,448" 
                class="{hoverImage:'images/manhattan_micromap/little-italy.png',selectedImage:'images/manhattan_micromap/little-italy.png',left:61,top:447}" />
       <area href="#" id="lowereastside" shape="poly" alt="Lower East Side"
                coords="74,443,109,453,107,460,105,466,98,471,67,474,59,478,65,470,75,468,69,457" 
                class="{hoverImage:'images/manhattan_micromap/lower-east-side.png',selectedImage:'images/manhattan_micromap/lower-east-side.png',left:62,top:442}" />
       <area href="#" id="midtowneast" shape="poly" alt="Midtown East"
                coords="127,343,119,340,107,364,97,358,111,332,124,338,123,341" 
                class="{hoverImage:'images/manhattan_micromap/midtown-east.png',selectedImage:'images/manhattan_micromap/midtown-east.png',left:96,top:332}" />
       <area href="#" id="midtownwest" shape="poly" alt="Midtown West"
                coords="93,323,108,330,84,375,80,374,79,368,71,364" 
                class="{hoverImage:'images/manhattan_micromap/midtown-west.png',selectedImage:'images/manhattan_micromap/midtown-west.png',left:71,top:323}" />
       <area href="#" id="morningsideheights" shape="poly" alt="Morningside Heights"
                coords="116,216,133,184,147,201,142,211,146,214,139,229" 
                class="{hoverImage:'images/manhattan_micromap/morningside-heights.png',selectedImage:'images/manhattan_micromap/morningside-heights.png',left:115,top:184}" />
       <area href="#" id="mtmorrispark" shape="poly" alt="Mt. Morris Park"
                coords="163,207,177,214,169,225,157,219" 
                class="{hoverImage:'images/manhattan_micromap/morris-park.png',selectedImage:'images/manhattan_micromap/morris-park.png',left:157,top:207}" />
       <area href="#" id="murrayhill" shape="poly" alt="Murray Hill"
                coords="95,362,111,370,109,372,117,377,114,382,105,377,102,380,99,381,95,384,86,379,94,363" 
                class="{hoverImage:'images/manhattan_micromap/murray-hill.png',selectedImage:'images/manhattan_micromap/murray-hill.png',left:86,top:362}" />
       <area href="#" id="petercoopervillage" shape="poly" alt="Peter Cooper Village"
                coords="103,405,109,407,110,413,101,407,101,405" 
                class="{hoverImage:'images/manhattan_micromap/peter-cooper-village.png',selectedImage:'images/manhattan_micromap/peter-cooper-village.png',left:102,top:405}" />
       <area href="#" id="rooseveltisland" shape="poly" alt="Roosevelt Island"
                coords="136,365,144,345,161,321,174,306,173,314,137,363" 
                class="{hoverImage:'images/manhattan_micromap/roosevelt-island.png',selectedImage:'images/manhattan_micromap/roosevelt-island.png',left:136,top:306}" />
       <area href="#" id="soho" shape="poly" alt="Soho"
                coords="7,430,53,432,71,441,69,444,62,444,57,453,36,436,37,432" 
                class="{hoverImage:'images/manhattan_micromap/soho.png',selectedImage:'images/manhattan_micromap/soho.png',left:35,top:430}" />
       <area href="#" id="stuyvesanttown" shape="poly" alt="Stuyvesant Town"
                coords="100,412,111,417,107,426,96,420,98,413" 
                class="{hoverImage:'images/manhattan_micromap/stuyvesant-town.png',selectedImage:'images/manhattan_micromap/stuyvesant-town.png',left:96,top:411}" />
       <area href="#" id="suttonplace" shape="poly" alt="Sutton Place"
                coords="132,344,137,347,131,354,126,352,130,345" 
                class="{hoverImage:'images/manhattan_micromap/sutton-place.png',selectedImage:'images/manhattan_micromap/sutton-place.png',left:126,top:343}" />
       <area href="#" id="tribeca" shape="poly" alt="Tribeca"
                coords="36,438,52,452,43,468,32,464,35,437" 
                class="{hoverImage:'images/manhattan_micromap/tribeca.png',selectedImage:'images/manhattan_micromap/tribeca.png',left:31,top:439}" />
       <area href="#" id="tudorcity" shape="poly" alt="Tudor City"
                coords="116,368,121,370,119,375,115,373,114,370" 
                class="{hoverImage:'images/manhattan_micromap/tudor-city.png',selectedImage:'images/manhattan_micromap/tudor-city.png',left:114,top:368}" />
       <area href="#" id="turtlebay" shape="poly" alt="Turtle Bay"
                coords="125,347,123,352,130,357,123,368,118,364,114,367,110,365,120,344,123,345" 
                class="{hoverImage:'images/manhattan_micromap/turtle-bay.png',selectedImage:'images/manhattan_micromap/turtle-bay.png',left:110,top:345}" />
       <area href="#" id="uppereastside" shape="poly" alt="Upper East Side"
                coords="167,278,167,284,172,294,139,343,112,330,137,281,149,286,157,273,159,264,150,257,156,242,161,242,154,256,159,272" 
                class="{hoverImage:'images/manhattan_micromap/upper-east-side.png',selectedImage:'images/manhattan_micromap/upper-east-side.png',left:111,top:243}" />
       <area href="#" id="upperwestside" shape="poly" alt="Upper West Side"
                coords="113,220,136,231,91,318,70,307" 
                class="{hoverImage:'images/manhattan_micromap/upper-west-side.png',selectedImage:'images/manhattan_micromap/upper-west-side.png',left:70,top:219}" />
       <area href="#" id="washingtonheights" shape="poly" alt="Washington Heights"
                coords="187,57,209,80,194,113,182,142,184,156,154,140,158,123,158,102,162,100,176,107,187,85,175,78" 
                class="{hoverImage:'images/manhattan_micromap/washington-heights.png',selectedImage:'images/manhattan_micromap/washington-heights.png',left:154,top:57}" />
       <area href="#" id="westgreenwichvillage" shape="poly" alt="W. Greenwich Village"
                coords="36,392,58,403,43,427,37,427,31,423,32,419,36,418,37,393" 
                class="{hoverImage:'images/manhattan_micromap/west-greenwich-village.png',selectedImage:'images/manhattan_micromap/west-greenwich-village.png',left:31,top:392}" />
       
        <area href="#" id="brooklyn" shape="poly" alt="brooklyn" coords="64,504,68,491,100,484,109,496,119,485,133,439,136,425,134,411,148,396,175,407,180,420,199,426,212,448,218,458,252,476,243,504,245,504" class="{hoverImage:'images/map/brooklyn.png',selectedImage:'images/map/brooklyn.png',left:33,top:393}" />
        <area href="#" id="queens" shape="poly" alt="Queens" coords="223,434,251,442,252,264,238,258,198,295,189,294,182,303,186,312,175,321,141,376,134,394,165,393,182,403,185,411,200,418,212,430" class="{hoverImage:'images/map/queens.png',selectedImage:'images/map/queens.png',left:133,top:257}" />
        <area href="#" id="bronx" shape="poly" alt="Bronx" coords="224,4,206,24,221,33,234,41,234,56,225,78,210,95,195,129,188,162,192,176,192,202,192,211,199,226,211,227,218,231,233,241,252,218,251,4,224,4" class="{hoverImage:'images/map/bronx.png',selectedImage:'images/map/bronx.png',left:189,top:0}" />


       </map>
</div>
