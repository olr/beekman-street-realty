﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Ownership_Brooklyn.ascx.cs" Inherits="Cntrls_Search_Ownership_Brooklyn" %>
<div class="page-titles">Ownership</div>


    <% if (Model.PropertyTypes.Contains(PropertyType.Sale))
       { %>
    <ul class="clearfix">
        <li class="col1"><%=Html.CheckBox("OwnershipTypes", "Condo", Model.OwnershipTypes.Contains(Ownership.Condo), new { @value = "Condo" }) %></li>
        <li class="col2"><%=Html.CheckBox("OwnershipTypes", "Co-op", Model.OwnershipTypes.Contains(Ownership.Coop), new { @value = "Coop" })%></li>
        <li class="col3"><%=Html.CheckBox("OwnershipTypes", "Condop", Model.OwnershipTypes.Contains(Ownership.Condop), new { @value = "Condop" })%></li>
        <li class="col4"><%=Html.CheckBox("OwnershipTypes", "Single Family", Model.OwnershipTypes.Contains(Ownership.SingleFamily), new { @value = "SingleFamily", @class = "property-type" })%></li>
        <li class="col5"><%=Html.CheckBox("OwnershipTypes", "Multi-Family", Model.OwnershipTypes.Contains(Ownership.MultiFamily), new { @value = "MultiFamily", @class = "property-type" })%></li>
        <li class="col6"><%=Html.CheckBox("OwnershipTypes", "Income Properties", Model.OwnershipTypes.Contains(Ownership.IncomeProperty), new { @value = "IncomeProperty", @class = "property-type" })%></li>
   
    </ul>
    <% }
       else
       { %>
    <ul class="clearfix">
        <li class="col1"><%=Html.CheckBox("OwnershipTypes", "Rental", Model.OwnershipTypes.Contains(Ownership.Rental), new { @value = "Rental", @class = "property-type" })%></li>
        <li class="col2"><%=Html.CheckBox("OwnershipTypes", "Condo", Model.OwnershipTypes.Contains(Ownership.Condo), new { @value = "Condo" }) %></li>
        <li class="col3"><%=Html.CheckBox("OwnershipTypes", "Co-op", Model.OwnershipTypes.Contains(Ownership.Coop), new { @value = "Coop" })%></li>
        <li class="col4"><%=Html.CheckBox("OwnershipTypes", "Condop", Model.OwnershipTypes.Contains(Ownership.Condop), new { @value = "Condop" })%></li>
    </ul>
    <% } %>
