﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;

public partial class Cntrls_Search_NoFee : System.Web.UI.UserControl, IRenderable<SearchOptions>
{
    protected SearchOptions Model = new SearchOptions();

    public void DataBind(SearchOptions data)
    {
        if (data != null)
            Model = data;
    }
}