﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NoFee.ascx.cs" Inherits="Cntrls_Search_NoFee" %>
<% if (Model.PropertyTypes.Contains(PropertyType.Rental))
   { %>
   
<ul class="apartmentfeatures clearfix">
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "No Fee Only", Model.ApartmentFeatures.Contains(ApartmentFeature.NoFee), new { @value = "NoFee" })%></li>
    <li>
        <%=Html.CheckBox("ApartmentFeatures", "Furnished Only", Model.ApartmentFeatures.Contains(ApartmentFeature.Furnished), new { @value = "Furnished" })%></li>
</ul>

<%} %>
