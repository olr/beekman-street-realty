﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Neighborhood_Manhattan_Micro.ascx.cs"
    Inherits="cntrls_Search_Neighborhood_Manhattan_Micro" %>
<script type="text/javascript">
    $(function () {
        $('.all-checkbox').click(function () {
            var IsChecked = $(this).attr('checked');
            $('input.manhattan').attr('checked', IsChecked);
        });
        $('input.manhattan').click(function () {
            var IsChecked = $(this).attr('checked');
            if ($('.all-checkbox').attr('checked'))
                $('.all-checkbox').attr('checked', IsChecked);
            else {
                var allChecked = true;
                $('input.manhattan').each(function () {
                    if (!$(this).attr('checked'))
                        allChecked = false;
                });
                if (allChecked)
                    $('.all-checkbox').attr('checked', IsChecked);
            }
        });
    });
   
</script>
<div class="section">
    <div style="margin-bottom: 8px;">
        <%=Html.CheckBox("Borough", "All Manhattan", Model.Boroughs.Contains(Borough.Manhattan), new { @value = Neighborhood.Manhattan, @class = "all-checkbox" })%>
    </div>
    <div class="clearfix">
        <div class="column neighborhood-col">
            <ul class="neighborhoods">
                <li>
                    <%=Html.CheckBox("Neighborhood", "Financial District", Model.Neighborhoods.Contains(Manhattan.FinancialDistrict), new { @value = "12", @class = "financial-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Battery Park City", Model.Neighborhoods.Contains(Manhattan.BatteryParkCity), new { @value = "3", @class = "batteryparkcity-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Tribeca", Model.Neighborhoods.Contains(Manhattan.Tribeca), new { @value = (int)Manhattan.Tribeca, @class = "tribeca-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Chinatown", Model.Neighborhoods.Contains(Manhattan.Chinatown), new { @value = (int)Manhattan.Chinatown, @class = "chinatown-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Little Italy", Model.Neighborhoods.Contains(Manhattan.LittleItaly), new { @value = (int)Manhattan.LittleItaly, @class = "littleitaly-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Soho", Model.Neighborhoods.Contains(Manhattan.Soho), new { @value = (int)Manhattan.Soho, @class = "soho-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Lower East Side", Model.Neighborhoods.Contains(Manhattan.LowerEastSide), new { @value = (int)Manhattan.LowerEastSide, @class = "lowereastside-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "East Greenwich Village", Model.Neighborhoods.Contains(Manhattan.E_GreenwichVillage), new { @value = "324", @class = "eastvillage-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Greenwich Village", Model.Neighborhoods.Contains(Manhattan.GreenwichVillage), new { @value = (int)Manhattan.GreenwichVillage, @class = "greenwichvillage-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "West Greenwich Village", Model.Neighborhoods.Contains(Manhattan.W_GreenwichVillage), new { @value = "325", @class = "westgreenwichvillage-checkbox manhattan" })%></li>
            </ul>
        </div>
        <div class="column neighborhood-col">
            <ul class="neighborhoods">
                <li>
                    <%=Html.CheckBox("Neighborhood", "Gramercy Park", Model.Neighborhoods.Contains(Manhattan.GramercyPark), new { @value = (int)Manhattan.GramercyPark, @class = "gramercypark-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Flatiron District", Model.Neighborhoods.Contains(Manhattan.FlatironDistrict), new { @value = (int)Manhattan.FlatironDistrict, @class = "flatirondistrict-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Chelsea", Model.Neighborhoods.Contains(Manhattan.Chelsea), new { @value = (int)Manhattan.Chelsea, @class = "chelsea-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Stuyvesant Town", Model.Neighborhoods.Contains(Manhattan.StuyvesantTown), new { @value = (int)Manhattan.StuyvesantTown, @class = "stuyvesanttown-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Peter Cooper Village", Model.Neighborhoods.Contains(Manhattan.PeterCooperVillage), new { @value = (int)Manhattan.PeterCooperVillage, @class = "petercoopervillage-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Kips Bay", Model.Neighborhoods.Contains(Manhattan.KipsBay), new { @value = (int)Manhattan.KipsBay, @class = "kipsbay-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Murray Hill", Model.Neighborhoods.Contains(Manhattan.MurrayHill), new { @value = (int)Manhattan.MurrayHill, @class = "murrayhill-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Tudor City", Model.Neighborhoods.Contains(Manhattan.TudorCity), new { @value = (int)Manhattan.TudorCity, @class = "tudorcity-checkbox manhattan" })%></li>
            <li>
                    <%=Html.CheckBox("Neighborhood", "Turtle Bay", Model.Neighborhoods.Contains(Manhattan.TurtleBay), new { @value = (int)Manhattan.TurtleBay, @class = "turtlebay-checkbox manhattan" })%></li>
                    <li>
                    <%=Html.CheckBox("Neighborhood", "Sutton Place", Model.Neighborhoods.Contains(Manhattan.SuttonPlace), new { @value = "2,28", @class = "suttonplace-checkbox manhattan" })%></li>
                
            </ul>
        </div>
        <div class="column neighborhood-col">
            <ul class="neighborhoods">
                <li>
                    <%=Html.CheckBox("Neighborhood", "Midtown East", Model.Neighborhoods.Contains(Manhattan.MidtownEast), new { @value = (int)Manhattan.MidtownEast, @class = "midtowneast-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Clinton", Model.Neighborhoods.Contains(Manhattan.Clinton), new { @value = (int)Manhattan.Clinton, @class = "clinton-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Midtown West", Model.Neighborhoods.Contains(Manhattan.MidtownWest), new { @value = (int)Manhattan.MidtownWest, @class = "midtownwest-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Upper East Side", Model.Neighborhoods.Contains(Manhattan.UpperEastSide), new { @value = (int)Manhattan.UpperEastSide, @class = "uppereastside-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Roosevelt Island", Model.Neighborhoods.Contains(Manhattan.RooseveltIsland), new { @value = (int)Manhattan.RooseveltIsland, @class = "rooseveltisland-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Carnegie Hill", Model.Neighborhoods.Contains(Manhattan.CarnegieHill), new { @value = (int)Manhattan.CarnegieHill, @class = "carnegiehill-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Upper West Side", Model.Neighborhoods.Contains(Manhattan.UpperWestSide), new { @value = "7,8,33", @class = "upperwestside-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Morningside Heights", Model.Neighborhoods.Contains(Manhattan.MorningsideHeights), new { @value = (int)Manhattan.MorningsideHeights, @class = "morningsideheights-checkbox manhattan" })%></li>
            <li>
                    <%=Html.CheckBox("Neighborhood", "East Harlem", Model.Neighborhoods.Contains(Manhattan.EastHarlem), new { @value = (int)Manhattan.EastHarlem, @class = "eastharlem-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Mt. Morris Park", Model.Neighborhoods.Contains(Manhattan.MtMorrisPark), new { @value = (int)Manhattan.MtMorrisPark, @class = "mtmorrispark-checkbox manhattan" })%></li>
                
            </ul>
        </div>
        <div class="column neighborhood-col">
            <ul class="neighborhoods">
               <li>
                    <%=Html.CheckBox("Neighborhood", "Hamilton Heights", Model.Neighborhoods.Contains(Manhattan.HamiltonHeights), new { @value = (int)Manhattan.HamiltonHeights, @class = "hamiltonheights-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Harlem", Model.Neighborhoods.Contains(Manhattan.WestHarlem), new { @value = (int)Manhattan.WestHarlem, @class = "harlem-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Inwood", Model.Neighborhoods.Contains(Manhattan.Inwood), new { @value = (int)Manhattan.Inwood, @class = "inwood-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Washington Heights", Model.Neighborhoods.Contains(Manhattan.WashingtonHeights), new { @value = (int)Manhattan.WashingtonHeights, @class = "washingtonheights-checkbox manhattan" })%></li>
                <li>
                    <%=Html.CheckBox("Neighborhood", "Hudson Heights", Model.Neighborhoods.Contains(Manhattan.HudsonHeights), new { @value = (int)Manhattan.HudsonHeights, @class = "hudsonheights-checkbox manhattan" })%></li>
                <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226;</li>
                <li><%=Html.CheckBox("Neighborhood", "All Brooklyn", Model.Boroughs.Contains(Borough.Brooklyn), new { @value = "121,60,62,63,65,66,67,68,69,70,71,73,74,75,76,124,78,79,80,81,82,83,84,322,86,87,88,89,91,92,93,94,326,95,125,96,97,98,99,100,126,101,102,103,104,105,327,127,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,61,72,122,51,64,53,90,49,48,77,52,50,85,123", @class = "brooklyn-checkbox" })%></li>
                <li><%=Html.CheckBox("Neighborhood", "All Queens", Model.Boroughs.Contains(Borough.Queens), new
{
    @value = "177,158,191,176,189,142,192,193,151,178,196,174,197,162,149,198,181,175,140,131,143,171,153,132,150,194,137,195,46,199,166,167," +
                    "200,135,157,164,165,201,148,172,130,145,202,163,188,138,169,183,203,204,187,205,206,136,129,155,170,180,159,147,207,47,179,182,208,133,144,134,209,152,184,154,146,156,210,211,161,160,186,185,173,45,212,168,141,139,44,190,36",
                                                                                                  @class = "queens-checkbox"
})%></li>
                <li><%=Html.CheckBox("Neighborhood", "All Bronx", Model.Boroughs.Contains(Borough.Bronx), new { @value = "224,4,206,24,221,33,234,41,234,56,225,78,210,95,195,129,188,162,192,176,192,202,192,211,199,226,211,227,218,231,233,241,252,218,251,4,224,4", @class = "bronx-checkbox" })%></li>


                <li>
                    <%=Html.CheckBox("Borough", "Out of Town", Model.Boroughs.Contains(Borough.OutOfTown), new { @value = "-100", @class = "Out-of-town-checkbox" })%></li>
            </ul>
        </div>
    </div>
</div>
