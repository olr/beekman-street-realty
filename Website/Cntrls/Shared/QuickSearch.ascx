﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QuickSearch.ascx.cs" Inherits="cntrls_Search_QuickSearch" %>
<script type="text/javascript" language="javascript">
    $(function () {
        var type;
        $('#quickSearch').bind('focus', function () { $(this).val(''); });
        $('#quickSearch').autofill({
            width: 208,
            handler: 'Service.svc/Search',
            parseResult: function (item) { type = item.Type; },
            onSelect: function (e, item, formatted) {
                if (typeof (item) != 'undefined') {
                    if (item.Type == 'L') {
                        location.href = 'detail.aspx?id=' + item.Value;
                    }
                    else if (item.Type == 'A' && item.Value > 0) {
                        location.href = 'agent_detail.aspx?agentid=' + item.Value;
                    }
                    else if (item.Type == 'B' && item.Value > 0) {
                        location.href = 'search.aspx?buid=' + item.Value;
                    }
                }
            }
        });
        $('#goButton').click(function () {
            if (type == 'L')
                window.location = "detail.aspx?id=" + $('#quickSearch').val();
            else if (type == 'A')
                window.location = 'agents.aspx?agentname=' + $('#quickSearch').val();
            else if (type == 'B')
                window.location = 'search.aspx?address=' + $('#quickSearch').val();
        });
    });
   
</script>
<div class="clearfix">
    <form id="quickSearchForm">
    <div class="quick_search_input left">
        <input id="quickSearch" class="qsearch corners" name="textfield" type="text" value="Building Address, Listing ID, Agent Name" />
    </div>
    </form>
    <div id="goButton" class="go-button corners cursor right">
        <%--<input id="goButton" src="images/go.png" type="image" />--%>
    </div>
</div>
