﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SimpleSearch.ascx.cs"
    Inherits="cntrls_simple_search" %>
<form method="post" action="result.aspx">
<div class="title">
    Quick Search
</div>
<div class="quick-search-container clearfix">
    <div class="quick-search-input-col left">
        <div class="radio left">
            <%= Html.RadioButton("PropertyType", Model.PropertyTypes.Contains(PropertyType.Sale), new {value="Sale", ID="sale"  })%>&nbsp;
            Sales&nbsp;&nbsp;
            <%= Html.RadioButton("PropertyType", Model.PropertyTypes.Contains(PropertyType.Rental), new { value = "Rental", ID="rent" })%>&nbsp;Rentals&nbsp;&nbsp;
        </div>
        <div class="drop-down left">
            <%=Html.DropDownList("Neighborhood", SiteHelper.GetNeighborhood(), "", new { @class = "neighborhoods" })%>
        </div>
        <div class="drop-down left">
            <%=Html.DropDownList("MinBedrooms", SiteHelper.LoadDictionary("Bedrooms.xml"), "", new { @class = "minbeds" })%>
        </div>
        <div class="drop-down-last left">
            <%=Html.DropDownList("MinBaths", SiteHelper.LoadDictionary("Bathrooms.xml"), Model.MinBathroom.ToString(), new { @class = "column minbaths" })%>
        </div>
    </div>
    <div class="quick-search-input-col price-col left">
        <div id="pricerange" class="left">
            <%=Html.RenderPartial("Cntrls/Search/PriceRange",Model) %>
        </div>
    </div>
    <div class="button search-button button-image">
        <input id="submitBtn" type="image" src="images/clear.png" />
    </div>
</div>
</form>
