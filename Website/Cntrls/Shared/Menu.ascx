﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Menu.ascx.cs" Inherits="cntrls_shared_menu" %>
<!--[if IE]> <link href="css/IE.css" rel="stylesheet" type="text/css" /> <![endif]-->
<!--[if IE 8]> <link href="css/IE8.css" rel="stylesheet" type="text/css" /> <![endif]-->
<ul class="sf-menu">
    <li class="search first">Search
        <ul  class="search">
            <li><a href="search.aspx?type=rent">Rentals</a></li>
            <li><a href="search.aspx">Sales</a></li>
            <li><a href="open_house.aspx?type=rent">Open Houses</a></li>
            <li class="last"><a href="search.aspx?nofee=1">No Fee Specials</a></li>
            <%--<li><a class="vow" href="vow_popup.aspx">Search All NYC</a></li>
            <li><a href="commercial_result.aspx">Commercial</a></li>
            --%>
            <!--<li><a href="featured_listings.aspx">Featured Listings</a></li>-->
        </ul>
    </li>
    <li class="about">About
        <ul  class="search">
            <li><a href="about.aspx">Our Company</a></li>
            <li><a href="testimonial.aspx">Testimonials</a></li>
            <li><a href="career.aspx">Careers</a></li>
            <li class="last"><a href="contact.aspx">Contact Us</a></li>
        </ul>
    </li>
    <li class="resource">Resources
        <ul  class="search">
            <li><a href="relocation.aspx">Corporate Relocation</a></li>
            <li><a href="neighborhood_guide.aspx">Neighborhood Information</a></li>
            <li><a href="renters_guide.aspx">Rentals Guide</a></li>
            <li class="last"><a href="mortgage_calculator.aspx" class="calclink2">Mortgage Calculator</a></li>
            
        </ul>
    </li>
    <li>
        <a class="listwithus-pop" href="list_with_us.aspx">
            List With Us
        </a>
    </li>
    <li>
        <a href="agents.aspx">
            Our Agents
        </a>
    </li>
    <li class="last">
        <a href="contact.aspx">
            Contact Us
        </a>
    </li>
</ul>
