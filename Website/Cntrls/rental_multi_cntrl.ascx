﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="rental_multi_cntrl.ascx.cs"
    Inherits="cntrls_default_rental_multi_cntrl" %>
<div class="title">
    Featured Rental
</div>
<div class="image left">
    <a href="<%=rentListing.GetDetailLink()%>">
        <img class="preload" src="<%= rentListing.GetDisplayPhoto() %>" /></a>
</div>
<div class="home-featured-button right">
    <div id="rent-pre" class="prev-button button-image left">
        <a href="#">
            <img src="images/clear.png" /></a></div>
    <div id="rent-next" class="next-button button-image left">
        <a href="#">
            <img src="images/clear.png" /></a></div>
            </div>
<div class="home-info left">
    <div class="header">
        <%=rentListing.GetListingBedroom()%>&nbsp;&nbsp;|&nbsp;<%=rentListing.GetPriceRentText(false)%>
    </div>
    <div class="neighborhood">
        <%=rentListing.GetArea()%>
    </div>
    <div class="description">
        <%= rentListing.GetDescriptionForDefalutPage()%>
    </div>
    <div class="button view-button button-image">
        <a href="<%=rentListing.GetDetailLink()%>">
            <img src="images/clear.png" /></a>
    </div>
</div>
