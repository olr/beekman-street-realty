﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgentBio.ascx.cs" Inherits="cntrls_AgentDetails_AgentBio" %>
<script type="text/javascript">
    function ShowBio() {
        $('.biopress .agent-links a').removeClass('disabled');
        $('.press_container').hide();
        $('.testimonial_container').hide();
        $('.bio_container').addClass('disabled').show();
        $('#agentbioLink').addClass('disabled'); 
    }
    function ShowPress() {
        $('.biopress .agent-links a').removeClass('disabled');
        $('.bio_container').hide();
        $('.testimonial_container').hide();
        $('.press_container').show();
        $('#agentPressLink').addClass('disabled');
    }
    function ShowTestimonial() {
        $('.biopress .agent-links a').removeClass('disabled');
        $('.bio_container').hide();
        $('.press_container').hide();
        $('.testimonial_container').show();
        $('#agentTestimonialLink').addClass('disabled');
    }
</script>
<div class="agent-details clearfix">
    <div class="biopress">
        <ul class="agent-links tabular clearfix">
            <%if (!string.IsNullOrEmpty(Model.AgentBio))
              {%>
            <li><a id="agentbioLink" class="disabled" href="javascript:ShowBio()">Bio</a></li>
            <%} %>
            <%if (Press.Count != 0 && (!string.IsNullOrEmpty(Model.AgentBio)))
              { %>
            <li>|</li>
            <%} %>
            <%if (Press.Count != 0)
              { %>
            <li><a id="agentPressLink" href="javascript:ShowPress()">Recent Press</a></li>
            <%} %>
            <%if ((Press.Count != 0 || (!string.IsNullOrEmpty(Model.AgentBio))) && Testimonial.Count != 0)
              { %>
            <li>|</li>
            <%} %>
            <%if (Testimonial.Count != 0)
              { %>
            <li><a id="agentTestimonialLink" href="javascript:ShowTestimonial()">Testimonials</a></li>
            <%} %>
        </ul>
        <div class="bio_container">
            <%= Model.AgentBio %>
        </div>
        <div class="press_container" style="display: none;">
            <%= this.RenderPartial("cntrls/AgentDetails/AgentPress", Press)%>
        </div>
        <div class="testimonial_container" style="display: none">
            <%= this.RenderPartial("cntrls/AgentDetails/AgentTestimonial", Testimonial)%>
        </div>
    </div>
</div>
