﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_AgentDetails_AgentPress : System.Web.UI.UserControl, IRenderable<CompanyNewsList>
{
    protected CompanyNewsList Press ;

    public void DataBind(CompanyNewsList data)
    {
        Press = data ?? new CompanyNewsList();
        list.DataSource = Press;
        list.DataBind();
    }
}