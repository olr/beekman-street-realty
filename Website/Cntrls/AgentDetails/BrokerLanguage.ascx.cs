﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Text;

public partial class Cntrls_AgentDetails_BrokerLanguage : BaseControl<Cntrls_AgentDetails_BrokerLanguage, BrokerInfo>, IRenderable<BrokerInfo>
{
    protected List<string> langs = null;
    protected string languagesString = null;
    public void DataBind(BrokerInfo b)
    {
        Model = b ?? new BrokerInfo();
        langs = ServiceLocator.GetRepository().GetBrokerByID(Model.ID).Languages;
        StringBuilder sb = new StringBuilder("<b>Languages:</b>");
        if (langs.Count != 0)
        {
            foreach (string lan in langs)
            {
                sb.Append(lan);
            }
            sb = sb.Remove((sb.Length - 1), 1);
        }
        else
            sb = new StringBuilder();
        languagesString = sb.ToString();

    }
}