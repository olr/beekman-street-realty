﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_AgentDetails_AgentBio : BaseControl<cntrls_AgentDetails_AgentBio, BrokerInfo>, IRenderable<BrokerInfo>
{
    protected CompanyNewsList Press = new CompanyNewsList();
    protected CompanyNewsList Testimonial = new CompanyNewsList();
    public void DataBind(BrokerInfo _data)
    {
        if (_data != null)
        {
            Model = _data;
            if (!string.IsNullOrEmpty(Model.AgentBio))
                Model.AgentBio = SiteHelper.FormatWeb(Model.AgentBio);
            Press = Model.GetPress();
            Testimonial = Model.GetTestimonial();
            this.DataBind();
        }
    }
}