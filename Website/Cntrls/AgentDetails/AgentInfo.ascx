﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgentInfo.ascx.cs" Inherits="cntrls_AgentDetails_AgentInfo" %>
<script type="text/javascript">
    $(document).ready(function () {
        $("a.email-me").colorbox({ iframe: true, innerWidth: 380, innerHeight: 490 });
        $('.bio_container').expander({ slicePoint: 620 });
    });
</script>

    <div class="agent_detail_background corners clearfix">
        <div class="agent_photo agent_result_thumb left">
            <img id="Img1" src='<%#Model.PhotoUrl%>' alt="" runat="server" />
        </div>
        <div class="left">
        <div class="agent_info">
            <div class="agentname">
                <%= Model.FirstName%>
                <%= Model.LastName%></div>
                <div class="agent-title">
                    <%= Model.GetTitle()%>
                </div>
            <div class="agentphone" <%=Html.DisplayStyle(!string.IsNullOrWhiteSpace(Model.WorkPhone))%>>
                O:
                <%=SiteHelper.FormatPhone(Model.WorkPhone)%></div>
            <div class="agentphone" <%=Html.DisplayStyle(!string.IsNullOrWhiteSpace(Model.MobilePhone)) %>>
                M:
                <%=SiteHelper.FormatPhone(Model.MobilePhone)%></div>
        </div>
        <%--   <% if (Model.Languages.Count > 0)
        {%>
            <div ><span class="phone">Languages:<br /></span> <%= Format_Language(Model.Languages)%> </div>
            <% }  %>    --%>
        <div class="agent-tools">
            <div>
                <a class="email-me" href="<%=Model.GetEmailMeLink() %>">
                    Email Me
                </a>
            </div>
            <div>
                <a class="vcard" target="_blank" href="<%=Model.GetVCardLink() %>">
                    Download Contact
                </a>
            </div>
            <%if (!string.IsNullOrWhiteSpace(Model.PrivateUrl))
              { %>
            <div>
                <a class="privatesite" target="_blank" href="<%=Model.PrivateUrl %>">
                    Personal Site
                </a>
            </div>
           <%} %>
        </div>
    </div>
    </div>
