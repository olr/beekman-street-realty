﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;

public partial class cntrls_AgentDetails_AgentSocial : System.Web.UI.UserControl, IRenderable<BrokerInfo>
{
    protected BrokerInfo Model = new BrokerInfo();

    public void DataBind(BrokerInfo _data)
    {
        if (_data != null)
        {
            Model = _data;
        }
    }
}