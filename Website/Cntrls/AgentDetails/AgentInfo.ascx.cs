﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_AgentDetails_AgentInfo :BaseControl<cntrls_AgentDetails_AgentInfo, BrokerInfo>, IRenderable<BrokerInfo>
{
    
         public void DataBind(BrokerInfo _data)
    {
        if (_data != null)
        {
            Model = _data;
            if (!string.IsNullOrEmpty(Model.AgentBio))
                Model.AgentBio = SiteHelper.FormatWeb(Model.AgentBio);
            this.DataBind();
        }
    }

         public string Format_Language(List<string> items)
         {
             string val = "";

             foreach (string x in items)
             {

                 val += " " + x;

             }
             val = val.Remove(val.Length - 1, 1);
             return val;
         }
    
}