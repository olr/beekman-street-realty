﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgentListings.ascx.cs"
    Inherits="cntrls_AgentDetails_AgentListings" %>
<script type="text/javascript">

    $(function () {
        $('a.disabled').each(function (i) {
            $(this).removeAttr('disabled');
            $(this).attr('readonly', 'readonly');
        });
    });

</script>
<div class="agentlisting-container">
    <div class="agent-listing-nav corners clearfix" <%=Html.DisplayStyle(showNav) %>>
        <div class="active_listings left">
            <asp:HyperLink runat="server" Text="My Listings" ID="activeLink" />
            <asp:Literal runat="server" ID="pipeDivider" Text="|" />
            <asp:HyperLink runat="server" Text="Recent Transactions" ID="recentLink" />
        </div>
        <div class="pages right">
            <%if (showAll)
              { %>
            View:
            <asp:HyperLink runat="server" Text="All" ID="allLink" />&nbsp;[<%=(countAll) %>]
            <%if (showSale)
              {%>
            |
            <asp:HyperLink runat="server" Text="Sales" ID="salesLink" />&nbsp;[<%=(countSale) %>]
            <%} if (showRental)
              { %>
            |
            <asp:HyperLink runat="server" Text="Rentals" ID="rentalsLink" />&nbsp;[<%=(countRental) %>]
            <%}
              } if (showRentCommercial && showAll)
              {%>
            |
            <%} if (showRentCommercial)
              {%>
            <asp:HyperLink runat="server" Text="Commercial Rentals" ID="commRentLink" />&nbsp;[<%=(countCommRental) %>]
            <%} if (showSaleCommercial && showAll)
              {%>
            |
            <%} if (showSaleCommercial)
              { %>
            <asp:HyperLink runat="server" Text="Commercial Sales" ID="commSaleLink" />&nbsp;[<%=(countCommSale) %>]
            <%} %>
        </div>
    </div>
    <div class="active-listings" <%=Html.DisplayStyle(showActive) %>>
        <olr:ShortForm ID="ShortForm1" runat="server" />
        <olr:OLRPageNav ID="PageNav2" TotalInfoFormat="agent" runat="server" UsePostback="false"
            PrevImageUrl="images/icons/prev.png" NextImageUrl="images/icons/next.png" />
    </div>
    <div class="recent-transactions" <%=Html.DisplayStyle(!showActive) %>>
        <olr:RecentTransactions ID="RecentTransactions1" runat="server" />
    </div>
</div>
