﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OtherAgents.ascx.cs" Inherits="cntrls_AgentDetails_OtherAgents" %>
<script type="text/javascript">
    $(function () {
        $('select.otheragents').bind('change', function () {
            var val = $(this).val();
            if (val.length > 0) {
                window.location = 'agent_detail.aspx?agentid=' + val;
            }
        });
    });
</script>

<div class="other-agents-label">
    Other Agents
</div>
<div class="other-agents-dropdown">
    <%=Html.DropDownList(Model, new { @class="otheragents"}) %>
</div>
