﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Drawing;
using System.Reflection;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_AgentDetails_AgentListings : BaseControl<cntrls_AgentDetails_AgentListings, BrokerInfo>, IRenderable<BrokerInfo>
{
    protected BrokerInfo data;
    string currentType = "all";
    string currentView = "active";
    protected int countAll, countRental, countSale, countCommRental, countCommSale;
    protected bool showNav = true;
    protected bool showActive = true;
    protected bool showRecent = false;
    protected bool showSale = true;
    protected bool showRental = true;
    protected bool showAll = true;
    protected bool showSaleCommercial = true;
    protected bool showRentCommercial = true;

    protected override void OnInit(EventArgs e)
    {
        this.Controls.InitSortables(SortClicked);
        base.OnInit(e);
    }
    protected bool ShowRentComm()
    {
        SearchOptions op = new SearchOptions();
        op.PageIndex = 0;
        op.PropertyTypes.Add(PropertyType.Commercial);
        op.PropertyTypes.Add(PropertyType.Rental);
        int total = 0;
        var listings = data.GetCommercialListings(op, out total);
        countCommRental = total;
        return total > 0;
    }
    protected bool ShowSaleComm()
    {
        SearchOptions op = new SearchOptions();
        op.PageIndex = 0;
        op.PropertyTypes.Add(PropertyType.Commercial);
        op.PropertyTypes.Add(PropertyType.Sale);
        int total = 0;
        var listings = data.GetCommercialListings(op, out total);
        countCommSale = total;
        return total > 0;
    }
    protected bool ShowSales()
    {
        SearchOptions op = new SearchOptions();
        op.PageIndex = 0;
        op.PropertyTypes.Add(PropertyType.Sale);
        int total = 0;
        var listings = data.GetListings(op, out total);
        countSale = total;
        return listings.Count > 0;
    }
    protected bool ShowRentals()
    {
        SearchOptions op = new SearchOptions();
        op.PropertyTypes.Add(PropertyType.Rental);
        op.PageIndex = 0;
        int total = 0;
        var listings = data.GetListings(op, out total);
        countRental = total;
        return listings.Count > 0;
    }
    void SortClicked(object sender, CommandEventArgs e)
    {
        SearchOptions options = GetSearchOptions();
        options.OrderBy = fn.StringToEnum<SortOrder>(e.CommandName);
        options.IsDescending = ((SortDirection)e.CommandArgument).Equals(SortDirection.Descending);
        options.PageIndex = 0;
        LoadData(options);
    }

    public void DataBind(BrokerInfo _data)
    {
        data = _data;
        if (data != null)
        {
            showSale = ShowSales();
            showRental = ShowRentals();
            showRentCommercial = ShowRentComm();
            showSaleCommercial = ShowSaleComm();
            showAll = showSale || showRental;
            countAll = countRental + countSale;
            SearchOptions options = GetSearchOptions();
            LoadData(options);
        }
    }

    SearchOptions GetSearchOptions()
    {
        SearchOptions options = new SearchOptions();
        options.IsDescending = true;
        int pageIndex = fn.Get<int>(Request.QueryString["page"]);
        if (pageIndex > 0)
            options.PageIndex = pageIndex - 1;
        if (!string.IsNullOrEmpty(Request.QueryString["type"]))
        {
            if ("sale".Equals(Request.QueryString["type"]))
                options.PropertyTypes.Add(PropertyType.Sale);
            else if ("rental".Equals(Request.QueryString["type"]))
                options.PropertyTypes.Add(PropertyType.Rental);
            else if ("commRent".Equals(Request.QueryString["type"]))
            {
                options.PropertyTypes.Add(PropertyType.Commercial);
                options.PropertyTypes.Add(PropertyType.Rental);
            }
            else if ("commSale".Equals(Request.QueryString["type"]))
            {
                options.PropertyTypes.Add(PropertyType.Commercial);
                options.PropertyTypes.Add(PropertyType.Sale);
            }
        }
        if (options.PropertyTypes.Count == 0)
        {
            options.PropertyTypes.Add(PropertyType.Sale);
            options.PropertyTypes.Add(PropertyType.Rental);
        }
        if ("recent".Equals(Request.QueryString["view"]))
        {
            options.RecentTransactions = true;
            options.OrderBy = SortOrder.DateModified;
            options.IsDescending = true;
            options.PageSize = 40;
        }
        return options;
    }
    void LoadData(SearchOptions options)
    {
        SetLinks(options);
        int commListingNum = 0;
        int listing = 0, total = 0;
        var commListings = data.GetCommercialListings(options, out commListingNum);
        var listings = data.GetListings(options, out listing);

        var active = this.Controls.Find<HyperLink>("activeLink") ?? new HyperLink();
        var pipe = this.Controls.Find<Literal>("pipeDivider") ?? new Literal();
        var recent = this.Controls.Find<HyperLink>("recentLink") ?? new HyperLink();
        if (!options.RecentTransactions && listings != null && listings.Count > 0)
        {
            this.Controls.DataBind<Listings>(listings);
            this.Controls.SetPagers(options.PageIndex + 1, options.PageSize, listing);
            listings.CreatePagingSession();
        }
        else if (!options.RecentTransactions && commListings != null && commListings.Count > 0)
        {
            this.Controls.DataBind<Listings>(commListings);
            this.Controls.SetPagers(options.PageIndex + 1, options.PageSize, commListingNum);
            commListings.CreatePagingSession();
        }
        else
            this.Controls.DataBind<Listings>(listings);
        total = listing + commListingNum;
        if (currentType.Equals("all") && total == 0)
        {
            if (!options.RecentTransactions && Request.QueryString["view"] != "all")
            {
                options.RecentTransactions = true;
                active.Visible = false;
                pipe.Visible = false;
                LoadData(options);
            }
            else
                showNav = false;
        }
        else
        {
            if (!HasRecentTransactions())
            {
                pipe.Visible = false;
                recent.Visible = false;

                //hide the recentTransactions table
                var rctl = this.Controls.Find<ListingsControl>("RecentTransactions1");
                if (rctl != null)
                    rctl.Visible = false;
            }
        }
    }

    private bool HasRecentTransactions()
    {
        var options = new SearchOptions();
        options.PropertyTypes.Add(PropertyType.Sale);
        options.PropertyTypes.Add(PropertyType.Rental);
        options.RecentTransactions = true;
        int total = 0;
        data.GetListings(options, out total);
        return total > 0;
    }

    void SetLinks(SearchOptions options)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["type"]))
            currentType = Request.QueryString["type"];
        if (!string.IsNullOrEmpty(Request.QueryString["view"]))
            currentView = Request.QueryString["view"];

        if (options.RecentTransactions) //override querystring
            currentView = "recent";

        var a = this.Controls.Find<HyperLink>("allLink") ?? new HyperLink();
        var s = this.Controls.Find<HyperLink>("salesLink") ?? new HyperLink();
        var r = this.Controls.Find<HyperLink>("rentalsLink") ?? new HyperLink();
        var cr = this.Controls.Find<HyperLink>("commRentLink") ?? new HyperLink();
        var cs = this.Controls.Find<HyperLink>("commSaleLink") ?? new HyperLink();
        string[] par = { "type", "page" };
        string basepath = Request.Path + Request.QueryString.ExcludeParameter(par);
        a.NavigateUrl = basepath + "&type=all";
        s.NavigateUrl = basepath + "&type=sale";
        r.NavigateUrl = basepath + "&type=rental";
        cr.NavigateUrl = basepath + "&type=commRent";
        cs.NavigateUrl = basepath + "&type=commSale";
        switch (currentType.ToLower())
        {
            case "sale":
                s.Enabled = false;
                s.CssClass = "disabled";
                break;
            case "rental":
                r.Enabled = false;
                r.CssClass = "disabled";
                break;
            case "commrent":
                cr.Enabled = false;
                cr.CssClass = "disabled";
                break;
            case "commsale":
                cs.Enabled = false;
                cs.CssClass = "disabled";
                break;
            default:
                a.Enabled = false;
                a.CssClass = "disabled";
                break;
        }

        //view links
        var active = this.Controls.Find<HyperLink>("activeLink") ?? new HyperLink();
        var recent = this.Controls.Find<HyperLink>("recentLink") ?? new HyperLink();
        basepath = Request.Path + Request.QueryString.ExcludeParameter("view");
        active.NavigateUrl = basepath + "&view=all";
        recent.NavigateUrl = basepath + "&view=recent";

        switch (currentView.ToLower())
        {
            case "recent":
                recent.Enabled = false;
                recent.CssClass = "disabled";
                active.Enabled = true;
                active.CssClass = "enabled";
                break;
            default:
                active.Enabled = false;
                active.CssClass = "disabled";
                break;
        }


        showRecent = "recent".Equals(currentView);
        showActive = !showRecent;
    }
}
