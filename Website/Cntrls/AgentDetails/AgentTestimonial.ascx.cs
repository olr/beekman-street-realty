﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class Cntrls_AgentTestimonial : System.Web.UI.UserControl, IRenderable<CompanyNewsList>
{
    protected CompanyNewsList list;
    protected void Page_Load(object sender, EventArgs e)
    {
        testimonial_list.DataSource = list;
        testimonial_list.DataBind();
    }

    public void DataBind(CompanyNewsList data)
    {
        list = data??new CompanyNewsList();
    }
}