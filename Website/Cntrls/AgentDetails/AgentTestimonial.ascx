﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgentTestimonial.ascx.cs"
    Inherits="Cntrls_AgentTestimonial" %>
    <script type="text/javascript">
        $(function () {
            //console.log('111');
            $('.testimonial_description').expander({ slicePoint: 30 });
        })
    </script>
<asp:ListView runat="server" ID="testimonial_list">
    <LayoutTemplate>
        <asp:Literal runat="server" ID="itemPlaceHolder"></asp:Literal>
    </LayoutTemplate>
    <ItemTemplate>
    <div class="date">
        <%#(Container.DataItem as CompanyNewsInfo).FileDate %>
    </div>
    <div class='title'>
       <%#(Container.DataItem as CompanyNewsInfo).Title %>
    </div>
     <div class='source'>
       <%#(Container.DataItem as CompanyNewsInfo).Source %>
    </div>
     <div class='testimonial_description'>
       <%#(Container.DataItem as CompanyNewsInfo).Description %>
    </div>
    </ItemTemplate>
</asp:ListView>
