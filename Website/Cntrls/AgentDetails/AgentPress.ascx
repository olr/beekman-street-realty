﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgentPress.ascx.cs" Inherits="cntrls_AgentDetails_AgentPress" %>
<asp:ListView runat="server" ID="list">
    <LayoutTemplate>
        <ul class="press-list">
            <li runat="server" id="itemPlaceHolder"></li>
        </ul>
    </LayoutTemplate>
    <ItemTemplate>
        <li>
            <div class="press-title"><%# Eval("Title")%></div>
            <div class="press-info"><%# Eval("Source")%> - <%# Eval("FileDate", "{0:MMM dd, yyyy}")%> <a href="<%# Eval("FileUrl") %>" target="_blank">Read Article</a></div>
        </li>
    </ItemTemplate>
</asp:ListView>
<p <%=Html.DisplayStyle(Press.Count==0)%>>
    Press information not available.
</p>