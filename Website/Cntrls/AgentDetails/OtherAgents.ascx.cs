﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_AgentDetails_OtherAgents : System.Web.UI.UserControl, IRenderable<BrokerInfo>
{
    protected IDictionary<string, string> Model;

    public void DataBind(BrokerInfo data)
    {
        Model = new Dictionary<string, string>();
        Model.Add("Select Agent", string.Empty);

        if (data != null)
        {
            AgentSearchOptions options = new AgentSearchOptions();
            int total;
            var agents = ServiceLocator.GetRepository().GetBrokers(options, out total);

            foreach (var agent in agents)
            {
                Model.Add(string.Format("{0} {1}", agent.FirstName, agent.LastName), agent.ID.ToString());
            }
        }
    }
}