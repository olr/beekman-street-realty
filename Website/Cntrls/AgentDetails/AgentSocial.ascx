﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgentSocial.ascx.cs" Inherits="cntrls_AgentDetails_AgentSocial" %>


<!--Kaman's Style-->
<a class="agents-facebook-icon column" href="<%=Model.FacebookUrl%>" <%=Html.DisplayStyle(!string.IsNullOrWhiteSpace(Model.FacebookUrl))%> target="_blank">
  <img src="images/clear.png"/>
</a>
&nbsp;
<a class="agents-linkedin-icon column" href="<%=Model.LinkedInUrl%>" <%=Html.DisplayStyle(!string.IsNullOrWhiteSpace(Model.LinkedInUrl))%> target="_blank">
    <img src="images/clear.png"/>
</a>
&nbsp;
<a class="agents-twitter-icon column" href="<%=Model.TwitterUrl%>" <%=Html.DisplayStyle(!string.IsNullOrWhiteSpace(Model.TwitterUrl))%> target="_blank">
    <img src="images/clear.png">
</a>

