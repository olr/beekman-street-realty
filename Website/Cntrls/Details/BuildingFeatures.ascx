﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BuildingFeatures.ascx.cs" Inherits="cntrls_Details_BuildingAmenities" %>
<div class="detail-container corners">
<div class="section" <%=Html.DisplayStyle(Model.GetBuildingFeatures().Count>0)%>>
    <div class="page-titles">Building Features</div>
    <div class='bftop10'>
            <%=Html.DataList(Display10ApartmentFeatures( Model.GetBuildingFeatures()), new { @class = "building-features clearfix" })%></div>
        <div class='bfrest' style="display: none;">
            <%=Html.DataList(rest, new { @class = "building-features clearfix" })%></div>
</div></div>

