﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Description.ascx.cs" Inherits="cntrls_Details_Description" %>
<script type="text/javascript">
    function DisplayHideFullDescription(el) {
        var shrt = $get('<%= ShortDescriptionPanel.ClientID%>');
        var full = $get('<%= LongDescriptionPanel.ClientID%>');
        if (el == 'more') {
            shrt.style.display = 'none';
            full.style.display = 'block';
        } else {
            shrt.style.display = 'block';
            full.style.display = 'none';
        }

    }

</script>
<div class="detail_description_container">
    <div class="detail_description_title">
        <asp:Label ID="DescriptionTitleLabel" runat="server" Text=""></asp:Label>
    </div>
    <div class="detail_description">
        <asp:Panel ID="ShortDescriptionPanel" runat="server">
            <asp:Label ID="ShortDescriptionLabel" runat="server" Text=""></asp:Label>&nbsp;<asp:LinkButton ID="MoreLinkButton" runat="server" Text="<%$ Resources:Details, MoreLinkButton %>" OnClientClick="javascript:DisplayHideFullDescription('more'); return false;" Visible="False"></asp:LinkButton>
        </asp:Panel>
        <asp:Panel ID="LongDescriptionPanel" runat="server" style="display:none">
            <asp:Label ID="LongDescriptionLabel" runat="server" Text=""></asp:Label>&nbsp;<asp:LinkButton ID="LessLinkButton" runat="server" Text="<%$ Resources:Details, LessLinkButton %>" OnClientClick="javascript:DisplayHideFullDescription('less'); return false;"></asp:LinkButton>
        </asp:Panel> 
    </div>
</div>
