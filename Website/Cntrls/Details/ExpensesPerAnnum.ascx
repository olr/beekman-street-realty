﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExpensesPerAnnum.ascx.cs" Inherits="cntrls_Details_ExpensesPerAnnum" %>
<div class="detail-container corners">
<div class="section" <%=Html.DisplayStyle(Model.GetExpenseDetail().Count>0)%>>
    <div class="page-titles">Expenses Per Annum</div>
    <%=Html.DataList(Model.GetExpenseDetail(), new { @class = "expense-details clearfix" })%>
    <div <%=Html.DisplayStyle(Model.GetFinancialSummary().Count>0)%>>
        <%=Html.DataList(Model.GetFinancialSummary(), new { @class = "expense-details clearfix" })%>
    </div>
</div>
</div>

