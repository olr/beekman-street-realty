﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_Details_IncomePerAnnum : BaseControl<cntrls_Details_IncomePerAnnum, ListingInfo>, IRenderable<ListingInfo>
{
    public void DataBind(ListingInfo data)
    {
        Visible = false;
        if (data != null)
        {
            Model = data;
            Visible = Model.GetIncomeDetail().Count > 0;
        }
    }
}