﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Web;
using BrokerTemplate.Core;

public partial class cntrls_Details_Paging : System.Web.UI.UserControl
{
    protected override void OnInit(EventArgs e)
    {
        List<PagingInfo> pages;
        SearchOptions c;
        int total;
        this.Visible = false;
        if (TempStorage.TryGet<List<PagingInfo>>("PagingInfo", out pages))
        {
            string id = Request.QueryString["id"];
            PagingInfo result = pages.Find(w => w.CurrentListingId == id);
            if (result != null)
            {
                int index = pages.FindIndex(w => w.CurrentListingId == id);
                if (TempStorage.TryGet<SearchOptions>("SearchOptions", out c))
                {
                    if (pages.Count >= c.PageSize && index == pages.Count - 1) //if we are at the last record on the list
                    {
                        //search forward
                        c.PageIndex++;
                        var list = ServiceLocator.GetSearchService().RunSearch(c, out total);
                        if (list.Count > 0)
                        {
                            pages.AddRange(list.GetPagingList(result.ListPage));
                            pages.Refresh();
                            result = pages.Find(w => w.CurrentListingId == id);
                            this.Visible = result != null;
                            TempStorage.Add("PagingInfo", pages);
                        }
                    }
                    else if (c.PageIndex > 0 && pages.Count > 0 && index == 0) //if we are at the first record
                    {
                        //search backward
                        c.PageIndex--;
                        var list = ServiceLocator.GetSearchService().RunSearch(c, out total);
                        if (list.Count > 0)
                        {
                            var temp = list.GetPagingList(result.ListPage);
                            temp.AddRange(pages);
                            pages = temp;
                            pages.Refresh();
                            result = pages.Find(w => w.CurrentListingId == id);
                            this.Visible = result != null;
                            TempStorage.Add("PagingInfo", pages);
                        }
                    }
                }
                this.Visible = true;
            }

            if (result != null)
            {
                NextImageButton.Visible = true;
                PreviousImageButton.Visible = true;
                if (!string.IsNullOrWhiteSpace(result.PreviousListingId))
                    PreviousImageButton.NavigateUrl = "~/detail.aspx?id=" + result.PreviousListingId;
                else
                    PreviousImageButton.Visible = false;
                if (!string.IsNullOrWhiteSpace(result.NextListingId))
                    NextImageButton.NavigateUrl = "~/detail.aspx?id=" + result.NextListingId;
                else
                    NextImageButton.Visible = false;
                ReturnImageButton.NavigateUrl = result.ListPage;

                //piping
                //prevDivider.Visible = PreviousImageButton.Visible;
                nextDivider.Visible = NextImageButton.Visible;

            }
        }
        
    }
}