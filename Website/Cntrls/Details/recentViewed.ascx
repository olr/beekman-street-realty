﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="recentViewed.ascx.cs"
    Inherits="Cntrls_Details_recentViewed" %>
<asp:ListView runat="server" ID="listingList">
    <LayoutTemplate>
        <ul class="recent-list clearfix">
            <li runat="server" id="itemPlaceHolder" />
        </ul>
    </LayoutTemplate>
    <ItemTemplate>
        <%--<li class="tb <%# GetClassName(Container.DataItem as Photo) %>">
                    <a href='<%# Eval("MediaUrl") %>'>
                        <img src='<%# Eval("MediaUrl") %>' title='<%# Eval("DisplayName") %>' alt='' />
                    </a>
                </li>--%>
                 <div class="recentlyviewed-container corners left">
        <li class="left">
            <div class="item-photo">
                <a href="detail.aspx?id=<%#(Container.DataItem as ListingInfo).ListingID %>">
                    <img class="preload" alt="" src="<%#(Container.DataItem as ListingInfo).GetDisplayPhoto() %>">
                </a>
            </div>
            <div class="item-address">
                <%#GetDisplayAddress(Container.DataItem as ListingInfo)%>
            </div>
            <div class="item-info">
                <div>
                    <div><%#GetDisplayAptSize(Container.DataItem as ListingInfo)%></div>
                <div>
                   <%#(Container.DataItem as ListingInfo).GetPriceRentText(true)%></div>
            </div>
        </li>
        </div>
    </ItemTemplate>
</asp:ListView>
