﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_Details_ListingInfo : System.Web.UI.UserControl, IRenderable<ListingInfo>
{
    protected ListingInfo data = new ListingInfo();

    public void DataBind(ListingInfo _data)
    {
        data = _data;
        this.DataBind();
    }
}