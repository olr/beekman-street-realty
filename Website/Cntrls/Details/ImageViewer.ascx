﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImageViewer.ascx.cs" Inherits="cntrls_Details_ImageViewer" %>

<asp:PlaceHolder runat="server" ID="galleryPanel">

<%--<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var galleries = $('.ad-gallery').adGallery({
            loader_image: 'images/loading_transparent.gif',
            start_at_index: 0,
            description_wrapper: false,
            thumb_opacity: 0.7,
            animate_first_image: false,
            animation_speed: 400,
            width: false,
            height: false,
            display_next_and_prev: true,
            display_back_and_forward: false,
            scroll_jump: 0, // If 0, it jumps the width of the container
            slideshow: { enable: false },
            enable_keyboard_move: true,
            cycle: false,
            callbacks: {
                init: false,
                afterImageVisible: false,
                beforeImageVisible: false
            }
        });

        <% if (MediaCount <= 1) { %>
            $('.ad-nav').hide();

        <% } %>
    });
</script>--%>

<%--<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var galleries = $('.ad-gallery').adGallery({
            loader_image: 'images/loading_transparent.gif',
            start_at_index: 0,
            description_wrapper: false,
            thumb_opacity: 0.7,
            animate_first_image: false,
            animation_speed: 400,
            width: false,
            height: false,
            display_next_and_prev: true,
            display_back_and_forward: false,
            scroll_jump: 0, // If 0, it jumps the width of the container
            slideshow: { enable: false },
            enable_keyboard_move: true,
            cycle: false,
            callbacks: {
                init: false,
                afterImageVisible: false,
                beforeImageVisible: false
            }
        });

        <% if (MediaCount <= 1) { %>
            $('.ad-nav').hide();

        <% } %>
    });
</script>
--%>

<div class="mediaviewer">
			<div class="media-mask">
				<div class="media-window clearfix" id="media-window">
					<div class="media-pane image">
						<div class="m-images-mask">
                        <div class="previmage"></div>
                        <div class="nextimage"></div>
							<div class="m-images-window" id="m-images-window">
                            <%=GeneratMainFrame()%>
									<%--<div class="m-slide"><img src="http://www.olr.com/Pictures/Interior_Photo/42805_int_photo6150426.jpg"/></div>
									<div class="m-slide"><img src="http://www.olr.com/Pictures/Interior_Photo/42805_int_photo6150427.jpg"/></div>
									<div class="m-slide"><img src="http://www.olr.com/Pictures/Interior_Photo/42805_int_photo6150428.jpg"/></div>
									<div class="m-slide"><img src="http://www.olr.com/Pictures/Interior_Photo/42805_int_photo6150429.jpg"/></div>
									<div class="m-slide"><img src="http://www.olr.com/Pictures/Interior_Photo/42805_int_photo6150430.jpg"/></div>
									<div class="m-slide"><img src="http://www.olr.com/Pictures/Interior_Photo/42805_int_photo6496960.jpg"/></div>
									<div class="m-slide"><img src="http://www.olr.com/Pictures/Interior_Photo/42805_int_photo6150431.jpg"/></div>
									<div class="m-slide"><img src="http://www.olr.com/Pictures/Interior_Photo/42805_int_photo6150432.jpg"/></div>
									<div class="m-slide"><img src="http://www.olr.com/Pictures/Interior_Photo/42712_int_photo6142376.jpg"/></div>
									<div class="m-slide"><img src="http://www.olr.com/Pictures/Interior_Photo/42712_int_photo6496980.jpg"/></div>
									<div class="m-slide"><img src="http://www.olr.com/Pictures/Interior_Photo/42712_int_photo6142378.jpg"/></div>
									<div class="m-slide"><img src="http://www.olr.com/Pictures/Interior_Photo/42712_int_photo6142371.jpg"/></div>--%>
							</div>
						</div>
						<div class="media-slider">
                             <asp:ListView runat="server" ID="thumbList">
            <LayoutTemplate>
							<a id="m-slider-left"></a>
                            <div class="m-slider-mask">
								<div class="m-slider-window clearfix" id="m-slider-window">
							<asp:Literal ID="itemPlaceHolder" runat="server"></asp:Literal>
                            	</div>
							</div>
							<a id="m-slider-right"></a>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <%--<%#GetID(Container.DataItem as Photo)%>--%>
									<div class="m-slider-thumb" id="<%#GetID(Container.DataItem as Photo)%>"><a><img src=" <%#(Container.DataItem as Photo).MediaUrl %>"></img></a></div>
									<%--<div class="m-slider-thumb" id="m-slider-thumb2"><a><img src="http://www.olr.com/Pictures/Interior_Photo/42805_int_photo6150427.jpg"></img></a></div>
									<div class="m-slider-thumb" id="m-slider-thumb3"><a><img src="http://www.olr.com/Pictures/Interior_Photo/42805_int_photo6150428.jpg"></img></a></div>
									<div class="m-slider-thumb" id="m-slider-thumb4"><a><img src="http://www.olr.com/Pictures/Interior_Photo/42805_int_photo6150429.jpg"></img></a></div>
									<div class="m-slider-thumb" id="m-slider-thumb5"><a><img src="http://www.olr.com/Pictures/Interior_Photo/42805_int_photo6150430.jpg"></img></a></div>
									<div class="m-slider-thumb" id="m-slider-thumb6"><a><img src="http://www.olr.com/Pictures/Interior_Photo/42805_int_photo6496960.jpg"></img></a></div>
									<div class="m-slider-thumb" id="m-slider-thumb7"><a><img src="http://www.olr.com/Pictures/Interior_Photo/42805_int_photo6150431.jpg"></img></a></div>
									<div class="m-slider-thumb" id="m-slider-thumb8"><a><img src="http://www.olr.com/Pictures/Interior_Photo/42805_int_photo6150432.jpg"></img></a></div>
									<div class="m-slider-thumb" id="m-slider-thumb9"><a><img src="http://www.olr.com/Pictures/Interior_Photo/42712_int_photo6142376.jpg"></img></a></div>
									<div class="m-slider-thumb" id="m-slider-thumb10"><a><img src="http://www.olr.com/Pictures/Interior_Photo/42712_int_photo6496980.jpg"></img></a></div>
									<div class="m-slider-thumb" id="m-slider-thumb11"><a><img src="http://www.olr.com/Pictures/Interior_Photo/42712_int_photo6142378.jpg"></img></a></div>
									<div class="m-slider-thumb" id="m-slider-thumb12"><a><img src="http://www.olr.com/Pictures/Interior_Photo/42712_int_photo6142371.jpg"></img></a></div>
							--%>
                            </ItemTemplate>
                            </asp:ListView>
						</div>  
					</div>
				<%--	<div class="media-pane image"></div>
					<div class="media-pane image"></div>
					<div class="media-pane image"></div>--%>
				</div>
			</div>
			
	</div>


<%--<div class="ad-gallery">
  <div class="ad-image-frame corners">
    <div class="ad-image-wrapper corners"></div>
  </div>
  <div class="ad-nav">
    <div class="ad-thumbs">
        <asp:ListView runat="server" ID="thumbList">
            <LayoutTemplate>
              <ul class="ad-thumb-list">
                <li runat="server" id="itemPlaceHolder" />
              </ul>
            </LayoutTemplate>
            <ItemTemplate>
                <li class="tb <%# GetClassName(Container.DataItem as Photo) %>">
                    <a href='<%# Eval("MediaUrl") %>'>
                        <img src='<%# Eval("MediaUrl") %>' title='<%# Eval("DisplayName") %>' alt='' />
                    </a>
                </li>
            </ItemTemplate>
        </asp:ListView>
    </div>
  </div>
</div>
--%>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="singlePanel" Visible="false">
<div class="ad-gallery">
    <div class="ad-image-frame">
        <div class="ad-image-wrapper default">
            <img runat="server" id="mainPhoto" src="~/images/picdetailsth.jpg" alt="" />
        </div>
    </div>
</div>
</asp:PlaceHolder>