﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IncomePerAnnum.ascx.cs" Inherits="cntrls_Details_IncomePerAnnum" %>
<div class="detail-container corners">
<div class="section" <%=Html.DisplayStyle(Model.GetIncomeDetail().Count>0)%>>
    <div class="page-titles">Income Per Annum Actual</div>
    <%=Html.DataList(Model.GetIncomeDetail(), new { @class = "income-details clearfix" })%>
</div>
</div>
