﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_Details_BrokerSummary : System.Web.UI.UserControl, IRenderable<ListingInfo>
{
    protected ListingInfo data = new ListingInfo();

    public void DataBind(ListingInfo _data)
    {
        if (_data != null)
        {
            data = _data;
            this.DataBind(false);
            this.Visible = !string.IsNullOrWhiteSpace(data.Description);
        }
    }
}