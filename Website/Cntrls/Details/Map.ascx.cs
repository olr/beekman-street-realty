﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Configuration;

public partial class Cntrls_Details_Map : BaseControl<Cntrls_Details_Map,ListingInfo>, IRenderable<ListingInfo>
{
    protected double Latitude { get; set; }
    protected double Longitude { get; set; }
    protected bool isExclusive = false;
    protected bool showAddress = false;

    public void DataBind(ListingInfo model)
    {
        Model = model;
        var building = model.GetBuilding();
        isExclusive = Model.IsExclusive;
        showAddress = Model.ShowAddress;
        if (building != null)
        {
            var APIKey = ConfigurationManager.AppSettings["GoogleAPIKey"];
            LocationPoint l = new LocationPoint { Address = string.Format("{0}, {1}, {2} {3}", building.Address, building.City, building.State, building.ZipCode) };
            if (SiteHelper.GeocodeAddress(l, APIKey))
            {
                Latitude = l.Latitude;
                Longitude = l.Longitude;
            }
        }

    }
}