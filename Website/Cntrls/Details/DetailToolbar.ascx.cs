﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_Details_DetailToolbar : BaseControl<cntrls_Details_DetailToolbar, ListingInfo>, IRenderable<ListingInfo>
{
    protected bool HasPhoto { get; set; }
    protected bool HasFloorplan { get; set; }
    protected bool HasVT { get; set; }
    protected bool HasVideo { get; set; }
    protected bool HasPDF { get; set; }
    protected bool HasOLRMax { get; set; }
    protected bool EmbVideo { get; set; }
    protected bool printChoice { get; set; }
    private Media media = null;

    public void DataBind(ListingInfo _data)
    {
        if (_data != null)
        {
            Model = _data;
            media = Model.GetMedia();
            if (media != null)
            {
                HasPhoto = media.Photos.Count > 0;
                HasFloorplan = media.Floorplans.Count > 0;
                HasVideo = media.Videos.Count > 0;
                HasVT = media.VirtualTours.Count > 0;
                HasPDF = media.OtherFiles.Count(x =>
                    x.MediaUrl.EndsWith("pdf", StringComparison.InvariantCultureIgnoreCase)) > 0;
                EmbVideo = IsEmbVideo();
                printChoice = HasPrintChoice();
            }
            if (_data.OLRMaxAlbumID > 0)
            {
                HasOLRMax = true;
                //HasPhoto = false; //do not show photo link
            }
        }
    }
    private bool HasPrintChoice()
    {
        string p=Model.GetPrintLink();
        if (p.Contains("PrintPlan.aspx")) 
            return true;
        else
        return false;
    }
    protected bool IsEmbVideo()
    {
        string v = Model.GetVideoLink();
        if (!string.IsNullOrEmpty(v))
            if (v.Contains("video.aspx?id="))
                return true;
        return false;
    }
    protected string GetPDFUrl()
    {
        if (media != null &&
            media.OtherFiles.Count(x => x.MediaUrl.EndsWith("pdf", StringComparison.InvariantCultureIgnoreCase)) > 0)
        {
            return
                media.OtherFiles.Where(x => x.MediaUrl.EndsWith("pdf", StringComparison.InvariantCultureIgnoreCase))
                                .ToList()[0].MediaUrl;
        }
        return string.Empty;
    }
}