﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_Details_Description : System.Web.UI.UserControl
{
    private const int DESCRP_LENGTH = 1000;
    public string DescriptionTitle { get; set; }
    public string Description { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        DescriptionTitleLabel.Text = DescriptionTitle;
        ShortDescriptionLabel.Text = SiteHelper.GetNotesDisplayString(Description, DESCRP_LENGTH );
        if (Description.Length > DESCRP_LENGTH)
        {
            MoreLinkButton.Visible = true;
            LongDescriptionLabel.Text = Description;
        }
    }
}