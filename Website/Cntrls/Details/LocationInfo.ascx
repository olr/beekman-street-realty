﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LocationInfo.ascx.cs" Inherits="cntrls_Details_LocationInfo" %>
<div class="detail_top_info_left">
    <div>
        <asp:Label ID="NeighborhoodLabel" runat="server" CssClass="borough_neighborhood" Text=""></asp:Label>&nbsp;
        (<asp:Label ID="CrossStreetsLabel" runat="server" Text=""></asp:Label>)
    </div>
    <div>
        <asp:Label ID="PropertyServiceLevelLabel" runat="server" Text=""></asp:Label>
    </div>
</div>
