﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GeneralDetails.ascx.cs" Inherits="cntrls_Details_ApartmentDetail" %>
<div class="detail-container corners">
<div class="section">
    <div class="page-titles">General Details</div>
    <%=Html.DataList(Model.GetPropertyDetail(), new { @class = "property-details clearfix" })%>
</div>
</div>