﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class Cntrls_Details_ImageViewerForBuilding : System.Web.UI.UserControl, IRenderable<BuildingInfo>
{
    protected BuildingInfo data = new BuildingInfo();
    protected int MediaCount { get; set; }

    public void DataBind(BuildingInfo _data)
    {
        data = _data;

        Photos media = data.GetMedia().Photos;
        if (media.Count == 0)
        {
            if (string.IsNullOrEmpty(data.PhotoUrl))
                media.Add(new Photo { MediaUrl = Resources.Media.NoListingPhoto }); //add default photo
            else
                media.Add(new Photo { MediaUrl = data.GetBuildingPhotos()}); //add default photo
        }

        thumbList.DataSource = media;
        thumbList.DataBind();

        MediaCount = media.Count;
    }

    private int count = 0;
    protected string GetClassName(Photo obj)
    {
        count++;
        string className = string.Empty;
        if (count % 4 == 1)
            className = "first";
        else if (count % 4 == 0)
            className = "last";
        return className + " " + count;
    }
}

