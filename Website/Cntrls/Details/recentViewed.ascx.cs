﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Web;
using BrokerTemplate.Core;

public partial class Cntrls_Details_recentViewed : BaseControl<Cntrls_Details_recentViewed, List<RecentlyViewListings>>, IRenderable<List<RecentlyViewListings>>
{
    protected List<ListingInfo> list;
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void DataBind(List<RecentlyViewListings> data)
    {
        Model = data ?? new List<RecentlyViewListings>();
        Model.Reverse();
        list = new List<ListingInfo>();
        ListingInfo l;
        if (Model.Count > 0)
        {
            if (Model.Count > 6)
                Model.RemoveRange(6, Model.Count -6);//only leave 6 recent viewed listings
            foreach (var listing in Model)
            {
                l = ServiceLocator.GetRepository().GetListingInfoByID(listing.ListingId) ?? ServiceLocator.GetRepository().GetRecentListingInfoByID(listing.ListingId);
                list.Add(l);
            }
        }
            
        listingList.DataSource = list;
        listingList.DataBind();
    }
    public string GetDisplayAddress(ListingInfo info)
    {
        string display = info.GetDisplayAddress();
        if (display != info.Neighborhood)
            display += "<br/>" + info.Neighborhood;
        return display;
    }
    public string GetDisplayAptSize(ListingInfo info)
    {
        string display = "<div>" + info.GetListingBedroom() + "</div>";

        display += "<div>" + info.GetListingBathroom() + "</div>";
        return display;
    }
}