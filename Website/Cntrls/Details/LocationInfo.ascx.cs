﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_Details_LocationInfo : System.Web.UI.UserControl
{
    public string CrossStreet1 { get; set; }
    public string CrossStreet2 { get; set; }
    public string Neighborhood { get; set; }
    public string PropertyType { get; set; }
    public string BuildingType { get; set; }
    public string ServiceLevel { get; set; }
    public string Area { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        CrossStreetsLabel.Text = CorrectCrossStreet(CrossStreet1 + " and " + CrossStreet2);
        NeighborhoodLabel.Text = (string.IsNullOrWhiteSpace(Area) ? Area : Area +", " ) + Neighborhood;
        PropertyServiceLevelLabel.Text = PropertyType.Replace(";", string.Empty);
        if (string.IsNullOrWhiteSpace(PropertyType))
            PropertyServiceLevelLabel.Text = ServiceLevel;
        else
            PropertyServiceLevelLabel.Text += ", " + ServiceLevel;
        if (!string.IsNullOrWhiteSpace(PropertyServiceLevelLabel.Text.Trim()))
        {
            if (!string.IsNullOrWhiteSpace(BuildingType))
                PropertyServiceLevelLabel.Text += ", " + BuildingType.Replace(";", ", ");
        }
        else
        {
            if (!string.IsNullOrWhiteSpace(BuildingType))
                PropertyServiceLevelLabel.Text = BuildingType.Replace(";", ", ");
        }
    }

    private string CorrectCrossStreet(string value)
    {
        string[] results = value.Split(' ');
        for (int i = 0; i < results.Length; i++)
        {
            switch (results[i].ToLower())
            {
                case "street":
                    results[i] = "St";
                    break;
                case "avenue":
                    results[i] = "Ave";
                    break;
                case "road":
                    results[i] = "Rd";
                    break;
                case "blvd":
                case "boulevard":
                    results[i] = "Blvd";
                    break;
            }
        }

        return string.Join(" ",results);
    }

    
}