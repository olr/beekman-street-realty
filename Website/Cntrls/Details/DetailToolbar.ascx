﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DetailToolbar.ascx.cs"
    Inherits="cntrls_Details_DetailToolbar" %>
<ul class="detail-toolbar tabular clearfix">
    <li class="photos-icon cursor icon button-image" style="display: <%=HasPhoto?"block":"none"%>"><a class="photolink tip "
        href="<%= Model.GetAllPhotoLink() %>" title="View All Photos">
         <img class="swapImage {src: 'images/clear.png'}" src="images/clear.png"  /></a> </li>
    <li class="floorplan-icon cursor icon button-image" style="display: <%=HasFloorplan?"block":"none"%>"><a class="tip"
        href="<%= Model.GetFloorplanLink() %>" title="View Floorplan" target="_blank">
         <img class="swapImage {src: 'images/clear.png'}" src="images/clear.png" /></a> </li>
    <li style="display: <%=HasOLRMax?"block":"none"%>">
        <div class="olrmax-wrapper tip" title="Full Screen Photos">
            <%= Model.GetOLRMaxScript() %>
        </div>
    </li>
    <li class="video-icon cursor icon button-image" style="display: <%=HasVideo?"block":"none"%>">
    <%if (EmbVideo)
      { %>
    <a class="videolink tip "
        href="<%= Model.GetVideoLink() %>" title="View Video">
        <img class="swapImage {src: 'images/clear.png'}" src="images/clear.png" /></a><%}
      else
      { %>
        <a class="tip"
        href="<%= Model.GetVideoLink() %>" title="View Video" target="_blank">
        <img class="swapImage {src: 'images/clear.png'}" src="images/clear.png" /></a><%} %>
    </li>
    <li class="email-icon cursor icon button-image"><a class=" emailfriend tip " href="<%= Model.GetEmailFriendLink() %>"
        title="Email Listing">
        <img class="swapImage {src: 'images/clear.png'}" src="images/clear.png"/></a> </li>
    <li class="print-icon cursor icon button-image">
    <%if (printChoice)
      { %>
    <a target="_blank" class="printbutton resize tip" href="<%= Model.GetPrintLink() %>"
        title="Print Listing"><%}
      else
      { %>
    <a target="_blank" class="printbutton tip" href="<%= Model.GetPrintLink() %>"
        title="Print Listing"><%} %>
        <img class="swapImage {src: 'images/clear.png'}" src="images/clear.png" /></a> </li>
    <li class="map-icon cursor icon button-image"><a class=" map tip "href="<%= Model.GetMapLink() %>" title="View Map">
        <img class="swapImage {src: 'images/clear.png'}" src="images/clear.png" /></a> </li>
    <li class="calculator-icon cursor icon button-image" style="display: <%=Model.Price>0?"block":"none"%>"><a class=" calclink tip"
        href="<%= Model.GetMortgageCalculatorLink() %>" title="Mortgage Calculator">
        <img class="swapImage {src: 'images/clear.png'}" src="images/clear.png" /></a> </li>
    <li class="pdf-icon cursor icon button-image" style="display: <%=HasPDF?"block":"none"%>"><a class=" pdflink tip" target="_blank" href="<%= GetPDFUrl() %>"
        title="View PDF">
        <img class="swapImage {src: 'images/clear.png'}" src="images/clear.png" /></a> </li>
    <li class="share-icon cursor">
        <!-- AddThis Button BEGIN -->
        <%--<div class="addthis_toolbox addthis_default_style share-icon cursor icon button-image">
            <a href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4cfe8dba00125a11"
                class="addthis_button_compact">
               <img class="swapImage {src: 'images/clear.png'}" src="images/clear.png" /></a>
        </div>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4cfe8dba00125a11"></script>--%>
        <!-- AddThis Button END -->
     <div class="button-image left">
          
                <span class='st_sharethis' >
                <img class="swapImage {src: 'images/clear.png'}" src="images/clear.png" /></span>
          
            </div>
             <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    </li>
</ul>
