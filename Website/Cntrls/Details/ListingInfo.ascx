﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListingInfo.ascx.cs" Inherits="cntrls_Details_ListingInfo" %>
<!-- LISTING INFO START -->

<div class="detail_titles">
    <img src="~/images/listingDetails.png" runat="server" alt="" />
</div>

<ul class="tabular listingInfo clearfix">
    <li><span class="label"><%# Resources.Details.WebIDLabel %></span> <%# data.ListingID %></li>
    <li><span class="label"><%# Resources.Details.OwnershipLabel %></span> <%# data.Ownership %></li>
    <li><span class="label"><%# Resources.Details.NeighborhoodLabel %></span> <%# data.GetArea() %></li>
    <li><span class="label"><%# data.GetPriceRentLabel() %></span> <%# data.GetPriceRentText(false) %> <%# data.BuildIsNoFee() %> </li>
</ul>
<div class="detail_divider"></div>

<!-- LISTING INFO END -->

