﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApartmentFeatures.ascx.cs"
    Inherits="cntrls_Details_AptFeatures" %>
<div class="detail-container corners">
    <div class="section" <%=Html.DisplayStyle(Model.GetApartmentFeatures().Count>0)%>>
        <div class="page-titles">
            Apartment Features</div>
        <div class='aftop10'>
            <%=Html.DataList(Display10ApartmentFeatures( Model.GetApartmentFeatures()), new { @class = "building-features clearfix" })%></div>
        <div class='afrest' style="display: none;">
            <%=Html.DataList(rest, new { @class = "building-features clearfix" })%></div>
    </div>
</div>
