﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="yelp.ascx.cs" Inherits="Cntrls_Details_yelp" %>
<script type="text/javascript">
    $(function () {
        $('.poi').click(function () {
            getTerm("restaurant");
        });
    })
    function getTerm(term) {
        // alert(term);
        getYelp(term);
    }

    function getYelp(term) {
        var auth = {
            consumerKey: "5YzY5baGaxu_yy_77v67qQ",
            consumerSecret: "OFFfMbJuXBesyxEz3qxDH2XIpsA",
            accessToken: "l1fHKqNk0Y8RmpZFIGylR4wLSg_S1ivx",
            accessTokenSecret: "2Vug11rMld5KFe_8RqfHq2lbPTc",
            serviceProvider: {
                signatureMethod: "HMAC-SHA1"
            }
        };
        var terms = term;
        var near = '<%=Model.Address+" "+Model.Neighborhood+" "+Model.City %>'; //'50 Broad Street,New York City'; // location; // 
        var accessor = {
            consumerSecret: auth.consumerSecret,
            tokenSecret: auth.accessTokenSecret
        };

        parameters = [];
        parameters.push(['term', terms]);
        parameters.push(['location', near]);
        parameters.push(['callback', 'cb']);
        parameters.push(['oauth_consumer_key', auth.consumerKey]);
        parameters.push(['oauth_consumer_secret', auth.consumerSecret]);
        parameters.push(['oauth_token', auth.accessToken]);
        parameters.push(['oauth_signature_method', 'HMAC-SHA1']);

        var message = {
            'action': 'http://api.yelp.com/v2/search',
            'method': 'GET',
            'parameters': parameters
        };

        OAuth.setTimestampAndNonce(message);
        OAuth.SignatureMethod.sign(message, accessor);

        var parameterMap = OAuth.getParameterMap(message.parameters);

        var handleReturndata = function (data) {
            if (data && data.businesses) {
                var name = new Array();
                var category = new Array();
                var ratingImg = new Array();
                var url = new Array();
                $.each(data.businesses, function (i) {
                    name[i] = this.name;
        //            if (this.categories.count >= 1) {
                        if (this.categories[0].toString().indexOf(",") == -1) {
                            category[i] = this.categories[0];
                        }
                        else {
                            var c = this.categories[0].toString().split(',');
                            category[i] = c[0];
                        }
           //         }
//                    else
//                        category[i] = " ";
                    ratingImg[i] = this.rating_img_url_small;
                    url[i] = this.url;
                });
                var ids = new Array();
                for (var i = 0; i < 6; i++) {
                    ids[i] = "item" + i;
                }
                for (var i = 0; i < 6&&i<ids.length; i++) {
                    $('tr#' + ids[i] + '>td.name>a').attr('href', url[i]).text(name[i]);
                    $('tr#' + ids[i] + '>td.rating>a').attr('href', url[i])
                    $('tr#' + ids[i] + ' img').attr('src', ratingImg[i]);
                    $('tr#' + ids[i] + '>td.middle').html(category[i].toString());
                }
            }
        };
        $.ajax({
            'url': message.action,
            'data': parameterMap,
            'dataType': 'jsonp',
            'jsonpCallback': 'cb',
            'success': handleReturndata
        });
    }
</script>
<div id="yelp-searchbox">
    <ul class="yelp clearfix">
        <li><a id="restTab" href="javascript:getTerm('restaurant')">Restaurants</a></li>
        <li><a id="barTab" href="javascript:getTerm('Bars&nightlife')">Bars &amp; Nightlife</a></li>
        <li class="link-padding"><a id="shopTab" href="javascript:getTerm('shopping')">Shopping</a></li>
        <li><a id="healthTab" href="javascript:getTerm('Health&Fitness')">Health &amp; Fitness</a></li>
    </ul>
<%--    <hr />
   --%> <div id="shopping" class="yelp" style="">
        <table class="reviews">
            <tr class="yelp" id='item1'>
                <td class="name"><a href="#" target="_blank"></a></td>
                <td class="middle"></td>
                <td class="rating"><a href="#"><img alt="Rating" src=""><br /></a></td>
            </tr>
            <tr class="yelp" id='item2'>
                <td class="name"><a href="#" target="_blank"></a></td>
                <td class="middle"></td>
                <td class="rating"><a href="#" target="_blank"><img alt="Rating" src="" /><br /></a></td>
            </tr>
            <tr class="yelp" id='item3'>
                <td class="name"><a href="#" target="_blank"></a></td>
                <td class="middle"></td>
                <td class="rating"><a href="#" target="_blank"><img alt="Rating" src=""><br/></a></td>
            </tr>
            <tr class="yelp" id='item4'>
                <td class="name"><a href="#" target="_blank"></a></td>
                <td class="middle"></td>
                <td class="rating"><a href="#" target="_blank"><img alt="Rating" src="" /><br /></a></td>
            </tr>
            <tr class="yelp" id='item5'>
                <td class="name"><a href="#" target="_blank"></a></td>
                <td class="middle"></td>
                <td class="rating"><a href="#" target="_blank"><img alt="Rating" src="" /><br /></a></td>
            </tr>
        </table>
    </div>
</div>
