﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_Details_ImageViewer : System.Web.UI.UserControl, IRenderable<ListingInfo>
{
    protected ListingInfo data = new ListingInfo();
    protected int MediaCount { get; set; }
    Photos media { get; set; }
    public void DataBind(ListingInfo _data)
    {
        data = _data;

        media = data.GetMedia().Photos;
        if (media.Count == 0)
        {
            if (string.IsNullOrEmpty(data.BuildingPhotoUrl))
                media.Add(new Photo { MediaUrl = Resources.Media.NoListingPhoto }); //add default photo
            else
                media.Add(new Photo { MediaUrl = data.GetDisplayPhoto() }); //add default photo
        }
        if (!data.ShowBuildingPhoto && media[0].MediaUrl.Contains("www.olr.com/BuildingPhoto/"))
            media[0] = new Photo { MediaUrl = Resources.Media.NoListingPhoto };
        thumbList.DataSource = media;
        thumbList.DataBind();

        MediaCount = media.Count;
    }

    private int count = 0;
    protected string GetID(Photo obj)
    {
        count++;
        string className = "m-slider-thumb";
        return className + count;
    }

    protected string GeneratMainFrame()
    {
        string result = string.Empty;
        if (MediaCount == 1)
            return "<div class='m-slide'><img src='" + media[0].MediaUrl + "'/></div>";
        else
        {
            foreach (var photo in media)
            {
                if(!string.IsNullOrEmpty(photo.DisplayName))
                    result += "<div class='m-slide'><img src='" + photo.MediaUrl + "'/><p class='m-slide-caption'>"+photo.DisplayName+"</p></div>";
                else
                    result += "<div class='m-slide'><img src='" + photo.MediaUrl + "'/></div>";
            }
            return result;
        }
    }
}