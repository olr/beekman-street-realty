﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class Cntrls_Details_yelp : BaseControl<Cntrls_Details_yelp, ListingInfo>, IRenderable<ListingInfo>
{
    public void DataBind(ListingInfo data) {
        Model = data;
    }
}