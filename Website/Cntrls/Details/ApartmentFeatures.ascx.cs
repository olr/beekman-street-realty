﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_Details_AptFeatures : BaseControl<cntrls_Details_AptFeatures, ListingInfo>, IRenderable<ListingInfo>
{
    protected IList<string> l10, rest,all;
    public void DataBind(ListingInfo data)
    {
        Visible = false;
        if (data != null)
        {
            Model = data;
            Visible = Model.GetApartmentFeatures().Count > 0;
        }
    }
    /**/
    public IList<string> Display10ApartmentFeatures(IList<string> list)
    {
        l10 = new List<string>();
        rest = new List<string>();
        all = list;
        if (list.Count <= 10)
            return list;
        else
        {
            int i = 0;
            foreach (var af in list)
            {
                if (i++ < 10)
                    l10.Add(af);
                else
                    rest.Add(af);
            }
            l10.Add("<a href='#' class='afshowAll'>[MORE]</a>");
            rest.Add("<a href='#' class='afshow10'>[HIDE]</a>");
            return l10;
        }
    }
    
}