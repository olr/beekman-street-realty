﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IDXLogo.ascx.cs" Inherits="cntrls_Details_IDXLogo" %>

<div class="idxlogo" id="idxlogo" style="display:<%#data.IsIDXListing?"block":"none"%>">
    <img width="100" height="40" border="0" src="images/mls_idx.gif" alt="" />
    <div class="idxCompany"><%=companyName%></div>
    <div class="idxAgent">Listing Agent <%=agentName%></div>
    <div class="idxCopy">&copy; <%=DateTime.Today.Year.ToString()%> Manhattan MLS, Inc.</div>
    <div class="idxCopy">All Rights Reserved</div>
</div>