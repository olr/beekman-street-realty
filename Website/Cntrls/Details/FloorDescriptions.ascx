﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FloorDescriptions.ascx.cs" Inherits="cntrls_Details_FloorDescriptions" %>
<div class="detail-container">
<div class="section" <%=Html.DisplayStyle(Model.GetFloorByFloorDescription().Count>0)%>>
    <div class="page-titles">Floor By Floor Description</div>
               
    <%=Html.DataList(Model.GetFloorByFloorDescription(), new { @class = "floor-descriptions clearfix" })%>
</div>
</div>