﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;

public partial class cntrls_Details_IDXLogo : System.Web.UI.UserControl, IRenderable<ListingInfo>
{
    protected ListingInfo data = new ListingInfo();
    protected string companyName = string.Empty;
    protected string agentName = string.Empty;


    public void DataBind(ListingInfo _data)
    {
        data = _data;
        this.Visible = false;
        if (data != null)
        {
            if (data.IsIDXListing)
            {
                string[] info = data.CompanyName.Split(new char[] { '|' });
                if (info != null && info.Length == 2)
                {
                    companyName = info[0];
                    agentName = info[1];
                }
                else
                    companyName = data.CompanyName;
            }
            this.Visible = data.IsIDXListing;
        }
        this.DataBind(false);
    }
}