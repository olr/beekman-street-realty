﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContactInfo.ascx.cs" Inherits="cntrls_Details_ContactInfo" %>
<asp:ListView runat="server" ID="contacts">
    <LayoutTemplate>
        <div class="listing-contacts">
            <asp:Literal runat="server" ID="itemPlaceHolder" />
        </div>
    </LayoutTemplate>
    <ItemTemplate>
    <!--2 Column Layout-->
        <div class="contact clearfix">
            <div class="agent-thumbnail left">
            <a href="<%# (Container.DataItem as BrokerInfo).GetDetailLink() %>" class="green">
                <img id="Img1" runat="server" src='<%# Eval("PhotoUrl") %>' alt="" class="agent-photo" />
            </a>
            </div>
            <div class="agent-info-container right">
            <div class="agent-name">
                <a href="<%# (Container.DataItem as BrokerInfo).GetDetailLink() %>" class="green">
                    <%# Eval("FirstName") %> <%# Eval("LastName") %>
                </a>
            </div>
            <div class="agent-phone" <%# Html.DisplayStyle(Eval("WorkPhone","{0}").Length>1) %>>
                <span class="label">O:</span> <%# Eval("WorkPhone") %>
            </div>
            <div class="agent-phone" <%# Html.DisplayStyle(Eval("MobilePhone", "{0}").Length > 1)%>>
                <span class="label">M:</span> <%# Eval("MobilePhone")%>
            </div>
            
            <div class="agent-actions">
                <div>
                    <a class="email-me" href="<%# (Container.DataItem as BrokerInfo).GetEmailMeLink(Model) %>">
                        Email Me
                    </a>
                </div>
                <div>
                    <a class="schedule-viewing" href="<%# (Container.DataItem as BrokerInfo).GetScheduleViewingLink(Model) %>">
                        Schedule A Viewing
                    </a>
                </div>
                <div>
                    <a href="<%# (Container.DataItem as BrokerInfo).GetVCardLink() %>">
                        Download Contact
                    </a>
                </div>
            </div>
        </div>
        </div>

        <!--3 Column Layout-->
        <%--  <div class="contact clearfix">
            <div class="agent-name">
                <a href="<%# (Container.DataItem as BrokerInfo).GetDetailLink() %>" class="green">
                    <%# Eval("FirstName") %> <%# Eval("LastName") %>
                </a>
            </div>
            <div class="agent-thumbnail">
            <a href="<%# (Container.DataItem as BrokerInfo).GetDetailLink() %>" class="green">
                <img id="Img2" runat="server" src='<%# Eval("PhotoUrl") %>' alt="" class="agent-photo" />
            </a>
            </div>
            <div class="agent-info-container">
            <div class="agent-phone" <%# Html.DisplayStyle(Eval("WorkPhone","{0}").Length>1) %>>
                <span class="label">O:</span> <%# Eval("WorkPhone") %>
            </div>
            <div class="agent-phone" <%# Html.DisplayStyle(Eval("MobilePhone", "{0}").Length > 1)%>>
                <span class="label">M:</span> <%# Eval("MobilePhone")%>
            </div>
            
            <div class="agent-actions">
                <div class="detail-contact-button cursor left">
                    <a class="email-me" href="<%# (Container.DataItem as BrokerInfo).GetEmailMeLink(Model) %>">
                        Email Me
                    </a>
                </div>
                <div class="detail-contact-button cursor left">
                    <a class="schedule-viewing" href="<%# (Container.DataItem as BrokerInfo).GetScheduleViewingLink(Model) %>">
                        Schedule A Viewing
                    </a>
                </div>
                <div class="detail-contact-button cursor left">
                    <a href="<%# (Container.DataItem as BrokerInfo).GetVCardLink() %>">
                        Download Contact
                    </a>
                </div>
            </div>
        </div>
        </div>--%>
    </ItemTemplate>
    <ItemSeparatorTemplate>
        <hr />
    </ItemSeparatorTemplate>
</asp:ListView>
