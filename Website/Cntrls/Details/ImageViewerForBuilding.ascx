﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImageViewerForBuilding.ascx.cs" Inherits="Cntrls_Details_ImageViewerForBuilding" %>
<asp:PlaceHolder runat="server" ID="galleryPanel">

<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var galleries = $('.ad-gallery').adGallery({
            loader_image: 'images/loading_transparent.gif',
            start_at_index: 0,
            description_wrapper: false,
            thumb_opacity: 0.7,
            animate_first_image: false,
            animation_speed: 400,
            width: false,
            height: false,
            display_next_and_prev: true,
            display_back_and_forward: false,
            scroll_jump: 0, // If 0, it jumps the width of the container
            slideshow: { enable: false },
            enable_keyboard_move: true,
            cycle: false,
            callbacks: {
                init: false,
                afterImageVisible: false,
                beforeImageVisible: false
            }
        });

        <% if (MediaCount <= 1) { %>
            $('.ad-nav').hide();

        <% } %>
    });
</script>


<div class="ad-gallery">
  <div class="ad-image-frame corners">
    <div class="ad-image-wrapper"></div>
  </div>
  <div class="ad-nav">
    <div class="ad-thumbs">
        <asp:ListView runat="server" ID="thumbList">
            <LayoutTemplate>
              <ul class="ad-thumb-list">
                <li runat="server" id="itemPlaceHolder" />
              </ul>
            </LayoutTemplate>
            <ItemTemplate>
                <li class="tb <%# GetClassName(Container.DataItem as Photo) %>">
                    <a href='<%# Eval("MediaUrl") %>'>
                        <img src='<%# Eval("MediaUrl") %>' title='<%# Eval("DisplayName") %>' alt='' />
                    </a>
                </li>
            </ItemTemplate>
        </asp:ListView>
    </div>
  </div>
</div>

</asp:PlaceHolder>