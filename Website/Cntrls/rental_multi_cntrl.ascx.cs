﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_default_rental_multi_cntrl : BaseControl<cntrls_default_rental_multi_cntrl, string>, IRenderable<string>
{
    protected ListingInfo rentListing;
    public int rentCounter = 0;
    public void DataBind(string data)
    {
        Model = data;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        rentListing = ServiceLocator.GetRepository().GetListingInfoByID(Model);
    }
    public string GetBedroomTag(ListingInfo l)
    {
        int beds = l.Bedrooms;
        if (beds == 1)
            return " Bedroom";
        else
            return " Bedrooms";
    }
}