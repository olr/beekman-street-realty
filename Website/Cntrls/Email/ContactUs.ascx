﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContactUs.ascx.cs" Inherits="cntrls_Email_ContactUs" %>

<form runat="server">
<div id="formPanel" runat="server" visible="true">
    <table border="0" cellpadding="0">
        <tr>
            <td class="contact_titles">
                I am a&nbsp;&nbsp;
            </td>
            <td>
                <asp:RadioButtonList CssClass="radio" ID="CustomerType" runat="server" RepeatDirection="Horizontal"
                    CellPadding="3" RepeatColumns="5">
                    <asp:ListItem Text="Renter" Value="Renter" Selected="True" />
                    <asp:ListItem Text="Buyer" Value="Buyer" />
                    <asp:ListItem Text="Seller" Value="Seller" />
                    <asp:ListItem Text="Landlord" Value="Landlord" />
                    <asp:ListItem Text="Other" Value="Other" />
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    <div style="margin-top: 10px; margin-bottom: 10px;" class="contact_titles">
        How did you hear of us?
    </div>
    <div class="form clearfix">
        <%--        <table style="width: 70%;">
            <tr>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Through a friend or co-worker " />
                    Friend or co-worker
                </td>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Through a family member" />
                    Family member
                </td>
            </tr>
            <tr>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Through an agent" />
                    A
                    Base 2.0
                    agent
                </td>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="In a newspaper" />
                    Newspaper
                </td>
            </tr>
            <tr>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="In a magazine" />
                    Magazine
                </td>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Google" />
                    Google
                </td>
            </tr>
            <tr>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="On-Line Residential" />
                    On-Line Residential
                </td>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Streeteasy" />
                    Streeteasy
                </td>
            </tr>
            <tr>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Yahoo" />
                    Yahoo
                </td>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Craigslist" />
                    Craigslist
                </td>
            </tr>
            <tr>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Somewhere on the web" />
                    Other Website
                </td>
                <td style="width: 50%;">
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Other" />
                    Other
                </td>
            </tr>
        </table>--%>
<%--        <div style="margin: 10px 0 3px 0;">
    If Other, please specify
</div>
<asp:TextBox ID="TextBox1" runat="server" Style="width: 303px" />
<div>
    <table cellspacing="0" class="contact-form">
        <tr>
            <td colspan="2">
                <div class="contact_titles">
                    Contact information
                </div>
            </td>
        </tr>
        <tr>
            <td style="width: 30%;">
                First Name
            </td>
            <td style="width: 70%;">
                <asp:TextBox ID="TextBox2" runat="server" Style="width: 197px" />
                <asp:RequiredFieldValidator ControlToValidate="FirstName" ID="RequiredFieldValidator4"
                    runat="server" ErrorMessage="*" />
            </td>
        </tr>
        <tr>
            <td style="width: 30%;">
                Last Name
            </td>
            <td style="width: 70%;">
                <asp:TextBox ID="TextBox3" runat="server" Style="width: 197px" />
                <asp:RequiredFieldValidator ControlToValidate="LastName" ID="RequiredFieldValidator5"
                    runat="server" ErrorMessage="*" />
            </td>
        </tr>
        <tr>
            <td style="width: 30%;">
                Email
            </td>
            <td style="width: 70%;">
                <asp:TextBox ID="TextBox4" runat="server" Style="width: 197px" />
                <asp:RequiredFieldValidator ControlToValidate="Email" ID="RequiredFieldValidator6"
                    runat="server" ErrorMessage="*" />
                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="Email" EnableClientScript="true"
                    ID="RegularExpressionValidator4" runat="server" ErrorMessage="*" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
            </td>
        </tr>
        <tr>
            <td style="width: 30%;">
                Mobile Phone
            </td>
            <td style="width: 70%;">
                <asp:TextBox ID="TextBox5" class="mobilephone" runat="server" Style="width: 197px" />
                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="MobilePhone"
                    EnableClientScript="true" ID="RegularExpressionValidator5" runat="server" ErrorMessage="*"
                    ValidationExpression="1?\s*\W?\s*([2-9][0-8][0-9])\s*\W?\s*([2-9][0-9]{2})\s*\W?\s*([0-9]{4})(\se?x?t?(\d*))?" />
            </td>
        </tr>
        <tr>
            <td style="width: 33%;">
                Alternate Phone
            </td>
            <td style="width: 70%;">
                <asp:TextBox ID="TextBox6" class="altphone" runat="server" Style="width: 197px" />
                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="AltPhone" EnableClientScript="true"
                    ID="RegularExpressionValidator6" runat="server" ErrorMessage="*" ValidationExpression="1?\s*\W?\s*([2-9][0-8][0-9])\s*\W?\s*([2-9][0-9]{2})\s*\W?\s*([0-9]{4})(\se?x?t?(\d*))?" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="contact_titles">
                    Comments
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox ID="TextBox7" TextMode="MultiLine" runat="server" Width="297" Height="100" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-top: 10px;">
                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="SubmitButton_Click">
                   <div class="submit-button button-image"> <img src="images/clear.png" /></div>
                </asp:LinkButton>
            </td>
        </tr>
    </table>
</div>
--%>    

    <!--Kaman's Style Here-->
        <div class="column-left left">
            <ul>
                <li>
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Through a friend or co-worker " />
                    Friend or co-worker </li>
                <li>
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Through an agent" />
                    A Beekman Street Realty agent</li>
                <li>
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="In a magazine" />
                    Magazine</li>
                <li>
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="On-Line Residential" />
                    On-Line Residential </li>
                    <li>
                        <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Yahoo" />
                        Yahoo </li>
                  
                <li>
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Somewhere on the web" />
                    Other Website </li>
            </ul>
        </div>
        <div class="column-right left">
            <ul>
                <li>
                  <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Through a family member" />
                    Family member</li>
                <li>
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="In a newspaper" />
                    Newspaper</li>
                <li>
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Google" />
                    Google</li>
                <li>
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Streeteasy" />
                    Streeteasy </li>
                <li>
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Craigslist" />
                    Craigslist </li>
                <li>
                    <input type="checkbox" style="vertical-align: middle;" name="ReferralSource" value="Other" />
                    Other</li>
            </ul>
        </div>
</div>

<div style="margin: 10px 0 3px 0;">
    If Other, please specify
</div>
<asp:TextBox ID="OtherTextBox" runat="server" Style="width: 303px" />
<div>
   
                <div class="contact_titles page-titles" style="margin:10px 0;">
                   <b>My Contact information</b>
                </div>
       
       <ul class="fill-in">
          <li class="clearfix">
              <span>First Name</span>
                <asp:TextBox ID="FirstName" runat="server" Style="width: 197px; float:right;" />
                <asp:RequiredFieldValidator ControlToValidate="FirstName" ID="RequiredFieldValidator1"
                    runat="server" ErrorMessage="*Please enter your first name." CssClass="error-message-text" />
         </li>
               <li class="clearfix"> Last Name
          
                <asp:TextBox ID="LastName" runat="server" Style="width: 197px; float:right;" />
                <asp:RequiredFieldValidator ControlToValidate="LastName" ID="RequiredFieldValidator2"
                    runat="server" ErrorMessage="*Please enter your last name." CssClass="error-message-text" />
         </li>
         <li class="clearfix">
                Email
      
                <asp:TextBox ID="Email" runat="server" Style="width: 197px; float:right;" />
                <asp:RequiredFieldValidator ControlToValidate="Email" ID="RequiredFieldValidator3"
                    runat="server" ErrorMessage="*Please enter your email address." CssClass="error-message-text" />
                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="Email" EnableClientScript="true"
                    ID="RegularExpressionValidator2" runat="server" ErrorMessage="*Please enter a valid email address." CssClass="invalid-message-text" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
         </li>
              <li>  Mobile Phone
          
                <asp:TextBox ID="MobilePhone" class="mobilephone" runat="server" Style="width: 197px; float:right;" />
                <asp:RequiredFieldValidator ControlToValidate="MobilePhone" ID="RequiredFieldValidator4"
                    runat="server" ErrorMessage="*Please enter your phone number." CssClass="error-message-text" />
                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="MobilePhone"
                    EnableClientScript="true" ID="RegularExpressionValidator1" runat="server" ErrorMessage="*Please enter a valid mobile phone number" CssClass="invalid-message-text"
                    ValidationExpression="1?\s*\W?\s*([2-9][0-8][0-9])\s*\W?\s*([2-9][0-9]{2})\s*\W?\s*([0-9]{4})(\se?x?t?(\d*))?" />
       </li>
       &nbsp;
            <li>    Alternate Phone
       
                <asp:TextBox ID="AltPhone" class="altphone" runat="server" Style="width: 197px; float:right;" />
                
                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="AltPhone" EnableClientScript="true"
                    ID="RegularExpressionValidator3" runat="server" ErrorMessage="" CssClass="error-message-text" ValidationExpression="1?\s*\W?\s*([2-9][0-8][0-9])\s*\W?\s*([2-9][0-9]{2})\s*\W?\s*([0-9]{4})(\se?x?t?(\d*))?" />
         </li>
         &nbsp;
                <div class="contact_titles">
                    Comments
                </div>
          
                <asp:TextBox ID="Comments" TextMode="MultiLine" runat="server" Width="297" Height="100" />
        <br></br>
                <asp:LinkButton ID="SubmitButton" runat="server" OnClick="SubmitButton_Click">
                   <div class="short-button button-image"><span class="font">submit</span> <img src="images/clear.png" /></div>
                </asp:LinkButton>
          </ul>
</div>
<!--Kaman's Style Ends-->
</div>
<div id="messagePanel" runat="server" visible="false">
    <p style="padding-top: 30px;">
        Thank you for contacting us. If your matter is urgent please call 212-624-6714. One
        of our agents will be happy to assist you. We appreciate your inquiry.
    </p>
</div>
<div id="sentfailed" runat="server" visible="false">
    <div class="footer">
        <p>
            Sorry, your email sent failed.</p>
        <p>
            <%=exception.ToString() %></p>
    </div>
</div>
</form>
