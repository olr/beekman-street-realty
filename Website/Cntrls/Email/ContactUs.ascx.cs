﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Configuration;
using System.Net.Mail;

public partial class cntrls_Email_ContactUs : System.Web.UI.UserControl
{
    protected Exception exception;
    protected string company;
    //protected override void OnInit(EventArgs e)
    //{
    //    SubmitButton.Click += new EventHandler(SubmitButton_Click);
    //}
    protected void Page_Load(object sender, EventArgs e)
    {
        company = ConfigurationManager.AppSettings["CompanyName"];
    }
    protected void SubmitButton_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {

            StringBuilder sb = new StringBuilder();


            sb.AppendLine("Client Information<br />");
            sb.AppendLine("----------------------------------------------------------------------------------------------------<br />");
            sb.AppendFormat("I am a: {0}<br />\n", CustomerType.SelectedValue);

            string referralSource = Request["ReferralSource"];
            if (!string.IsNullOrEmpty(OtherTextBox.Text))
            {
                if (!string.IsNullOrEmpty(referralSource))
                    referralSource += "," + OtherTextBox.Text;
            }

            sb.AppendFormat("How did you hear of us: {0}<br />\n", referralSource);
            sb.AppendFormat("First Name: {0}<br />\n", FirstName.Text);
            sb.AppendFormat("Last Name: {0}<br />\n", LastName.Text);
            sb.AppendFormat("Email: {0}<br />\n", Email.Text);
            sb.AppendFormat("Mobile Phone: {0}<br />\n", MobilePhone.Text);
            sb.AppendFormat("Alter Phone: {0}<br />\n", AltPhone.Text);
            sb.AppendFormat("Comments: <br />{0}<br />\n", Comments.Text);
            sb.AppendLine("----------------------------------------------------------------------------------------------------<br />");

            string toEmail = ConfigurationManager.AppSettings["defaultEmail"];

            if (!string.IsNullOrEmpty(toEmail))
            {
                MailMessage message = new MailMessage("noreply@olr.com", toEmail);
                message.ReplyToList.Add(Email.Text);
                message.Subject = @"Website lead from http://beekmanstreet.com/ - General Contact";
                message.IsBodyHtml = true;
                message.Bcc.Add("jun@olr.com");
                message.Body = sb.ToString();

                try
                {
                    using (var client = new SmtpClient())
                    {
                        client.Send(message);
                        //throw(new Exception("error"));
                        formPanel.Visible = false;
                        messagePanel.Visible = true;
                    }
                    
                }
                catch (Exception ex)
                {
                    exception = ex;
                    formPanel.Visible = false;
                    sentfailed.Visible = true;
                }
            }
        }
    }
}