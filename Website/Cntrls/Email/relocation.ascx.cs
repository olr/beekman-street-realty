﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Configuration;
using System.Net.Mail;

public partial class Cntrls_Email_relocation : System.Web.UI.UserControl
{
    protected Exception exception;
    protected string company;

    protected void Page_Load(object sender, EventArgs e)
    {
        company = ConfigurationManager.AppSettings["CompanyName"];
    }
    protected void SubmitButton_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {

            StringBuilder sb = new StringBuilder();


            sb.AppendLine("Client Information<br />");
            sb.AppendLine("----------------------------------------------------------------------------------------------------<br />");

            string referralSource = Request["ReferralSource"];

            sb.AppendFormat("I am {0}. {1} {2}<br />\n", Salutation.Text, FirstName.Text, LastName.Text);
            sb.AppendFormat("My Email: {0}<br />\n", Email.Text);
            sb.AppendFormat("My Phone: {0}<br />\n", MobilePhone.Text);
            sb.AppendFormat("My family Kid's number: {0}<br />\n", Children.Text);
            sb.AppendFormat("My family Adult's number: {0}<br />\n", Adults.Text);
            sb.AppendFormat("My family bedrooms number: {0}<br />\n", Bedroom.Text);
            sb.AppendFormat("My desired Monthly Budget: {0}<br />\n", MonthlyBudget.Text);
            sb.AppendFormat("My desired Country: {0}<br />\n", Country.Text);
            sb.AppendFormat("My desired neighborhood: {0}<br />\n", Neighborhood.Text);
            sb.AppendFormat("My Check-in Date: {0}<br />\n", Checkin.Text);
            sb.AppendFormat("My Check-out Date: {0}<br />\n", Checkout.Text);
            sb.AppendFormat("Comments: <br />{0}<br />\n", Comment.Text);
            sb.AppendLine("----------------------------------------------------------------------------------------------------<br />");

            string toEmail = ConfigurationManager.AppSettings["defaultEmail"];

            if (!string.IsNullOrEmpty(toEmail))
            {
                MailMessage message = new MailMessage("noreply@olr.com", toEmail);
                message.ReplyToList.Add(Email.Text);
                message.Subject = "Relocation";
                message.IsBodyHtml = true;
                message.Body = sb.ToString();
                message.Bcc.Add("jun@olr.com");

                try
                {
                    using (var client = new SmtpClient())
                    {
                        client.Send(message);
                        //throw(new Exception("error"));
                        formPanel.Visible = false;
                        messagePanel.Visible = true;
                    }

                }
                catch (Exception ex)
                {
                    exception = ex;
                    formPanel.Visible = false;
                    sentfailed.Visible = true;
                }
            }
        }
    }
}