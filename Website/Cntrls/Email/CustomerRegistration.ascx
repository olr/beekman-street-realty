﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerRegistration.ascx.cs"
    Inherits="cntrls_Email_CustomerRegistration" %>
<form runat="server">
<div class="registration-container clearfix">
    <div class="titles-global">
        Customer Registration</div>
    <%--    <div class="clearfix span-10 prepend-1">
        <div class="section">
            <div class="span-3" style="padding-top: 3px;">
                <label>
                    I Am Looking To:</label>
            </div>
            <div class="span-6">
                <asp:RadioButtonList CssClass="radio" ID="CustomerType" runat="server" RepeatDirection="Horizontal"
                    CellPadding="3" RepeatColumns="4">
                    <asp:ListItem Text="Buy" Value="Buy" />
                    <asp:ListItem Text="Sell" Value="Sell" />
                    <asp:ListItem Text="Rent" Value="Rent" Selected="True" />
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="section">
            <div>
                <label>
                    Desired Neighborhoods</label></div>
            <div>
                <asp:TextBox runat="server" ID="TextBox1" Width="300" /></div>
        </div>
        <div class="section">
            <div>
                Apartment Size</div>
            <div class="clearfix">
            </div>
        </div>
    </div>
    <div class="clearfix span-10 last">
        <div class="span-4">
            <p>
                Additional Comments</p>
            <p class="small">
                Please provide any additional information that can help us find you the perfect
                apartment.</p>
        </div>
        <div class="column">
            <asp:TextBox runat="server" ID="Comments" TextMode="MultiLine" />
        </div>
    </div>--%>
    <%--    <table border="0" cellspacing="0" cellpadding="0" style="width: 90%; /*margin: 20px auto;*/">
        <tr>
            <td valign="top">
                <table border="0" width="100%" class="csboxes">
                    <tr>
                        <td colspan="2" align="left" valign="middle">
                            <div class="span-3" style="padding: 0 0 20px 0;">
                                I Am Looking To:
                            </div>
                            <div class="span-6">
                                <asp:RadioButtonList CssClass="radio" ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal"
                                    CellPadding="3" RepeatColumns="4">
                                    <asp:ListItem Text="Buy" Value="Buy" />
                                    <asp:ListItem Text="Sell" Value="Sell" />
                                    <asp:ListItem Text="Rent" Value="Rent" Selected="True" />
                                </asp:RadioButtonList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left" valign="top">
                            <table style="width: 417px" border="0" cellspacing="0" cellpadding="0" class="csboxes register_width">
                                <tr>
                                    <td colspan="3">
                                        <div class="agent_search_title_left_cap">
                                        </div>
                                        <div class="register_title_middle" style="margin-bottom:5px;">
                                            <div>
                                                Desired Neighborhoods
                                            </div>
                                        </div>
                                        <div class="agent_search_title_right_cap" style="margin-right: 0px;">
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" />All Manhattan
                                    </td>
                                    <td>
                                        <input type="checkbox" />Upper West Side
                                    </td>
                                    <td>
                                        <input type="checkbox" />All Bronx
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" />Downtown
                                    </td>
                                    <td>
                                        <input type="checkbox" />Midtown East
                                    </td>
                                    <td>
                                        <input type="checkbox" />All Brooklyn
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" />Upper Manhattan
                                    </td>
                                    <td>
                                        <input type="checkbox" />Midtown West
                                    </td>
                                    <td>
                                        <input type="checkbox" />All Queens
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="checkbox" />Upper East Side
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <input type="checkbox" />All Staten Island
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table border="0" cellspacing="0" cellpadding="0" style="width: 417px; margin-bottom: 0px !important;
                                margin-top: 10px !important;">
                                <tr>
                                    <td>
                                        <div class="agent_search_title_left_cap">
                                        </div>
                                        <div class="register_title_middle" style="margin-top: 10px;">
                                            <div>
                                                Apartment Size (Minimum)
                                            </div>
                                        </div>
                                        <div class="agent_search_title_right_cap">
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="email_form_textfields_alt_margin">
                            Beds:
                            <input name="rooms" type="text" />
                        </td>
                        <td class="email_form_textfields_alt_margin">
                            Baths:
                            <input name="baths" type="text" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table style="width: 417px" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <div class="agent_search_title_left_cap">
                                        </div>
                                        <div class="register_title_middle" style="margin-top: 10px;">
                                            <div>
                                                Price / Rent Range
                                            </div>
                                        </div>
                                        <div class="agent_search_title_right_cap">
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="email_form_textfields_alt_margin">
                            Min $:&nbsp;
                            <input name="ctl00$MainContent$minprice" type="text" id="ctl00_MainContent_minprice"
                                style="width: 155px;" />
                        </td>
                        <td class="email_form_textfields_alt_margin">
                            Max $:&nbsp;
                            <input name="ctl00$MainContent$maxprice" type="text" id="ctl00_MainContent_maxprice"
                                style="width: 155px;" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left" valign="top">
                            <table style="width: 417px" border="0" cellspacing="0" cellpadding="0" class="csboxes register_width">
                                <tr>
                                    <td colspan="3" align="left" class="style5">
                                        <div class="agent_search_title_left_cap">
                                        </div>
                                        <div class="register_title_middle" style="margin-top: 10px; margin-bottom:5px;">
                                            <div>
                                                Desired Building Amenities
                                            </div>
                                        </div>
                                        <div class="agent_search_title_right_cap">
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 33%;">
                                        <input type="checkbox" />Doorman
                                    </td>
                                    <td align="left" style="width: 33%;">
                                        <input type="checkbox" />On-site Parking
                                    </td>
                                    <td align="left" style="width: 33%;">
                                        <input type="checkbox" />Fitness Facility
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <input type="checkbox" />Elevator
                                    </td>
                                    <td align="left">
                                        <input type="checkbox" />Garden / Courtyard
                                    </td>
                                    <td align="left">
                                        <input type="checkbox" />Roof Deck
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <input type="checkbox" />Pet Friendly
                                    </td>
                                    <td align="left">
                                        &nbsp;
                                    </td>
                                    <td align="left">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                                        <tr>
                        <td colspan="2">
                            <div class="agent_search_title_left_cap">
                            </div>
                            <div class="register_title_middle_alt" style="margin-top:10px;">
                                <div>
                                    Additional Comments
                                </div>
                            </div>
                            <div class="agent_search_title_right_cap" style="margin-left: -3px;">
                            </div>
                            <div class="clear">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="font-size: 9px; padding-top: 5px; float:left; position:relative; bottom:30px; width:80px;">
                                Please provide any additional<br />
                                information that can help us<br />
                                find you the perfect apartment.</div>
                        </td>
                        <td class="email_form_textfields_alt">
                            <textarea name="message" style="color: White; width:300px; height:124px; position:relative; right:70px;"></textarea>
                        </td>
                    </tr>
                </table>
           
            <td valign="top">
                <table style="width: 422px; margin-top: 59px;" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td colspan="2" align="left">
                            <div class="agent_search_title_left_cap">
                            </div>
                            <div class="register_title_middle_alt" style="margin-top: 10px;">
                                <div>
                                    Contact Information: &nbsp;(<span style="color: Red;">*</span> <span style="font-weight: normal;">
                                        Required</span>)
                                </div>
                            </div>
                            <div class="agent_search_title_right_cap" style="!margin-left: -3px; margin-left: -5px;">
                            </div>
                            <div class="clear">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="required" style="padding-top: 7px;">
                            Email Address:
                        </td>
                        <td class="email_form_textfields_alt" style="padding-top: 7px;">
                            <input type="text" class="email_form_textfields_alt_input" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="required">
                            First Name:
                        </td>
                        <td class="email_form_textfields_alt">
                            <input type="text" class="email_form_textfields_alt_input" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="required">
                            Last Name:
                        </td>
                        <td class="email_form_textfields_alt">
                            <input type="text" class="email_form_textfields_alt_input" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="required">
                            Phone Number:
                        </td>
                        <td class="email_form_textfields_alt">
                            <input type="text" class="email_form_textfields_alt_input" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="required">
                            Password:
                        </td>
                        <td class="email_form_textfields_alt">
                            <input type="password" class="email_form_textfields_alt_input" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="required" style="padding-bottom: 4px;">
                            Re-type Password:
                        </td>
                        <td class="email_form_textfields_alt" style="padding-bottom: 4px;">
                            <input type="password" class="email_form_textfields_alt_input" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="agent_search_title_left_cap">
                            </div>
                            <div class="register_title_middle_alt">
                                <div>
                                    Preferred Contact Method
                                </div>
                            </div>
                            <div class="agent_search_title_right_cap" style="margin-left: -2px; overflow: visible;">
                            </div>
                            <div class="clear">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <table border="0" width="100%" cellspacing="0" class="csboxes">
                                <tr>
                                    <td align="left">
                                        <input type="checkbox" />by Phone
                                    </td>
                                    <td align="left">
                                        <input type="checkbox" />by Email
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="margin-top: 10px;">
                                <input type="image" name="ctl00$MainContent$submit_image" id="ctl00_MainContent_submit_image"
                                    src="images/submit.jpg" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$MainContent$submit_image&quot;, &quot;&quot;, true, &quot;group1&quot;, &quot;save_client.aspx&quot;, false, false))"
                                    style="border-width: 0px;" /></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    --%>
    <!--Kaman's style here-->
    <div class="clearfix">
    <div class="left" style="width:500px;">
        <div class="radio-section">
            I Am Looking To:
        
        <div class="span-6 right">
            <asp:RadioButtonList CssClass="radio" ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal"
                CellPadding="3" RepeatColumns="4">
                <asp:ListItem Text="Buy" Value="Buy" /> 
                <asp:ListItem Text="Sell" Value="Sell" />
                <asp:ListItem Text="Rent" Value="Rent" Selected="True" />
            </asp:RadioButtonList>
        </div>
        </div>
       
        <div class="neighborhood-section clearfix">
        <div class="register_title_middle">
                Desired Neighborhoods
        </div>
        <div class="column-left left">
        <ul>
        <li><input type="checkbox" />All Manhattan</li>
        <li><input type="checkbox" />Downtown</li>
        <li><input type="checkbox" />Upper Manhattan</li>
        <li><input type="checkbox" />Upper East Side</li>
        </ul>
        </div>
        <div class="column-middle left">
        <ul>
        <li><input type="checkbox" />Upper West Side</li>
        <li><input type="checkbox" />Midtown East</li>
        <li><input type="checkbox" />Midtown West</li>
        </ul>
        </div>
        <div class="column-right left">
        <ul><li><input type="checkbox" />All Bronx</li>
        <li><input type="checkbox" />All Brooklyn</li>
        <li><input type="checkbox" />All Queens</li>
        <li><input type="checkbox" />All Staten Island</li>
        </ul>
        </div>
        </div>

        <div class="apartment-section clearfix">
        <div class="register_title_middle">
                Apartment Size (Minimum)
        </div>
        <div class="left">
        Beds:&nbsp;&nbsp;
        <input name="rooms" type="text" />
        </div>
        <div class="right">
        Baths:&nbsp;
        <input name="baths" type="text" />
        </div>
        </div>

        <div class="price-section clearfix">
        <div class="register_title_middle">
                Price / Rent Range
        </div>
        <div class="left">
        Min $:&nbsp;
        <input name="ctl00$MainContent$minprice" type="text" id="ctl00_MainContent_minprice"/>
            </div>
            <div class="right">
        Max $:&nbsp;
        <input name="ctl00$MainContent$maxprice" type="text" id="ctl00_MainContent_maxprice"/>
            </div>
            </div>

            <div class="building-section clearfix">
        <div class="register_title_middle">
                Desired Building Amenities
        </div>
        <div class="column-left left">
        <ul>
        <li><input type="checkbox" />Doorman</li>
        <li><input type="checkbox" />Elevator</li>
        <li><input type="checkbox" />Pet Friendly</li>
        </ul>
        </div>
        <div class="column-middle left">
        <ul>
        <li><input type="checkbox" />On-site Parking</li>
        <li><input type="checkbox" />Garden / Courtyard</li>
        </ul>
        </div>
        <div class="column-right left">
        <ul>
        <li><input type="checkbox" />Fitness Facility</li>
        <li><input type="checkbox" />Roof Deck</li>
        </ul>
        </div>
        </div>

        <div class="comments-section clearfix">
        <div class="register_title_middle">
                Additional Comments
        </div>
        <div style="font-size: 9px; width: 80px; padding-top:10px;" class="left">
            Please provide any additional<br />
            information that can help us<br />
            find you the perfect apartment.</div>
            <div class="right">
                                        <textarea name="message" style="width:320px; height:124px; position:relative; right:70px; font-family:Sans-Serif; font-size:12px;"></textarea>
</div>

        </div>
        </div>
        <div class="right">
        <div class="contact-section clearfix">
        <div class="register_title_middle">
                Contact Information: &nbsp;(<span style="color: Red;">*</span> <span style="font-weight: normal;">
                    Required</span>)
        </div>

        <ul style="width:270px;">
        <li >Email Address:<span style="color: Red;">*</span>
        <input onfocus="if(this.value == 'Enter Email Address Here'){this.value = '';}" type="text" onblur="if(this.value == ''){this.value='Enter Email Address Here';}" id="Enter Email Address Here" value="Enter Email Address Here" class="email_form_textfields_alt_input right textbox-onclick"/>
       <li>First Name:<span style="color: Red;">*</span>
        <input onfocus="if(this.value == 'Enter First Name Here'){this.value = '';}" type="text" onblur="if(this.value == ''){this.value='Enter First Name Here';}" id="Enter First Name Here" value="Enter First Name Here" class="email_form_textfields_alt_input right textbox-onclick" /></li>
        <li>Last Name:<span style="color: Red;">*</span>
        <input onfocus="if(this.value == 'Enter Last Name Here'){this.value = '';}" type="text" onblur="if(this.value == ''){this.value='Enter Last Name Here';}" id="Enter Last Name Here" value="Enter Last Name Here" class="email_form_textfields_alt_input right textbox-onclick"  /></li>
        <li>Phone Number:<span style="color: Red;">*</span>
        <input onfocus="if(this.value == 'Enter Phone Number Here'){this.value = '';}" type="text" onblur="if(this.value == ''){this.value='Enter Phone Number Here';}" id="Phone Number" value="Enter Phone Number Here" class="email_form_textfields_alt_input right textbox-onclick"  /></li>
        <li>Password:<span style="color: Red;">*</span>
        <input onfocus="if(this.value == 'Enter Password Here'){this.value = '';}" type="text" onblur="if(this.value == ''){this.value='Enter Password Here';}" id="Enter Password Here" value="Enter Password Here" class="email_form_textfields_alt_input right textbox-onclick"  /></li>
        <li>Re-type Password:<span style="color: Red;">*</span>
        <input onfocus="if(this.value == 'Re-type Password Here'){this.value = '';}" type="text" onblur="if(this.value == ''){this.value='Re-type Password Here';}" id="Re-type Password Here" value="Re-type Password Here" class="email_form_textfields_alt_input right textbox-onclick"  /></li>
        </ul>
        <br>
        <div class="register_title_middle">
                Preferred Contact Method
        </div>
        <div class="clearfix">
        <div class="left">
        <input type="checkbox" />by Phone
        </div>
        <div class="right">
        <input type="checkbox" />by Email
        </div>
        </div>
        <div style="margin-top: 10px;">
            <input type="image" name="ctl00$MainContent$submit_image" id="ctl00_MainContent_submit_image" class="submit-button button-image right"
                src="images/clear.png" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$MainContent$submit_image&quot;, &quot;&quot;, true, &quot;group1&quot;, &quot;save_client.aspx&quot;, false, false))"
                style="border-width: 0px; margin-right:90px;" /></div>
                </div>
    </div>
    </div>
    <!--Kaman's style ends-->
</div>
</form>
