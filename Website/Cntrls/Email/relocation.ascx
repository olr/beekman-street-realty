﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="relocation.ascx.cs" Inherits="Cntrls_Email_relocation" %>
<div class="static-pages corners relocation clearfix">
    <div class="titles-global" style="margin-bottom: 20px;">
            corporate relocation
        </div>
        <div class="text">
            <p>
                <span>A Job-Related Move: Transition with Ease - The Beekman Street Realty Corporate Relocation Program</span>
            </p>
            <p>Moving to New York City for work can be overwhelming and time-consuming for those not familiar with this dynamic real estate market. Whether your job brings you to the New York metro area for one month or one year, Beekman Street Realty can find you a comfortable furnished home environment as an alternative to costly hotels. We will recommend appropriate neighborhoods and help you transition into your new living environment. </p>
            <p>Contact us to learn more about our Corporate Relocation program. </p>
            </div>
<%--        <p>
        Focusing on the destination services part of the relocation industry we pride ourselves
        in assisting the corporate client with finding the perfect apartment. We tailor
        our services for each transferee from an executive to a new hire so that they receive
        the highest quality service relevant to them.</p>
    <p>
        Under the umbrella of “destination services” our forte is home search, including
        corporate housing, but we also include area tours, bank account, Social Security
        services and school information.</p>
    <p>
        We know that finding a new place to live is one of the most stressful parts of relocation
        not only for the transferee but also the relocation company. Using our knowledge
        of the city we leverage our exclusive listings along with broker only databases
        and landlord contacts to ensure a smooth home search every time.</p>
    <p>
        If you have any questions regarding relocating to New York City please call Thomas
        Walker, Director of Relocation, direct at 718 757 1556. Also look at our Resources
        section.</p>
--%>   <%-- <div class="contact-us">
        Call us at 212-624-6714 or submit an email.</div>--%>
    <br />
    <form id="Form1" runat="server">
    <div id="formPanel" runat="server" visible="true">
        <div class="form clearfix">
        <div class="clearfix">
            <div class="column-left left relative">
                <ul>
                    <li class="clearfix">Salutation (Mr, Ms, Dr, Etc)
                        <asp:TextBox ID="Salutation" runat="server" Style="width: 197px; float: right;"></asp:TextBox>
                    </li>
                    <li class="clearfix">First Name
                        <asp:TextBox ID="FirstName" runat="server" Style="width: 197px; float: right;" />
                        <asp:RequiredFieldValidator ControlToValidate="FirstName" ID="RequiredFieldValidator1"
                            runat="server" ErrorMessage="*Please enter your first name." CssClass="error-message-text" />
                    </li>
                    <li class="clearfix">Last Name
                        <asp:TextBox ID="LastName" runat="server" Style="width: 197px; float: right;" />
                        <asp:RequiredFieldValidator ControlToValidate="LastName" ID="RequiredFieldValidator2"
                            runat="server" ErrorMessage="*Please enter your last name." CssClass="error-message-text" />
                    </li>
                    <li class="clearfix">Email
                        <asp:TextBox ID="Email" runat="server" Style="width: 197px; float: right;" />
                        <asp:RequiredFieldValidator ControlToValidate="Email" ID="RequiredFieldValidator3"
                            runat="server" ErrorMessage="*Please enter your email address." CssClass="error-message-text" />
                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="Email" EnableClientScript="true"
                            ID="RegularExpressionValidator2" runat="server" ErrorMessage="*Please enter a valid email address."
                            CssClass="invalid-message-text-absolute" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                    </li>
                    <li class="clearfix">Mobile Phone
                        <asp:TextBox ID="MobilePhone" class="mobilephone" runat="server" Style="width: 197px;
                            float: right;" />
                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="MobilePhone"
                            EnableClientScript="true" ID="RegularExpressionValidator1" runat="server" ErrorMessage="*Please enter your mobile phone number"
                            CssClass="error-message-text" ValidationExpression="1?\s*\W?\s*([2-9][0-8][0-9])\s*\W?\s*([2-9][0-9]{2})\s*\W?\s*([0-9]{4})(\se?x?t?(\d*))?" />
                    </li>
                    <li class="clearfix">Country
                        <asp:TextBox ID="Country" runat="server" Style="width: 197px; float: right;"></asp:TextBox>
                    </li>
                    <li class="clearfix">Move-in Date (mm/dd/yyyy)
                        <asp:TextBox ID="Checkin" runat="server" Style="width: 197px; float: right;"></asp:TextBox>
                    </li>
                    <li class="clearfix">Move-out Date (mm/dd/yyyy)
                        <asp:TextBox ID="Checkout" runat="server" Style="width: 197px; float: right;"></asp:TextBox>
                    </li>
                </ul>
            </div>
            <div class="column-right right relative">
                <ul>
                    <li class="clearfix">No. of Bedroom<asp:TextBox ID="Bedroom" runat="server" Style="width: 197px;
                        float: right;"></asp:TextBox></asp:TextBox> </li>
                    <li class="clearfix">No. of Adults<asp:TextBox ID="Adults" runat="server" Style="width: 197px;
                        float: right;"></asp:TextBox></asp:TextBox> </li>
                    <li class="clearfix">No. of Children<asp:TextBox ID="Children" runat="server" Style="width: 197px;
                        float: right;"></asp:TextBox></asp:TextBox> </li>
                    <li class="clearfix">Monthly budget (US Dollar)<asp:TextBox ID="MonthlyBudget" runat="server"
                        Style="width: 197px; float: right;"></asp:TextBox></asp:TextBox> </li>
                    <li class="clearfix">Neighborhood<asp:TextBox ID="Neighborhood" runat="server" Style="width: 197px;
                        float: right;"></asp:TextBox></asp:TextBox> </li>
                    <li>Comments
                        <asp:TextBox ID="Comment" runat="server" TextMode="MultiLine" Style="width: 97.5%;
                            height: 56px;"></asp:TextBox>
                    </li>
                </ul>
            </div>
            </div>
        <br />
        <asp:LinkButton ID="SubmitButton" runat="server" OnClick="SubmitButton_Click" class="right">
                   <div class="short-button button-image corners"><span class="font">submit</span> <img src="images/clear.png" /></div>
        </asp:LinkButton>
        </div>
       
    </div>
    <div id="messagePanel" runat="server" visible="false">
        <p style="padding-top: 30px;">
            Thank you for contacting us. If your matter is urgent please call 212-624-6714. One
            of our agents will be happy to assist you. We appreciate your inquiry.
        </p>
    </div>
    <div id="sentfailed" runat="server" visible="false">
        <div class="footer">
            <p>
                Sorry, your email sent failed.</p>
            <p>
                <%=exception.ToString() %></p>
        </div>
    </div>
    </form>
</div>
