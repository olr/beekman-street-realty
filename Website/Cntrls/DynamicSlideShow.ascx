﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DynamicSlideShow.ascx.cs" Inherits="cntrls_DynamicSlideShow" %>
<div id="slider" class="nivoSlider corners">

<asp:ListView runat="server" ID="listView">
    <LayoutTemplate>
        <asp:Literal runat="server" ID="itemPlaceHolder" />
    </LayoutTemplate>
    <ItemTemplate>
        <a href="detail.aspx?id=<%# Eval("ListingID") %>" <%# Html.DisplayStyle(firstImage) %>>
            <img src='<%# Eval("ImageUrl") %>' rel='<%# Eval("ImageUrl") %>' title='<%# Eval("Caption") %>' width='950' height='357' />
        </a>
    </ItemTemplate>
</asp:ListView>
                       
</div>