﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgentFiles.ascx.cs" Inherits="Cntrls_Userdoc_AgentFiles" %>
<script type="text/javascript">


   
///***********************                Service                   *************************************//
    var EditDocumentTitle = function (id) {
        var title = prompt("Type in your new file title");
        var proxy = new ServiceProxy("Service.svc/");
        if (title.length > 0) {
            //update database
            proxy.invoke("EditFileTitle", { fileId: id, newTitle: title }, function (result) {
                $('a[id=' + id + '_link]').html(title);
            });
        }
    };
    var RemoveDocument = function (id) {
        var del = confirm("are you sure you want to delete this file?");
        var proxy = new ServiceProxy("Service.svc/");
        if (del == true) {
            proxy.invoke("RemoveFile", { fileID: id}, function (result) {
                $('li[id=' + id + ']').attr('style', 'display:none;'); 
            });
        }
    }

    var GetFile = function (id) {
        var proxy = new ServiceProxy("Service.svc/");
        proxy.invoke("GetFile", { fileID: id }, null)
    }
</script>
<asp:ListView ID="fileList" runat="server">
    <LayoutTemplate>
        <div class="msg_body corners clearfix">
            <ul class="left">
                <asp:Literal runat="server" ID="itemPlaceHolder" />
            </ul>
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <li id="<%#(Container.DataItem as UserFile).FileID %>"><%--<%#CustomFile.GetFile(fn.Get<int>((Container.DataItem as UserFile).FileID) )%>--%>
            <div id="<%#(Container.DataItem as UserFile).FileID %>_title" class="<%#GetClass(Container.DataItem as UserFile)%> docTitle left"><%--docs--%>
                <a id="<%#(Container.DataItem as UserFile).FileID %>_link" href="<%#GetFile((Container.DataItem as UserFile).FileID )%>">
                    <%#(Container.DataItem as UserFile).Title %></a></div>
            <div class="docs-options file admin" style="<%=IsAdmin(isAdmin)%>">
                <a href="javascript:EditDocumentTitle(<%#(Container.DataItem as UserFile).FileID %>)">Edit</a> |
                <a href="javascript:RemoveDocument(<%#(Container.DataItem as UserFile).FileID %>)">Remove</a></div>
            <div class="docs-info">
                Date uploaded<%--<%#GetAuthorName(Convert.ToInt32((Container.DataItem as UserFile).AgentID)) %>--%> - <%#(Container.DataItem as UserFile).date.ToShortDateString() %>
            </div>
        </li>
    </ItemTemplate>
</asp:ListView>
