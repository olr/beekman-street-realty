﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgentCategory.ascx.cs"
    Inherits="Cntrls_Userdoc_AgentCategory" %>
<script type="text/javascript">
    $(function () {
        $("a.upload-pop").colorbox({ iframe: true, innerWidth: 370, innerHeight: 160, onClosed: function () { location.reload(true); } });
//        var True=true;
//        var Flase=false;
//        if(<%=isAdmin %>)
//        {
//        console.log('category true');
//         $('div.admin').attr('style','display:block');
//        }
//        else
//        {
//        console.log('category false');
//         $('div.admin').attr('style','display:none');
//        }
        $('a.wrap').click(function (event) {
            var clicked = $(this);
            var id = clicked.attr('id');
            if (clicked.text().indexOf('+') > -1) {
                clicked.html('<span><b>-</b></span>');
                $('div.' + id).attr('style', 'display:block');
            }
            else if (clicked.text().indexOf('-') > -1) {
                clicked.html('<span><b>+</b></span>');
                $('div.' + id).attr('style', 'display:none');
            }
        })
    });

    /*********************              service                   **************************/
    var proxy = new ServiceProxy("Service.svc/");
    var EditCategory = function (id) {
        //$('#uploadFile').trigger('onclick');
        var title = prompt("type in your new category title:");
        var current = $('div[id=' + id + ']').find('div.titles-global').html();
        if (title != null && title != "") {
            //console.log(title);
            proxy.invoke("EditCategory", { id: id, title: title }, function (result) {
                $('div[id=' + id + ']').find('div.titles-global').html(title);
            });
        }
    }
    var RemoveCategory = function (id) {
        var r = confirm("are you sure you want to delete this category?");
        if (r == true)
            proxy.invoke("RemoveCategory", { id: id }, function (result) {
                //console.log("result" + result);
                if (result == 1) {
                    $('div[id=' + id + ']').attr('style', 'display:none');
                    $('div.cPlus_' + id).attr('style', 'display:none');
                }
            });
    }

   
</script>
<asp:ListView ID="categoryList" runat="server">
    <LayoutTemplate>
        <div class="msg_list">
            <div class="msg_wrapper clearfix">
                <asp:Literal runat="server" ID="itemPlaceHolder" />
            </div>
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <div id="<%#(Container.DataItem as UserCategory).categoryID %>" class="msg_head_container folio-corners clearfix">
            <div class="titles-global docfolio-title left admin">
                <%#(Container.DataItem as UserCategory).categoryTitle %></div>
            <div class="folio-buttons left admin category" style="<%=IsAdmin(isAdmin)%>">
                <a href='javascript:EditCategory(<%#(Container.DataItem as UserCategory).categoryID %>)'>
                    <span>Edit</span></a>
            </div>
            <div class="folio-buttons left admin category" style="<%=IsAdmin(isAdmin)%>">
                <a href='javascript:RemoveCategory(<%#(Container.DataItem as UserCategory).categoryID %>)'>
                    <span>Remove</span></a></div>
            <div class="upload-pop folio-buttons right" style="<%=IsAdmin(isAdmin)%>">
                <a href="UploadFile.aspx?categoryid=<%#(Container.DataItem as UserCategory).categoryID %>&agentid=<%=agentid %>"
                    class="upload-pop"><span>upload file</span></a></div>
            <div class="folio-buttons right admin category">
                <a href='javascript:function abc(){return false;}' class="wrap"
                    id="<%#(Container.DataItem as UserCategory).categoryID %>_cPlus"><span><b>+</b></span>
                </a>
            </div>
        </div>
        <div class='<%#(Container.DataItem as UserCategory).categoryID %>_cPlus' style="display:none;">
            <%=Html.RenderPartial("cntrls/Userdoc/AgentFiles", GetFilesByCategory())%>
        </div>
    </ItemTemplate>
    <EmptyDataTemplate>
        <p class="no-results">
            You have no documents on your docfolio.
        </p>
    </EmptyDataTemplate>
</asp:ListView>
