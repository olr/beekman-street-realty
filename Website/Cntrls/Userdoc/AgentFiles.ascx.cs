﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

public partial class Cntrls_Userdoc_AgentFiles : System.Web.UI.UserControl,IRenderable<CustomerFileSystemTransferModel>
{
    protected int agentid;
    UserFiles files;
    protected bool isAdmin;
    string[] credential = { "mediaservice", "jedi1234" };
    public void DataBind(CustomerFileSystemTransferModel data)
    {
        agentid = data.AgentId;
        files = data.Files;
        isAdmin = ServiceLocator.GetRepository().IsAdmin(agentid);
        fileList.DataSource = files;
        fileList.DataBind();
    }
    //protected string GetAuthorName(int authorid)
    //{
    //    return CustomFile.GetAuthorName(authorid);
    //}
    protected string GetClass(UserFile file)
    {
        if (file.Path.EndsWith(".pdf"))
            return "pdf";
        else if (file.Path.EndsWith("jpg"))
            return "img";
        else if (file.Path.EndsWith("xls") || file.Path.EndsWith("xlsx"))
            return "xls";
        else
            return "doc";
    }
    protected string IsAdmin(bool admin)
    {
        if (!admin)
            return "display:none";
        else
            return "display:block";
    }
    protected string GetDate(string oriDate)
    {
        string date = oriDate;
        Match match = Regex.Match(date, @"[\w]+,[\d\w\s]+(2011|2012)");
        if (match.Success)
            return match.Groups[0].Value;
        else
            return string.Empty;
    }
    protected string GetFile(string fileId)
    {
        return CustomFile.GetFile(Convert.ToInt32( fileId));
    }
}