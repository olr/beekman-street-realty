﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Web.UI.HtmlControls;

public partial class Cntrls_Userdoc_AgentCategory : System.Web.UI.UserControl, IRenderable<CustomerFileSystemTransferModel>
{
    protected UserCategories uc;
    protected CustomerFileSystemTransferModel model;
    protected int agentid;
    protected bool isAdmin;
    int i = 0;

    public void DataBind(CustomerFileSystemTransferModel cs)
    {
        model = cs;
        agentid = model.AgentId;
        uc = cs.Categories;
        isAdmin = ServiceLocator.GetRepository().IsAdmin(agentid);
        categoryList.DataSource = uc;
        categoryList.DataBind();
    }
    protected UserCategory GetCategory()
    {
        return uc[i++];
    }
    protected string IsAdmin(bool admin)
    {
        if (!admin)
            return "display:none";
        else
            return "display:block";
    }
    protected CustomerFileSystemTransferModel GetFilesByCategory()
    {
        CustomerFileSystemTransferModel m = new CustomerFileSystemTransferModel();
        UserCategory uc = GetCategory();
        UserFiles ufs = new UserFiles();
        string title = model.SearchTitle;
        if (!string.IsNullOrEmpty(title))
        {
            ufs = ServiceLocator.GetRepository().SearchFileByCategoryAndTitle(uc.categoryID, title);
        }
        else
        {
            ufs = ServiceLocator.GetRepository().GetFilesByCategoryID(uc.categoryID);
        }
        m.Files = ufs;
        m.AgentId = model.AgentId;
        m.SearchTitle = model.SearchTitle;
        return m;
    }

    //public over

}