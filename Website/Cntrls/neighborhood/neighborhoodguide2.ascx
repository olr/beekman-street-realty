﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="neighborhoodguide2.ascx.cs" Inherits="Cntrls_neighborhood_neighborhoodguide2" %>
<div class="neighborhood-photo">
    <%if (string.IsNullOrEmpty(photo))
      { %>
    <img class="corners" src="images/neighborhoods/upperwestside.jpg" alt="" />
    <%}
      else
      { %>
    <img class="corners" src="<%=photo%>" alt="" />
    <%} %>
</div>
<div class="neighborhood-title">
    <%if (string.IsNullOrEmpty(title))
      {%>
    Upper East Side<%}
      else
      { %>
    <div id="title">
        <%=title%>
    </div>
    <%} %>
</div>
<div class="neighborhood-info">
    <%if (string.IsNullOrEmpty(desc))
      { %>
    Brooklyn is the most populous of New York City's five boroughs, with nearly 2.6 million residents, and the second-largest in area. Since 1896, Brooklyn has had the same boundaries as Kings County, which is now the most populous county in New York State and the second-most densely populated county in the United States, after New York County (Manhattan). It is also the westernmost county on Long Island.<br /><br />
Brooklyn was an independent city until its consolidation with New York City in 1898, and continues to maintain a distinct culture, independent art scene, and unique architectural heritage. Many Brooklyn neighborhoods are ethnic enclaves where particular ethnic groups and cultures predominate.<%}
      else
      { %>
    <div id="desc">
        <%=desc%>
    </div>
    <%} %>
</div>
