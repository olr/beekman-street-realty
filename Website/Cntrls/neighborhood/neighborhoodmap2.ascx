﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="neighborhoodmap2.ascx.cs" Inherits="Cntrls_neighborhood_neighborhoodmap2" %>
<script src="js/jquery.metadata.js" type="text/javascript"></script>
<script src="js/jquery.neighborhoodmap.js" type="text/javascript"></script>
<script src="js/jquery.tooltip.js" type="text/javascript"></script>


<div class="neighborhood-container">
    <ul>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=financialdistrict" id="financial" >Financial District</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=tribeca" id="tribeca" >Tribeca</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=soho" id="soho">Soho </a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=lowereastside" id="les" >Lower East Side</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=eastvillage" id="eastvillage" >East Village</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=greenwichvillage" id="greenwichvillage">Greenwich Village</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=gramercypark" id="gramercy">Gramercy Park</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=chelsea" id="chelsea">Chelsea</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=suttonplace" id="suttonplace">Sutton Place</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=murrayhill" id="murrayhill" >Murray Hill</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=midtowneast" id="midtowneast">Midtown East</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=clinton" id="clinton">Clinton</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=midtownwest" id="midtownwest">Midtown West</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=uppereastside" id="uppereastside">Upper East Side</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=upperwestside" id="upperwestside" >Upper West Side</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=uppermanhattan" id="uppermanhattan">Upper Manhattan</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=harlem" id="harlem" >Harlem</a> </li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=brooklyn" id="brooklyn">Brooklyn</a></li>
       <li> <a href="neighborhood_guide2.aspx?neighborhood=queens" id="queens"> Queens</a></li>
    </ul>     
</div>