﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using BrokerTemplate.Core;

public partial class Cntrls_neighborhood_neighborhoodguide2: System.Web.UI.UserControl, IRenderable<string>
{
    protected string title;
    protected string photo;
    protected string desc;
    private string name;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }
    public void DataBind(string name)
    {
        var xml = XElement.Load(Server.MapPath("~/App_Data/NeighborhoodData.xml"));
        var node = xml.Elements("neighborhood").Where(x => x.Attribute("name").Value == name).FirstOrDefault();

        if (node != null)
        {
            title = node.Element("title").Value;
            photo = node.Element("image").Value;
            desc = node.Element("description").Value;
        }
        else
        {
            node = xml.Elements("neighborhood").Where(x => x.Attribute("name").Value == "midtowneast").FirstOrDefault();
            title = node.Element("title").Value;
            photo = node.Element("image").Value;
            desc = node.Element("description").Value;
        }
    }
}