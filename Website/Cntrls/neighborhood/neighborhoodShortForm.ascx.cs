﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class Cntrls_neighborhood_neighborhoodShortForm : System.Web.UI.UserControl, IRenderable<string>
{
    protected string data;
    protected string preNeighborhood="neighborhood";
    protected List<int> neighborhoods;
    string currentType = "all";
    string currentView = "active";
    protected bool showNav = true;
    protected bool showActive = true;
    protected bool showSale = true;
    protected bool showRental = true;
    protected bool showAll = true;
    protected void setPreNeighborhood()
    {
        preNeighborhood = data;
    }
    protected override void OnInit(EventArgs e)
    {
        this.Controls.InitSortables(SortClicked);
        base.OnInit(e);
    }
    private Listings GetListings(SearchOptions options, out int total)
    {
        Listings list = new Listings();
        options.Neighborhoods = neighborhoods;
        list = ServiceLocator.GetSearchService().RunSearch(options, out total);
       
        return list;
    }
    protected bool ShowSales()
    {
        SearchOptions op = new SearchOptions();
        op.PageIndex = 0;
        op.PropertyTypes.Add(PropertyType.Sale);
        int total = 0;
        var listings = GetListings(op, out total);
        return total > 0;
    }
    protected bool ShowRentals()
    {
        SearchOptions op = new SearchOptions();
        op.PropertyTypes.Add(PropertyType.Rental);
        op.PageIndex = 0;
        int total = 0;
        var listings =GetListings(op, out total);
        return total > 0;
    }
    void SortClicked(object sender, CommandEventArgs e)
    {
        SearchOptions options = GetSearchOptions();
        options.OrderBy = fn.StringToEnum<SortOrder>(e.CommandName);
        options.IsDescending = ((SortDirection)e.CommandArgument).Equals(SortDirection.Descending);
        options.PageIndex = 0;
        LoadData(options);
    }
    private List<int> GetNeighborhoods(string nh)
    {
        List<int> neighborhoods=new List<int>();
        var xml = XElement.Load(Server.MapPath("~/App_Data/NeighborhoodData.xml"));
        var node = xml.Elements("neighborhood").Where(x => x.Attribute("name").Value == data).FirstOrDefault();
        string value = node.Attribute("value").Value;
        string[] n = value.Split(',');
        if (n.Length != 0)
        {
            foreach (string ne in n)
                neighborhoods.Add(Convert.ToInt32(ne));
        }
        return neighborhoods;
    }
    public void DataBind(string _data)
    {
        data = _data;
        neighborhoods = GetNeighborhoods(data);
        if (data != null)
        {
            showSale = ShowSales();
            showRental = ShowRentals();
            showAll = showSale || showRental;
            if (showAll == false)
                showNav = false;
            SearchOptions options = GetSearchOptions();
            LoadData(options);
        }
    }

    SearchOptions GetSearchOptions()
    {
        SearchOptions options = new SearchOptions();
        options.IsDescending = true;
        options.Neighborhoods = neighborhoods;
        int pageIndex = fn.Get<int>(HttpContext.Current.Request.QueryString["page"]);
        if (pageIndex > 0)
            options.PageIndex = pageIndex - 1;
        if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["type"]))
        {
            if ("sale".Equals(HttpContext.Current.Request.QueryString["type"]))
                options.PropertyTypes.Add(PropertyType.Sale);
            else if ("rental".Equals(HttpContext.Current.Request.QueryString["type"]))
                options.PropertyTypes.Add(PropertyType.Rental);
        }
        if (options.PropertyTypes.Count == 0)
        {
            options.PropertyTypes.Add(PropertyType.Sale);
        }
        return options;
    }
    void LoadData(SearchOptions options)
    {
        
        int total = 0;
        var listings = GetListings(options, out total);
        if (total == 0)
        {
            if (options.PropertyTypes.Contains(PropertyType.Sale))
            {
                options.PropertyTypes.Remove(PropertyType.Sale);
                options.PropertyTypes.Add(PropertyType.Rental);
            }
            else
            {
                options.PropertyTypes.Remove(PropertyType.Rental);
                options.PropertyTypes.Add(PropertyType.Sale);
            }
            listings = GetListings(options, out total);
        }
        SetLinks(options);
        //Response.Write(total);
        this.Controls.DataBind<Listings>(listings);
        this.Controls.SetPagers(options.PageIndex + 1, options.PageSize, total);
        
        if (!options.RecentTransactions && listings != null && listings.Count > 0)
        {
            listings.CreatePagingSession();
        }
        //if (currentType.Equals("all") && total == 0)
        //{
        //    if (!options.RecentTransactions && HttpContext.Current.Request.QueryString["view"] != "all")
        //    {
        //        options.RecentTransactions = true;
               
        //        LoadData(options);
        //    }
        //    else
        //        showNav = false;
        //}
    }


    void SetLinks(SearchOptions options)
    {
        if (options.PropertyTypes.Contains(PropertyType.Rental))
            currentType = "rental";
        else
            currentType = "sale";
       
        var s = this.Controls.Find<HyperLink>("salesLink") ?? new HyperLink();
        var r = this.Controls.Find<HyperLink>("rentalsLink") ?? new HyperLink();

        string[] par = { "type", "page" };
        string basepath = HttpContext.Current.Request.Path + HttpContext.Current.Request.QueryString.ExcludeParameter(par);
        s.NavigateUrl = basepath + "&type=sale";
        r.NavigateUrl = basepath + "&type=rental";
        switch (currentType.ToLower())
        {
            case "sale":
                s.Enabled = false;
                s.CssClass = "disabled";
                break;
            case "rental":
                r.Enabled = false;
                r.CssClass = "disabled";
                break;
        }

        //view links
        //var active = this.Controls.Find<HyperLink>("activeLink") ?? new HyperLink();
        //var recent = this.Controls.Find<HyperLink>("recentLink") ?? new HyperLink();
        //basepath = HttpContext.Current.Request.Path + HttpContext.Current.Request.QueryString.ExcludeParameter("view");
        //active.NavigateUrl = basepath + "&view=all";
        //recent.NavigateUrl = basepath + "&view=recent";

        //switch (currentView.ToLower())
        //{
        //    case "recent":
        //        recent.Enabled = false;
        //        recent.CssClass = "disabled";
        //        active.Enabled = true;
        //        active.CssClass = "enabled";
        //        break;
        //    default:
        //        active.Enabled = false;
        //        active.CssClass = "disabled";
        //        break;
        //}


        //showRecent = "recent".Equals(currentView);
        //showActive = !showRecent;
    }
}
