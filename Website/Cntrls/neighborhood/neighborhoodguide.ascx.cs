﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_neighborhoodguide : System.Web.UI.UserControl, IRenderable<string>
{
    protected string title;
    protected string photo;
    protected string desc;
    private string name;
    protected bool sbuttonShow;
    protected bool rbuttonShow;
    SearchOptions options;
    protected string nb;

    protected override void OnInit(EventArgs e)
    {
        //var xml = XElement.Load(Server.MapPath("~/App_Data/NeighborhoodData.xml"));
        base.OnInit(e);
    }
    public void DataBind(string name)
    {
        nb = name;
        options = new SearchOptions();
        var xml = XElement.Load(Server.MapPath("~/App_Data/NeighborhoodData.xml"));
        var node = xml.Elements("neighborhood").Where(x => x.Attribute("name").Value == name).FirstOrDefault();

        if (node != null)
        {
            title = node.Element("title").Value;
            photo = node.Element("image").Value;
            desc = node.Element("description").Value;
        }
        else
        {
            node = xml.Elements("neighborhood").Where(x => x.Attribute("name").Value == "uppereastside").FirstOrDefault();

            title = node.Element("title").Value;
            photo = node.Element("image").Value;
            desc = node.Element("description").Value;
        }
        List<int> l = GetArea(node.Attribute("value").Value);
        l.RemoveDuplicate();
        foreach (int neighborhood in l)
        {
            options.Neighborhoods.Add(neighborhood);
        }
        int[] buttons = DisplayButton();
        sbuttonShow = buttons[0] > 0;
        rbuttonShow = buttons[1] > 0;
    }
    private List<int> GetArea(string number)
    {
        List<int> list = new List<int>();
        if (!string.IsNullOrEmpty(number))
            Array.ForEach<string>(number.Split(new char[] { ',' }), x => list.Add(Convert.ToInt32(x)));
        return list;
    }
    public int[] DisplayButton()
    {
        int[] val = new int[2];

        options.PropertyTypes.Add(PropertyType.Sale);
        val[0] = RunSearch(options);
        options.PropertyTypes.Remove(PropertyType.Sale);
        options.PropertyTypes.Add(PropertyType.Rental);
        val[1] = RunSearch(options);
        return val;
    }

    protected Listings listings = null;
    int RunSearch(SearchOptions options)
    {
        int _total = 0;
        if (options != null)
        {
            listings = ServiceLocator.GetSearchService().RunSearch(options, out _total);
            if (listings != null)
            {
                this.Controls.SetPagers(options.PageIndex + 1, options.PageSize, _total);
                this.Controls.DataBind<Listings>(listings);
                listings.CreatePagingSession(); //save paging info

            }
            TempStorage.Add("SearchOptions", options);
            this.Controls.DataBind<SearchOptions>(options);

        }
        return _total;
    }
}