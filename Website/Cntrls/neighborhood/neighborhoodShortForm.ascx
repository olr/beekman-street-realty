﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="neighborhoodShortForm.ascx.cs"
    Inherits="Cntrls_neighborhood_neighborhoodShortForm" %>
<div class="agentlisting-container">
    <div class="clearfix" <%=Html.DisplayStyle(showNav) %>>
       
        <div class="pages right">
            <%if (showAll)
              { %>
            View:
            <%if (showSale)
              {%>
            <asp:HyperLink class='salelink' runat="server" Text="Sales" ID="salesLink" />
            <%} if (showRental)
              { %>
          <%if (showSale)
            { %>  |<%} %>
            <asp:HyperLink class='rentlink' runat="server" Text="Rentals" ID="rentalsLink" />
            <%}
              } %>
        </div>
    </div>
    <div class="active-listings" <%=Html.DisplayStyle(showActive) %>>
        <olr:ShortForm ID="ShortForm1" runat="server" />
        <olr:OLRPageNav ID="PageNav2" runat="server" UsePostback="false" PrevImageUrl="images/icons/prev.png"
            NextImageUrl="images/icons/next.png" />
    </div>
</div>
