﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class cntrls_DynamicSlideShow : System.Web.UI.UserControl
{
    private bool _flag = true;
    protected bool firstImage
    {
        get
        {
            bool ret = _flag;
            _flag = !_flag;
            return ret;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            listView.DataSource = SiteHelper.GetSlideshows();
            listView.DataBind();
        }
    }
}