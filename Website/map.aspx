﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.master" AutoEventWireup="true"
    CodeFile="map.aspx.cs" Inherits="map" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
            body { padding:5px;margin:0; }
            .overlay { width: 400px; overflow:hidden; }
    </style>

  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAsw1rDRNIFqU6G5hduNJzcLdiH2mac174&sensor=false">
    </script>
    <script type="text/javascript">
      var geocoder;
  var infowindow = new google.maps.InfoWindow();
  var marker;
  var map;
        $(function () {
      geocoder = new google.maps.Geocoder();

            var options = {
                center: new google.maps.LatLng(<%=Latitude%>,<%=Longitude%>),
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_canvas"),
            options);

            var True=true;
            var False=false;
            //geocoder
            if(<%=showAddress %>){
            var lat = <%=Latitude%>;
    var lng = <%=Longitude%>;

            console.log(lat+" "+lng);
            var latlng = new google.maps.LatLng(lat, lng);

            geocoder.geocode( { 'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          map.setZoom(14);
          marker = new google.maps.Marker({
              position: latlng,
              map: map
          });
         // infowindow.setContent(results[1].formatted_address);
          infowindow.open(map, marker);
        }
      } else {
        console.log('failed'+ google.maps.GeocoderStatus);
      }

    });
    }// if is excusive and showaddress
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
<body>
    <div class="map-container" style="width:400px;overflow:hidden;">
        <h1 class="title">
            View Map</h1>
        <h2 id="address">
            <%=Model.GetDisplayAddress() %> | Listing ID
            <%=Model.ListingAgentID %></h2>
        <%--<h2 id="listingid">
            Listing ID:
            <%=Model.ListingAgentID %></h2>--%>
        <div class="w_popup_body">
            <div id="map_canvas" style="width:400px">
            </div>
        </div>
    </div>
    </body>
</asp:Content>
