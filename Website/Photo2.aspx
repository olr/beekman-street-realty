﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Photo2.aspx.cs" Inherits="Photo2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/overlay.css" rel="stylesheet" type="text/css" />

</head>
<body>
<div class="header" style="padding:12px 0;">
            <img src="images/logo.png" alt="" style="height:50px; margin-left:5px;" />
        </div>
        <hr />
    <h2 class="address">
        <%= Model.GetDisplayAddress() %>&nbsp; &nbsp;&nbsp;Listing ID:&nbsp;
        <%=Model.ListingID %></h2>
        <%--<div class="neighborhood"><%=Model.Neighborhood %> </div>--%>
<hr />

    <form id="form1" runat="server">
    
<asp:ListView runat="server" ID="list">
    <LayoutTemplate>
        <div class="galleryContainer">
            <ul class="tabular clearfix gallery">
                <li runat="server" id="itemPlaceHolder" />
            </ul>
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <li>
            <img src="<%# Eval("MediaUrl")%>" alt="" />
        </li>
    </ItemTemplate>
</asp:ListView>

 <a class="print-icon right print" href="photoPrint.aspx?id=<%=Model.ListingID %>" target="_blank"><img src="images/clear.png" alt="print"/></a>

   <div class="print-brokerinfo">  <%=Html.RenderPartial("cntrls/Print/BrokerInfo", Model)%> </div>
    </form>
</body>
</html>
