﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Configuration;
using System.Text;
public partial class email_form : System.Web.UI.Page
{
    protected BrokerInfo Model = new BrokerInfo();
    protected Exception exception;
    protected ListingInfo listing;

    protected override void OnInit(EventArgs e)
    {
        sendEmail.Click += new EventHandler(Send_Email);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int id = fn.Get<int>(Request.QueryString["id"]);

            if (id > 0)
            {
                Model = ServiceLocator.GetRepository().GetBrokerByID(id);
            }
            else
            {
                Model = new BrokerInfo();
                Model.FirstName = ConfigurationManager.AppSettings["SiteName"];
                Model.Email = ConfigurationManager.AppSettings["defaultEmail"];
            }


            if (Model != null)
            {
                //this.Controls.DataBind<BrokerInfo>(Model);
                listing = GetListing();
                if (listing != null)
                {
                    messageText.Text = "Please contact me about the following Web ID: " + listing.ListingID;
                }

                this.DataBind(true);
            }
        }
    }

    ListingInfo GetListing()
    {
        ListingInfo l = null;
        string lid = Request.QueryString["listingid"];
        if (!string.IsNullOrEmpty(lid))
            l = ServiceLocator.GetRepository().GetListingInfoByID(lid);
        return l;
    }

    void Send_Email(object sender, EventArgs e)
    {
        //if (reEmail.Text != yourEmail.Text)
        //{
        //    RegularExpressionValidator2.ErrorMessage = "please type in the same email address";
        //    RegularExpressionValidator2.Visible = true;
        //    return;
        //}
        int id = fn.Get<int>(Request.QueryString["id"]);
        String companyName = ConfigurationManager.AppSettings["CompanyName"];
        String siteName = ConfigurationManager.AppSettings["SiteName"];
        Model = ServiceLocator.GetRepository().GetBrokerByID(id);
        listing = GetListing();
        if (IsValid && Model != null && !string.IsNullOrEmpty(Model.Email))
        {
            StringBuilder body = new StringBuilder();

            body.AppendLine("<div class='overlay_container' style='background-color: #ffffff; color: #2D2D2D; font-family: Arial; font-size: 12px;line-height: 1.2em;width:1000px;'>");
            body.AppendFormat("<div style='width:1000px;clear:both;display:block;'><div style='width:500px;float:left;'><img src='http://{0}/images/PRINT-Logo.jpg' style='width:176px; border:none;' /></div>", Request.Url.Host);
            body.Append("<div style='float:right;width:500px;'>Manhattan Management Group<br/>P:(212)967-7013<br/>http://www.mmgnyc.com</div></div>");
            body.Append("<div><h3>New Alert Submission:</h3></div>");
            //body.AppendFormat("<div>{0}: \t {1} {2}</div>", Resources.Email.ToLabel, Model.FirstName, Model.LastName);
            body.AppendFormat("<div>{0}: \t {1}</div>", Resources.Email.ClientNameEmailLabel, yourName.Text);
            body.AppendFormat("<div>{0}: \t {1}</div>", Resources.Email.ClientEmailEmailLabel, yourEmail.Text);
            body.AppendFormat("<div>{0}: \t {1}</div>", Resources.Email.ClientPhoneEmailLabel, yourPhone.Text);
            //body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.SubjectLabel, subjectEmail.Text);
            if (!string.IsNullOrEmpty(Request.QueryString["listingid"]))
                body.AppendFormat("<div>Listing: \t{0}</div>", "Listing ID# " + listing.ListingID);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientMessageEmailLabel, messageText.Text);
            if (!string.IsNullOrEmpty(Request.QueryString["listingid"]))
                body.AppendFormat("You can <a href='http://{1}/detail.aspx?id={0}'>view the listing by clicking here</a>", Request.QueryString["listingid"], Request.Url.Host);
            else

                body.AppendFormat("I want to talk to you. please contact with me");
            if (PreferEmail.Checked)
                body.AppendFormat("<div>I prefer to be contact by email</div>");
            else
                body.AppendFormat("<div>I prefer to be contact by phone</div>");
            body.AppendLine("</div>");

            MailMessage message = new MailMessage(yourEmail.Text, Model.Email);
            if (string.IsNullOrEmpty(Request.QueryString["listingid"]))
            {
                message.Subject = "You have a message from xxx.com";

            }
            else
                message.Subject = "Regarding Web id " + Request.QueryString["listingid"];
            message.Body = body.ToString();
            message.IsBodyHtml = true; 
            //message.Bcc.Add("info@mmgnyc.com");
            SmtpClient client = new SmtpClient();
            try
            {
                client.Send(message);
                formpanel.Visible = false;
                sentpanel.Visible = true;
            }
            catch (Exception ex)
            {
                exception = ex;
                formpanel.Visible = false;
                sentfailed.Visible = true;
            }
        }
    }

}