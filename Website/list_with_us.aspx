﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.master" AutoEventWireup="true"
    CodeFile="list_with_us.aspx.cs" Inherits="list_with_us" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
    <link href="css/site.css" rel="stylesheet" type="text/css" />
    <!--[if IE]> <link href="css/IE.css" rel="stylesheet" type="text/css"> <![endif]-->
    <script src="js/cufon-yui.js" type="text/javascript"></script>
    <script src="js/HelveticaThin_300.font.js" type="text/javascript"></script>
    <script type="text/javascript">
        Cufon.replace('.titles-global');
        Cufon.replace('.page-titles');
        Cufon.replace('.font');
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <form runat="server">
    <div class="list-with-us-page">
        <div class="titles-global" style="margin-bottom: 0px; color: #283E69;">
            List Your Property With Us
            <%--List With Us--%>
        </div>
         <br />
        <p>
            <span>New York City Landlords and Property Managers: List Your Apartment with Beekman
                Street Realty</span></p>
        <p>
            If you have an apartment to rent out, and you are interested in reaching the maximum
            number of pre-qualified renters in the New York City Metropolitan area, list your
            apartment with us today at no cost to you. We will feature your listing on our website,
            <a href="default.aspx" target="_parent">www.BeekmanStreetRealty.com</a>, allowing you maximum exposure. Our listings reach thousands
            of apartment seekers and we will find you the right tenant.
        </p>
        <p>
            Contact us today to list your rental property with Beekman Street Realty.</p>
        
        <div class="page-titles" style="text-transform: none; color: #283E69;">
            Your Contact Information
        </div>
        <div class="list-with-us-container clearfix" runat="server" id="content_Area">
            <div class="list-with-us-columns left">
                <div class="list-with-us-labels">
                    First Name
                </div>
                <div class="list-with-us-inputs">
                    <asp:TextBox runat="server" ID="FirstName" CssClass="list-with-us-inputs-input" Width="168"></asp:TextBox>
                    <asp:RequiredFieldValidator ErrorMessage="*Please enter your first name." CssClass="error-message-text"
                        runat="server" ID="required1" ControlToValidate="FirstName" ValidationGroup="group1"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="list-with-us-columns right">
                <div class="list-with-us-labels">
                    Last Name
                </div>
                <div class="list-with-us-inputs">
                    <asp:TextBox runat="server" ID="LastName" CssClass="list-with-us-inputs-input" Width="168"></asp:TextBox>
                    <asp:RequiredFieldValidator ErrorMessage="*Please enter your last name." CssClass="error-message-text"
                        runat="server" ID="RequiredFieldValidator1" ControlToValidate="LastName" ValidationGroup="group1"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="list-with-us-container clearfix">
            <div class="list-with-us-columns left">
                <div class="list-with-us-labels">
                    Phone
                </div>
                <div class="list-with-us-inputs" nowrap>
                    <asp:TextBox runat="server" ID="phone" CssClass="list-with-us-inputs-input" Width="168"></asp:TextBox>
                    <asp:RequiredFieldValidator ErrorMessage="*Please enter your phone number." CssClass="error-message-text"
                        runat="server" ID="RequiredFieldValidator2" ControlToValidate="phone" ValidationGroup="group1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ControlToValidate="phone" ErrorMessage="*Please enter a valid phone number."
                        CssClass="invalid-message-text" runat="server" ID="reg2" ValidationExpression="1?\s*\W?\s*([2-9][0-8][0-9])\s*\W?\s*([2-9][0-9]{2})\s*\W?\s*([0-9]{4})(\se?x?t?(\d*))?"></asp:RegularExpressionValidator>
                </div>
            </div>
        </div>
        <div class="list-with-us-container clearfix">
            <div>
                <%--<div class="list-with-us-columns left">--%>
                <div class="list-with-us-labels">
                    I am a(n)
                </div>
                <div class="list-with-us-radio left">
                    <%--<div class="list-with-us-inputs">--%>
                    <asp:RadioButton GroupName="groupowner" ValidationGroup="group1" ID="owner" runat="server"
                        Checked="true" />
                    Owner
                    <asp:RadioButton GroupName="groupowner" ValidationGroup="group1" ID="Developer" runat="server" />
                    Developer
                    <asp:RadioButton GroupName="groupowner" ValidationGroup="group1" ID="PropertyManager"
                        runat="server" />Property Manager
                    <asp:RadioButton GroupName="groupowner" ValidationGroup="group1" ID="Other" runat="server" />Other
                </div>
            </div>
        </div>
        <div class="list-with-us-container clearfix">
            <div>
                <div class="list-with-us-labels">
                    This apartment is for
                </div>
                <div class="list-with-us-radio left">
                    <asp:RadioButton GroupName="groupType" ValidationGroup="group2" ID="Sale" runat="server"
                        Checked="true" />Sale
                    <asp:RadioButton GroupName="groupType" ValidationGroup="group2" ID="Rent" runat="server" />Rent
                    <asp:RadioButton GroupName="groupType" ValidationGroup="group2" ID="SaleAndRent"
                        runat="server" />Sale/Rent
                </div>
            </div>
        </div>
        <br />
        <div class="page-titles" style="text-transform: none; color: #283E69;">
            Property Information
        </div>
        <div class="list-with-us-container clearfix">
            <div class="list-with-us-columns left">
                <div class="list-with-us-labels">
                    Address
                </div>
                <div class="list-with-us-inputs">
                    <asp:TextBox runat="server" ID="Building_street" CssClass="list-with-us-inputs-input"
                        Width="168"></asp:TextBox>
                    <asp:RequiredFieldValidator ErrorMessage="*Please enter the building address." CssClass="error-message-text"
                        runat="server" ID="RequiredFieldValidator3" ControlToValidate="Building_street"
                        ValidationGroup="group1"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="list-with-us-columns right">
                <div class="list-with-us-labels">
                    City / State
                </div>
                <div class="list-with-us-inputs">
                    <asp:TextBox runat="server" ID="CityState" CssClass="list-with-us-inputs-input" Width="168"></asp:TextBox>
                    <asp:RequiredFieldValidator ErrorMessage="*Please enter the City and State." CssClass="error-message-text"
                        runat="server" ID="RequiredFieldValidator4" ControlToValidate="CityState" ValidationGroup="group1"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="list-with-us-container clearfix">
            <div class="list-with-us-columns left">
                <div class="list-with-us-labels">
                    Zip Code
                </div>
                <div class="list-with-us-inputs">
                    <asp:TextBox runat="server" ID="ZipCode" CssClass="list-with-us-inputs-input" Width="168"></asp:TextBox>
                    <asp:RequiredFieldValidator ErrorMessage="*Please enter the Zip Code." CssClass="error-message-text"
                        runat="server" ID="RequiredFieldValidator9" ControlToValidate="ZipCode" ValidationGroup="group1"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="list-with-us-columns right">
                <div class="list-with-us-labels">
                    Apartment
                </div>
                <div class="list-with-us-inputs">
                    <asp:TextBox runat="server" ID="Apartment" CssClass="list-with-us-inputs-input" Width="168"></asp:TextBox>
                    <asp:RequiredFieldValidator ErrorMessage="*Please enter the apartment number." CssClass="error-message-text"
                        runat="server" ID="RequiredFieldValidator8" ControlToValidate="Apartment" ValidationGroup="group1"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="list-with-us-container clearfix">
            <div class="list-with-us-columns left">
                <div class="list-with-us-labels">
                    Size
                </div>
                <div class="list-with-us-inputs">
                    <asp:DropDownList runat="server" ID="bedrooms" ValidationGroup="group1">
                        <asp:ListItem>Select Size</asp:ListItem>
                        <asp:ListItem>Studio [No Bedroom]</asp:ListItem>
                        <asp:ListItem>1 Bedroom</asp:ListItem>
                        <asp:ListItem>2 Bedroom</asp:ListItem>
                        <asp:ListItem>3 Bedroom</asp:ListItem>
                        <asp:ListItem>4 Bedroom+</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" InitialValue="Select Size" ValidationGroup="group1"
                        ControlToValidate="bedrooms" ID="reg3" ErrorMessage="*Please choose a size."
                        CssClass="error-message-text"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="list-with-us-columns right">
                <div class="list-with-us-labels">
                    Location
                </div>
                <div class="list-with-us-inputs">
                    <asp:DropDownList runat="server" ID="Region" ValidationGroup="group1">
                        <asp:ListItem>Select Location</asp:ListItem>
                        <asp:ListItem>Downtown </asp:ListItem>
                        <asp:ListItem>Midtown East </asp:ListItem>
                        <asp:ListItem>Midtown West </asp:ListItem>
                        <asp:ListItem>Upper East Side </asp:ListItem>
                        <asp:ListItem>Upper West Side </asp:ListItem>
                        <asp:ListItem>Upper Manhattan </asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" InitialValue="Select Region" ValidationGroup="group1"
                        ControlToValidate="Region" ID="RequiredFieldValidator7" ErrorMessage="*Please choose a location."
                        CssClass="error-message-text"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="list-with-us-container clearfix">
            <div class="list-with-us-columns left">
                <div class="list-with-us-labels">
                    Bathrooms
                </div>
                <div class="list-with-us-inputs">
                    <asp:DropDownList runat="server" ID="bathrooms" ValidationGroup="group1">
                        <asp:ListItem>Select Baths</asp:ListItem>
                        <asp:ListItem>1&nbsp;&nbsp; Bath</asp:ListItem>
                        <asp:ListItem>2&nbsp;&nbsp; Baths</asp:ListItem>
                        <asp:ListItem>3&nbsp;&nbsp; Baths</asp:ListItem>
                        <asp:ListItem>4+ Baths</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="list-with-us-columns right">
                <div class="list-with-us-labels">
                    Neighborhood
                </div>
                <div class="list-with-us-inputs">
                    <asp:DropDownList runat="server" ID="Neighborhood" ValidationGroup="group1">
                        <asp:ListItem>Select Neighborhood</asp:ListItem>
                        <asp:ListItem>Financial District</asp:ListItem>
                        <asp:ListItem>Tribeca </asp:ListItem>
                        <asp:ListItem>Soho </asp:ListItem>
                        <asp:ListItem>Lower East Side </asp:ListItem>
                        <asp:ListItem>Greenwich Village </asp:ListItem>
                        <asp:ListItem>Gramercy Park </asp:ListItem>
                        <asp:ListItem>Chelsea </asp:ListItem>
                        <asp:ListItem>Sutton Place </asp:ListItem>
                        <asp:ListItem>Murray Hill </asp:ListItem>
                        <asp:ListItem>Midtown East </asp:ListItem>
                        <asp:ListItem>Clinton </asp:ListItem>
                        <asp:ListItem>Midtown East </asp:ListItem>
                        <asp:ListItem>Upper East Side </asp:ListItem>
                        <asp:ListItem>Upper West Side </asp:ListItem>
                        <asp:ListItem>Harlem </asp:ListItem>
                        <asp:ListItem>Upper Manhattan </asp:ListItem>
                        <asp:ListItem>All Manhattan </asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" InitialValue="Select Neighborhood" ValidationGroup="group1"
                        ControlToValidate="Neighborhood" ID="RequiredFieldValidator6" ErrorMessage="*Please choose a neighborhood."
                        CssClass="error-message-text"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="list-with-us-container clearfix">
            <div class="list-with-us-columns left">
                <div class="list-with-us-labels">
                    Asking Price
                </div>
                <div class="list-with-us-inputs">
                    <asp:TextBox runat="server" ID="price" CssClass="list-with-us-inputs-input" Width="168"
                        ValidationGroup="group1"></asp:TextBox>
                    <asp:RequiredFieldValidator ErrorMessage="*Please enter an asking price." CssClass="error-message-text"
                        runat="server" ID="RequiredFieldValidator5" ControlToValidate="price" ValidationGroup="group1"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="list-with-us-columns right">
                <div class="list-with-us-labels">
                    Square Footage
                </div>
                <div class="list-with-us-inputs">
                    <asp:TextBox runat="server" ID="sqft" CssClass="list-with-us-inputs-input" Width="168"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="list-with-us-container clearfix">
            <div class="left">
                <div class="list-with-us-labels">
                    Building Features
                </div>
                <div class="checkboxes left">
                    <asp:CheckBox ID="Elevator_CheckBox" Text="Elevator" runat="server" />&nbsp;&nbsp;
                    <asp:CheckBox ID="Doorman_CheckBox" Text="Doorman" runat="server" />&nbsp;&nbsp;
                    <asp:CheckBox ID="Pet_Friendly_CheckBox" Text="Pet Friendly" runat="server" />&nbsp;&nbsp;
                    <asp:CheckBox ID="Gym_CheckBox" Text="Fitness Facility" runat="server" />&nbsp;&nbsp;
                    <asp:CheckBox ID="Laundry_CheckBox" Text="Laundry" runat="server" />&nbsp;&nbsp;
                    <asp:CheckBox ID="Outdoor_Space_CheckBox" Text="Outdoor Space" runat="server" />&nbsp;&nbsp;
                </div>
            </div>
        </div>
        <div class="list-with-us-container clearfix">
            <div class="left">
                <div class="list-with-us-labels" style="margin-top: 6px;">
                    Apartment Features
                </div>
                <div class="apt-amenities-checkboxes left">
                    <asp:CheckBox ID="Furnished" Text="Furnished" runat="server" />&nbsp;&nbsp;
                    <asp:CheckBox ID="Terrace_Balcony" Text="Outdoor Space" runat="server" />&nbsp;&nbsp;
                    <asp:CheckBox ID="Washer_Dryer" Text="Washer / Dryer" runat="server" />&nbsp;&nbsp;
                    <asp:CheckBox ID="Dishwasher" Text="Dishwasher" runat="server" />&nbsp;&nbsp;
                </div>
            </div>
        </div>
        <%-- <div class="list-with-us-container clearfix">
            <div class="list-with-us-radio left">
                <asp:RadioButton ID="saleRadio" runat="server" GroupName="propertyType" Checked="true" />
                Sales 
            </div>
            <div class="list-with-us-radio left">
                <asp:RadioButton ID="rentRadio" runat="server" GroupName="propertyType" />
                <span>Rentals</span> 
            </div>
        </div>--%>
        <div class="list-with-us-container clearfix">
            <div>
                Additional Comments
            </div>
            <div class="text-area">
                <asp:TextBox runat="server" ID="comments" TextMode="MultiLine"></asp:TextBox>
            </div>
            <div class="button button-image short-button">
                <span class="font">Submit</span>
                <asp:ImageButton runat="server" ID="buttom1" ImageUrl="images/clear.png" ValidationGroup="group1"
                    OnClick="buttom1_Click" />
            </div>
        </div>
        <div runat="server" id="sent_ok" visible="false">
            <p>
                Thank you for contacting us. One of our agents will be happy to assist you. We appreciate
                your inquiry.
            </p>
        </div>
    </div>
    </form>
</asp:Content>
