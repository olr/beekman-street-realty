﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Xml.Linq;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class neighborhood_guide2 : BasePage<neighborhood_guide2,string>
{
    protected string neighborhood;
    protected string pType;
    protected string searchType;
    protected void Page_Load(object sender, EventArgs e)
    {
        pType = Request.QueryString["type"];
        searchType = Request.QueryString["searchtype"];
        Model = Request.QueryString["neighborhood"]??"uppereastside";

        neighborhood = Model;
        //Response.Write(Model);
        //int neighborhoodid=node.Attribute(
        this.Controls.DataBind<string>(neighborhood);
    }
}