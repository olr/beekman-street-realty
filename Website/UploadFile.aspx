﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.master" AutoEventWireup="true" CodeFile="UploadFile.aspx.cs" Inherits="UploadFile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        $(function () {
            $('input[type=submit]').click(function () {
               // console.log($('#uploadFile').val());
                parent.$.fn.colorbox.close();
            });
        })
    </script>
    <!--[if IE]> <link href="css/IE.css" rel="stylesheet" type="text/css"> <![endif]-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<div class="popup_upload">
    <form id="fileUpload" enctype="multipart/form-data" runat="server">
    <p>Please select the file you wish to upload to docfolio.</p>

    <input type="file" name="uploadFile" id="uploadFile" runat="server" class="browsebutton"/>
   <%-- <input type="submit" id="uploadBtn" />--%>
   <asp:Button ID="UploadBtn" Text="Upload" runat="server" onclick="UploadBtn_Click" class="uploadbutton"/>
    <%--onclick="UploadFile(<%#(Container.DataItem as UserCategory).categoryID %>)"--%>
    </form>
</div>
</asp:Content>

