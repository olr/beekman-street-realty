﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="FeaturedBuildings.aspx.cs" Inherits="FeaturedBuildings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
<div class="static-pages clearfix">
        <div class="static-content clearfix">
    <div class="titles-global">
        featured buildings
    </div>            <br/>
            <%=Html.RenderPartial("Cntrls/Lists/FeaturedBuildings",Model) %>
        </div>
    </div>
</asp:Content>

