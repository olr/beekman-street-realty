﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="neighborhood_news.aspx.cs" Inherits="neighborhood_news" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
        <div class="neighborhood_news_container clearfix">
            <div class="left_neighborhood_col">
                <div class="neighborhood_feed_logos">
                    <img src="images/tnyt.png" alt="" />
                </div>
                <%=Html.RenderPartial("cntrls/Lists/Rss", "http://feeds.nytimes.com/nyt/rss/RealEstate")%>
                <div class="neighborhood_feed_logos">
                    <img src="images/curbed_logo.png" alt="" />
                </div>
                <%=Html.RenderPartial("cntrls/Lists/Rss", "http://feeds.feedburner.com/curbed/BHBe")%>

                <div class="neighborhood_feed_logos">
                    <img src="http://therealdeal.com/images/realdeallogo.jpg" alt="" />
                </div>
                <%=Html.RenderPartial("cntrls/Lists/Rss", "http://feeds.feedburner.com/trdnews")%>
            </div>

            <div class="right_neighborhood_col">

                <div class="neighborhood_feed_logos">
                    <h1>Brooklynian</h1>
                </div>
                <%=Html.RenderPartial("cntrls/Lists/Rss", "http://brooklynian.com/forum/rss")%>

                <div class="neighborhood_feed_logos">
                    <h1>Brokelyn</h1>
                </div>
                <%=Html.RenderPartial("cntrls/Lists/Rss", "http://www.brokelyn.com/feed")%>

                <div class="neighborhood_feed_logos">
                    <h1>Only The Blog Knows Brooklyn</h1>
                </div>
                <%=Html.RenderPartial("cntrls/Lists/Rss", "http://onlytheblogknowsbrooklyn.com/feed/rss/")%>
                
            </div>

        </div>
</asp:Content>

