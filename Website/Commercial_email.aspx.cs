﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net.Mail;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Configuration;

public partial class email_agent : System.Web.UI.Page
{
    protected BrokerInfo Model = new BrokerInfo();

    protected override void OnInit(EventArgs e)
    {
        sendEmail.Click += new EventHandler(Send_Email);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int id = fn.Get<int>(Request.QueryString["id"]);
            if (id > 0)
            {
                Model = ServiceLocator.GetRepository().GetBrokerByID(id);
            }
            else
            {
                Model = new BrokerInfo();
                Model.FirstName = ConfigurationManager.AppSettings["SiteName"];
                Model.Email = ConfigurationManager.AppSettings["defaultEmail"];
            }
            if (Model != null)
            {
                this.Controls.DataBind<BrokerInfo>(Model);
                var l = GetListing();
                if (l != null)
                {
                    subjectEmail.Text = "Request more info: " + l.GetDisplayAddress();
                    messageText.Text = "Please contact me about the following Web ID: " + l.ListingID;
                }

                this.DataBind(true);
            }
        }
    }

    ListingInfo GetListing()
    {
        ListingInfo l = null;
        string lid = Request.QueryString["listingid"];
        if (!string.IsNullOrEmpty(lid))
            l = ServiceLocator.GetRepository().GetListingInfoByID(lid);
        return l;
    }

    void Send_Email(object sender, EventArgs e)
    {
        int id = fn.Get<int>(Request.QueryString["id"]);
        Model = ServiceLocator.GetRepository().GetBrokerByID(id);
        if (IsValid && Model != null && Model.ID > 0)
        {
            StringBuilder body = new StringBuilder();
            string propertyType = Request["PropertyType"];
            body.AppendLine("<div class='overlay_container' style='background-color: #ffffff; color: #2D2D2D; font-family: Arial; font-size: 12px;line-height: 1.2em;'>");
            body.AppendFormat("<div>{0}: {1} {2}</div>", Resources.Email.ToLabel, Model.FirstName, Model.LastName);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientNameEmailLabel, yourName.Text);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientEmailEmailLabel, yourEmail.Text);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientPhoneEmailLabel, yourPhone.Text);
            body.AppendFormat("My desired property type is: {0}<br />\n", propertyType);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.SubjectLabel, subjectEmail.Text);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientMessageEmailLabel, messageText.Text);
            body.AppendLine("</div>");

            MailMessage message = new MailMessage(yourEmail.Text, Model.Email);
            message.Subject = subjectEmail.Text;
            message.Body = body.ToString();
            message.CC.Add(yourEmail.Text);
            message.IsBodyHtml = true; 

            SmtpClient client = new SmtpClient();
            try
            {
                client.Send(message);
            }
            catch
            {
            }
            finally
            {
                formpanel.Visible = false;
                sentpanel.Visible = true;
            }
        }
    }

}
