﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class map : BasePage<map, ListingInfo>
{
    protected string Address { get; set; }
    protected string APIKey { get; set; }
    protected double Longitude { get; set; }
    protected double Latitude { get; set; }
    protected bool isExclusive = false;
    protected bool showAddress = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Model = this.GetListing();
            if (Model != null)
            {
                this.Controls.DataBind<ListingInfo>(Model);

                if (!Model.IsCommercialListing)
                {
                    isExclusive = Model.IsExclusive;
                    showAddress = Model.ShowAddress;
                    var building = Model.GetBuilding();
                    if (building != null)
                    {
                        BrokerTemplate.Web.LocationPoint l = SiteHelper.GeocodeAddress(Model.BuildingID);
                        if (l.Longitude != 0)
                        {
                            Latitude = l.Latitude;
                            Longitude = l.Longitude;
                        }
                        else
                        {
                            l = new LocationPoint { Address = string.Format("{0}, {1}, {2} {3}", Model.Address.Replace(' ', '-'), Model.City.Replace(' ', '-'), Model.State.Replace(' ', '-'), Model.Zip) };

                            if (SiteHelper.GeocodeAddress(l))
                            {
                                Latitude = l.Latitude;
                                Longitude = l.Longitude;
                            }
                        }
                    }
                }
                else
                {
                    CommercialInfo cl = (CommercialInfo)Model;
                    isExclusive = cl.IsExclusive;
                    showAddress = cl.ShowAddress;
                    LocationPoint l = new LocationPoint { Address = string.Format("{0}, {1}, {2} {3}", cl.Address.Replace(' ', '-'), cl.City.Replace(' ', '-'), cl.State.Replace(' ', '-'), cl.Zip) };

                    if (SiteHelper.GeocodeAddress(l))
                    {
                        Latitude = l.Latitude;
                        Longitude = l.Longitude;
                    }
                }
            }
        }
    }
}