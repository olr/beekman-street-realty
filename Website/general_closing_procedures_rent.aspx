﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="general_closing_procedures_rent.aspx.cs" Inherits="general_closing_procedures_rent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages guides">
        <a href="renters_guide.aspx">Renter's Guide</a> > <a href="standard_fund_requirements_rent.aspx">
            Standard Fund Requirements</a> > <span>General Closing Procedures</span> > <a href="typical_questions_answered_rent.aspx">
                Typical Questions Answered</a>
        <div class="page-titles-guides">
            General Closing Procedures
        </div>
        <br />
        <p>
            Approval Requirements differ depending on the landlord/management company and/or
            on the building. The following are general guidelines that apply in most instances:
        </p>
        <ul>
            <li>Renters need to show income of at least 40-50 times monthly rent. </li>
            <li>Rent should be 25% of renters' annual income. Ex: annual income = $100,000, rent
                $25,000 per year.</li>
            <li>Renters must show a secure and stable employment history.</li>
            <li>Renters needs to have good credit history. A major component of the approval process
                is based on the credit report. </li>
            <li>What makes a credit report bad? </li>
            <ul>
                <li>A couple old late payments are usually ok. </li>
                <li>High revolving balances are not good. </li>
                <li>Past due payments that are outstanding are bad. </li>
                <li>Delinquencies and collections are very bad. </li>
                <li>A public record (a day in court) is bad </li>
                <li>A Landlord-Tenant record is very bad. </li>
            </ul>
            <p>
                If you think your credit report is bad, advise your Netmore Realty Group broker
                so they can prepare you for the application process. Landlords/management companies
                have different ways of dealing with credit problems.
            </p>
            <p>
                Some landlords/management companies allow renters to increase security deposit while
                others require a guarantor. Some may simply reject the application. Your Netmore
                Realty Group broker will advise you in order to save you time, money and frustration.
            </p>
            <li>Renters must show a good rental history.</li>
            <li>Rental, credit and employment information is generally referenced and submitted
                in form of a letter or sometimes by a phone call.</li>
            <li>Renters need to have a social security number. If you do not have a social security
                number or are not applying for one you must have a guarantor co-signing your lease.</li>
            <li><strong>Bank Statements:</strong> In most cases landlords/management companies require
                copies of a renters' 3 most recent bank statements from a checking, savings, stock
                account or any other financial institution. </li>
            <li>Renter should have backup detail sheets available upon request.</li>
            <li>Most rent application forms require listing bank account information.</li>
            <li><strong>Letter of Employment:</strong> Renter should get a Letter of Employment
                on their company's letterhead, with the following: </li>
            <ul>
                <li>Position</li>
                <li>Length of employment</li>
                <li>Annual income.</li>
            </ul>
            In majority of cases an offer or an acceptance letter for a new position is not
            sufficient to show employment.
            <li><strong>Letter of Landlord Reference:</strong> Letter of reference from a previous
                landlord is one of the most important reference letters a renter can submit to a
                landlord/management company who is processing their application. </li>
            <li><strong>Pay Stubs:</strong> copies of renters' 3 most recent pay stubs are usually
                required.</li>
            <li>Students need to have a guarantor co-sign their lease. </li>
            Certain buildings won't accept students. This is legal in NY State and not challengeable
            in a court or law.
            <li><strong>Tax Returns:</strong> Some landlords and some management companies require
                potential renters to include a copy of their most recent Federal Tax Return (1040).
            </li>
            <ul>
                <li>Renters should have their tax return available just in case it is required.
                </li>
                <li>Self-employed renters: if income comes from different sources, you will be required
                    to submit tax returns and a letter from a CPA stating the nature of your business,
                    as well as, projected future income.</li>
                <li>Business owners looking to rent are almost always required to submit a tax return.</li></ul>
            If renters do not meet the above mentioned criteria there are usually two possible
            solutions:
            <li>Renters can get a co-signer or guarantor to co-sign the lease for you. </li>
            <li>Some owners will allow tenants to prepay 6 months to 12 months rent up front.</li>
            <li><strong>Guarantor or co-signer:</strong> A guarantor or co-signer is a person who
                guarantees the entire rent for the entire lease term and lease renewals should a
                renter default on their rent payments. A guarantor can be anyone who agrees to sign
                on behalf of a renter and who is willing qualified to act as a guarantor.</li>
            <li>Qualifications of a guarantor or co-signer: documents required from a guarantor
                are similar to applicant(s):</li>
            <ul>
                <li>Application Form: Same form as the applicant fills out. </li>
                <li>Credit Report: A guarantor's credit report must be in good standing. </li>
                <li>Federal Tax Returns: A guarantor is required to submit federal tax returns as proof
                    of income.</li>
                <li>Guarantor must show annual income of 80-100 times monthly rent. </li>
                <li>A Guarantor is not required to be present at lease signing. </li>
                In some cases, landlords/management companies require a guarantor to reside in the
                tri- state area (New York, New Jersey or Connecticut).
            </ul>
        </ul>
        <br />
    </div>
</asp:Content>
