﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.master" AutoEventWireup="true"
    CodeFile="DocfolioLogin.aspx.cs" Inherits="DocfolioLogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
    $(document).ready(function(){
        
            var isvalid = <%= valid.ToString().ToLower()%>;
            var True=true;
            var False=false;
            //alert(isvalid);
            if (isvalid)
            {
                //alert(isvalid);
                parent.$.fn.colorbox.close();
                parent.window.location = "docfolio.aspx";
            }
        
    });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="agent-login-container">
        <p class="span">
            *This area is restricted to agents who work for this company.
        </p>
        <form id="loginForm" runat="server">
        <div class="username" style="margin-top: 14px;">
            <%--<span>Username:</span> <input type="text" style="margin-left:4px; width:150px;" />--%>
            <div class="clearfix">
                <asp:Label ID="Label1" Text="Username:" runat="server"></asp:Label>
                <asp:TextBox ID="uname" runat="server"></asp:TextBox>
            </div>
            &nbsp;
            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator3" ControlToValidate="uname"
                runat="server" ErrorMessage="*Please enter your username." CssClass="error-message-text" />
        </div>
        <div class="password" style="margin-top: 7px;">
            <%--<span>Password:</span> <input type="text" style="margin-left:7px; width:150px;" />--%>
            <div class="clearfix">
                <asp:Label ID="Label2" Text="Password:" runat="server"></asp:Label>&nbsp;
                <asp:TextBox ID="pwd" runat="server" TextMode="Password"></asp:TextBox>
            </div>
            &nbsp;
            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" ControlToValidate="pwd"
                runat="server" ErrorMessage="*Please enter your password." CssClass="error-message-text" />
        </div>
        <asp:Label ID="ErrorMsg" runat="server"></asp:Label>
        <div class="submit submit-button button-image" style="margin-top: 7px; margin-left: 125px;">
            <%--<img src="img/common/submit.png" />--%>
            <asp:ImageButton ID="submitBtn" class="submitBtn" ImageUrl="images/clear.png" runat="server"
                OnClick="submitBtn_Click" />
        </div>
        </form>
        <p>
            *Your username and password are the same as OLR.
        </p>
        <p>
            *If you do not have your login credentials, please contact your firm's administrator.</p>
    </div>
</asp:Content>
