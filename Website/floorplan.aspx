﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="floorplan.aspx.cs" Inherits="floorplan" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title></title>
     <style type="text/css">
      td, th
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #666666;
            line-height:17px;
        }
        .print-icon 
        {
            background:url(images/icon-sprite.png) no-repeat; 
            background-position: -203px 0px; 
            width:29px; 
            height:31px;
            margin-top:5px;
        }
        .print-icon:hover 
        {
            background:url(images/icon-sprite.png) no-repeat; 
            background-position: -203px -31px; 
            width:29px; 
            height:31px;
        }
        img {border:none;}
        .style5 {font-size:26px; line-height:40px; font-weight:bold;}
        .disclaimer {font-size:11px; line-height:15px;}

    </style>
    <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript">
       $(function () {
            $('#print_button').click(function () {
            window.location="floorplan.aspx?id=<%=Model.ListingID %>&print=t";
            });

            <%if(print=="true" ){%>
           
                $('#print_button').attr('style', 'display:none;');
                $('.zoom').attr('style', 'display:none;');
                window.print();
           <%} %>

        })
    </script>
</head>
<body>
    <div class="wrap">
        <div style="margin:-8px auto 0 auto; width: 700px;">
            <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" valign="top">
                        <%if (Model.GetMedia().Floorplans.Count > 0)
                          {%>
                        <%=Html.RenderPartial("cntrls/Print/header", Model)%>
                       
                        <%= Html.RenderPartial("cntrls/Print/floorplans", Model)%>
                        <%=Html.RenderPartial("cntrls/Print/footer", Model)%>
                        <% }
                         %>
                        
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
