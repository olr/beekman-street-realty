﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="search_commercial.aspx.cs" Inherits="search_commercial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/search.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("img#nav3").attr("src", "images/menu/menu_selected_04.png");
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <form method="post" action="result.aspx">
    <%=Html.Hidden("IsCommercialSearch", "1", null) %>
    <div class="clearfix">
    <div class="sale_rent_toggle">
            <input type="radio" name="PropertyType" checked="checked" value="Sale" /><span>Sales</span>
            <input type="radio" name="PropertyType" value="Rental" /><span>Rentals</span>
    </div>
    <%=Html.RenderPartial("cntrls/Search/Map_Manhattan_Macro", Model)%>
    <div class="option_col">

        <div class="page_titles">Location</div>
        <div class="title_underline"></div>
        
        <%=Html.RenderPartial("cntrls/Search/Neighborhood_Manhattan_Macro", Model)%>

        <div class="section clearfix">

            <div class="column half">
                <div id="dynamic-price-range">
                    <%=Html.RenderPartial("cntrls/Search/PriceRange", Model)%>
                </div>
            </div>

        </div>

        <div class="section clearfix" id="dynamic-commercial-options" style="width:45%;">
            <%=Html.RenderPartial("cntrls/Search/CommercialForm", Model)%>
        </div>

        <div class="search_buttons">
            <input type="image" src="images/submit.jpg" class="column" />
            <a href="search.aspx"><img src="images/reset.jpg" alt="reset" /></a>
        </div>

    </div>
    
    </div>

    </form>

</asp:Content>

