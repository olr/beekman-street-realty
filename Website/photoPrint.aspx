﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="photoPrint.aspx.cs" Inherits="photoPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title></title>
    <style type="text/css">
      td, th
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            color: #666666;
        }
    </style>
    <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.colorbox-min.js"></script>
    <script type="text/javascript">
        $(function () {
            window.print();
        })
    </script>
</head>
<body>
    <div class="wrap">
        <div style="margin:-8px auto 0 auto; width: 700px;">
            <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" valign="top">
                        <%if (Model.GetMedia().Photos.Count > 0)
                          {%>
                        <%=Html.RenderPartial("cntrls/Print/header", Model)%>
                        <%= Html.RenderPartial("cntrls/Print/Photos", Model)%>
                        <%=Html.RenderPartial("cntrls/Print/footer", Model)%>
                        <% }
                          %>
                        
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
