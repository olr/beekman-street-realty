﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;


public partial class Photo2 : BasePage<Photo2, ListingInfo>
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Model = ServiceLocator.GetRepository()
                                 .GetListingInfoByID(Request.QueryString["id"]);
            if (Model != null)
            {
                this.Controls.DataBind<ListingInfo>(Model);
                this.DataBind();

                var media = Model.GetMedia();
                list.DataSource = media.Photos;
                list.DataBind();
            }
        }

    }
}