﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.master" AutoEventWireup="true"
    CodeFile="email_form.aspx.cs" Inherits="email_form" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
    <link href="css/site.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $('input.reemail').keypress(function () {

            });
        })
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="formpanel" runat="server">
        <form id="Form1" runat="server">
        <div class="email-form-container">
            <h1 class="email-form-title">
                Send Email to
                <%=Model.FirstName %>
                <%=Model.LastName %></h1>
            <div class="section clearfix">
                <div class="left section-title">
                    Your Name</div>
                <div class="right">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="yourName"
                        runat="server" ErrorMessage="*Please enter your name." CssClass="error-message-text" />
                    <asp:TextBox ID="yourName" runat="server" />
                </div>
            </div>
            <div class="section clearfix">
                <div class="left section-title">
                    Your Email</div>
                <div class="right">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="yourEmail"
                        runat="server" ErrorMessage="*Please enter your email." CssClass="error-message-text" />
                    <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="yourEmail" ID="RegularExpressionValidator1"
                        runat="server" ErrorMessage="*Please enter a valid email address." CssClass="invalid-message-text" ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$" />
                 <asp:TextBox class='email' ID="yourEmail" runat="server" />
                   </div>
            </div>
            <div class="section clearfix">
                <div class="left section-title">
                    Phone Number</div>
                <div class="right">
                    <asp:TextBox ID="yourPhone" runat="server" />
                </div>
            </div>
            <div class="section clearfix">
                <div class="left section-title">
                    How would you like to be contacted?</div>
                <div class="right clearfix">
                    <div class="left" style="margin:7px 10px 0 0;">
                        <asp:RadioButton GroupName="prefer" ID="PreferEmail" runat="server" Checked="true" CssClass="email-radio" />
                        email</div>
                    <div class="left" style="margin-top:7px;">
                        <asp:RadioButton GroupName="prefer" ID="PreferPhone" runat="server" CssClass="email-radio"/>
                        phone &nbsp;&nbsp;</div>
                </div>
                 </div>
                <div class="section clearfix">
                    <div class="left section-title">
                        International country code
                        <br>
                        (if outside U.S.)</div>
                    <div class="right" style="padding-top: 17px;">
                        <asp:TextBox ID="Countrycode" runat="server" />
                    </div>
                </div>
                <div class="section clearfix">
                    <div class="section-title">
                        Your Comments</div>
                    <div class="text-area">
                        <asp:TextBox ID="messageText" runat="server" CssClass="textBox" TextMode="MultiLine" Width="418"></asp:TextBox>
                    </div>
                </div>
                <div class="section clearfix">
                    <div class="section-title left">
                        Please re-enter your email address</div>
                    <div class="reenter-email right">
                        <asp:TextBox class='reemail' ID="reEmail" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="reEmail"
                            runat="server" ErrorMessage="*Please re-enter your email address." CssClass="error-message-text" />
                        <asp:CompareValidator ID="CompareValidator1"  ControlToCompare="yourEmail" ControlToValidate="reEmail" runat="server" ErrorMessage="*Email address does not match." CssClass="invalid-message-text"></asp:CompareValidator>
                        </div>
                </div>
                <div class="submit button-image right">
                    <%--<a href=""><img src="images/clear.png"></a>--%>
                    <asp:Button ID="sendEmail" runat="server" Text="Send Email" />
                </div>
            </div>
        </form>
    </div>
    <div id="sentpanel" runat="server" visible="false">
        <div class="footer">
            <p>
                Thank You. Your message has been sent, a representative will contact you shortly.</p>
        </div>
    </div>
    <div id="sentfailed" runat="server" visible="false">
        <div class="footer">
            <p>
                Sorry, your email sent failed.</p>
            <p>
                <%=exception.ToString() %></p>
        </div>
    </div>
</asp:Content>
