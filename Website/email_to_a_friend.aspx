﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popup.master" CodeFile="email_to_a_friend.aspx.cs" Inherits="email_to_a_friend" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $('input, textarea').bind('focus', function () {
                $(this).siblings('label').hide();
            });
            $('input, textarea').bind('blur', function () {
                if ($(this).val().length == 0)
                    $(this).siblings('label').show();
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1 class="title">Email to a Friend</h1>

    <div id="formpanel" runat="server">
        <form id="Form1" runat="server">
            <div class="w_popup_body">
                <h2><%# Model.GetDisplayAddress() %></h2>
                <fieldset class="form">
                    <div class="section">
                        <label>Subject</label>
                        <input type="text" />
                    </div>
                    &nbsp;
                    <div class="section">
                        <label>Friend's Name</label>
                        <asp:TextBox ID="recepientName" runat="server" class="nameTextBox" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="recepientName" runat="server" ErrorMessage="*Please enter your friend's name." CssClass="error-message-text"/><%--Display="Dynamic"--%>
                    </div>
                    <div class="section">
                        <label>Friend's Email</label>
                        <asp:TextBox ID="recepientEmail" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="recepientEmail" runat="server" ErrorMessage="*Please enter your friend's email address." CssClass="error-message-text" /><%--Display="Dynamic"--%>
                        <asp:RegularExpressionValidator ControlToValidate="recepientEmail" EnableClientScript="true" ID="RegularExpressionValidator2" runat="server" ErrorMessage="*Please enter a valid email address." CssClass="invalid-message-text" ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$" /><%--Display="Dynamic"--%>
                    </div>
                    <div class="section">
                        <label>Your Name</label>
                        <asp:TextBox ID="senderName" runat="server" class="nameTextBox" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="senderName" runat="server" ErrorMessage="*Please enter your name." CssClass="error-message-text" /><%--Display="Dynamic"--%>
                    </div>
                    <div class="section">
                        <label>Your Email</label>
                        <asp:TextBox ID="senderEmail" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="senderEmail" runat="server" ErrorMessage="*Please enter your email address." CssClass="error-message-text" /><%--Display="Dynamic"--%>
                        <asp:RegularExpressionValidator ControlToValidate="senderEmail" ID="RegularExpressionValidator1" runat="server" ErrorMessage="*Please enter a valid address." CssClass="invalid-message-text" ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$" /><%--"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"--%><%--Display="Dynamic"--%>
                    </div>
                    <div class="section">
                        <label>Your Message</label>
                        <asp:TextBox ID="messageText" runat="server" CssClass="textBox" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </fieldset>
                <div class="buttons">
                    <asp:Button ID="sendEmail" runat="server" Text="Send Email" />
                    
                </div>
            </div>
        </form>
    </div>

      <div id="sentpanel" runat="server" visible="false">
        <div class="footer">
            <p>
              Thank you. This listing has been sent to <%=recepientName.Text %>.</p>
        </div>
    </div>
    <div id="sentfailed" runat="server" visible="false">
        <div class="footer">
            <p>
                Sorry, your email sent failed.</p>
            <p>
                <%=exception.ToString() %></p>
        </div>
    </div>
</asp:Content>

