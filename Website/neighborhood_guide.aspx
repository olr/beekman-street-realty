﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="neighborhood_guide.aspx.cs" Inherits="neighborhood_guide" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
    <script type="text/javascript">
        function getNeighborhood(name) {
            var json = JSON.stringify({ 'neighborhood': name });
            $.ajax({
                type: 'POST',
                url: 'Service.svc/GetNeighborhoodContent',
                contentType: 'application/json',
                dataType: 'json',
                data: json,
                processData: true,
                success: handleReturnData
            });
        }
        function handleReturnData(data) {
            $('.neighborhood-photo-description-col').html(data.GetNeighborhoodContentResult.Content);
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages clearfix">
        <div class="titles-global" style="margin-bottom: 20px;">
           neighborhood information
        </div>
        <div class="neighborhood-description-container clearfix">
            <div class="neighborhood-col-guide left">
                <div style="width: 254px; overflow: hidden; position: relative; margin-top: 20px;
                    margin-right: 0px; float: left;">
                    <%=Html.RenderPartial("cntrls/Neighborhood/neighborhoodmap")%>
                </div>
            </div>
            <div class="neighborhood-photo-description-col right">
                <div class="neighborhood-description-col">
                    <%= Html.RenderPartial("Cntrls/neighborhood/neighborhoodguide","uppereastside")%>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
