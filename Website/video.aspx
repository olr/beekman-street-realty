﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.master" AutoEventWireup="true" CodeFile="video.aspx.cs" Inherits="video" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <h1 class="title">Video Player</h1>
    <h2 id="address">
        <%=listing.GetDisplayAddress() %></h2>
    <h2 id="listingid">
        Listing ID:
        <%=listing.ListingAgentID %></h2>
    <div class="video-container">
        <asp:Literal runat="server" ID="embedCode" Text="Video not available." />
    </div>

</asp:Content>

