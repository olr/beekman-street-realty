﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="buildingDetail.aspx.cs" Inherits="buildingDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
    <script type="text/javascript" src="js/jquery.ad-gallery.js"></script>
    <link href="css/jquery.ad-gallery.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages clearfix">
        <div class="static-content clearfix">
            <br>
            <div class="condos-slideshow left">
                <!--   <img src="images/chelsea.png" />-->
                <olr:ImageViewerForBuilding ID="ImageViewer2" runat="server" />
            </div>
            <div class="condos-info left">
                <p>
                    <em>
                        <%=Model.BuildingName %>
                        <br>
                       <span class="featured-address"> <%=Model.Address %></span></em></p>
                        <br>
                <p>
                    <%=Model.Neighborhood %>
                    <br>
                   [<%=Model.CrossStreet1 %>
                    &
                    <%=Model.CrossStreet2 %>]</p>
                    <br />
                    <div class="clearfix">
                    <div class="col left">
                        <div><b> Building Details</b></div>
                        <div class="section">
                            <%=Html.DataList(Model.GetBuildingDetail(), new { @class = "property-details clearfix" })%>
                        </div>
                    </div>
                    <div class="col right">
                    <div> <b>Building Features</b></div>
                    <div> <%=Html.DataList(Model.GetBuildingFeatures(), new { @class = "building-features clearfix" })%></div></div>
                   </div>
               
                <p>
                    <%if (fb != null)
                      { %>
                      <div><b>Description</b></div>
                    <%=fb.Description%><%} %>
                </p>
                <br />
                <%if (!string.IsNullOrEmpty(fb.ExternalLink))
                  { %>
                <div class="view-website-button cursor button-image">
                    <a href="<%=fb.ExternalLink%>" target="_blank">
                        <img src="images/clear.png" /></a></div>
                <br />
                <%} %>
                <p>
                    <b>For More Information Contact:</b></p>
                <p>
                    <b>Base 2.0</b></p>
                    <p>555 5th Avenue, </p>
                    <p>New York, NY 10555</p>
                <p>
                    T: 212-624-6714</p>
                    <p>M: 917-555-5555</p>
                <br />
                <div class="contact-us-button button-image">
                    <a class="email-me" href="EmailToAgent_Building.aspx?bid=<%=Model.ID %>">
                        <img src="images/clear.png" /></a>
                </div>
            </div>
        </div>
        <div class='building_listings'>
            <olr:FeaturedBuildingListings ID="FeaturedBuildingListings" runat="server" />
        </div>
    </div>
</asp:Content>
