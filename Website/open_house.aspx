﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true"
    CodeFile="open_house.aspx.cs" Inherits="open_house" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(function () {
        $("input:radio").change(function () {
                if ($("input:radio:checked").val() == "rent") {
                    window.location.replace("open_house.aspx?type=rent");
                }
                else {
                    window.location.replace("open_house.aspx?type=sale");
                }
            });

            $("img#nav4").attr("src", "images/menu/menu_selected_05.png");
            <% if(pType=="sale"){ %>
                $('input#saleRadio').attr("checked","true");
            <%} else{ %>
                $('input#rentRadio').attr("checked","true");
            <%} %>
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="search-container open-house-page clearfix">
        <div class="titles-global left">
        open houses
        </div>
        <form action="open_house.aspx" method="get">
        <div class="openhouse_search_container">
            <div class="open-house-title">
                Open House Type</div>
           
            <div style="margin: 3px 5px 0 0;">
                <a href="open_house.aspx?type=sale">
                    <input id="saleRadio" name="propertyType" type="radio" value="sale" />
                </a>
            </div>
            <div class="refine_search_text">
                Sales</div>
            
            <div style="margin: 3px 5px 0 0;">
                <a href="open_house.aspx?type=rent">
                    <input id="rentRadio" name="propertyType" type="radio" value="rent" />
                </a>
            </div>
            <div class="refine_search_text">
                <span>Rentals</span></div>
            <div class="clear">
            </div>
        </div>
        </form>
        <div class="result-container">
            <olr:OLRPageNav ID="PageNav1" runat="server" UsePostback="false" PrevImageUrl="images/icons/arrow_left.png"
                NextImageUrl="images/icons/arrow_right.png" />
            <olr:ShortForm runat="server" ID="ShortForm1" NoResultMessage="There are currently no open houses scheduled." />
            <olr:OLRPageNav ID="PageNav2" runat="server" UsePostback="false" PrevImageUrl="images/icons/arrow_left.png"
                NextImageUrl="images/icons/arrow_right.png" />
        </div>
    </div>
</asp:Content>
