﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="agentSearch.aspx.cs" Inherits="agentSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
    <script type="text/javascript" src="js/animate.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#LanguageDropDownList').change(function () {
                window.location = "agents.aspx?language=" + $('#LanguageDropDownList').val();
            })
            $('#searchNameImg').click(function () {
                window.location = "agents.aspx?fn=" + $('#fname').val() + "&ln=" + $('#lname').val();
            })
        })
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="agent_header relative">
        <img src="images/agent_search.png" />
        <div class="agent-detail-instruction">Click Here to Scroll Through Our Agents</div>
        <div id="rwd">
            <img src="images/agent_rwd.png" />
        </div>
        <div id="fwd">
            <img src="images/agent_fwd.png" />
        </div>
    </div>
    <div class="agent-face-wrapper">
        <%=Html.RenderPartial("cntrls/search/AgentSearchByFace",Model) %>
    </div>
    <%=Html.RenderPartial("cntrls/search/AgentSearchByLetter") %>
    <%=Html.RenderPartial("cntrls/search/AgentSearchByName") %>
    <%=Html.RenderPartial("cntrls/search/AgentSearchByLanguage") %>
</asp:Content>
