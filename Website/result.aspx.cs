﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OLRData;
using System.Xml.Linq;
using Microsoft.Practices.Unity;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class result : BasePage<result, SearchOptions>
{
    protected bool showBrooklyn;
    protected override void OnInit(EventArgs e)
    {
        this.Controls.InitSortables(SortClicked);
        base.OnInit(e);
    }

    void SortClicked(object sender, CommandEventArgs e)
    {
        SearchOptions opts;
        if (TempStorage.TryGet<SearchOptions>("SearchOptions", out opts))
        {
            opts.OrderBy = fn.StringToEnum<SortOrder>(e.CommandName);
            opts.IsDescending = ((SortDirection)e.CommandArgument).Equals(SortDirection.Descending);
            opts.PageIndex = 0;
            RunSearch(opts);
        }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SearchOptions opts;
            if (!string.IsNullOrEmpty(Request.QueryString["nofee"]))
                NoFee.Visible = true;
            if (this.Request.HttpMethod == "POST")
            {
                Model = this.ParseSearchOptions();
                if (Model.PropertyTypes.Contains(PropertyType.Sale))
                    Model.CustomOrder = SaleOrder;
                else if (Model.PropertyTypes.Contains(PropertyType.Rental))
                    Model.CustomOrder = RentalOrder;
            }
            else if (TempStorage.TryGet<SearchOptions>("SearchOptions", out opts))
            {
                Model = opts;
            }
            else
            {
                Model = this.ParseSearchOptions();
            }

            if (Model != null)
            {
                if(Model.Boroughs.Contains(Borough.Brooklyn))
                    showBrooklyn=true;
                else
                    showBrooklyn=false;
                if (!string.IsNullOrEmpty(Request.QueryString["sort"]))
                {
                    ReSort(Model, Request.QueryString["sort"]);
                    return;
                }
                if (Request.QueryString["page"] != null)
                {
                    int page = fn.Get<int>(Request.QueryString["page"]);
                    if (page > 0)
                        Model.PageIndex = page - 1;
                }
                RunSearch(Model);
            }
            else
                Response.Redirect("~/search.aspx");
        }
    }

    void ReSort(SearchOptions options, string sortOrder)
    {
        var lastSortOrder = options.OrderBy;
        switch (sortOrder)
        {
            case "price": options.OrderBy = SortOrder.Price; break;
            case "size": options.OrderBy = SortOrder.Size; break;
            case "location": options.OrderBy = SortOrder.Address; break;
        }
        if (lastSortOrder.Equals(options.OrderBy))
            //reverse sort order
            options.IsDescending = !options.IsDescending;
        options.PageIndex = 0;
        TempStorage.Add("SearchOptions", options);
        Response.Redirect("~/result.aspx");
    }
    Action<Orderable<TempResult>> SaleOrder = o => o.Desc(
      x => (x.IsBMLSListing == false && x.IsIDXListing == false && x.IsMarketed == false && x.SaleListingType == 2) ? 1 : 0,
      x => x.Price);

    Action<Orderable<TempResult>> RentalOrder = o => o.Desc(
        x => (x.IsBMLSListing == false && x.IsIDXListing == false && x.IsMarketed == false && x.RentalListingType == 1) ? 1 : 0,
        x => x.Rent2);

    //Action<Orderable<
    void RunSearch(SearchOptions options)
    {
        if (options != null)
        {
            int _total = 0;
            Listings listings = null;
            if (options.PropertyTypes.Contains(PropertyType.Commercial))
                listings = ServiceLocator.GetComercialSearchService().RunSearch(options, out _total);
            else
                listings = ServiceLocator.GetSearchService().RunSearch(options, out _total);
            if (listings != null)
            {
                if (listings.Count == 1&&!string.IsNullOrEmpty(Request.QueryString["transfer"]))
                {
                    string listingid = listings.FirstOrDefault().ListingID;
                    Response.Redirect("detail.aspx?id=" + listingid);
                }
                this.Controls.SetPagers(options.PageIndex + 1, options.PageSize, _total);
                
                this.Controls.DataBind<Listings>(listings);
                listings.CreatePagingSession(); //save paging info

            }
            Model = options;
            TempStorage.Add("SearchOptions", options);
            this.Controls.DataBind<SearchOptions>(options);
            ShowVowLink = lastPage(options.PageIndex, options.PageSize, _total);
        }
    }

    protected bool ShowVowLink { get; set; }
    private bool lastPage(int pageIndex, int pageSize, int totalItems)
    {
        int numPages = (int)Math.Ceiling(totalItems / (double)pageSize);
        numPages = numPages > 0 ? numPages : 1;
        return pageIndex + 1 == numPages;
    }
}
