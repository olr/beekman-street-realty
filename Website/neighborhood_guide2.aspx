﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="neighborhood_guide2.aspx.cs" Inherits="neighborhood_guide2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
    <script type="text/javascript">
        function getNeighborhood(name) {
            var json = JSON.stringify({ 'neighborhood': name });
            $.ajax({
                type: 'POST',
                url: 'Service.svc/GetNeighborhoodContent_2',
                contentType: 'application/json',
                dataType: 'json',
                data: json,
                processData: true,
                success: handleReturnData
            });
        }
        function handleReturnData(data) {
            //$('.neighborhood-photo-description-col').html(data.GetNeighborhoodContent_2Result.Content);
            $('.neighborhood-shortform').html(data.GetNeighborhoodListingsResult.Content);
        }

        $(function () {
         var path=window.location.href;

        $("input:radio").change(function () {
        console.log(path);
        if(path.indexOf("searchtype")>0){
        //console.log('1111');
            path=path.replace("searchtype","stype");}
                if ($("input:radio:checked").val() == "rental") {
                    window.location.replace(path+"&searchtype=rent");
                }
                else {
                    window.location.replace(path+"&searchtype=sale");
                }
            });
            //console.log(window.location.href);
            <% if(searchType=="rent"){ %>
                $('input#rentalRadio').attr("checked","true");
            <%}
            else { %>
             $('input#saleRadio').attr("checked","true");
             <%} %>
        });
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages clearfix">
        <div class="titles-global" style="margin-bottom: 20px;">
            neighborhood guide
        </div>
        
        <div class="neighborhood-description-container clearfix">
            <div class="neighborhood-col-guide left">
                <div style="width: 254px; overflow: hidden; position: relative; margin-top: 20px;
                    margin-right: 0px; float: left;">
                    <%=Html.RenderPartial("cntrls/Neighborhood/neighborhoodmap2")%>
                </div>
            </div>
            <div class="neighborhood-photo-description-col right">
                <div class="neighborhood-description-col">
                    <%=Html.RenderPartial("Cntrls/neighborhood/neighborhoodguide2",neighborhood)%>
                </div>
            </div>
        </div>
        <olr:SimpleRefineSearch ID="simplerefinesearch" runat="server" />
        <div class="neighborhood-shortform">
            <olr:neighborhoodShortForm ID="nsf" runat="server" />
        </div>
    </div>
</asp:Content>
