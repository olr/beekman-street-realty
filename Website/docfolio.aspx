﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="docfolio.aspx.cs" Inherits="docfolio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            //hide the all of the element with class msg_body  
            $(".msg_body").show();
            //toggle the componenet with class msg_body  
            $(".msg_wrapper").click(function () {
                $(this).next(".msg_body").slideToggle(222);
            });
             //console.log("<%=isAdmin %>");
            var True=true;
            var False=false;
            console.log(<%=isAdmin %>);
            if(<%=isAdmin %>)
            {
            console.log("true");
                $('div.admin').attr('style','display:block');
            }
            else
            {
            console.log("false");
                $('div.admin').attr('style','display:none');
            }
            /***************                      Search                                   ******************/
            $('div#searchBtn').click(function () {
                var title = $('input#searchTitle').val();
                //console.log(title);
                var url = window.location.pathname;
                window.location = url + "?title=" + title;
            });

            /****************************      service          *****************************************/
            var proxy = new ServiceProxy("Service.svc/");
            var searchTitle = "<%=searchTitle %>";
            var agentid=<%=brokerID %>;
            if(searchTitle!=null&&searchTitle!="")
            {
                   proxy.invoke("SearchCategory", {title:searchTitle, agentid:agentid}, function (result) {
                   $('div#Categories').html(result.Content);
                           // console.log(agentid);
                   });
            }
            else
            {//console.log(agentid);
                proxy.invoke("ShowCategory", {agentid:agentid}, function (result) {
                    $('div#Categories').html(result.Content);
                });
            }
            
        });
        var AddCategory = function () {
            var proxy = new ServiceProxy("Service.svc/");
             var agentid=<%=brokerID %>;
            var title = prompt("type in your category title:");
            console.log(title);
            if (title != null && title != "")
                proxy.invoke("AddCategory", { title: title,agentid:agentid }, function (result) {
                    $('div#Categories').html(result.Content);
                });
        }
       var CancelSearch=function(){
       var url = window.location.pathname;
                window.location=url;
       }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="welcome-msg-wrapper">
        <div class="welcome-msg-container clearfix">
            <div class="left">
                Welcome,
                <%=broker.FirstName %>
                &nbsp;<%=broker.LastName %>
                !
            </div>
            <div class="right">
                You're signed in as <%if(isAdmin){ %> Administrator<%} else { %> Broker <%} %>. | <a href="default.aspx">Log Out</a>
            </div>
        </div>
    </div>
    <div class="msg_list">
        <div class="clearfix">
            <div class="add-category-buttons left admin">
                <a href='javascript:AddCategory()' id="_cAdd"><span>Add Category</span></a></div>
            <form action="docfolio.aspx" method="get">
            <div class="docfolio-search right clearfix">
                <input type="text" name='title' id="searchTitle" class="folio-corners left" />
                <div id="searchBtn" class="search-button button-image left cursor">
                </div>
            </div>
            </form>
            <div class="right cancel-search-button">
                <a href="javascript:CancelSearch()">Cancel Search </a>
            </div>
        </div>
    </div>
    <div id='Categories'>
    </div>
</asp:Content>
