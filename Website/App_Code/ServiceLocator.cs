﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BrokerTemplate.Core;
using Microsoft.Practices.Unity;

namespace BrokerTemplate.Web
{
    /// <summary>
    /// Summary description for ServiceLocator
    /// </summary>
    public static class ServiceLocator
    {
        static ISearch _search;
        public static ISearch GetSearchService()
        {
            if (_search == null)
            {
                var uc = GetUnityContainer();
                _search = uc.Resolve<ISearch>();
            }
            return _search;
        }

        static ICommercialSearch _commercialSearch;
        public static ICommercialSearch GetComercialSearchService()
        {
            if (_commercialSearch == null)
            {
                var uc = GetUnityContainer();
                _commercialSearch = uc.Resolve<ICommercialSearch>();
            }
            return _commercialSearch;
        }

        public static IRepository GetRepository()
        {
            IRepository repo = GetUnityContainer().Resolve<IRepository>();
            return repo;
        }

        private static IUnityContainer GetUnityContainer()
        {
            IContainerAccessor accessor = HttpContext.Current.ApplicationInstance as IContainerAccessor;
            if (accessor == null)
                return null;
            return accessor.Container;
        }

    }
}