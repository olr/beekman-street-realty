﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.ServiceModel;
using System.Web;
using BrokerTemplate.Core;
using System.Xml.Linq;
using OLRData;
using OLRDataLayer;
using System.Xml;
using System.Xml.XPath;


namespace BrokerTemplate.Web
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true,
        InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceContract]
    public class Service
    {

        [OperationContract]
        [WebInvoke(
                Method = "POST",
                BodyStyle = WebMessageBodyStyle.Wrapped,
                ResponseFormat = WebMessageFormat.Json)]
        public HtmlContent SearchCount(SearchFormModel model)
        {
            HtmlContent html = new HtmlContent();
            if (model != null)
            {
                using (var client = new VOWClient.Client())
                {
                    var criteria = new VOWClient.API.SearchCriteria();
                    criteria.SearchType = model.PropertyType == "Sale" ? VOWClient.API.SearchType.Sale : VOWClient.API.SearchType.Rental;
                    //parse neighborhoods
                    if (model.Neighborhood != null && model.Neighborhood.Count > 0)
                    {
                        foreach (var nlist in model.Neighborhood)
                        {
                            foreach (var n in nlist.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                criteria.Neighborhoods.Add(OLRDataLayer.OLRConvert.GetNeighborhoodExtended(fn.Get<long>(n)));
                        }
                    }
                    //parse Borough
                    if (model.Borough != null)
                    {
                        if(model.Borough=="184")
                        criteria.Borough =VOWClient.API.Borough.Brooklyn ;
                    }
                    //parse ownerships
                    if (model.OwnershipTypes != null && model.OwnershipTypes.Count > 0)
                    {
                        foreach (var o in model.OwnershipTypes)
                        {
                            switch (o.ToLowerInvariant())
                            {
                                case "condo": criteria.Ownerships.Add(VOWClient.API.Ownership.Condo); break;
                                case "coop": criteria.Ownerships.Add(VOWClient.API.Ownership.Coop); break;
                                case "condop": criteria.Ownerships.Add(VOWClient.API.Ownership.Condop); break;
                                case "singlefamily": criteria.Ownerships.Add(VOWClient.API.Ownership.SingleFamily); break;
                                case "multifamily": criteria.Ownerships.Add(VOWClient.API.Ownership.MultiFamily); break;
                            }
                        }
                    }
                    if (model.MinPrice != 0)                                                                              //minPrice
                        criteria.MinPrice = model.MinPrice;
                    if (model.MaxPrice != 0)                                                                            //maxPrice
                        criteria.MaxPrice = model.MaxPrice;

                    if (model.MinBedrooms != 0)                                                                           //minbeds
                        criteria.MinBeds = model.MinBedrooms;

                    if (model.MinBaths != 0)                                                                             //minbaths
                        criteria.MinBaths = model.MinBaths;

                    //if (model.StudiosOnly)                                                                                    //studio only
                    //    criteria.StudioOnly = true;
                    if (model.ApartmentFeatures.Contains("Furnished"))             //Furnished Only
                        criteria.FurnishedOnly = true;
                    if (model.ApartmentFeatures.Contains("OutdoorSpace"))          //ApartmentFeature outdoor space
                        criteria.OutdoorSpace = true;
                    if (model.ApartmentFeatures.Contains("WasherDryer"))               //ApartmentFeature Washer Dryer
                        criteria.WasherDryer = true;
                    if (model.ApartmentFeatures.Contains("Fireplace"))                     //ApartmentFeature Fireplace
                        criteria.Fireplace = true;
                    if (model.ApartmentFeatures.Contains("HighCeilings"))                         //ApartmentFeature.HighCeilings
                        criteria.HighCeilings = true;
                    if (model.ApartmentFeatures.Contains("1,2,4096"))                         //ApartmentFeature.Duplex
                        criteria.Duplex = true;
                    if (model.ApartmentFeatures.Contains("ParkView"))                         //ApartmentFeature.ParkView
                        criteria.ParkView = true;
                    if (model.ApartmentFeatures.Contains("RiverView"))                         //ApartmentFeature.RiverView
                        criteria.RiverView = true;



                    if (model.BuildingFeatures.Contains("AttendedLobby"))                    //BuildingFeature.AttendedLobby
                        criteria.AttendedLobby = true;
                    if (model.BuildingFeatures.Contains("FitnessFacility"))                         //BuildingFeature.FitnessFacility
                        criteria.FitnessFacility = true;
                    if (model.BuildingFeatures.Contains("SwimmingPool"))                         //BuildingFeature.Pool
                        criteria.Pool = true;
                    if (model.BuildingFeatures.Contains("RoofDeck"))                         //BuildingFeature.RoofDeck
                        criteria.RoofDeck = true;
                    if (model.BuildingFeatures.Contains("LaundryRoom"))                         //BuildingFeature.Laundry
                        criteria.Laundry = true;
                    if (model.BuildingFeatures.Contains("NewDevelopment"))                         //BuildingFeature.NewDevelopment
                        criteria.NewDevelopment = true;
                    if (model.BuildingFeatures.Contains("PetFriendly"))                                 //BuildingFeature.PetFriendly
                        criteria.PetsAllowed = true;
                    if (model.BuildingFeatures.Contains("GardenCourtyard"))                         //BuildingFeature.GardenCourtyard
                        criteria.GardenOrCourtyard = true;
                    if (model.BuildingFeatures.Contains("PreWar"))                                      //BuildingFeature.PreWar
                        criteria.Prewar = true;
                    if (model.BuildingFeatures.Contains("TownhouseApt"))                                      //BuildingFeature.TownhouseApt
                        criteria.TownhouseApt = true;




                    //if (model.ShortTermRental)                 //ShortTermRental
                    //    criteria.ShortTermRental = true;


                    var result = client.GetListings(criteria);
                    html.Content = "Your search results: <span> " + result.Info.AbsolutelyTotalCount.ToString() + "</span>";
                }
            }
            return html;
        }

        [OperationContract]
        [WebGet(UriTemplate = "/search?q={query}",
                BodyStyle = WebMessageBodyStyle.Bare,
                ResponseFormat = WebMessageFormat.Json)]
        public QuickSearchResults Search(string query)
        {
            QuickSearchResults results = new QuickSearchResults();
            query = query.Trim();
            string[] qs = query.Split(' ');
            foreach (string q in qs)
            {
                if (q.ToUpper().StartsWith("E.") || q.ToUpper() == "E")
                {
                    query = query.Replace(q, "East");
                    break;
                }
                if (q.ToUpper().StartsWith("W.") || q.ToUpper() == "W")
                {
                    query = query.Replace(q, "West");
                    break;
                }
            }
            var ret = ServiceLocator.GetSearchService().QuickSearch(query);
            foreach (var r in ret)
            {
                results.Add(r);
            }
            return results;
        }


        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public HtmlContent AgentSearch(string q)
        {
            AgentSearchOptions options = new AgentSearchOptions();
            options.Name = q;
            options.GetAll = true;
            int total = 0;
            var results = ServiceLocator.GetRepository().GetBrokers(options, out total);
            //results.Sort(new AgentComparer());
            return new HtmlContent { Content = HttpContext.Current.RenderPartial("cntrls/Lists/Agents", results) };
        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public List<String> GetRandomSaleFeaturedListings()
        {
            List<String> saleIDs = new List<string>();
            SearchOptions option = new SearchOptions();
            option.PropertyTypes.Add(PropertyType.Sale);
            option.PageSize = 10;
            int total = 0;
            Listings salelist = ServiceLocator.GetSearchService().GetRandomFeaturedListings(option);
            
            if (salelist.Count > 0)
            {
                ListingInfo[] salelistingArray = salelist.ToArray();
                foreach (ListingInfo l in salelistingArray)
                {
                    if (l.GetDisplayPhoto() == Resources.Media.NoListingPhoto)
                        salelist.Remove(l);
                }
                foreach (ListingInfo l in salelist)
                {
                    saleIDs.Add(l.ListingID);
                }
                return saleIDs;
            }
            else
            {
                option.FeaturedListings = false;
                salelist = ServiceLocator.GetSearchService().RunSearch(option, out total);
                ListingInfo[] salelistingArray = salelist.ToArray();
                foreach (ListingInfo l in salelistingArray)
                {
                    if (l.GetDisplayPhoto() == Resources.Media.NoListingPhoto)
                        salelist.Remove(l);
                }
                foreach (ListingInfo l in salelist)
                {
                    saleIDs.Add(l.ListingID);
                }
                return saleIDs;
            }
        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public List<String> GetRandomRentalFeaturedListings()
        {
            List<string> rentIDs = new List<string>();
            SearchOptions data = new SearchOptions();
            data.PropertyTypes.Add(PropertyType.Rental);
            data.PageSize = 10;
            data.FeaturedListings = true;
            int total = 0;
            Listings rentlist = ServiceLocator.GetSearchService().GetRandomFeaturedListings(data);
            if (rentlist.Count > 0)
            {
                ListingInfo[] rentlistingArray = rentlist.ToArray();
                foreach (ListingInfo l in rentlistingArray)
                {
                    if (l.GetDisplayPhoto() == Resources.Media.NoListingPhoto)
                        rentlist.Remove(l);
                }
                foreach (ListingInfo l in rentlist)
                {
                    rentIDs.Add(l.ListingID);
                }
                return rentIDs;
            }
            else
            {
                data.FeaturedListings = false;
                rentlist = ServiceLocator.GetSearchService().RunSearch(data, out total);
                ListingInfo[] rentlistingArray = rentlist.ToArray();
                foreach (ListingInfo l in rentlistingArray)
                {
                    if (l.GetDisplayPhoto() == Resources.Media.NoListingPhoto)
                        rentlist.Remove(l);
                }
                int i = 0;
                foreach (ListingInfo l in rentlist)
                {
                    rentIDs.Add(l.ListingID);
                }
                return rentIDs;
            }
        }
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public HtmlContent RenderSaleFeaturedListing(string id)
        {
            HtmlContent content = new HtmlContent();
            content.Content = HttpContext.Current.RenderPartial("cntrls/default/sale_multi_cntrl", id);
            return content;
        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public HtmlContent RenderRentalFeaturedListing(string id)
        {
            HtmlContent content = new HtmlContent();
            content.Content = HttpContext.Current.RenderPartial("cntrls/default/rental_multi_cntrl", id);
            return content;
        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public HtmlContent AgentSearchByOffice(string office)
        {
            AgentSearchOptions options = new AgentSearchOptions();
            options.CompanyID = fn.Get<int>(office);
            int total = 0;
            var results = ServiceLocator.GetRepository().GetBrokers(options, out total);

            return new HtmlContent { Content = HttpContext.Current.RenderPartial("cntrls/agent_result/agent_result", results) };
        }


        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public HtmlContent RenderPriceRange(string saleRental)
        {
            SearchOptions options = new SearchOptions();
            if (saleRental.Equals("Rental"))
                options.PropertyTypes.Add(PropertyType.Rental);
            else
                options.PropertyTypes.Add(PropertyType.Sale);

            return new HtmlContent { Content = HttpContext.Current.RenderPartial("cntrls/Search/PriceRange", options) };
        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]


        public HtmlContent NoFee(string saleRental)
        {
            SearchOptions options = new SearchOptions();
            if (saleRental.Equals("Rental"))
                options.PropertyTypes.Add(PropertyType.Rental);
            else
                options.PropertyTypes.Add(PropertyType.Sale);

            return new HtmlContent { Content = HttpContext.Current.RenderPartial("cntrls/Search/NoFee", options) };
        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public HtmlContent RenderOwnership(string saleRental)
        {
            SearchOptions options = new SearchOptions();
            if (saleRental.Equals("Rental"))
                options.PropertyTypes.Add(PropertyType.Rental);
            else
                options.PropertyTypes.Add(PropertyType.Sale);

            return new HtmlContent { Content = HttpContext.Current.RenderPartial("cntrls/Search/Ownership", options) };
        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]

        public HtmlContent RenderApartmentFeatures(string saleRental)
        {
            SearchOptions options = new SearchOptions();
            if (saleRental.Equals("Rental"))
                options.PropertyTypes.Add(PropertyType.Rental);
            else
                options.PropertyTypes.Add(PropertyType.Sale);

            return new HtmlContent { Content = HttpContext.Current.RenderPartial("cntrls/Search/ApartmentFeatures", options) };
        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public HtmlContent RenderCommercialForm(string saleRental)
        {
            SearchOptions options = new SearchOptions();
            if (saleRental.Equals("Rental"))
                options.PropertyTypes.Add(PropertyType.Rental);
            else
                options.PropertyTypes.Add(PropertyType.Sale);

            return new HtmlContent { Content = HttpContext.Current.RenderPartial("cntrls/Search/CommercialForm", options) };
        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public HtmlContent RenderSimpleSearchForm(string saleRental)
        {
            SearchOptions options = new SearchOptions();
            if (saleRental.Equals("Rental"))
                options.PropertyTypes.Add(PropertyType.Rental);
            else
                options.PropertyTypes.Add(PropertyType.Sale);

            return new HtmlContent { Content = HttpContext.Current.RenderPartial("cntrls/simple_search", options) };
        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public HtmlContent GetNeighborhoodContent(string neighborhood)
        {
            return new HtmlContent { Content = HttpContext.Current.RenderPartial("cntrls/neighborhood/neighborhoodguide", neighborhood) };
        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public HtmlContent GetNeighborhoodContent_2(string neighborhood)
        {
            return new HtmlContent { Content = HttpContext.Current.RenderPartial("cntrls/neighborhood/neighborhoodguide2", neighborhood) };
        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public HtmlContent GetNeighborhoodListings(string neighborhood)
        {
            return new HtmlContent { Content = HttpContext.Current.RenderPartial("cntrls/neighborhood/neighborhoodShortForm", neighborhood) };
        }

        #region customerCategory and Files
        /********************            category             ******************/
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public HtmlContent AddCategory(string title,int agentid)
        {
            int result=  ServiceLocator.GetRepository().AddCategory(title);
            if (result == 1)   // create category successful
            {
                return ShowCategory(agentid);
            }
            else// or we can do something else
            {
               return  ShowCategory(agentid);
            }
        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public HtmlContent ShowCategory(int agentid)
        {
            UserCategories categories = ServiceLocator.GetRepository().GetAllCategories();
            return new HtmlContent { Content = HttpContext.Current.RenderPartial("cntrls/Userdoc/agentCategory", new CustomerFileSystemTransferModel { Categories = categories, SearchTitle = "", AgentId=agentid }) };
        }
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public HtmlContent SearchCategory(string title, int agentid)
        {
            UserCategories categories = ServiceLocator.GetRepository().SearchCategories(title);
            return new HtmlContent { Content = HttpContext.Current.RenderPartial("cntrls/Userdoc/agentCategory", new CustomerFileSystemTransferModel { Categories=categories,SearchTitle=title, AgentId=agentid}) };
        }


        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public int EditCategory(int id, string title) 
        {
            return ServiceLocator.GetRepository().ChangeCategoryTitle(id, title);
        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public int RemoveCategory(int id)   //not finish, still need to delete the file
        {
            return ServiceLocator.GetRepository().DeleteCategory(id);
        }

        /********************            Files              *******************/
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public int EditFileTitle(int fileId,string newTitle)
        {
                int result=ServiceLocator.GetRepository().ChangeFileTitle(fileId, newTitle);
                return result;

        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public int RemoveFile(int fileID) 
        {
            int result = ServiceLocator.GetRepository().DeleteFile(fileID);
            return result;
        }

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public void GetFile(int fileID)
        {
             CustomFile.GetFile(fileID);
            //return result;
        }
      
        #endregion

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Wrapped,
            ResponseFormat = WebMessageFormat.Json)]
        public void ClearSearch()
        {
            TempStorage.Remove("SearchOptions");
        }

        public class HtmlContent
        {
            public string Content { get; set; }
        }

        
    }
}