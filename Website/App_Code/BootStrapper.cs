﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BrokerTemplate.Core;
using Microsoft.Practices.Unity;
using System.Text;
using System.Web.UI;

namespace BrokerTemplate.Web
{
    /// <summary>
    /// Summary description for BootStrapper
    /// </summary>
    public class BootStrapper : AbstractBootStrapper
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            CreateContainer();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (ex is HttpException)
            {
                if (ex.InnerException is ViewStateException)
                {
                    Response.Redirect(Request.Url.AbsoluteUri);
                    return;
                }
                if (((HttpException)(ex)).GetHttpCode() == 404)
                    return;
            }

            StringBuilder theBody = new StringBuilder();
            theBody.Append("URL: " + Request.Url + "\n");
            theBody.Append("IP: " + Request.ServerVariables["REMOTE_HOST"] + "\n");
            theBody.Append("Exception: " + ex.InnerException.Message + "\n");

            try
            {
                //new Prowl.ProwlClient().PostNotification(new Prowl.ProwlNotification { Description = theBody.ToString(), Event = "Error" });
            }
            catch
            {
            }
        }

        public override void CreateContainer()
        {
            IUnityContainer container = new UnityContainer();
            container.RegisterType<ISearch, ListingSearch>(new ContainerControlledLifetimeManager());
            container.RegisterType<ICommercialSearch, CommercialSearch>(new ContainerControlledLifetimeManager());
            container.RegisterType<IRepository, Repository>();
            Container = container;
        }
    }
}