﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrokerTemplate.Web
{
    /// <summary>
    /// Summary description for RecentlyViewListings
    /// </summary>
    public class RecentlyViewListings
    {
        public string ListingId { get; set; }
        public int BuildingId { get; set; }
        public DateTime DateAdded = DateTime.Now;
    }
}