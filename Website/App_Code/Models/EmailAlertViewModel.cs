﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BrokerTemplate.Core;

namespace BrokerTemplate.Web
{
    /// <summary>
    /// Summary description for EmailAlertViewModel
    /// </summary>
    public class EmailAlertViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Comments { get; set; }
        public SearchCriteria Criteria { get; set; }
    }
}