﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;

namespace BrokerTemplate.Web
{
    /// <summary>
    /// Summary description for NeighborhoodViewModel
    /// </summary>
    public class NeighborhoodViewModel
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public IList<KeyValuePair<string, string>> Links { get; set; }
        
        public NeighborhoodViewModel() 
        {
            Links = new List<KeyValuePair<string, string>>();
        }
    }
}