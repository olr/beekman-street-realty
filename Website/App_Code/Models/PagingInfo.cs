﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrokerTemplate.Web
{
    /// <summary>
    /// Summary description for Paging
    /// </summary>
    public class PagingInfo
    {
        public PagingInfo()
        {
        }

        public string PreviousListingId { get; set; }
        public string NextListingId { get; set; }
        public string CurrentListingId { get; set; }
        public string ListPage { get; set; }
    }
}