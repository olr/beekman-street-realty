﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace BrokerTemplate.Web
{
    [DataContract]
    public class SearchFormModel
    {
        [DataMember]
        public string PropertyType { get; set; }

        [DataMember]
        public string Borough { get; set; }

        [DataMember]
        public List<string> Neighborhood { get; set; }

        [DataMember]
        public List<string> OwnershipTypes { get; set; }

        [DataMember]
        public int MinBedrooms { get; set; }

        [DataMember]
        public int MinBaths { get; set; }

        [DataMember]
        public int MinPrice { get; set; }

        [DataMember]
        public int MaxPrice { get; set; }

        [DataMember]
        public List<string> BuildingFeatures { get; set; }

        [DataMember]
        public List<string> ApartmentFeatures { get; set; }

    }
}