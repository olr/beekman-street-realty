﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BrokerTemplate.Core;

/// <summary>
/// Summary description for FeaturedPropertyGroup
/// </summary>
namespace BrokerTemplate.Web
{
    public class FeaturedPropertyGroup
    {
        private List<ListingInfo> saleList;
        private List<ListingInfo> rentalList;
        public FeaturedPropertyGroup()
        {
            saleList = new List<ListingInfo>();
            rentalList = new List<ListingInfo>();
        }
        public FeaturedPropertyGroup(ListingInfo saleListing, ListingInfo rentalListing)
            : this()
        {
            saleList.Add(saleListing);
            rentalList.Add(rentalListing);
        }
        public FeaturedPropertyGroup(List<ListingInfo> saleListings, List<ListingInfo> rentalListings)
            : this()
        {
            saleList = saleListings;
            rentalList = rentalListings;
        }

        public void Add(PropertyType type, ListingInfo listing)
        {
            if (type == PropertyType.Rental)
                rentalList.Add(listing);
            else
                saleList.Add(listing);
        }

        public List<ListingInfo> GetList(PropertyType type)
        {
            if (type == PropertyType.Rental)
                return rentalList;
            else
                return saleList;
        }
        public ListingInfo GetListing(PropertyType type)
        {
            if (type == PropertyType.Rental)
            {
                return rentalList.FirstOrDefault();
            }
            else
            {
                return saleList.FirstOrDefault();
            }
        }
    }
}