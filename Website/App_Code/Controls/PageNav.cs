﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace OLR.Controls
{
    /// <summary>
    /// generic page navigation control
    /// </summary>
    public class OLRPageNav : WebControl, IPostBackEventHandler, INamingContainer
    {

        #region Properties

        private int _totalItems = 0;
        public int TotalItems { get { return _totalItems; } set { _totalItems = value; } }

        private int _currentPage = 1;
        public int CurrentPage { get { return _currentPage; } set { _currentPage = value; } }

        private int _pageSize = 0;
        public int PageSize { get { return _pageSize; } set { _pageSize = value; } }

        public int NumPages
        {
            get
            {
                int t = (int)Math.Ceiling(TotalItems / (double)PageSize);
                return t > 0 ? t : 1;
            }
        }

        private int _showItems = 10;
        public int ShowItems { get { return _showItems; } set { _showItems = value; } }

        private string _firstImageUrl;
        public string FirstImageUrl { get { return _firstImageUrl; } set { _firstImageUrl = value; } }

        private string _prevImageUrl;
        public string PrevImageUrl { get { return _prevImageUrl; } set { _prevImageUrl = value; } }

        private string _nextImageUrl;
        public string NextImageUrl { get { return _nextImageUrl; } set { _nextImageUrl = value; } }

        private string _lastImageUrl;
        public string LastImageUrl { get { return _lastImageUrl; } set { _lastImageUrl = value; } }


        //Added by Bobby
        private string _firstText;
        public string FirstText { get { return _firstText; } set { _firstText = value; } }

        private string _lastText;
        public string LastText { get { return _lastText; } set { _lastText = value; } }


        private string _prevText;
        public string PrevText { get { return _prevText; } set { _prevText = value; } }

        private string _nextText;
        public string NextText { get { return _nextText; } set { _nextText = value; } }

        private string _seperator;
        public string Seperator { get { return _seperator; } set { _seperator = value; } }

        private string _infoSeparator = "&nbsp;|&nbsp;Go to page: ";
        public string InfoSeparator { get { return _infoSeparator; } set { _infoSeparator = value; } }

        private string _pageIndicatorFormat = "&nbsp;|&nbsp;You are on page <b>{0}</b> of <b>{1}</b>";
        public string PageIndicatorFormat { get { return _pageIndicatorFormat; } set { _pageIndicatorFormat = value; } }

        private string _totalInfoFormat = "There are <b> {0} </b> listings that match your criteria.";
        private string type = "listings";

        public string TotalInfoFormat
        {
            get
            {
                if (type == "press")
                    return "There are <b> {0} </b> articles.";
                else if (type == "testimonial")
                    return "There are <b> {0} </b> testimonials.";
                else if (type == "agent")
                    return "There are <b> {0} </b> listings.";
                else
                    return _totalInfoFormat;
            }
            set { type = value; }
        }

        //format string for generating custom links (like as /sitedir/page{0}/)
        private string _formattedLink = string.Empty;
        public string FormattedLink { get { return _formattedLink; } set { _formattedLink = value; } }

        private bool _usePostback = true;
        public bool UsePostback { get { return _usePostback; } set { _usePostback = value; } }

        private bool _renderLast = false;
        public bool RenderLastLink { get { return _renderLast; } set { _renderLast = value; } }

        private bool _renderFirst = false;
        public bool RenderFirstLink { get { return _renderFirst; } set { _renderFirst = value; } }

        #endregion

        #region IPostBackEventHandler

        public event CommandEventHandler PageClicked;

        void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
        {
            //raise clicked event
            if (PageClicked != null)
            {
                CommandEventArgs c = new CommandEventArgs("PageNum", Convert.ToInt32(eventArgument));
                PageClicked(this, c);
            }
        }

        #endregion

        public override void RenderControl(HtmlTextWriter writer)
        {
            if (TotalItems == 0)
                return;

            //make sure server form exists
            if (Page != null && UsePostback) Page.VerifyRenderingInServerForm(this);

            if (!string.IsNullOrEmpty(CssClass))
                writer.Write("<div class=\"" + CssClass + "\">");
            else
                writer.Write("<div class=\"pagenav\">");


            int pageEnd = getEndPage();
            int startPage = getStartPage();

            writer.Write("<div class=\"pagenav_prev\">");
            if (CurrentPage > 1)
            {
                if (startPage > 2 && RenderFirstLink)
                    writer.Write(RenderFirst());
                writer.Write(RenderPrev());
            }
            writer.Write("&nbsp;</div>");

            writer.Write("<div class=\"pagenav_middle\">");
            writer.Write(RenderPageInfo());
            if (NumPages > 1)
            {
                for (int i = getStartPage(); i <= pageEnd; i++)
                {
                    if (i == CurrentPage)
                        writer.Write(RenderCurrent(i));
                    else
                        writer.Write(RenderPage(i));
                }
            }
            writer.Write("</div>");

            writer.Write("<div class=\"pagenav_next\">");
            if (CurrentPage < NumPages)
            {
                writer.Write(RenderNext());
                if (pageEnd < NumPages && RenderLastLink)
                    writer.Write(RenderLast());
            }
            writer.Write("</div>");

            writer.Write("</div>");
        }

        protected virtual string RenderPageInfo()
        {
            string totalInfo = string.Format(TotalInfoFormat, TotalItems);
            string pageInfo = string.Format(PageIndicatorFormat, CurrentPage, NumPages);

            if (TotalItems <= 1)
                return string.Empty;

            if (NumPages > 1)
                return string.Format(@"<span class=""pageinfo"">{0}{1}{2}</span>", totalInfo, pageInfo, InfoSeparator);
            else
                return string.Format(@"<span class=""pageinfo"">{0}</span>", totalInfo);
        }

        protected virtual string RenderCurrent(int index)
        {
            return string.Format(@"<span class=""current"">{0}{1}</span>&nbsp", index, Seperator);
        }

        protected virtual string RenderPage(int index)
        {
            return string.Format(@"<span class=""pagelink""><a href=""{0}"">{1}</a>{2}</span>&nbsp",
                GetLink(index), index.ToString(), Seperator);
        }

        protected virtual string RenderFirst()
        {
            return string.Format(@"<span class=""firstpage""><a href=""{0}"">{1}</a></span>",
                GetLink(1), getFirstImage());
        }

        protected virtual string RenderPrev()
        {
            return string.Format(@"<span class=""prevpage""><a href=""{0}"">{1}</a></span>",
                GetLink(CurrentPage - 1), getPrevImage());
        }

        protected virtual string RenderNext()
        {
            return string.Format(@"<span class=""nextpage""><a href=""{0}"">{1}</a></span>",
                GetLink(CurrentPage + 1), getNextImage());
        }

        protected virtual string RenderLast()
        {
            return string.Format(@"<span class=""lastpage""><a href=""{0}"">{1}</a></span>",
                GetLink(NumPages), getLastImage());
        }

        /// <summary>
        /// get the page link
        /// uses either postback reference with the pageindex as the argument
        /// or a regular querystring link
        /// </summary>
        protected virtual string GetLink(int page)
        {
            if (UsePostback)
                return Page.ClientScript.GetPostBackClientHyperlink(this, page.ToString());
            else if (!string.IsNullOrEmpty(_formattedLink))
                return String.Format(_formattedLink, page);
            else
            {
                //build querystring
                NameValueCollection coll = new NameValueCollection();
                foreach (var k in Page.Request.QueryString.AllKeys)
                {
                    if (!k.Equals("page"))
                        coll.Add(k, Page.Request.QueryString[k]);
                }
                string url = coll.Count > 0 ? ToQueryString(coll) : string.Empty;
                var delim = coll.Count > 0 ? "&" : "?";

                return url + delim + "page=" + page.ToString();
            }
        }

        private string ToQueryString(NameValueCollection nvc)
        {
            return "?" + string.Join("&", Array.ConvertAll(nvc.AllKeys, key => string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(nvc[key]))));
        }

        private string getFirstImage()
        {
            string image = string.IsNullOrEmpty(_firstText) ? " &lsaquo;&lsaquo; " : _firstText;
            if (!string.IsNullOrEmpty(FirstImageUrl))
                image = string.Format(@"<img src=""{0}"" alt=""First"" />", FirstImageUrl);

            return image;
        }

        private string getPrevImage()
        {
            string image = string.IsNullOrEmpty(_prevText) ? " &lsaquo; " : _prevText;

            if (!string.IsNullOrEmpty(PrevImageUrl))
                image = string.Format(@"<img src=""{0}"" alt=""Previous"" />", PrevImageUrl);
            return image;
        }

        private string getNextImage()
        {
            string image = string.IsNullOrEmpty(_nextText) ? " &rsaquo;  " : _nextText;

            if (!string.IsNullOrEmpty(NextImageUrl))
                image = string.Format(@"<img src=""{0}"" alt=""Next"" />", NextImageUrl);
            return image;
        }

        private string getLastImage()
        {
            string image = string.IsNullOrEmpty(_lastText) ? " &rsaquo;&rsaquo; " : _lastText;

            if (!string.IsNullOrEmpty(LastImageUrl))
                image = string.Format(@"<img src=""{0}"" alt=""Last"" />", LastImageUrl);
            return image;
        }

        private int getStartPage()
        {
            int pageStart = 1;
            if (NumPages < ShowItems) pageStart = 1;
            else if (CurrentPage < Convert.ToInt32(ShowItems / 2)) pageStart = 1;
            else if ((NumPages - CurrentPage) < ShowItems - 1) pageStart = NumPages - ShowItems - 1;
            else pageStart = CurrentPage - Convert.ToInt32(ShowItems / 2) + 1;
            return pageStart > 0 ? pageStart : 1;
        }

        private int getEndPage()
        {
            int pageEnd = 1;
            if (NumPages < ShowItems) pageEnd = NumPages;
            else if (CurrentPage <= Convert.ToInt32(ShowItems / 2)) pageEnd = ShowItems;
            else if ((NumPages - CurrentPage) < ShowItems - 1) pageEnd = NumPages;
            else pageEnd = CurrentPage + Convert.ToInt32(ShowItems / 2);
            return pageEnd < NumPages ? pageEnd : NumPages;
        }

    }

}