﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Text;
using BrokerTemplate.Core;

namespace BrokerTemplate.Web
{
    /// <summary>
    /// Summary description for ListingsControl
    /// </summary>
    public abstract class ListingsControl : System.Web.UI.UserControl, IRenderable<Listings>, ISortable
    {
        IRepository db;

        private bool _flag = false;
        protected string OddEvenString { get { return (_flag = !_flag) ? "odd" : "even"; } }

        public ListingsControl()
        {
            db = ServiceLocator.GetRepository();
        }

        protected SortDirection SortingDirection
        {
            get { return (SortDirection)(ViewState["SortDirection"] ?? SortDirection.Descending); }
            set { ViewState["SortDirection"] = value; }
        }

        public event CommandEventHandler SortClicked;

        public abstract void DataBind(Listings data);

        public virtual void SortData(LinkButton link, ListView list)
        {
            string expression = string.Empty;
            string arrow = "~/images/sort_arrow.png";
            SortDirection sort = SortDirection.Ascending;
            if (SortingDirection == SortDirection.Ascending)
            {
                arrow = "~/images/sort_arrow_down.png";
                sort = SortDirection.Descending;
            }
            SortingDirection = sort;
            var priceArrow = (System.Web.UI.WebControls.Image)list.FindControl("PriceRentArrow");
            var neighborhoodArrow = (System.Web.UI.WebControls.Image)list.FindControl("NeighborhoodArrow");
            var sizeArrow = (System.Web.UI.WebControls.Image)list.FindControl("SizeArrow");
            var typeArrow = (System.Web.UI.WebControls.Image)list.FindControl("TypeArrow");

            if (priceArrow != null)
                priceArrow.Visible = false;
            if (neighborhoodArrow != null)
                neighborhoodArrow.Visible = false;
            if (sizeArrow != null)
                sizeArrow.Visible = false;
            if (typeArrow != null)
                typeArrow.Visible = false;

            if (link != null)
            {
                switch (link.CommandArgument)
                {
                    case "Price":
                        priceArrow.Visible = true;
                        priceArrow.ImageUrl = arrow;
                        break;
                    case "Neighborhood":
                        neighborhoodArrow.Visible = true;
                        neighborhoodArrow.ImageUrl = arrow;
                        break;
                    case "Beds":
                        sizeArrow.ImageUrl = arrow;
                        sizeArrow.Visible = true;
                        break;
                    case "Type":
                        typeArrow.Visible = true;
                        typeArrow.ImageUrl = arrow;
                        break;
                    default:
                        priceArrow.Visible = true;
                        priceArrow.ImageUrl = arrow;
                        break;
                }
            }

            if (SortClicked != null)
                SortClicked(this, new CommandEventArgs(link.CommandArgument, sort));

        }

        #region Helpers
        protected string BuildBuildingType(object item)
        {
            ListingInfo l = item as ListingInfo;
            string descrip = string.Empty;
            if (!string.IsNullOrWhiteSpace(l.BuildingType))
            {
                descrip = l.BuildingType;
            }
            return descrip;
        }

        protected string BuildIsNoFee(ListingInfo l)
        {
            string result = string.Empty;
            if (l.IsNoFee)
                result = "<br/><span style='color:#dd5931;'>NO FEE</span>";
            return result;
        }

        protected string BuildBathroom(ListingInfo l)
        {
            string result = string.Empty;
            if (string.IsNullOrEmpty(BuildSize(l)))
                return result;
            if (!l.IsBuildingForSale)
            {
                if (l.Bathrooms == 0)
                    result = "";
                else if (l.Bathrooms == 1)
                {
                    result = l.Bathrooms + " Bath";
                }
                else
                {
                    result = l.Bathrooms + " Baths";

                }
            }
            else
                result = l.Bathrooms == 0 ? string.Empty : l.Bathrooms + " Baths";
            return result;
        }


        protected string BuildSize(ListingInfo l)
        {
            string result = string.Empty;

            if (l.IsCommercialListing)
                return result;

            if (!l.IsBuildingForSale)
            {
                if (l.Bedrooms == 0)
                    result = "Studio";
               
                else
                {
                    result = l.Bedrooms + " Bedroom";

                }
            }
            else
                result = l.Bedrooms == 0 ? string.Empty : l.Bedrooms + " Bedroom";
            return result;
        }

        protected string BuildSquareFootage(ListingInfo l)
        {
            string descrip = string.Empty;
            descrip = (l.SquareFootage == 0 || l.SquareFootage == -1) ? string.Empty : l.SquareFootage.ToString("#,###") + " " + Resources.SearchResultListings.ASFText;
            return descrip;
        }
        private string[] BuildBasisContact(ListingInfo l)
        {
            String[] str1 = null;

            if (l != null && l.GetBrokers().Count > 0)
            {
                str1 = new String[4];

                var brokers = l.GetBrokers();
                str1[0] = brokers[0].FirstName + " " + brokers[0].LastName;
                str1[1] = brokers[0].MobilePhone;
                str1[2] = brokers[0].WorkPhone;
                str1[3] = brokers[0].Email;
            }
            else
            {
                str1 = new String[2];
            }
            return str1;
        }



        protected string BuildBasisContactFormat(ListingInfo l)
        {
            StringBuilder sb = new StringBuilder();
            var brokers = l.GetBrokers();
            string[] str = BuildBasisContact(l);
            if (brokers.Count == 0)
                return "";
            else
            {
                sb.AppendFormat(@"<div class=""result-contact-container""><ul> <li class=""result-container-headers"">{0}</li></ul>", str[0]);
                if (!string.IsNullOrWhiteSpace(str[1]))
                {
                    sb.AppendFormat(@"<ul class=""clearfix""><li class=""result-contact-container-li"">Mobile:</li><li class=""result-contact-container-li-alt"">{0}"
                        + @"</li></ul>", str[1]);
                }
                if (!string.IsNullOrWhiteSpace(str[2]))
                {
                    sb.AppendFormat(@"<ul class=""clearfix""><li class=""result-contact-container-li"">Office</li><li class=""result-contact-container-li-alt"">" +
                        @"{0}</li></ul>", str[2]);
                }
                sb.AppendFormat(@"<ul><li><a href=""{0}"" class=""email-Agent"">{1}</a></li></ul></div>", brokers[0].GetEmailMeLink(l), "Email Agent");
            }

            return sb.ToString();

        }
        protected string BuildContact(ListingInfo l)
        {
            StringBuilder sb = new StringBuilder();
            BrokerInfo primary = null;
            BrokerInfo secondary = null;
            if (l.ListingAgentID > 0)
            {
                var brokers = l.GetBrokers();
                brokers.Sort((a, b) => a.FirstName.CompareTo(b.FirstName));
                if (brokers.Count == 1)
                    sb.Append(BuildContact(brokers[0], l));
                else if (!l.IsIDXListing && brokers.Count > 1)
                {
                    if (brokers[0].ID == l.ListingAgentID)
                    {
                        primary = brokers[0];
                        secondary = brokers[1];
                    }
                    else
                    {
                        primary = brokers[1];
                        secondary = brokers[0];
                    }
                    sb.Append(BuildContact(primary, l));
                    sb.Append(BuildContact(secondary, l));
                }
            }
            if (l.IsIDXListing)
            {
                sb.Append(@"<img class=""idx-logo"" src=""images/mls_idx.gif"" alt="""" />");
            }
            return sb.ToString();
        }
        protected string BuildContact(ListingInfo l, bool isAgentPage,int agentId)
        {
            string result = string.Empty;
            if (!isAgentPage)
                result= BuildContact(l);
            else
            {
                StringBuilder sb = new StringBuilder();
                if (l.ListingAgentID > 0)
                {
                    var brokers = l.GetBrokers();
                    BrokerInfo bi = (from b in brokers
                                     where b.ID == agentId
                                     select b).FirstOrDefault();
                    sb.Append(BuildContact(bi, l));
                }
                if (l.IsIDXListing)
                {
                    sb.Append(@"<img class=""idx-logo"" src=""images/mls_idx.gif"" alt="""" />");
                }
                result = sb.ToString();
            }
            return result;
        }
        private string BuildContact(BrokerInfo b, ListingInfo l)
        {
            StringBuilder sb = new StringBuilder();
            if (b != null && b.CompanyID > 0)
            {
                sb.Append(@"<div class=""contact agentactions"" style='padding-bottom:3px;'>");
                sb.AppendFormat(@"<a  href=""{2}"">{0} {1}</a><br />", b.FirstName, b.LastName, b.GetDetailLink());
                if (!string.IsNullOrWhiteSpace(b.WorkPhone))
                    sb.AppendFormat("{0} <span>{1}</span><br />", Resources.SearchResultListings.OfficeLabel, b.WorkPhone);
                if (!string.IsNullOrWhiteSpace(b.MobilePhone))
                    sb.AppendFormat("{0} <span>{1}</span><br />", Resources.SearchResultListings.MobileLabel, b.MobilePhone);
                //if (!string.IsNullOrWhiteSpace(b.Fax ))
                //    sb.AppendFormat("{0} <span style='color:black;'>{1}</span><br />", "F: " , b.Fax );
                sb.AppendFormat(@"<a href=""{0}"" class=""email-me"">{1}</a>", b.GetEmailMeLink(l), "Email Me");
                sb.Append("</div><br/>");
            }
            else if (l.IsIDXListing)
            {
                sb.Append(@"<img class=""idx"" src=""images/mls_idx.gif"" alt=""""/>");
            }
            return sb.ToString();
        }

        protected string BuildRoomCount(ListingInfo l)
        {
            return string.Format("<table><tr><td style='width:55px;'>&nbsp;&nbsp;&nbsp;{0}</td><td style='width:40px;'>{1}</td><td style='width:30px;'>{2}</td></td></table>", l.Rooms, l.Bedrooms, l.Bathrooms);
        }

        protected string GetAgentName(ListingInfo r)
        {
            var b = db.GetBrokerByID((int)r.ListingAgentID);
            if (b != null)
                return "<a href='agent_detail.aspx?agentid=" + b.ID.ToString() + "'>" + b.FirstName + " " + b.LastName + "</a>";
            else
                return string.Empty;
        }

        protected string GetAgentOffice(ListingInfo r)
        {
            var b = db.GetBrokerByID((int)r.ListingAgentID);
            if (b != null)
                return SiteHelper.FormatPhone(b.WorkPhone);
            else
                return string.Empty;
        }

        protected string GetAgentMobile(ListingInfo r)
        {
            var b = db.GetBrokerByID((int)r.ListingAgentID);
            if (b != null)
                return SiteHelper.FormatPhone(b.MobilePhone);
            else
                return string.Empty;
        }

        protected string GetAddress(ListingInfo l)
        {
            return string.Format("<a href='detail.aspx?id={0}'>{1}</a>", l.ListingID, l.GetDisplayAddress());
        }

        protected string GetCrossStreets(ListingInfo l)
        {
            if (!string.IsNullOrWhiteSpace(l.CrossStreet1) && !string.IsNullOrWhiteSpace(l.CrossStreet1))
                return SiteHelper.CorrectCrossStreet(string.Format("{0} and {1}", l.CrossStreet1, l.CrossStreet2));
            else
                return string.Empty;
        }

        protected string GetServiceLevel(ListingInfo l)
        {
            if (string.IsNullOrWhiteSpace(l.ServiceLevel) || l.ServiceLevel.ToLower() == "none")
                return string.Empty;
            return "&nbsp;" + l.ServiceLevel;
        }

        protected string BuildRETaxesLabel(ListingInfo l)
        {
            string descrip = string.Empty;
            if (l.RETaxes > 0)
                descrip = string.Format("{0}", Resources.SearchResultListings.ReTaxesText);
            return descrip;

        }

        protected string BuildRETaxes(ListingInfo l)
        {
            string descrip = string.Empty;
            if (l.RETaxes > 0)
                descrip = string.Format("{0:C0}", l.RETaxes);
            return descrip;

        }

        protected string BuildMaintCC(ListingInfo l)
        {
            string descrip = string.Empty;
            if (l.Price == 0)
                return descrip;
            if (l.Maintenance > 0 && !l.IsBuildingForSale)
                descrip = string.Format("{0:C0}", l.Maintenance);
            if (l.CommonCharges > 0)
                descrip = string.Format("{0:C0}", l.CommonCharges);

            return descrip;
        }

        protected string BuildMaintCCLabel(ListingInfo l)
        {
            string descrip = string.Empty;
            if (l.Price == 0)
                return descrip;
            if (l.Maintenance > 0 && !l.IsBuildingForSale)
                descrip = Resources.SearchResultListings.MaintanenceText;
            if (l.CommonCharges > 0)
                descrip = Resources.SearchResultListings.CommonChargesText;
            return descrip;
        }

        protected string BuildPriceRentLabel(ListingInfo l)
        {
            StringBuilder descrip = new StringBuilder();
            if (l.Price > 0)
            {
                descrip.Append(Resources.SearchResultListings.PriceLabel);

            }
            else if (l.Rent > 0)
            {
                descrip.Append(Resources.SearchResultListings.RentsLabel);
                if (l.FurnishedRent > 0)
                    descrip.Append("<br/>Rent(F):");
            }
            else if (l.FurnishedRent > 0 && l.Rent == 0)
                descrip.Append("Rent(F):");
            return descrip.ToString();
        }

        protected string BuildPriceRent(ListingInfo l, bool isShortForm)
        {
            return l.GetPriceRent(isShortForm);
        }
        protected string BuildStatusLable(ListingInfo l)
        {
            return "Status:";
        }
        protected string BuildStatus(ListingInfo l)
        {
            return l.GetStatus();
        }
        protected virtual string BuildListingInfo(ListingInfo l)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(@"<span class=""titlegreen"">{0}</span>", l.GetPriceRentText(false));

            if (l.SquareFootage > 0)
                sb.AppendFormat(" | ASF. {0:#,###}", l.SquareFootage);

            if (!string.IsNullOrEmpty(l.Ownership))
                sb.AppendFormat(" | {0}", l.Ownership);

            return sb.ToString();
        }

        protected string GetOpenHouses(ListingInfo l)
        {
            string result = string.Empty;
            var openhouses = l.GetOpenHouses();
            if (openhouses.Count > 0)
            {
                int count = 0;

                foreach (var o in openhouses)
                {
                    if(o.ByAppointment)
                        result += string.Format(" {0},  {1}-{2} &nbsp;&nbsp;By Appointment Only.<br/> ", SiteHelper.FormatDate((DateTime)o.OpenHouseDate), o.StartTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim(), o.EndTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim());
                    else
                        result += string.Format(" {0},  {1}-{2} <br/> ", SiteHelper.FormatDate((DateTime)o.OpenHouseDate), o.StartTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim(), o.EndTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim());
                    count++;
                    if (count >= 2)
                        break;
                }
                if (!string.IsNullOrWhiteSpace(result))
                {
                    if (count == 1)
                        result = string.Format(@"<span class=""openhouse"">Open House:<br/> {0}</span>", result);
                    else if (count > 1)
                        result = string.Format(@"<span class=""openhouse"">Open Houses:<br/> {0}</span>", result);
                }
            }
            return result;
        }

        protected string BuildToolLinks(ListingInfo l)
        {
            StringBuilder sb = new StringBuilder();
            var media = l.GetMedia();
            if (media != null)
            {
                if (media.Photos.Count > 0)
                {
                    TagBuilder a = new TagBuilder("a");
                    a.AddCssClass("photolink tip");
                    a.Attributes.Add("title", "View Photos");
                    a.Attributes.Add("href", l.GetAllPhotoLink());
                    a.InnerHtml = @"Photos";
                    sb.AppendFormat(@"{0}", a.ToString());
                }
                if (media.Floorplans.Count > 0)
                {
                    TagBuilder f = new TagBuilder("a");
                    f.AddCssClass("photolink tip");
                    f.Attributes.Add("title", "View Floorplan");
                    f.Attributes.Add("href", l.GetFloorplanLink());
                    f.InnerHtml = @"Floorplan";

                    if (media.Photos.Count > 0)
                        sb.Append(" | ");
                    sb.AppendFormat(@"{0}", f.ToString());
                }
            }
            return sb.ToString();
        }

        protected string BuildServiceLevel(ListingInfo listing)
        {
            string serviceLevel = listing.ServiceLevel;
            if (!string.IsNullOrEmpty(serviceLevel) && serviceLevel.Contains("Full-Time"))
                serviceLevel = serviceLevel.Replace("Full-Time", "FT");
            if (!string.IsNullOrEmpty(serviceLevel) && serviceLevel.ToLower().Contains("none"))
                serviceLevel = serviceLevel.Replace("None", "");
            return serviceLevel;

        }

        private int counter = 0;
        protected virtual string BuildClassName()
        {
            counter++;
            string className = "middle";
            if (counter % 3 == 1)
                className = "first";
            else if (counter % 3 == 0)
                className = "last";
            return className;
        }

        #endregion

    }
}