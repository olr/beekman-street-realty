﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BrokerTemplate.Web;
using BrokerTemplate.Core;
using VOWClient;
using VOWClient.API;
using OLRDataLayer;
using OLRData;

/// <summary>
/// Summary description for ConvertCriteria
/// </summary>

namespace BrokerTemplate.Web
{
    public static class VOWSearch
    {
        public static VOWClient.API.SearchCriteria getVOWCriteria(SearchOptions options)// return type is VOWClient.API.SearchCriteria 
        {
            VOWClient.API.SearchCriteria criteria=new VOWClient.API.SearchCriteria();
            if (options.PropertyTypes.Count > 0)                                // Property Type
            {
                if (options.PropertyTypes.Contains(PropertyType.Rental))
                    criteria.SearchType = SearchType.Rental;
                else if(options.PropertyTypes.Contains(PropertyType.Sale))
                    criteria.SearchType = SearchType.Sale;
                else if (options.PropertyTypes.Contains(PropertyType.BuildingForSale))
                    criteria.SearchType = SearchType.BuildingForSale;
            }
            if (options.Neighborhoods.Count > 0)                                                                //neighborhood
            {
                foreach (int n in options.Neighborhoods)
                {
                    criteria.Neighborhoods.Add(OLRConvert.GetNeighborhoodExtended(n));
                }
            }
            if (options.MinPrice != 0)                                                                              //minPrice
                criteria.MinPrice = options.MinPrice;
            if (options.MaxPrice != 0)                                                                            //maxPrice
                criteria.MaxPrice = options.MaxPrice;
            if (options.MaxBedroom != 0)                                                                           //maxbeds
                criteria.MaxBeds = options.MaxBedroom;
            if (options.MinBedroom != 0)                                                                           //minbeds
                criteria.MinBeds = options.MinBedroom;
            if (options.MaxBathroom != 0)                                                                        //maxbaths
                criteria.MaxBaths = (int)options.MaxBathroom; 
            if (options.MinBathroom != 0)                                                                             //minbaths
                criteria.MinBaths = (int)options.MinBathroom; 

            if (options.StudiosOnly)                                                                                    //studio only
                criteria.StudioOnly = true;
            if (options.ApartmentFeatures.Contains(ApartmentFeature.Furnished))             //Furnished Only
                criteria.FurnishedOnly = true;
            if (options.OwnershipTypes.Count > 0)                                                               //ownership
            {
                foreach (var ownership in options.OwnershipTypes)
                {
                    if (ownership == Core.Ownership.Condo)
                        criteria.Ownerships.Add(VOWClient.API.Ownership.Condo);
                    else if(ownership==Core.Ownership.Coop)
                        criteria.Ownerships.Add(VOWClient.API.Ownership.Coop);
                    else if (ownership == Core.Ownership.Condop)
                        criteria.Ownerships.Add(VOWClient.API.Ownership.Condop);
                    else if (ownership == Core.Ownership.Rental)
                        criteria.Ownerships.Add(VOWClient.API.Ownership.Rental);
                    else if (ownership == Core.Ownership.MultiFamily)
                        criteria.Ownerships.Add(VOWClient.API.Ownership.MultiFamily);
                    else if (ownership == Core.Ownership.MixedUse)
                        criteria.Ownerships.Add(VOWClient.API.Ownership.MixedUse);
                    else if (ownership == Core.Ownership.SingleFamily)
                        criteria.Ownerships.Add(VOWClient.API.Ownership.SingleFamily);
                    else if (ownership == Core.Ownership.IncomeProperty)
                        criteria.Ownerships.Add(VOWClient.API.Ownership.IncomeProperty);
                    else if (ownership == Core.Ownership.Commercial)
                        criteria.Ownerships.Add(VOWClient.API.Ownership.Commercial);
                    else if (ownership == Core.Ownership.Institutional)
                        criteria.Ownerships.Add(VOWClient.API.Ownership.Institutional);
                    else if (ownership == Core.Ownership.Garage)
                        criteria.Ownerships.Add(VOWClient.API.Ownership.Garage);
                    else if (ownership == Core.Ownership.DevelopmentSite)
                        criteria.Ownerships.Add(VOWClient.API.Ownership.DevelopmentSite);
                }
            }

            if (options.ApartmentFeatures.Contains(ApartmentFeature.OutdoorSpace))          //ApartmentFeature outdoor space
                criteria.OutdoorSpace = true;
            if (options.ApartmentFeatures.Contains(ApartmentFeature.WasherDryer))               //ApartmentFeature Washer Dryer
                criteria.WasherDryer = true;
            if (options.ApartmentFeatures.Contains(ApartmentFeature.Fireplace))                     //ApartmentFeature Fireplace
                criteria.Fireplace = true;
            if (options.ApartmentFeatures.Contains(ApartmentFeature.HighCeilings))                         //ApartmentFeature.HighCeilings
                criteria.HighCeilings= true;
            if (options.ApartmentFeatures.Contains(ApartmentFeature.DishWasher))                 //ApartmentFeature DishWasher
                criteria.Dishwasher = true;
            if (options.ApartmentFeatures.Contains(ApartmentFeature.Penthouse))                   //ApartmentFeature.Penthouse
                criteria.Penthouse = true;
            if (options.ApartmentFeatures.Contains(ApartmentFeature.Duplex))                         //ApartmentFeature.Duplex
                criteria.Duplex = true;
            if (options.ApartmentFeatures.Contains(ApartmentFeature.ParkView))                         //ApartmentFeature.ParkView
                criteria.ParkView = true;
            if (options.ApartmentFeatures.Contains(ApartmentFeature.RiverView))                         //ApartmentFeature.RiverView
                criteria.RiverView= true;



            if (options.BuildingFeatures.Contains(BuildingFeature.AttendedLobby))                    //BuildingFeature.AttendedLobby
                criteria.AttendedLobby = true;
            if (options.BuildingFeatures.Contains(BuildingFeature.FitnessFacility))                         //BuildingFeature.FitnessFacility
                criteria.FitnessFacility = true;
            if (options.BuildingFeatures.Contains(BuildingFeature.SwimmingPool))                         //BuildingFeature.FitnessFacility
                criteria.Pool = true;
            if (options.BuildingFeatures.Contains(BuildingFeature.RoofDeck))                         //BuildingFeature.FitnessFacility
                criteria.RoofDeck = true;
            if (options.BuildingFeatures.Contains(BuildingFeature.LaundryRoom))                         //BuildingFeature.FitnessFacility
                criteria.Laundry = true;
            if (options.BuildingFeatures.Contains(BuildingFeature.NewDevelopment))                         //BuildingFeature.FitnessFacility
                criteria.NewDevelopment = true;
            if (options.BuildingFeatures.Contains(BuildingFeature.PetFriendly))                                 //BuildingFeature.PetFriendly
                criteria.PetsAllowed = true;
            if (options.BuildingFeatures.Contains(BuildingFeature.GardenCourtyard))                         //BuildingFeature.GardenCourtyard
                criteria.GardenOrCourtyard = true;
            if (options.BuildingFeatures.Contains(BuildingFeature.PreWar))                                      //BuildingFeature.PreWar
                criteria.Prewar = true;
            if (options.BuildingFeatures.Contains(BuildingFeature.TownhouseApt))                                      //BuildingFeature.TownhouseApt
                criteria.TownhouseApt = true;




            if (options.ShortTermRental)                 //ShortTermRental
                criteria.ShortTermRental = true;



            return criteria;
        }

        public static int GetVOWResultAccount(VOWClient.API.SearchCriteria criteria)
        {
            using (Client client = new Client())
            {
                SearchResult<ListingResult> result = client.GetListings(criteria);
                return result.Info.TotalCount;
            }
        }
    }
}