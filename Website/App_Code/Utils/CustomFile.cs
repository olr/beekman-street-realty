﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.Configuration;

/// <summary>
/// upload action--Get author name
/// </summary>
namespace BrokerTemplate.Web
{
    public static class CustomFile
    {
        private static string path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data/DocumentAuthor.xml");
        private static string[] credential = { "mediaservice", "jedi1234" };
        private static string foldername = ConfigurationManager.AppSettings["CompanyName"].ToLower();// "dmm";
        public static int AddFile(int categoryID, HttpPostedFile file, int agentid)
        {
            int insert = 0, upload = 0, exist = 0;
            exist = CheckFileExist(foldername, file);
            if (exist == 0)
            {
                insert = InsertFileToDB(categoryID, file, agentid);
                upload = UploadFile(categoryID, file, foldername);
                //AddAuthor(agentid);
                if (insert > 0 && upload > 0)   // Add File Successful
                    return 1;
                else                                         // Add File Unsuccessful
                    return 0;
            }
            else
            {
                insert = InsertFileToDB(categoryID, file, agentid);
                //AddAuthor(agentid);
                if (insert > 0)   // Add File Successful
                    return 1;
                else                                         // Add File Unsuccessful
                    return 0;
            }
        }
        private static int InsertFileToDB(int categoryID, HttpPostedFile file, int agentid)
        {
            string title = Path.GetFileName(file.FileName);
            //Regex re = new Regex(@"*.[doc|docx|txt|pdf]",RegexOptions.IgnoreCase);
            int result = ServiceLocator.GetRepository().AddFile(categoryID, title, agentid);
            return result;
        }

        private static void CopyStream(Stream input, Stream output)
        {
            byte[] b = new byte[32768];
            int r;
            while ((r = input.Read(b, 0, b.Length)) > 0)
                output.Write(b, 0, r);
        }

        private static int UploadFile(int categoryID, HttpPostedFile file, string folderName)
        {
            CreateFolder(folderName);

            using (WebClient client = new WebClient())
            {
                try
                {
                    string url = "ftp://media.olr.com/client/" + folderName + "/" + file.FileName;

                    client.Credentials = new NetworkCredential(credential[0], credential[1]);
                    MemoryStream mem = new MemoryStream();
                    CopyStream(file.InputStream, mem);
                    byte[] buffer = mem.GetBuffer();
                    client.UploadData(url, buffer);
                    return 1;
                }
                catch
                {
                }
            }
            return 0;
        }
        private static int CreateFolder(string folderName)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://media.olr.com/client");

            request.Credentials = new NetworkCredential(credential[0], credential[1]);
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            List<string> result = new List<string>();
            while (!reader.EndOfStream)
            {
                result.Add(reader.ReadLine());
            }
            reader.Close();
            responseStream.Close();
            response.Close();
            foreach (var r in result)
            {
                if (string.Equals(r.ToLower(), folderName.ToLower()))
                    return 1;
            }

            // Create new folder in ftp://media.olr.com/client

            FtpWebRequest newRequest = (FtpWebRequest)WebRequest.Create("ftp://media.olr.com/client/" + folderName);
            newRequest.Method = WebRequestMethods.Ftp.MakeDirectory;
            newRequest.Credentials = new NetworkCredential(credential[0], credential[1]);
            try
            {
                FtpWebResponse r = (FtpWebResponse)newRequest.GetResponse();
                return 1;
            }
            catch (Exception e)
            {
                return 0;
            }
        }
        private static int CheckFileExist(string folderName, HttpPostedFile file)
        {
            FtpWebRequest newRequest = (FtpWebRequest)WebRequest.Create("ftp://media.olr.com/client/" + folderName + "/" + file.FileName);
            newRequest.Credentials = new NetworkCredential(credential[0], credential[1]);
            newRequest.Method = WebRequestMethods.Ftp.GetFileSize;

            try
            {
                FtpWebResponse response = (FtpWebResponse)newRequest.GetResponse();
                return 1;
            }
            catch (WebException ex)
            {
                FtpWebResponse r = (FtpWebResponse)ex.Response;
                if (r.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    return 0;
                else
                    return 1;
            }

        }
        public static string GetFile(int fileid)
        {
            UserFile file = ServiceLocator.GetRepository().GetFileByFileID(fileid);
            string path = "http://media.olr.com/client/" + foldername + "/" + file.Path;
            return path;
        }
        //private static void AddAuthor(int authorid)
        //{
        //    var xml = XElement.Load(path);
        //    var authors = xml.Elements("Author");


        //    BrokerInfo info = ServiceLocator.GetRepository().GetBrokerByID(authorid);
        //    string name = info.FirstName + " " + info.LastName;

        //    if (authors.Count() == 0)
        //    {
        //        xml.AddFirst(new XElement("Author", new XElement("ID", authorid.ToString()), new XElement("Name", name)));
        //    }
        //    else
        //    {
        //        XElement author = authors.Where(x => x.Element("ID").Value == authorid.ToString()).FirstOrDefault();
        //        if (author == null || string.IsNullOrEmpty(author.Element("Name").Value))// the author is not exist in "DocumentAuthor" file
        //        {
        //            var lastAuthor = authors.Last();
        //            lastAuthor.AddAfterSelf(new XElement("Author", new XElement("ID", authorid.ToString()), new XElement("Name", name)));
        //        }
        //    }
        //    xml.Save(path);
        //}

        //public static string GetAuthorName(int authorid)
        //{
        //    var xml = XElement.Load(path);
        //    var author = xml.Elements("Author").Where(x => x.Element("ID").Value == authorid.ToString()).FirstOrDefault();
        //    string name=    author.Element("Name").Value;
 
        //    return name;
        //}

    }
    public class CustomerFileSystemTransferModel // this is for search
    {
        public UserCategories Categories { get; set; }
        public UserFiles Files { get; set; }
        public string SearchTitle { get; set; }
        public int AgentId { get; set; }
    }
}