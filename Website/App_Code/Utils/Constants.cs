﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrokerTemplate.Core
{

    public static class Neighborhood
    {
        //city codes for nyc boroughs
        public const int Manhattan = 1064;
        public const int Brooklyn = 184;
        public const int Queens = 1278;
        public const int Bronx = 180;
        public const int StatenIsland = 1500;

        //general neighborhoods
        public const string Downtown = "9,13,14,41,42,325,15,324,27,20,19,31,6,3,12";
        public const string MidtownEast = "21,29,28,26,2,30,22,18";
        public const string MidtownWest = "5,7,23";
        public const string UpperEast = "4,32";
        public const string UpperWest = "17,8,33";
        public const string Harlem = "10,34";
        public const string UpperManhattan = "128,59,43,58,34,321,10";
        public const string Uptown = "10,17,34,43,58,59,128,321"; //upper manhattan + harlem


        //For Brooklyn
        public const string North = "61,69,90,80,104,49";
        public const string NorthWest = "48,72,77,85,122,123";
        public const string West = "50,51,52,53,64,88,119";
        public const string SouthWest = "60,62,65,84,116,121,124";
        public const string South = "66,73,87,89,93,95,110,111,326";
        public const string Central = "74,78,98,81,82,94,106,108";
        public const string East = "68,71,75,79,92,101,113,114,70,127";
        public const string SouthEast = "63,83,86,97,99,327";

        //brooklyn neighborhoods not in the garfield list
        //bath beach,bensonhurt,bergen beach,borough park,brighton beach,broadway junction,brownsville
        //canarsie,city line,coney island,cypress hills,dyker heights,east flatbush,flatlands
        //fort hamilton,fulton ferry,georgetown,gerritsen beach,gravesend,highland park,homecrest
        //manhattan beach,manhattan terrace,mapleton,marine park,mill basin,mill island,navy yard,new lots,new utrecht,northside,
        //ocean hill,ocean parkway,paerdegat basin,remsen village,rugby,sea gate,southside,
        //spring creek,starrett city,stuyvesant heights,weeksville,wingate
        public const string GarfieldOtherBrooklyn = "67,76,322,91,125,96,100,126,102,103,105,107,109,112,115,117,118,120";
    }

}