﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Data;
using System.Text;
using BrokerTemplate.Core;
using System.Net.Mail;
using System.Configuration;
using System.Xml;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Globalization;
using OLRDataLayer;
using OLRData;

namespace BrokerTemplate.Web
{
    /// <summary>
    /// Summary description for SiteHelper
    /// </summary>
    public static class SiteHelper
    {
        public static void CreateAgentLastNameXml()
        {
            string xmlFile = HttpContext.Current.Server.MapPath("~/AgentInfoByLastName.xml");
            //get the date of the file
            FileInfo info = new FileInfo(xmlFile);
            if (info != null)
            {
                if (info.LastWriteTime > DateTime.Now.AddHours(-4))
                    return;
            }
            AgentSearchOptions options = new AgentSearchOptions();
            options.OrderBy = SortOrder.LastName;
            options.PageSize = 1000;
            int total;
            List<BrokerInfo> agents = ServiceLocator.GetRepository().GetBrokers(options, out total);
            XmlTextWriter writer = new XmlTextWriter(xmlFile, null);
            //Use automatic indentation for readability.
            writer.Formatting = Formatting.Indented;
            //Write the root element
            writer.WriteStartDocument();
            writer.WriteStartElement("Agents");
            BrokerInfo lastAgent = new BrokerInfo();
            bool newElement = true;
            foreach (BrokerInfo agent in agents)
            {
                if (agent.LastName.StartsWith("A"))
                {
                    if (newElement)
                    {
                        writer.WriteStartElement("letterA");
                        newElement = false;
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "A");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "A");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    continue;
                }
                if (agent.LastName.StartsWith("B"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("B")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterB");
                        newElement = false;
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "B");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "B");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("C"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("C")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterC");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "C");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "C");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("D"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.ToUpper().StartsWith("D")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterD");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "D");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "D");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("E"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("E")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterE");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "E");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "E");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("F"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("F")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterF");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "F");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "F");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("G"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("G")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterG");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "G");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "G");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("H"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("H")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterH");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "H");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "H");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("I"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("I")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterI");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "I");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "I");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("J"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("J")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterJ");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "J");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "J");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("K"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("K")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterK");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "K");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "K");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("L"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("L")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterL");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "L");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "L");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("M"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("M")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterM");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "M");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "M");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("N"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("N")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterN");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "N");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "N");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("O"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("O")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterO");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "O");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "O");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("P"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("P")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterP");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "P");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "P");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("Q"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("Q")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterQ");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "Q");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "Q");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("R"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("R")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterR");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "R");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "R");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("S"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("S")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterS");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "S");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "S");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("T"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("T")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterT");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "T");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "T");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("U"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("U")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterU");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "U");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "U");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("V"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.ToUpper().StartsWith("V")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterV");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "V");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "V");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("W"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("W")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterW");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "W");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "W");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("X"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("X")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterX");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "X");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "X");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("Y"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("Y")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterY");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "Y");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "Y");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.LastName.StartsWith("Z"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.LastName.StartsWith("Z")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterZ");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "Z");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "Z");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }


            }
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndDocument();
            //Write the XML to file and close the writer
            writer.Close();
            writer = null;
        }



        public static void CreateAgentFirstNameXml()
        {
            string xmlFile = HttpContext.Current.Server.MapPath("~/AgentInfoByFirstName.xml");
            //get the date of the file
            FileInfo info = new FileInfo(xmlFile);
            if (info != null)
            {
                if (info.LastWriteTime > DateTime.Now.AddHours(-4))
                    return;
            }
            AgentSearchOptions options = new AgentSearchOptions();
            options.OrderBy = SortOrder.FirstName;
            options.PageSize = 1000;
            int total;
            List<BrokerInfo> agents = ServiceLocator.GetRepository().GetBrokers(options, out total);
            XmlTextWriter writer = new XmlTextWriter(xmlFile, null);
            //Use automatic indentation for readability.
            writer.Formatting = Formatting.Indented;
            //Write the root element
            writer.WriteStartDocument();
            writer.WriteStartElement("Agents");
            BrokerInfo lastAgent = new BrokerInfo();
            bool newElement = true;
            foreach (BrokerInfo agent in agents)
            {//newElement || !lastAgent.LastName.StartsWith("Y")
                if (agent.FirstName.StartsWith("A"))
                {
                    if (newElement)
                    {
                        writer.WriteStartElement("letterA");
                        newElement = false;
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "A");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "A");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    continue;
                }
                if (agent.FirstName.StartsWith("B"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("B")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterB");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "B");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "B");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    continue;
                }
                if (agent.FirstName.StartsWith("C"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.ToUpper().StartsWith("C")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterC");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "C");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "C");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("D"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.ToUpper().StartsWith("D")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterD");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "D");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "D");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("E"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("E")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterE");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "E");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "E");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("F"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("F")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterF");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "F");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "F");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("G"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("G")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterG");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "G");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "G");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("H"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("H")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterH");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "H");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "H");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("I"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("I")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterI");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "I");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "I");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("J"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("J")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterJ");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "J");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "J");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("K"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("K")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterK");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "K");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "K");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("L"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("L")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterL");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "L");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "L");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("M"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("M")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterM");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "M");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "M");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("N"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("N")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterN");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "N");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "N");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("O"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("O")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterO");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "O");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "O");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("P"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("P")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterP");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "P");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "P");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("Q"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("Q")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterQ");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "Q");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "Q");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("R"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("R")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterR");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "R");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "R");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("S"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("S")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterS");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "S");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "S");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("T"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("T")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterT");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "T");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "T");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("U"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("U")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterU");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "U");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "U");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("V"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.ToUpper().StartsWith("V")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterV");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "V");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "V");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("W"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("W")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterW");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "W");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "W");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("X"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("X")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterX");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "X");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "X");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("Y"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("Y")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterY");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "Y");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "Y");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }
                if (agent.FirstName.StartsWith("Z"))
                {
                    if (newElement || (lastAgent.LastName != null && !lastAgent.FirstName.StartsWith("Z")))
                    {
                        if (!newElement)
                        {
                            writer.WriteEndElement();
                        }
                        writer.WriteStartElement("letterZ");
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "Z");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    }
                    else
                    {
                        writer.WriteStartElement("Agent");
                        writer.WriteAttributeString("letter", "Z");
                        writer.WriteElementString("Name", agent.FirstName + " " + agent.LastName);
                        writer.WriteElementString("Link", "agent_detail.aspx?agentid=" + agent.ID.ToString());
                        writer.WriteEndElement();
                    } continue;
                }

                lastAgent = agent;
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndDocument();
            //Write the XML to file and close the writer
            writer.Close();
            writer = null;
        }
        private static int LINE_LENGTH = 99;
        public static string CorrectCrossStreet(string value)
        {
            string[] results = value.Split(' ');
            for (int i = 0; i < results.Length; i++)
            {
                switch (results[i].ToLower())
                {
                    case "street":
                        results[i] = "St";
                        break;
                    case "avenue":
                        results[i] = "Ave";
                        break;
                    case "road":
                        results[i] = "Rd";
                        break;
                    case "blvd":
                    case "boulevard":
                        results[i] = "Blvd";
                        break;
                }
            }

            return string.Join(" ", results);
        }

        public static bool GeocodeAddress(LocationPoint GP, string GoogleAPIKey)
        {
            string sURL = "http://maps.google.com/maps/geo?q=" + GP.Address + "&output=xml&key=" + GoogleAPIKey;
            WebRequest request = WebRequest.Create(sURL);
            request.Timeout = 10000;
            // Set the Method property of the request to POST.
            request.Method = "POST";
            // Create POST data and convert it to a byte array.
            string postData = "This is a test that posts this string to a Web server.";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            Stream dataStream = request.GetRequestStream();

            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            WebResponse response = request.GetResponse();
            // Display the status.
            //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();

            StringReader tx = new StringReader(responseFromServer);

            //return false;
            //System.Xml.XmlReader xr = new System.Xml.XmlReader();

            //return false;

            DataSet DS = new DataSet();
            DS.ReadXml(tx);
            //DS.ReadXml(dataStream);
            //DS.ReadXml(tx);



            int StatusCode = 0;
            Int32.TryParse(DS.Tables["Status"].Rows[0]["code"].ToString(), out StatusCode);
            if (StatusCode == 200)
            {
                string sLatLon = DS.Tables["Point"].Rows[0]["coordinates"].ToString();
                string[] s = sLatLon.Split(',');
                if (s.Length > 1)
                {
                    GP.Latitude = double.Parse(s[1]);
                    GP.Longitude = double.Parse(s[0]);
                }
                if (DS.Tables["Placemark"] != null)
                {
                    GP.Address = DS.Tables["Placemark"].Rows[0]["address"].ToString();
                }
                if (DS.Tables["PostalCode"] != null)
                {
                    GP.Address += " " + DS.Tables["PostalCode"].Rows[0]["PostalCodeNumber"].ToString();
                }
                return true;
            }
            return false;

        }

        public static bool GeocodeAddress(LocationPoint GP)
        {
            // "http://maps.google.com/maps/geo?q=" + GP.Address + "&output=xml";
            string sURL = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + GP.Address + "&sensor=false";
            WebRequest request = WebRequest.Create(sURL);
            request.Timeout = 10000;
            // Set the Method property of the request to POST.
            request.Method = "POST";
            // Create POST data and convert it to a byte array.
            string postData = "This is a test that posts this string to a Web server.";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            Stream dataStream = request.GetRequestStream();

            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            WebResponse response = request.GetResponse();
            // Display the status.
            //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();

            StringReader tx = new StringReader(responseFromServer);

            //return false;
            //System.Xml.XmlReader xr = new System.Xml.XmlReader();

            //return false;

            DataSet DS = new DataSet();
            DS.ReadXml(tx);
            //DS.ReadXml(dataStream);
            //DS.ReadXml(tx);

            // int StatusCode = 0;
            // Int32.TryParse(DS.Tables["Status"].Rows[0]["code"].ToString(), out StatusCode);
            //if (StatusCode == 200)
            if (DS.Tables["GeocodeResponse"].Rows[0]["status"].ToString() == "OK")
            {
                //string sLatLon = DS.Tables["Point"].Rows[0]["coordinates"].ToString();

                string Lat = DS.Tables["location"].Rows[0]["lat"].ToString();
                string Lon = DS.Tables["location"].Rows[0]["lng"].ToString();
                GP.Latitude = double.Parse(Lat);
                GP.Longitude = double.Parse(Lon);
                if (DS.Tables["formatted_address"] != null)
                {
                    GP.Address = DS.Tables["result"].Rows[0]["formatted_address"].ToString();
                }

                return true;
            }
            return false;

        }
        public static LocationPoint GeocodeAddress(int buildingid)
        {
            OLRWebContext db = new OLRWebContext();
            LocationPoint point = new LocationPoint();

            var geo = db.GetBuildingGeo(buildingid).FirstOrDefault();
            if (geo != null)
            {
                point.Latitude = geo.Latitude.GetValueOrDefault();
                point.Longitude = geo.Longitude.GetValueOrDefault();
            }
            return point;
        }
        public static string FormatDate(DateTime value)
        {
            string result = value.ToString("dddd") + ", ";

            switch (value.Month)
            {
                case 1:
                case 2:
                case 8:
                case 10:
                case 11:
                case 12:
                    result += value.ToString("MMM d");
                    break;
                case 9:
                    result += value.ToString("d");
                    break;
                default:
                    result += value.ToString("MMMM d");
                    break;

            }

            return result;
        }

        public static string GetListingImage(string fileLocation, string fileName, string olrRepository, string mlsRepository, string interiorRepository, string noImage, string imageUrlBase, string buildPhoto)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                return (string.IsNullOrWhiteSpace(buildPhoto)) ? noImage : imageUrlBase + buildPhoto;
            else
            {
                string location = string.Empty;
                switch (fileLocation.ToUpper())
                {
                    case "BOTH":
                    case "OLR":
                        location = olrRepository;
                        break;
                    case "MLS":
                        location = mlsRepository;
                        break;
                    default:
                        break;
                }
                location = Path.Combine(location, interiorRepository + fileName);
                return location;
            }
        }

        public static string GetNotesDisplayString(string value, int length)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return value;
            }
            int l = 0;
            for (int i = 0; i < value.Length; i++)
            {
                if (value[i] == '\n')
                {
                    int rem = i % LINE_LENGTH;
                    l += LINE_LENGTH - rem;
                }
                else
                {
                    l++;
                }
                if (l >= length)
                    break;
            }
            if (l > value.Length)
            {
                return value;
            }
            while ((l < value.Length) && !char.IsWhiteSpace(value[l]))
            {
                l++;
            }
            return value.Substring(0, l);
        }

        public static string FormatPhone(string value)
        {
            return value.Replace("-", ".").Replace("[", string.Empty).Replace("ext ", "x").Replace("]", string.Empty);
        }

        public static string FormatBio(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                value = value.Replace("\r\n", "<br />");
            }
            return value;
        }
        private static string path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data/Languages.xml");
        public static string GetCulture(string culture)
        {
            XElement root = XElement.Load(path);
            var languages = root.Elements("Language");
            var c = from l in languages
                    where l.Attribute("value").Value == culture
                    select l;
            return c.FirstOrDefault().Value;
        }
        public static string FormatWeb(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                value = value.Replace("\r\n", "<br />");
                value = value.Replace("&nbsp;", " ");
                value = Regex.Replace(value, @"^>\s+", string.Empty, RegexOptions.Multiline);
            }
            return value;
        }

        public static Dictionary<int, int> GetListingCount(Buildings list)
        {
            Dictionary<int, int> dict = new Dictionary<int, int>();

            var ids = list.Select(x => x.ID).ToList();
            using (var db = new DataLayer())
            {
                var apts = db.GetActiveSaleListingsAsQueryable().Union(db.GetActiveRentalListingsAsQueryable())
                                    .Where(x => ids.Contains(x.BuildingID ?? 0))
                                    .Select(x => new TempListing { BuildingID = x.BuildingID.GetValueOrDefault(), ListingID = x.ListingID.ToString() });
                var bfs = db.GetActiveBuildingForSaleAsQueryable()
                                   .Where(x => ids.Contains(x.THBldgID ?? 0))
                                   .Select(x => new TempListing { BuildingID = x.THBldgID.GetValueOrDefault(), ListingID = x.THListingID });

                var result = apts.Union(bfs).ToList();

                foreach (var id in ids)
                    dict.Add(id, result.Count(x => x.BuildingID == id));
            }
            return dict;
        }

        public static Dictionary<int, int> GetListingCount(List<FeaturedBuilding> list)
        {
            Dictionary<int, int> dict = new Dictionary<int, int>();

            var ids = list.Select(x => x.BuildingID).ToList();
            using (var db = new DataLayer())
            {
                var apts = db.GetActiveSaleListingsAsQueryable().Union(db.GetActiveRentalListingsAsQueryable())
                                    .Where(x => ids.Contains(x.BuildingID ?? 0))
                                    .Select(x => new TempListing { BuildingID = x.BuildingID.GetValueOrDefault(), ListingID = x.ListingID.ToString() });
                var bfs = db.GetActiveBuildingForSaleAsQueryable()
                                   .Where(x => ids.Contains(x.THBldgID ?? 0))
                                   .Select(x => new TempListing { BuildingID = x.THBldgID.GetValueOrDefault(), ListingID = x.THListingID });

                var result = apts.Union(bfs).ToList();

                foreach (var id in ids)
                    dict.Add(id.Value, result.Count(x => x.BuildingID == id));
            }
            return dict;
        }

        public static Listings GetRandomFeaturedListings(int maxRecords)
        {
            SearchOptions options = new SearchOptions();
            options.PropertyTypes.Add(PropertyType.Sale);
            options.PageSize = maxRecords;
            var list = ServiceLocator.GetSearchService().GetRandomFeaturedListings(options);
            return list;
        }

        public static void HandleEmailAlertRequest(this EmailAlertViewModel model)
        {
            try
            {
                if (model != null)
                {
                    MailMessage m = new MailMessage("noreply@olr.com", ConfigurationManager.AppSettings["defaultEmail"]);
                    m.Subject = "Website Email Alert Request";
                    m.IsBodyHtml = true;
                    m.Body = HttpContext.Current.RenderPartial("cntrls/Email/EmailAlertTemplate", model);

                    using (var client = new SmtpClient())
                    {
                        client.Send(m);
                    }
                }
            }
            catch
            {
            }
        }

        public static IDictionary<string, string> GetNeighborhood()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            XElement root = XElement.Load(HttpContext.Current.Server.MapPath("~/App_Data/Neighborhoods.xml"));
            IEnumerable<XElement> xml = from n in root.Elements("node") select n;

            foreach (XElement e in xml)
                dictionary.Add(e.Attribute("text").Value, e.Attribute("value").Value);
            return dictionary;
        }


        public static IDictionary<string, string> GetMinPriceList(this SearchOptions options)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            XElement root = XElement.Load(HttpContext.Current.Server.MapPath("~/App_Data/PriceRentListMin.xml"));
            string type = options.PropertyTypes.Contains(PropertyType.Rental) ? "R" : "P";
            IEnumerable<XElement> xml = from n in root.Elements("PriceRent")
                                        where (string)n.Element("Type") == type
                                        select n;
            string label_price = "";
            if (options.PropertyTypes.Contains(PropertyType.Rental))
            {
                label_price = "No Minimum";
            }
            else
            {
                label_price = "Min Price";
            }

            dictionary.Add(label_price, string.Empty);  //Resources.SearchControls.NoMiminumCaption
            foreach (XElement e in xml)
            {
                Int32 value = Convert.ToInt32(e.Element("Amount").Value);
                dictionary.Add(value.ToString("C0"), value.ToString());
            }
            if (options.PropertyTypes.Contains(PropertyType.Sale))
            {
                label_price = "No Minimum";
                dictionary.Add(label_price, string.Empty);  //Resources.SearchControls.NoMiminumCaption
            }
            return dictionary;
        }

        public static IDictionary<string, string> GetMaxPriceList(this SearchOptions options)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            XElement root = XElement.Load(HttpContext.Current.Server.MapPath("~/App_Data/PriceRentList.xml"));
            string type = options.PropertyTypes.Contains(PropertyType.Rental) ? "R" : "P";
            IEnumerable<XElement> xml = from n in root.Elements("PriceRent")
                                        where (string)n.Element("Type") == type
                                        select n;
            string label_price = "";
            if (options.PropertyTypes.Contains(PropertyType.Rental))
            {
                label_price = "Max Rent";

            }
            else
            {
                label_price = "Max Price";
            }
            dictionary.Add(label_price, string.Empty); //Resources.SearchControls.NoMaximumCaption
            foreach (XElement e in xml)
            {
                Int32 value = Convert.ToInt32(e.Element("Amount").Value);
                dictionary.Add(value.ToString("C0"), value.ToString());
            }

            label_price = "No Maximum";
            dictionary.Add(label_price, string.Empty); //Resources.SearchControls.NoMaximumCaption
            return dictionary;
        }

        public static IDictionary<string, string> LoadDictionary(string xmlFileName)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            XElement root = XElement.Load(HttpContext.Current.Server.MapPath("~/App_Data/" + xmlFileName));
            IEnumerable<XElement> xml = from n in root.Elements("node") select n;

            foreach (XElement e in xml)
                dictionary.Add(e.Attribute("text").Value, e.Attribute("value").Value);

            return dictionary;
        }

        public static NeighborhoodViewModel GetNeighborhoodInfo(string neighborhoodName)
        {
            var model = new NeighborhoodViewModel();
            var root = XElement.Load(HttpContext.Current.Server.MapPath("~/App_Data/Neighborhoods.xml"));
            var xml = (from n in root.Descendants("neighborhood")
                       where (string)n.Attribute("name") == neighborhoodName
                       select n).FirstOrDefault();
            if (xml != null)
            {
                model.Name = xml.Attribute("name").Value;
                model.ImageUrl = xml.Attribute("photo").Value;
                model.Description = xml.Element("description").Value;
                var links = xml.Element("links");
                if (links != null && links.HasElements)
                {
                    foreach (var x in links.Elements("link"))
                        model.Links.Add(new KeyValuePair<string, string>(x.Attribute("name").Value, x.Attribute("url").Value));
                }
            }
            return model;
        }
        public static List<SlideShowData> GetSlideshows()
        {
            List<SlideShowData> list = new List<SlideShowData>();
            using (OLRDataLayer.DataLayer db = new OLRDataLayer.DataLayer())
            {
                var aptFiles = from f in db.GetAllListingFiles()
                               join l in db.GetActiveListingsAsQueryable() on f.Apt_ListingID equals l.ListingID
                               where f.FileType == "s"
                               select new TempSelect
                               {
                                   FileName = f.FileName,
                                   FileLocation = f.FileLocation,
                                   FileType = f.FileType,
                                   DisplayName = f.DisplayName,
                                   ListingID = l.ListingID,
                                   THListingID = "",
                                   Price = l.Price,
                                   StatusCode = l.SaleStatusCode,
                                   RentalStatusCode = l.RentalStatusCode
                               };
                var bfsFiles = from f in db.GetAllListingFiles()
                               join l in db.GetActiveBuildingForSaleAsQueryable() on f.TH_ListingID equals l.THListingID
                               where f.FileType == "s"
                               select new TempSelect
                               {
                                   FileName = f.FileName,
                                   FileLocation = f.FileLocation,
                                   FileType = f.FileType,
                                   DisplayName = f.DisplayName,
                                   ListingID = null,
                                   THListingID = l.THListingID,
                                   Price = l.Price,
                                   StatusCode = l.SaleStatusCode,
                                   RentalStatusCode = -1
                               };
                var files = aptFiles.Union(bfsFiles).ToList();
                foreach (var file in files)
                    list.Add(GetSlideshowData(file));

            }
            return list;
        }

        private static SlideShowData GetSlideshowData(TempSelect file)
        {
            SlideShowData s = new SlideShowData();
            s.ListingID = file.ListingID.HasValue ? file.ListingID.Value.ToString() : file.THListingID;
            s.ImageUrl = BuildUrl(file.FileName, file.FileLocation, file.FileType);
            if (!string.IsNullOrEmpty(file.DisplayName))
                s.Caption = string.Format(@"{0}", file.DisplayName);
            return s;
        }

        private static string GetStatus(TempSelect file)
        {
            string status = string.Empty;
            if (file.Price > 0)
                status = OLRConvert.GetSaleStatus(file.StatusCode);
            else
                status = OLRConvert.GetRentalStatus(file.RentalStatusCode);
            return status;
        }
        public static ListingInfo GetRandomFeaturedListing(bool saleListing)
        {
            ListingInfo l = null;

            using (var db = new OLRDataLayer.DataLayer())
            {
                if (saleListing)
                {
                    var q = (from o in db.GetActiveOpenHouses()
                             join a in db.GetActiveListingsAsQueryable() on o.ListingID equals a.ListingID
                             where a.Price > 0
                             orderby o.OpenHouseDate descending
                             select o).FirstOrDefault();
                    if (q != null)
                        l = ServiceLocator.GetRepository().GetListingInfoByID(q.ListingID.ToString());
                }
                else
                {
                    var q = (from o in db.GetActiveOpenHouses()
                             join a in db.GetActiveListingsAsQueryable() on o.ListingID equals a.ListingID
                             where a.Rent2 > 0
                             orderby o.OpenHouseDate descending
                             select o).FirstOrDefault();
                    if (q != null)
                        l = ServiceLocator.GetRepository().GetListingInfoByID(q.ListingID.ToString());
                }
            }

            return l;
        }
        public static string GetFormatPrice(int price)
        {
            string result = price.ToString("#,#", CultureInfo.InvariantCulture);
            return result;
        }
        public static string GetDescriptionForDefalutPage(string description, string id)
        {
            string str, temp;
            int index = 0;
            str = description;
            temp = removeJScript(description);
            if (description.Length > 75)
            {
                temp = temp.Substring(0, 74);
                index = temp.LastIndexOf(" ");
                str = temp.Substring(0, index) + "...<a href='detail.aspx?id=" + id + "'>[MORE]</a> ";
            }
            return str;
        }
        private static string removeJScript(string ori)
        {
            string result = Regex.Replace(ori, "<script.*?</script>", "");
            return result;
        }
        private static string BuildUrl(string fileName, string fileLocation, string fileType)
        {
            string location = string.Empty;

            if (string.IsNullOrWhiteSpace(fileName))
                return location;

            if (!string.Equals("REM", fileLocation, StringComparison.InvariantCultureIgnoreCase))
            {
                switch (fileLocation.ToUpper())
                {
                    case "BOTH":
                    case "OLR":
                        location = Resources.Media.OLRImageRepository;
                        break;
                    case "MLS":
                        location = Resources.Media.MLSImageRepository;
                        break;
                    default:
                        break;
                }

                if (string.Equals("i", fileType, StringComparison.InvariantCultureIgnoreCase))
                    location = System.IO.Path.Combine(location, Resources.Media.InteriorImageRepository + fileName);
                else if (string.Equals("f", fileType, StringComparison.InvariantCultureIgnoreCase))
                    location = System.IO.Path.Combine(location, Resources.Media.FloorPlanImageRepository + fileName);
                else if (string.Equals("v", fileType, StringComparison.InvariantCultureIgnoreCase))
                    location = Resources.Media.OLRVTRepository + fileName;
                else if (string.Equals("s", fileType, StringComparison.InvariantCultureIgnoreCase))
                    location = "http://media.olr.com/Pictures/Slideshow/" + fileName;
            }
            else
                location = fileName;

            if (location.Length > 0 && !location.StartsWith("http://", StringComparison.InvariantCultureIgnoreCase))
                location = "http://" + location;

            return location;
        }
    }

    public class LocationPoint
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Address { get; set; }

    }

    class TempSelect
    {
        public string FileName { get; set; }
        public string FileLocation { get; set; }
        public string FileType { get; set; }
        public string DisplayName { get; set; }
        public int? ListingID { get; set; }
        public string THListingID { get; set; }
        public int? Price { get; set; }
        public long? StatusCode { get; set; }
        public long? RentalStatusCode { get; set; }
    }
    public class TempListing
    {
        public int BuildingID { get; set; }
        public string ListingID { get; set; }
    }
    public class SlideShowData
    {
        public string ListingID { get; set; }
        public string ImageUrl { get; set; }
        public string Caption { get; set; }
    }

}