﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BrokerTemplate.Core;

namespace BrokerTemplate.Web
{
    /// <summary>
    /// Summary description for BuildingExtensions
    /// </summary>
    public static class CompanyNewsExtensions
    {
        public static string GetImageLogo(this CompanyNewsInfo news)
        {
            if (!string.IsNullOrEmpty(news.ThumbnailUrl))
                return news.ThumbnailUrl;
            else
                return "images/me_news.jpg";
        }

    }
}