﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using BrokerTemplate.Core;
using OLRDataLayer;

/// <summary>
/// Summary description for SearchCriteriaExtensions
/// </summary>
public static class SearchCriteriaExtensions
{
    public static string GetDescription(this SearchCriteria c)
    {
        StringBuilder sb = new StringBuilder();
        if (c.SearchParam.Any(x => x.SearchProperty == "Neighborhood"))
        {
            List<string> neighborhoods = new List<string>();
            foreach (var p in c.SearchParam.Where(x => x.SearchProperty == "Neighborhood"))
            {
                foreach (string idStr in p.SearchValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (fn.IsNumber(idStr))
                    {
                        long? id = fn.Get<long?>(idStr);

                        if (OLRConvert.IsAreaNeighborhood(id, "BK"))
                        {
                            var nb = OLRConvert.GetNeighborhoodExtended(id);
                            if (!string.IsNullOrWhiteSpace(nb))
                                neighborhoods.Add(nb);
                        }
                    }
                }
            }
            if (neighborhoods.Count > 0)
                sb.AppendFormat(@"Neighborhood: {0}<br />", string.Join(", ", neighborhoods.ToArray()));
        }

        if (c.SearchParam.Any(x => x.SearchProperty == "OtherBorough"))
        {
            List<string> boroughs = new List<string>();
            foreach (var p in c.SearchParam.Where(x => x.SearchProperty == "OtherBorough"))
            {
                foreach (string str in p.SearchValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    boroughs.Add(str);
            }
            if (boroughs.Count>0)
                sb.AppendFormat(@"Borough: {0}<br />", string.Join(", ", boroughs.ToArray()));
        }

        //apt features
        List<string> aptfeatures = new List<string>();
        if (c.SearchParam.Any(x => x.SearchProperty == "WasherDryer"))
            aptfeatures.Add("Washer/Dryer");
        if (c.SearchParam.Any(x => x.SearchProperty == "FirePlaceType"))
            aptfeatures.Add("Fireplace");
        if (c.SearchParam.Any(x => x.SearchProperty == "OutDoorType"))
            aptfeatures.Add("Outdoor Space");
        if (aptfeatures.Count > 0)
            sb.AppendFormat(@"Apartment Features: {0}<br />", string.Join(", ", aptfeatures.ToArray()));

        //bldg features
        List<string> bldgfeatures = new List<string>();
        if (c.SearchParam.Any(x => x.SearchProperty == "Doorman"))
            bldgfeatures.Add("Attended Lobby");
        if (c.SearchParam.Any(x => x.SearchProperty == "CommonOutdoorSpace"))
            bldgfeatures.Add("Outdoor Space");
        if (c.SearchParam.Any(x => x.SearchProperty == "Elevator"))
            bldgfeatures.Add("Elevator");
        if (c.SearchParam.Any(x => x.SearchProperty == "PetFriendly"))
            bldgfeatures.Add("Pet Friendly");
        if (bldgfeatures.Count > 0)
            sb.AppendFormat(@"Building Features: {0}<br />", string.Join(", ", bldgfeatures.ToArray()));

        //ownership
        if (c.SearchParam.Any(x => x.SearchProperty == "Ownership"))
        {
            List<string> ownership = new List<string>();
            foreach (var p in c.SearchParam.Where(x => x.SearchProperty == "Ownership"))
            {
                foreach (string str in p.SearchValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (fn.IsNumber(str))
                    {
                        long? id = fn.Get<long?>(str);
                        string own = OLRConvert.GetOwnershipType(id);
                        if (!string.IsNullOrWhiteSpace(own))
                            ownership.Add(own);
                    }
                }
            }
            if (ownership.Count > 0)
                sb.AppendFormat(@"Ownership: {0}<br />", string.Join(", ", ownership.ToArray()));
        }

        //bedrooms
        if (c.SearchParam.Any(x => x.SearchProperty == "Beds"))
            sb.AppendFormat(@"Bedroom Size: {0}<br />", c.SearchParam
                .FirstOrDefault(x => x.SearchProperty == "Beds").SearchValue);

        //baths
        if (c.SearchParam.Any(x => x.SearchProperty == "Baths"))
            sb.AppendFormat(@"Bathroom Size: {0}<br />", c.SearchParam
                .FirstOrDefault(x => x.SearchProperty == "Baths").SearchValue);

        string priceLabel = "Price";
        if (string.Equals("rental", c.SearchType))
            priceLabel = "Rent";

        //min price
        if (c.SearchParam.Any(x => x.SearchProperty == "PriceMinimum"))
            sb.AppendFormat(@"Minimum {0}: {1}<br />", priceLabel,
                c.SearchParam.FirstOrDefault(x => x.SearchProperty == "PriceMinimum").SearchValue);

        //max price
        if (c.SearchParam.Any(x => x.SearchProperty == "PriceMaximum"))
            sb.AppendFormat(@"Maximum {0}: {1}<br />", priceLabel,
                c.SearchParam.FirstOrDefault(x => x.SearchProperty == "PriceMaximum").SearchValue);

        return sb.ToString();
    }
}