﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BrokerTemplate.Core;

namespace BrokerTemplate.Web
{
    /// <summary>
    /// Summary description for ListingsExtensions
    /// </summary>
    public static class ListingsExtensions
    {
        public static void CreatePagingSession(this Listings l)
        {
            TempStorage.Add("PagingInfo", l.GetPagingList());
        }

        public static List<PagingInfo> GetPagingList(this Listings l)
        {
            string returnPage = HttpContext.Current.Request.Url.AbsoluteUri;
            return l.GetPagingList(returnPage);
        }

        public static List<PagingInfo> GetPagingList(this Listings l, string returnPage)
        {
            List<PagingInfo> pageInfoList = new List<PagingInfo>();
            for (int i = 0; i < l.Count; i++)
            {
                PagingInfo p = new PagingInfo();
                if (i == 0)
                {
                    p.PreviousListingId = string.Empty;
                }
                else
                {
                    p.PreviousListingId = l[i - 1].ListingID;
                }
                p.CurrentListingId = l[i].ListingID;
                p.ListPage = returnPage;
                if (l[i] == l.Last())
                {
                    p.NextListingId = string.Empty;
                }
                else
                {
                    p.NextListingId = l[i + 1].ListingID;
                }
                pageInfoList.Add(p);

            }
            return pageInfoList;
        }
        
        public static void Refresh(this List<PagingInfo> pageInfoList)
        {
            for (int i = 0; i < pageInfoList.Count; i++)
            {
                PagingInfo p = pageInfoList[i];
                if (i == 0)
                {
                    p.PreviousListingId = string.Empty;
                }
                else
                {
                    p.PreviousListingId = pageInfoList[i - 1].CurrentListingId;
                }
                if (p == pageInfoList.Last())
                {
                    p.NextListingId = string.Empty;
                }
                else
                {
                    p.NextListingId = pageInfoList[i + 1].CurrentListingId;
                }
            }
        }
    }
}