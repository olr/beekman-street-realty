﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.Unity;
using OLR.Controls;
using System.Collections.Specialized;
using System.IO;
using BrokerTemplate.Core;
using System.Configuration;

namespace BrokerTemplate.Web
{
    /// <summary>
    /// Summary description for SiteExtensioncs
    /// </summary>
    public static class SiteExtensioncs
    {
        public static void SetPagers(this ControlCollection controls, int index, int pagesize, int total)
        {
            foreach (Control c in controls)
            {
                if (c is OLRPageNav)
                {
                    (c as OLRPageNav).CurrentPage = index;
                    (c as OLRPageNav).PageSize = pagesize;
                    (c as OLRPageNav).TotalItems = total;

                }
                if (c.HasControls())
                    SetPagers(c.Controls, index, pagesize, total);
            }
        }

        public static SearchOptions ParseSearchOptions(this Page page)
        {
            SearchOptions options = null;

            if (page.Request["PropertyType"] != null)
            {
                options = new SearchOptions();
                options.PageSize = 10;

                if (fn.IsEnum<PropertyType>(page.Request["PropertyType"]))
                    options.PropertyTypes.Add(fn.StringToEnum<PropertyType>(page.Request["PropertyType"]));
                //if(page.Request["PropertyType"]=="Rental")
                //    options.PropertyTypes.Add(PropertyType.Rental);
                else
                    options.PropertyTypes.Add(PropertyType.Sale);

                if (!string.IsNullOrEmpty(page.Request["Neighborhood"]))
                {
                    foreach (string val in page.Request["Neighborhood"].Split(new char[] { ',' }))
                    {
                        if (fn.IsNumber(val))
                            options.Neighborhoods.Add(fn.Get<int>(val));
                    }
                }
                if (!string.IsNullOrEmpty(page.Request["Borough"]))
                {
                    foreach (string val in page.Request["Borough"].Split(new char[] { ',' }))
                    {
                        if (fn.IsNumber(val))
                        {
                            options.Boroughs.Add(fn.Get<Borough>(val));
                        }
                    }
                    if (options.Boroughs.Contains(Borough.Manhattan))
                        options.Neighborhoods.Clear();
                }
                if (!string.IsNullOrEmpty(page.Request["OpenHouse"]))
                {
                    options.OpenHouse = true;
                }
                if (!string.IsNullOrEmpty(page.Request["OwnershipTypes"]))
                {
                    foreach (string val in page.Request["OwnershipTypes"].Split(new char[] { ',' }))
                    {
                        if (fn.IsEnum<Ownership>(val))
                            options.OwnershipTypes.Add(fn.StringToEnum<Ownership>(val));
                    }
                }

                if (page.Request["IsCommercialSearch"] == "1")
                {
                    if (options.PropertyTypes.Contains(PropertyType.Sale))
                    {
                        if (options.OwnershipTypes.Count == 0)
                        {
                            //add commercial ownership only
                            options.OwnershipTypes.Add(Ownership.Commercial);
                            options.OwnershipTypes.Add(Ownership.MixedUse);
                        }
                    }
                    else
                    {
                        options.OwnershipTypes.Clear();
                        options.PropertyTypes.Clear();
                        options.PropertyTypes.Add(PropertyType.Rental);
                        options.PropertyTypes.Add(PropertyType.Commercial);
                    }
                }

                if (!string.IsNullOrEmpty(page.Request["CLPropertyTypes"]))
                {
                    foreach (string val in page.Request["CLPropertyTypes"].Split(new char[] { ',' }))
                    {
                        if (fn.IsEnum<CLPropertyType>(val))
                            options.CLPropertyType.Add(fn.StringToEnum<CLPropertyType>(val));
                    }
                    if (options.CLPropertyType.Count > 0)
                    {
                        options.OwnershipTypes.Clear();
                        options.PropertyTypes.Clear();
                        options.PropertyTypes.Add(PropertyType.Rental);
                        options.PropertyTypes.Add(PropertyType.Commercial);
                    }
                }
                if (!string.IsNullOrEmpty(page.Request["BuildingFeatures"]))
                {
                    foreach (string val in page.Request["BuildingFeatures"].Split(new char[] { ',' }))
                    {
                        if (fn.IsEnum<BuildingFeature>(val))
                            options.BuildingFeatures.Add(fn.StringToEnum<BuildingFeature>(val));
                    }
                }
                if (!string.IsNullOrEmpty(page.Request["ApartmentFeatures"]))
                {
                    foreach (string val in page.Request["ApartmentFeatures"].Split(new char[] { ',' }))
                    {
                        if (fn.IsEnum<ApartmentFeature>(val))
                            options.ApartmentFeatures.Add(fn.StringToEnum<ApartmentFeature>(val));
                    }
                }
                if (!string.IsNullOrEmpty(page.Request["MinPrice"]))
                {
                    if (fn.IsNumber(page.Request["MinPrice"]))
                        options.MinPrice = fn.Get<int>(page.Request["MinPrice"]);
                }
                if (!string.IsNullOrEmpty(page.Request["MaxPrice"]))
                {
                    if (fn.IsNumber(page.Request["MaxPrice"]))
                        options.MaxPrice = fn.Get<int>(page.Request["MaxPrice"]);
                }
                if (!string.IsNullOrEmpty(page.Request["MinBedrooms"]))
                {
                    if (fn.IsNumber(page.Request["MinBedrooms"]))
                    {
                        int? minbeds = fn.Get<int?>(page.Request["MinBedrooms"]);
                        if (minbeds == 0)
                            options.StudiosOnly = true;
                        options.MinBedroom = fn.Get<int>(page.Request["MinBedrooms"]);
                    }
                }
                if (!string.IsNullOrEmpty(page.Request["MinBaths"]))
                {
                    
                        options.MinBathroom = fn.Get<double>(page.Request["MinBaths"]);
                }
            }
            else
                options = new SearchOptions();

            return options;
        }

        public static string GetArrowClass(this SearchOptions options, SortOrder sortOrder)
        {
            string className = string.Empty;
            if (options.OrderBy.Equals(sortOrder))
                className = options.IsDescending ? " desc" : " asc";
            return className;
        }

        public static ListingInfo GetListing(this Page page)
        {
            ListingInfo obj = null;
            string id = page.Request.QueryString["listingid"] ?? 
                        page.Request.QueryString["lid"] ??
                        page.Request.QueryString["id"];
            if (!string.IsNullOrEmpty(id))
                obj = ServiceLocator.GetRepository().GetListingInfoByID(id);
            return obj;
        }

        public static BrokerInfo GetBroker(this Page page)
        {
            BrokerInfo obj = null;
            string id = page.Request.QueryString["agentid"] ??
                        page.Request.QueryString["aid"] ??
                        page.Request.QueryString["id"];
            if (!string.IsNullOrEmpty(id) && fn.IsNumber(id))
                obj = ServiceLocator.GetRepository().GetBrokerByID(fn.Get<int>(id));
            return obj;
        }
    }
}