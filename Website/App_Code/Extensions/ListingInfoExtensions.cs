﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BrokerTemplate.Core;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Globalization;
using System.IO;
using System.Configuration;

namespace BrokerTemplate.Web
{
    /// <summary>
    /// Summary description for ListingInfoExtensions
    /// </summary>
    public static class ListingInfoExtensions
    {

        public static string GenerateCurrencyConverter()
        {
            Dictionary<string, double> ratesTable = ExchangeRateCalulator.GetRateTable();
            string currency = string.Format("<br><div class='converter'></div>  <select class='currency'>  <option selected='selected' class='USD' value='{17}'>US Dollar</option>" +
                                                                "<option class='AUD' value='{0}'>Australia Dollar</option>" +
                                                                "<option class='BRL' value='{1}'>Brazil Real</option>" +
                                                                "<option class='CAD' value='{2}'>Canada Dollar</option>" +
                                                                "<option class='CHF' value='{3}'>Switzerland Franc</option>" +
                                                                "<option class='CNY' value='{4}'>China Yuan</option>" +
                                                                "<option class='DKK'  value='{5}'>Denmark Krone</option>" +
                                                                "<option class='EUR' value='{6}'>EU Euro</option>" +
                                                                "<option class='GBP' value='{7}'>UK Pound</option>" +
                                                                "<option class='HKD' value='{8}'>Hong Kong Dollar</option>" +
                                                                "<option class='INR' value='{9}'>India Rupee</option>" +
                                                                "<option class='JPY' value='{10}'>Japan Yen</option>" +
                                                                "<option class='MXN' value='{11}'>Mexico Peso</option>" +
                                                                "<option class='NOK' value='{12}'>Norway Krone</option>" +
                                                                "<option class='NZD' value='{13}'>New Zealand Dollar</option>" +
                                                                "<option class='SEK' value='{14}'>Sweden Krona</option>" +
                                                                "<option class='TWD' value='{15}'>Taiwan N.T. Dollar</option>" +
                                                                "<option class='ZAR'  value='{16}'>South Africa Rand</option>" +
                                                                "</select>"
                                                                , ratesTable["AUD"]
                                                                , ratesTable["BRL"]
                                                                , ratesTable["CAD"]
                                                                , ratesTable["CHF"]
                                                                , ratesTable["CNY"]
                                                                , ratesTable["DKK"]
                                                                , ratesTable["EUR"]
                                                                , ratesTable["GBP"]
                                                                , ratesTable["HKD"]
                                                                , ratesTable["INR"]
                                                                , ratesTable["JPY"]
                                                                , ratesTable["MXN"]
                                                                , ratesTable["NOK"]
                                                                , ratesTable["NZD"]
                                                                , ratesTable["SEK"]
                                                                , ratesTable["TWD"]
                                                                , ratesTable["ZAR"]
                                                                , ratesTable["USD"]
                                                                );
            return currency;
        }
        public static BuildingInfo GetBuilding(this ListingInfo l)
        {
            return ServiceLocator.GetRepository().GetBuildingInfoByID(l.BuildingID);
        }
        public static string GetListingBedroom(this ListingInfo l)
        {
            string number = string.Empty;

            number = l.Bedrooms.ToString();
            if (l.Bedrooms > 0)
                return number + " Bedroom";
            else
                return "Studio";
        }
        public static string GetListingBathroom(this ListingInfo l)
        {
            string number = string.Empty;

            number = l.Bathrooms.ToString();
            if (l.Bathrooms > 1)
                return number + " Baths";
            else
                return l.Bathrooms + " Bath";
        }
        public static string GetPriceRentLabel(this ListingInfo l)
        {
            string label = string.Empty;
            if (l.Price > 0)
                label = Resources.Details.PriceLabel;
            else if (l.Rent > 0)
                label = Resources.Details.RentLabel;
            else if (l.FurnishedRent > 0)
                label = Resources.Details.FurnishedRentLabel;

            return label;
        }

        public static string GetPriceRent(this ListingInfo l, bool isShortForm)
        {
            string text = string.Empty;
            if (l.Price > 0)
                text = string.Format("{0:c0}", l.Price);
            else if (l.Rent > 0)
            {
                text = string.Format("{0:c0}", l.Rent);
                if (l.FurnishedRent > 0)
                {
                    if (isShortForm)
                        text += "&nbsp;<br/>" + string.Format("{0:c0}", l.FurnishedRent);
                    else
                        text += "&nbsp;|" + string.Format("{0:c0}", l.FurnishedRent);
                }
            }
            else if (l.FurnishedRent > 0 && l.Rent == 0)
                text += string.Format("{0:c0}", l.FurnishedRent);
            return text;
        }
        public static string GetPriceRentText(this ListingInfo l, bool isShortForm)
        {
            string text = string.Empty;
            if (l.Price > 0)
                text = string.Format("{0:c0}", l.Price);
            else if (l.Rent > 0)
            {
                text = string.Format("(UNF) {0:c0}", l.Rent);
                if (l.FurnishedRent > 0)
                {
                    if (isShortForm)
                        text += "&nbsp;<br/>" + string.Format("(F) {0:c0}", l.FurnishedRent);
                    else
                        text += "&nbsp;|" + string.Format("(F) {0:c0}", l.FurnishedRent);
                }
            }
            else if (l.FurnishedRent > 0 && l.Rent == 0)
                text += string.Format("(F) {0:c0}", l.FurnishedRent);

            return text;
        }
        public static string GetStatus(this ListingInfo l)
        {
            string text = "<b>" + l.Status + "</b>";
            return text;
        }
        public static IList<KeyValuePair<string, string>> GetFinancialDetail(this ListingInfo l)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();

            if (l.Price > 0)
            {
                if (l.TimeShare)
                {
                    list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0}</span><span class='time-share-label'> (Time Share Price)</span>", Resources.Details.PriceLabel), string.Format(@"<span class='symbol'></span><span class=""price p"">{0:c0}</span>", l.Price) + " " + GenerateCurrencyConverter()));
                    list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label timeShareLabel"">Time Share Period</span>"), string.Format(@"<span class=""price timeSharePeriod"">{0} Weeks</span>", l.TimeSharePeriodInWeeks)));
                }
                else
                    list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0}</span>", Resources.Details.PriceLabel), string.Format(@"<span class='symbol'></span><span class=""price p"">{0:c0}</span>", l.Price) + " " + GenerateCurrencyConverter()));

                if (l.Maintenance > 0 && !l.IsBuildingForSale)
                    list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0}</span>", Resources.Details.MaintenanceLabel), string.Format(@"<span class='symbol'></span><span class=""price maintenance"">{0:c0}</span>", l.Maintenance)));
                else if (l.CommonCharges > 0)
                    list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0}</span>", Resources.Details.CommonChargesLabel), string.Format(@"<span class='symbol'></span><span class=""price commonCharge"">{0:c0}</span>", l.CommonCharges)));
                if (l.RETaxes > 0)
                {
                    if (l.IsBuildingForSale)
                        list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0}</span>", "RE Taxes [Per Annum]"), string.Format(@"<span class='symbol'></span><span class=""price RETax"">{0:c0}</span>", l.RETaxes)));
                    else
                        list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0}</span>", Resources.Details.ReTaxesLabel), string.Format(@"<span class='symbol'></span><span class=""price RETax"">{0:c0}</span>", l.RETaxes)));
                }

                if (l.FinancingAllowed > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.FinancingAllowedLabel, string.Format("{0}%", l.FinancingAllowed)));
                if (l.SquareFootage > 0)
                    list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0}</span>", Resources.Details.PricePerSFLabel), string.Format(@"<span class='symbol'></span><span class=""price PPSF"">{0:c0}</span>", l.Price / l.SquareFootage)));

            }
            else if (l.Rent > 0 || l.FurnishedRent > 0)
            {
                if (l.Rent > 0)
                    if (l.IsCommercialListing)
                        list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0}</span>", Resources.Details.RentLabel), string.Format(@"<span class='symbol'></span><span class=""price p"">{0:c0} per month</span>", l.Rent) + " " + GenerateCurrencyConverter()));
                    else
                    {
                        if (l.ExtDaily)
                            list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0} [Daily]</span>", Resources.Details.RentLabel), string.Format(@"<span class='symbol'></span><span class=""price p"">{0:c0}</span>", l.Rent) + " " + GenerateCurrencyConverter()));
                        if (l.ExtWeekly)
                            list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0} [Weekly]</span>", Resources.Details.RentLabel), string.Format(@"<span class='symbol'></span><span class=""price p"">{0:c0}</span>", l.Rent) + " " + GenerateCurrencyConverter()));
                        else
                            list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0}</span>", Resources.Details.RentLabel), string.Format(@"<span class='symbol'></span><span class=""price p"">{0:c0}</span>", l.Rent) + " " + GenerateCurrencyConverter()));

                        if (l.FurnishedRent > 0)
                            list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0}</span>", Resources.Details.FurnishedRentLabel), string.Format(@"<span class='symbol'></span><span class=""price Furnished"">{0:c0}</span>", l.FurnishedRent)));

                    }
                else if (l.FurnishedRent > 0)
                    if (l.ExtDaily)
                        list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0} [Daily]</span>", Resources.Details.FurnishedRentLabel), string.Format(@"<span class='symbol'></span><span class=""price Furnished"">{0:c0}</span>", l.FurnishedRent) + " " + GenerateCurrencyConverter()));
                    else if (l.ExtWeekly)
                        list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0} [Weekly]</span>", Resources.Details.FurnishedRentLabel), string.Format(@"<span class='symbol'></span><span class=""price Furnished"">{0:c0}</span>", l.FurnishedRent) + " " + GenerateCurrencyConverter()));
                    else
                        list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0}</span>", Resources.Details.FurnishedRentLabel), string.Format(@"<span class='symbol'></span><span class=""price Furnished"">{0:c0}</span>", l.FurnishedRent) + " " + GenerateCurrencyConverter()));

                if (l.RentalFeeType == 'N')
                    list.Add(new KeyValuePair<string, string>(string.Empty, @"<span style=""color:#dd5931;"">NO FEE</span>"));
                else if (l.RentalFeeType == 'L')
                    list.Add(new KeyValuePair<string, string>(string.Empty, @"<span style=""color:#dd5931;"">LOW FEE</span>"));
                if (l.MinTerm > 0 || l.MaxTerm > 0)
                {
                    if (l.MinTerm == l.MaxTerm)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.TermLabel, string.Format("{0:d} Months", l.MinTerm)));
                    else
                        list.Add(new KeyValuePair<string, string>(Resources.Details.TermLabel, string.Format("{0:d} - {1:d} Months", new object[] { l.MinTerm, l.MaxTerm })));
                }
            }
            return list;
        }

        public static IList<KeyValuePair<string, string>> GetApartmentDetail(this ListingInfo l)
        {
            //List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
            List<KeyValuePair<string, string>> list = l.GetFinancialDetail().ToList();

            if (!l.IsBuildingForSale)
            {
                list.Add(new KeyValuePair<string, string>(Resources.Details.ApartmentSizeLabel, string.Format("{0}", l.ApartmentSize)));
                list.Add(new KeyValuePair<string, string>(Resources.Details.RoomsLabel, string.Format("{0}", l.Rooms)));
                list.Add(new KeyValuePair<string, string>(Resources.Details.BedroomsLabel, string.Format("{0}", l.Bedrooms)));
                list.Add(new KeyValuePair<string, string>(Resources.Details.BathroomsLabel, string.Format("{0}", l.Bathrooms)));

                if (l.SquareFootage > 0)
                    list.Add(new KeyValuePair<string, string>(@"<span alt=""Approximate Square Feet"" title=""Approximate Square Feet"">ASF</span> | SM", string.Format("{0:#,#} SF | {1:#,#} SM", l.SquareFootage, l.SquareFootage * .09290304)));
                list.Add(new KeyValuePair<string, string>(Resources.Details.OwnershipLabel, l.Ownership));

                if (!string.IsNullOrEmpty(l.ServiceLevel))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.ServiceLevelLabel, l.ServiceLevel));
                if (!string.IsNullOrEmpty(l.PetPolicy))
                {
                    if (l.PetWeightLimit > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.PetPolicyLabel, l.PetPolicy + " [" + l.PetWeightLimit + "lb limit]"));
                    else
                        list.Add(new KeyValuePair<string, string>(Resources.Details.PetPolicyLabel, l.PetPolicy));
                }
            }
            else
            {
                //list.Add(new KeyValuePair<string, string>(Resources.Details.PriceLabel, string.Format("{0:c0}", l.Price)));

                list.Add(new KeyValuePair<string, string>(Resources.Details.OwnershipLabel, l.Ownership));

                if (l.Rooms > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.RoomsLabel, string.Format("{0}", l.Rooms)));
                if (l.Bedrooms > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.BedroomsLabel, string.Format("{0}", l.Bedrooms)));
                if (l.Bathrooms > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.BathroomsLabel, string.Format("{0}", l.Bathrooms)));

                list.Add(new KeyValuePair<string, string>(Resources.Details.BuildingTypeLabel, l.BuildingType));
                if (!string.IsNullOrWhiteSpace(l.Block) && !string.IsNullOrWhiteSpace(l.Lot))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.BlockLotLabel, string.Format("{0}/{1}", l.Block, l.Lot)));
                if (!string.IsNullOrWhiteSpace(l.Zoning))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.ZoningLabel, l.Zoning));
                if (l.SquareFootage > 0)
                    list.Add(new KeyValuePair<string, string>(@"<span alt=""Approximate Square Feet"" title=""Approximate Square Feet"">ASF</span> | SM", string.Format("{0:#,#} SF | {1:#,#} SM", l.SquareFootage, l.SquareFootage * .09290304)));
                if (!string.IsNullOrWhiteSpace(l.LotSize))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.LotSizeLabel, l.LotSize));
                if (!string.IsNullOrWhiteSpace(l.BuiltSize))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.BuiltSizeLabel, l.BuiltSize));
                if (l.FloorCount > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.FloorsLabel, l.FloorCount.ToString()));
                if (l.ResidentialUnits > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.ResidentialUnitsLabel, l.ResidentialUnits.ToString()));
                if (l.CommercialUnits > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.CommercialUnitsLabel, l.CommercialUnits.ToString()));
                if (l.ProfessionalUnits > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.ProfessionalUnitsLabel, l.ProfessionalUnits.ToString()));
            }
            //list.Add(new KeyValuePair<string, string>(Resources.Details.WebIDLabel, l.ListingID));

            return list;
        }

        #region Garfield

        public static IList<KeyValuePair<string, string>> GetPropertyDetail(this ListingInfo l)
        {
            if (l is TownhouseInfo)
                return l.GetTownhousePropertyDetail();
            if (l is CommercialInfo)
                return GetCommercialPropertyDetail(l as CommercialInfo);

            List<KeyValuePair<string, string>> list = l.GetFinancialDetail().ToList();

            if (!l.IsBuildingForSale)
            {
                if (l.Status != null)
                    list.Add(new KeyValuePair<string, string>("Status", string.Format("<b>{0}</b>", l.Status)));
                list.Add(new KeyValuePair<string, string>(Resources.Details.ApartmentSizeLabel, string.Format("{0}", l.ApartmentSize)));
                list.Add(new KeyValuePair<string, string>(Resources.Details.RoomsLabel, string.Format("{0}", l.Rooms)));
                list.Add(new KeyValuePair<string, string>(Resources.Details.BedroomsLabel, string.Format("{0}", l.Bedrooms)));
                list.Add(new KeyValuePair<string, string>(Resources.Details.BathroomsLabel, string.Format("{0}", l.Bathrooms)));
                if (l.SquareFootage > 0)
                    list.Add(new KeyValuePair<string, string>(@"<span alt=""Approximate Square Feet"" title=""Approximate Square Feet"">ASF</span> | SM", string.Format("{0:#,#} SF | {1:#,#} SM", l.SquareFootage, l.SquareFootage * .09290304)));
                if (!string.IsNullOrEmpty(l.Ownership))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.OwnershipLabel, l.Ownership));
                if (!string.IsNullOrEmpty(l.BuildingType))
                {
                    if (l.BldgAmenities.Prewar)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.BuildingTypeLabel, "Pre-war " + l.BuildingType));
                    else if (l.BldgAmenities.Postwar)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.BuildingTypeLabel, "Post-war " + l.BuildingType));
                    else
                        list.Add(new KeyValuePair<string, string>(Resources.Details.BuildingTypeLabel, l.BuildingType));
                }
                string sizeLable = Resources.Details.FloorsLabel + " | Apts";
                string size = l.FloorCount.ToString() + " | " + l.ApartmentCount;
                if (l.FloorCount > 0)
                    list.Add(new KeyValuePair<string, string>(sizeLable, size));
                if (!string.IsNullOrEmpty(l.ServiceLevel))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.ServiceLevelLabel, l.ServiceLevel));
                if (!string.IsNullOrEmpty(l.PetPolicy) || !string.IsNullOrEmpty(BuildPetPolicy(l)))
                {
                    if (!string.IsNullOrEmpty(BuildPetPolicy(l)))
                    {
                        if (l.PetWeightLimit > 0)
                            list.Add(new KeyValuePair<string, string>(Resources.Details.PetPolicyLabel, BuildPetPolicy(l) + " [" + l.PetWeightLimit + "lb limit]"));
                        else
                            list.Add(new KeyValuePair<string, string>(Resources.Details.PetPolicyLabel, BuildPetPolicy(l)));
                    }
                    else
                    {
                        if (l.PetWeightLimit > 0)
                            list.Add(new KeyValuePair<string, string>(Resources.Details.PetPolicyLabel, l.PetPolicy + " [" + l.PetWeightLimit + "lb limit]"));
                        else
                            list.Add(new KeyValuePair<string, string>(Resources.Details.PetPolicyLabel, l.PetPolicy));
                    }
                }
            }
            return list;
        }
        /*if (l.PetWeightLimit > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.PetPolicyLabel, l.PetPolicy + " [" + l.PetWeightLimit + "lb limit]"));
                    else
                        list.Add(new KeyValuePair<string, string>(Resources.Details.PetPolicyLabel, l.PetPolicy));*/
        public static IList<KeyValuePair<string, string>> GetCommercialPropertyDetail(this CommercialInfo l)
        {
            List<KeyValuePair<string, string>> list = l.GetFinancialDetail().ToList();
            if (l != null)
            {
                list.Add(new KeyValuePair<string, string>("Property Type", l.PropertyType));
                list.Add(new KeyValuePair<string, string>("Use", l.TypeOfUse));
                if (l.SquareFootage > 0)
                    list.Add(new KeyValuePair<string, string>(@"<span alt=""Approximate Square Feet"" title=""Approximate Square Feet"">ASF</span> | SM", string.Format("{0:#,#} SF | {1:#,#} SM", l.SquareFootage, l.SquareFootage * .09290304)));
                if (!string.IsNullOrEmpty(l.LeaseType))
                    list.Add(new KeyValuePair<string, string>("Lease Type", l.LeaseType));
                if (l.DateAvailable != null)
                {
                    if (l.DateAvailable.Value > DateTime.Today)
                        list.Add(new KeyValuePair<string, string>("Availability", string.Format("{0:MM/dd/yyyy}", l.DateAvailable)));
                    else
                        list.Add(new KeyValuePair<string, string>("Availability", "Immediate"));
                }
                if (!string.IsNullOrEmpty(l.Term))
                    list.Add(new KeyValuePair<string, string>("Term", l.Term));

            }
            return list;
        }

        public static IList<KeyValuePair<string, string>> GetTownhousePropertyDetail(this ListingInfo l)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();

            if (l != null && l.IsBuildingForSale)
            {
                list.Add(new KeyValuePair<string, string>(string.Format(@"<span class=""price-label"">{0}</span>", Resources.Details.PriceLabel), string.Format(@"<span class='symbol'></span><span class=""price p"">{0:c0}</span>", l.Price) + " " + GenerateCurrencyConverter()));
                if (l.CommonCharges > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.CommonChargesLabel, string.Format("{0:c0}", l.CommonCharges)));
                if (l.FinancingAllowed > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.FinancingAllowedLabel, string.Format("{0}%", l.FinancingAllowed)));

                list.Add(new KeyValuePair<string, string>(Resources.Details.OwnershipLabel, l.Ownership));

                if (l.Status != null)
                    list.Add(new KeyValuePair<string, string>("Status", string.Format("<b>{0}</b>", l.Status)));
                if (!string.IsNullOrWhiteSpace(l.Block) && !string.IsNullOrWhiteSpace(l.Lot))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.BlockLotLabel, string.Format("{0} / {1}", l.Block, l.Lot)));
                //if (!string.IsNullOrWhiteSpace(l.Zoning))
                //    list.Add(new KeyValuePair<string, string>(Resources.Details.ZoningLabel, l.Zoning));
                if (l.SquareFootage > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.ASFLabel, string.Format("{0:#,#}", l.SquareFootage)));
                if (!string.IsNullOrWhiteSpace(l.LotSize))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.LotSizeLabel, l.LotSize));
                if (!string.IsNullOrWhiteSpace(l.BuiltSize))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.BuiltSizeLabel, l.BuiltSize));
                if (l.FloorCount > 0 && l.ApartmentCount > 0)
                    list.Add(new KeyValuePair<string, string>("Floors | Apts ", string.Format("{0} | {1}", l.FloorCount, l.ApartmentCount)));
                else if (l.FloorCount > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.FloorsLabel, l.FloorCount.ToString()));
                else if (l.ApartmentCount > 0)
                    list.Add(new KeyValuePair<string, string>("Apts", l.FloorCount.ToString()));
                if (l.YearBuilt > 0)
                    list.Add(new KeyValuePair<string, string>("Year Built", l.YearBuilt.ToString()));
            }

            return list;
        }

        public static IList<KeyValuePair<string, string>> GetIncomeDetail(this ListingInfo l)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
            if (l != null && l is TownhouseInfo)
            {
                TownhouseInfo t = l as TownhouseInfo;
                if (t.ResidentialRent > 0)
                    list.Add(new KeyValuePair<string, string>("Residential", string.Format("{0:c0}", t.ResidentialRent)));
                if (t.RetailRent > 0)
                    list.Add(new KeyValuePair<string, string>("Retail", string.Format("{0:c0}", t.RetailRent)));
                if (t.CommercialRent > 0)
                    list.Add(new KeyValuePair<string, string>("Commercial", string.Format("{0:c0}", t.CommercialRent)));
                if (t.ResidentialRent > 0 || t.CommercialRent > 0)
                    list.Add(new KeyValuePair<string, string>("Gross Income", string.Format("{0:c0}", t.CommercialRent + t.ResidentialRent)));

            }
            return list;
        }

        public static IList<KeyValuePair<string, string>> GetExpenseDetail(this ListingInfo l)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
            if (l != null && l is TownhouseInfo)
            {
                TownhouseInfo t = l as TownhouseInfo;
                if (t.RETaxes > 0)
                    list.Add(new KeyValuePair<string, string>("RE Taxes", string.Format("{0:c0}", t.RETaxes)));
                if (t.Maintenance > 0)
                    list.Add(new KeyValuePair<string, string>("Maintenance", string.Format("{0:c0}", t.Maintenance)));
                if (t.WaterSewer > 0)
                    list.Add(new KeyValuePair<string, string>("Water / Sewer", string.Format("{0:c0}", t.WaterSewer)));
                if (t.Insurance > 0)
                    list.Add(new KeyValuePair<string, string>("Insurance", string.Format("{0:c0}", t.Insurance)));

                int partialTotal = t.RETaxes + t.Maintenance + t.WaterSewer + t.Insurance;
                int otherExpenses = t.AnnualExpenses - partialTotal;

                if (otherExpenses > 0)
                    list.Add(new KeyValuePair<string, string>("Other", string.Format("{0:c0}", otherExpenses)));
            }
            return list;
        }

        public static IList<KeyValuePair<string, string>> GetFinancialSummary(this ListingInfo l)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
            if (l != null && l is TownhouseInfo)
            {
                TownhouseInfo t = l as TownhouseInfo;
                int grossExpenses = t.RETaxes + t.Maintenance + t.VaultTax + t.Electric + t.Heat
                                    + t.Insurance + t.WaterSewer + t.Management + t.Superintendent + t.Miscellaneous;
                int grossRent = t.ResidentialRent + t.CommercialRent + t.RetailRent;
                int netIncome = grossRent - grossExpenses;

                if (netIncome > 0)
                {
                    if (t.AnnualExpenses > 0)
                        list.Add(new KeyValuePair<string, string>("Gross Expenses", string.Format("{0:c0}", grossExpenses)));
                    if (grossRent > 0)
                        list.Add(new KeyValuePair<string, string>("Gross Income", string.Format("{0:c0}", grossRent)));
                    if (netIncome > 0)
                        list.Add(new KeyValuePair<string, string>("Net Operating Income", string.Format("{0:c0}", netIncome)));
                }
            }
            return list;
        }

        public static IList<KeyValuePair<string, string>> GetFloorByFloorDescription(this ListingInfo l)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
            if (l != null && l is TownhouseInfo)
            {
                TownhouseInfo t = l as TownhouseInfo;
                if (t.FloorDescriptions != null && t.FloorDescriptions.Count > 0)
                {
                    foreach (var fl in t.FloorDescriptions)
                    {
                        string desc = string.Empty;
                        if (!string.IsNullOrEmpty(fl.Dimension))
                            desc = string.Format(@"<span class=""dimension"">Size: {0}</span>", fl.Dimension);
                        if (!string.IsNullOrEmpty(fl.Description))
                            desc += string.Format(@"<p class=""floor-desc"">{0}</p>", fl.Description);
                        list.Add(new KeyValuePair<string, string>(fl.Floor, desc));
                    }
                }
            }
            return list;
        }

        private static string removeJScript(string ori)
        {
            string result = Regex.Replace(ori, "<script.*?</script>", "");
            return result;
        }
        public static string GetDescriptionForDefalutPage(this ListingInfo l)
        {
            string str, temp, lid;
            int index;
            str = l.ListingID;
            lid = l.ListingID;
            var listing = ServiceLocator.GetRepository().GetListingInfoByID(str);
            str = listing.Description;
            temp = removeJScript(str);
            if (str.Length > 75)
            {
                temp = temp.Substring(0, 74);
                index = temp.LastIndexOf(" ");
                str = temp.Substring(0, index) + "...<a href='detail.aspx?id=" + lid + "' style='color:#747474;'>[MORE]</a> ";
            }
            return str;
        }
        public static IList<string> GetApartmentFeatures(this ListingInfo l)
        {
            List<string> listori = new List<string>();
            List<string> list = new List<string>();
            if (l != null && !l.IsBuildingForSale)
            {
                listori = l.AptFeatures.ToList();
                foreach (var li in listori)
                {
                    if (li.Contains("Eat In"))
                        list.Add(li.Replace("Eat In", "Eat-In"));
                    else
                        list.Add(li);
                }
            }
            //if (l.WBFP > 0)
            //    list.Add("WBFP [" + l.WBFP+"]");
            //if (l.GasFP > 0)
            //    list.Add("GasFP [" + l.GasFP + "]");
            //if (l.DecoFP > 0)
            //    list.Add("DecoFP [" + l.DecoFP + "]");
            //if (l.PowderRmCount > 0)
            //list.Add("PowderRmCount [" + l.PowderRmCount + "]");
            return list;
        }
        public static IList<string> GetBuildingFeatures(this ListingInfo l)
        {
            List<string> list = new List<string>();

            if (l != null && l.IsBuildingForSale)
            {
                list = l.AptFeatures.ToList();
                list.AddRange(l.BldgAmenities.ToList());
                list.RemoveDuplicate();
            }
            else if (l != null)
            {
                list = l.BldgAmenities.ToList();
            }
            if (list.Contains("Elevator"))
            {
                list.Remove("Elevator");
                list.Insert(0, "Elevator");
            }
            List<string> aptFeature = l.AptFeatures.ToList();

            if (list.Contains("Dog Only"))
                list.Remove("Dog Only");
            if (list.Contains("Cat Only"))
                list.Remove("Cat Only");
            if (list.Contains("Digital TV"))
                list.Remove("Digital TV");
            if (list.Contains("No Dogs"))
                list.Remove("No Dogs");
            if (list.Contains("Pets Allowed"))
                list.Remove("Pets Allowed");
            if (list.Contains("No Cats"))
                list.Remove("No Cats");
            if (list.Contains("No Pets"))
                list.Remove("No Pets");
            if (list.Contains("Prewar"))
                list.Remove("Prewar");
            if (list.Contains("Postwar"))
                list.Remove("Postwar");


            return list;
        }

        public static IList<T> RemoveDuplicate<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                if (list.Count(x => x.Equals(list[n])) > 1)
                    list.RemoveAt(n);
            }
            return list;
        }

        #endregion

        #region PRINT

        public static IList<KeyValuePair<string, string>> GetFinancialDetailForPrint(this ListingInfo l, double rate, string currency)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
            string c = string.Empty;
            switch (currency)
            {
                case "USD": c = "USD"; break;
                case "AUD": c = "en-AU"; break;
                case "CAD": c = "en-CA"; break;
                case "BRL": c = "pt-BR"; break;
                case "CNY": c = "zh-CHS"; break;
                case "DKK": c = "da-DK"; break;
                case "EUR": c = "fr-FR"; break;
                case "HKD": c = "zh-HK"; break;
                case "INR": c = "gu-IN"; break;
                case "JPY": c = "ja-JP"; break;
                case "MXN": c = "es-MX"; break;
                case "NOK": c = "nb-NO"; break;
                case "NZD": c = "en-NZ"; break;
                case "ZAR": c = "af-ZA"; break;
                case "CHF": c = "fr-CH"; break;
                case "SEK": c = "sv-SE"; break;
                case "TWD": c = "zh-TW"; break;
                case "GBP": c = "en-GB"; break;
            }
            if (c == "USD")
            {
                if (l.Price > 0)
                {
                    list.Add(new KeyValuePair<string, string>(Resources.Details.PriceLabel, string.Format("{0:c0}", l.Price)));

                    if (l.Maintenance > 0 && !l.IsBuildingForSale)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.MaintenanceLabel, string.Format("{0:c0}", l.Maintenance)));
                    else if (l.CommonCharges > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.CommonChargesLabel, string.Format("{0:c0}", l.CommonCharges)));
                    if (l.RETaxes > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.ReTaxesLabel, string.Format("{0:c0}", l.RETaxes)));

                    if (l.FinancingAllowed > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.FinancingAllowedLabel, string.Format("{0}%", l.FinancingAllowed)));

                    if (l.SquareFootage > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.PricePerSFLabel, string.Format("{0:c0}", l.Price / l.SquareFootage)));

                }
                else if (l.Rent > 0 || l.FurnishedRent > 0)
                {
                    if (l.Rent > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.RentLabel, string.Format("{0:c0}", l.Rent)));
                    if (l.FurnishedRent > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.FurnishedRentLabel, string.Format("{0:c0}", l.FurnishedRent)));
                    if (l.IsNoFee)
                        list.Add(new KeyValuePair<string, string>("&nbsp;&nbsp;&nbsp;", @"<span style='color:#dd5931;'>NO FEE</span>"));
                    if (l.MinTerm > 0 || l.MaxTerm > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.TermLabel, string.Format("{0:d} - {1:d} Months", new object[] { l.MinTerm, l.MaxTerm })));

                }
            }
            else
            {
                CultureInfo ci = CultureInfo.CreateSpecificCulture(c);
                if (l.Price > 0)
                {
                    list.Add(new KeyValuePair<string, string>(Resources.Details.PriceLabel, string.Format("{1}&nbsp;&nbsp;[{0:c0}" + "US]", l.Price, (l.Price * rate).ToString("C", ci))));

                    if (l.Maintenance > 0 && !l.IsBuildingForSale)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.MaintenanceLabel, string.Format("{1}&nbsp;&nbsp;[{0:c0}US]", l.Maintenance, (l.Maintenance * rate).ToString("C", ci))));
                    else if (l.CommonCharges > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.CommonChargesLabel, string.Format("{1}&nbsp;&nbsp;[{0:c0}US]", l.CommonCharges, (l.CommonCharges * rate).ToString("C", ci))));
                    if (l.RETaxes > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.ReTaxesLabel, string.Format("{1}&nbsp;&nbsp;[{0:c0}US]", l.RETaxes, (l.RETaxes * rate).ToString("C", ci))));

                    if (l.FinancingAllowed > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.FinancingAllowedLabel, string.Format("{0}%", l.FinancingAllowed, (l.FinancingAllowed * rate).ToString("C", ci))));

                    if (l.SquareFootage > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.PricePerSFLabel, string.Format("{1}&nbsp;&nbsp;[{0:c0}US]", l.Price / l.SquareFootage, (l.Price / l.SquareFootage * rate).ToString("C", ci))));

                }
                else if (l.Rent > 0 || l.FurnishedRent > 0)
                {
                    if (l.Rent > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.RentLabel, string.Format("{1}&nbsp;&nbsp;[{0:c0}US]", l.Rent, (l.Rent * rate).ToString("C", ci))));
                    if (l.FurnishedRent > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.FurnishedRentLabel, string.Format("{1}&nbsp;&nbsp;[{0:c0}US]", l.FurnishedRent, (l.FurnishedRent * rate).ToString("C", ci))));
                    if (l.IsNoFee)
                        list.Add(new KeyValuePair<string, string>("&nbsp;&nbsp;&nbsp;", @"<span style='color:#dd5931;'>NO FEE</span>"));
                    if (l.MinTerm > 0 || l.MaxTerm > 0)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.TermLabel, string.Format("{0:d} - {1:d} Months", new object[] { l.MinTerm, l.MaxTerm })));

                }
            }

            return list;
        }

        public static IList<KeyValuePair<string, string>> GetListingDetailForPrint(this ListingInfo l)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();

            if (!l.IsBuildingForSale && !l.IsCommercialListing)
            {
                list.Add(new KeyValuePair<string, string>(Resources.Details.ApartmentSizeLabel, string.Format("{0}", l.ApartmentSize)));
                if (l.SquareFootage > 0)
                    list.Add(new KeyValuePair<string, string>("SF | SM (Approximate)", string.Format("{0:#,#} | ", l.SquareFootage) + string.Format("{0:#,#}", l.SquareFootage * .09290304)));
                list.Add(new KeyValuePair<string, string>(Resources.Details.RoomsLabel, string.Format("{0}", l.Rooms)));
                list.Add(new KeyValuePair<string, string>(Resources.Details.BedroomsLabel, string.Format("{0}", l.Bedrooms)));
                list.Add(new KeyValuePair<string, string>(Resources.Details.BathroomsLabel, string.Format("{0}", l.Bathrooms)));
            }
            else
            {
                if (l.IsBuildingForSale)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.OwnershipLabel, l.Ownership));
                else if (l.IsCommercialListing)
                {
                    CommercialInfo t = l as CommercialInfo;
                    if (!string.IsNullOrEmpty(t.LeaseType))
                        list.Add(new KeyValuePair<string, string>("Lease Type", t.LeaseType));
                    if (t.DateAvailable != null)
                    {
                        if (t.DateAvailable.Value > DateTime.Today)
                            list.Add(new KeyValuePair<string, string>("Availability", string.Format("{0:MM/dd/yyyy}", t.DateAvailable)));
                        else
                            list.Add(new KeyValuePair<string, string>("Availability", "Immediate"));
                    }
                    if (!string.IsNullOrEmpty(t.Term))
                        list.Add(new KeyValuePair<string, string>("Term", t.Term));
                }

                if (l.Rooms > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.RoomsLabel, string.Format("{0}", l.Rooms)));
                if (l.Bedrooms > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.BedroomsLabel, string.Format("{0}", l.Bedrooms)));
                if (l.Bathrooms > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.BathroomsLabel, string.Format("{0}", l.Bathrooms)));

                if (!string.IsNullOrWhiteSpace(l.Block) && !string.IsNullOrWhiteSpace(l.Lot))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.BlockLotLabel, string.Format("{0}/{1}", l.Block, l.Lot)));
                if (!string.IsNullOrWhiteSpace(l.Zoning))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.ZoningLabel, l.Zoning));
                if (l.SquareFootage > 0)
                    list.Add(new KeyValuePair<string, string>(@"<span alt=""Approximate Square Feet"" title=""Approximate Square Feet"">ASF</span> | SM", string.Format("{0:#,#} SF | {1:#,#} SM", l.SquareFootage, l.SquareFootage * .09290304)));
                if (!string.IsNullOrWhiteSpace(l.LotSize))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.LotSizeLabel, l.LotSize));
                if (!string.IsNullOrWhiteSpace(l.BuiltSize))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.BuiltSizeLabel, l.BuiltSize));
                if (l.FloorCount > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.FloorsLabel, l.FloorCount.ToString()));
                if (l.ResidentialUnits > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.ResidentialUnitsLabel, l.ResidentialUnits.ToString()));
                if (l.CommercialUnits > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.CommercialUnitsLabel, l.CommercialUnits.ToString()));
                if (l.ProfessionalUnits > 0)
                    list.Add(new KeyValuePair<string, string>(Resources.Details.ProfessionalUnitsLabel, l.ProfessionalUnits.ToString()));
            }
            return list;
        }
        public static string BuildPetPolicy(this ListingInfo l)
        {
            string policy = string.Empty;
            if (l.BldgAmenities != null)
            {
                if (l.BldgAmenities.PetsAllowed || l.BldgAmenities.PetsCasebyCase)
                    policy = "Pets Allowed";
                else if (l.BldgAmenities.NoCats)
                    policy = "No Cats";
                else if (l.BldgAmenities.NoDogs)
                    policy = "No Dogs";
            }
            return policy;
        }
        public static IList<KeyValuePair<string, string>> GetBuildingDetailForPrint(this ListingInfo l)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
            if (!l.IsBuildingForSale && !l.IsCommercialListing)
            {
                list.Add(new KeyValuePair<string, string>(Resources.Details.OwnershipLabel, l.Ownership));
                if (!string.IsNullOrEmpty(l.ServiceLevel))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.ServiceLevelLabel, l.ServiceLevel));
                if (!string.IsNullOrEmpty(l.BuildingType))
                    if (l.BldgAmenities.Prewar)
                        list.Add(new KeyValuePair<string, string>(Resources.Details.BuildingTypeLabel, "Pre-War " + l.BuildingType));
                    else
                        list.Add(new KeyValuePair<string, string>(Resources.Details.BuildingTypeLabel, "Post-War " + l.BuildingType));
                if (!string.IsNullOrEmpty(l.PetPolicy))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.PetPolicyLabel, l.PetPolicy));

                string sizeLable = Resources.Details.FloorsLabel + " | APTS";
                string size = l.FloorCount.ToString() + " | " + l.ApartmentCount;

                if (!string.IsNullOrWhiteSpace(l.BuiltSize))
                    list.Add(new KeyValuePair<string, string>(Resources.Details.BuiltSizeLabel, l.BuiltSize));
                if (l.FloorCount > 0)
                    list.Add(new KeyValuePair<string, string>(sizeLable, size));
            }
            else if (l.IsBuildingForSale)
            {
                list.Add(new KeyValuePair<string, string>(Resources.Details.BuildingTypeLabel, l.BuildingType));
            }
            else if (l.IsCommercialListing)
            {
                CommercialInfo t = l as CommercialInfo;
                list.Add(new KeyValuePair<string, string>(Resources.Details.BuildingTypeLabel, l.BuildingType));
                list.Add(new KeyValuePair<string, string>("Property Type", t.PropertyType));
                list.Add(new KeyValuePair<string, string>("Use Type", t.TypeOfUse));
            }
            return list;
        }


        #endregion

        public static string GetDisplayAddress(this ListingInfo l)
        {
            if (l.ShowAddress)
            {
                return l.Address;
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(l.DisplayAddress))
                    return l.DisplayAddress;
                else
                {
                    if (!string.IsNullOrWhiteSpace(l.CrossStreet1))
                        return string.Format("{0} & {1}", l.CrossStreet1, l.CrossStreet2);
                    else if (!string.IsNullOrWhiteSpace(l.Neighborhood))
                        return l.Neighborhood;
                }
                return string.Empty;
            }
        }
        public static string GetNeiborhoodForShortForm(this ListingInfo l)
        {
            string address = GetDisplayAddress(l);
            if (address == l.Neighborhood)
                return string.Empty;
            else
                return l.Neighborhood;
        }

        private static string RemoveDigits(string str)
        {
            return Regex.Replace(str, @"\d", "");
        }
        public static string GetShowAddress(this ListingInfo l, out bool withStreetAddress)
        {
            if (l.ShowAddress)
            {
                withStreetAddress = false;
                return l.Address;
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(l.DisplayAddress))
                {
                    withStreetAddress = false;
                    return l.DisplayAddress;
                }

                else
                {
                    string streetAddress = RemoveDigits(l.Address);
                    if (!string.IsNullOrWhiteSpace(l.CrossStreet1))
                    {
                        withStreetAddress = true;
                        return string.Format("{0} & {1}", l.CrossStreet1, l.CrossStreet2);
                    }

                    else
                        if (!string.IsNullOrWhiteSpace(l.CrossStreet1) && !string.IsNullOrWhiteSpace(streetAddress))
                        {
                            withStreetAddress = true;
                            return string.Format("{0} & {1}", streetAddress, l.CrossStreet1);
                        }

                }
                withStreetAddress = false;
                return string.Empty;
            }
        }
        /*************************************    new over  *************************************************/

        public static string GetShowAddress(this ListingInfo l)
        {
            StringBuilder sb = new StringBuilder();

            bool withStreetAddress = false;
            string displayAddress = l.GetShowAddress(out withStreetAddress);
            string neighborhood = l.GetArea();
            if (!displayAddress.Equals(neighborhood, StringComparison.InvariantCultureIgnoreCase))
                sb.AppendFormat("<h2>{0} | {1}<h2>", displayAddress, neighborhood);
            else
                sb.AppendFormat("<h2>{0}</h2>", displayAddress);

            if (!withStreetAddress)
                sb.AppendFormat(@"<div class=""cross-streets"">{0}</div>", l.GetCrossStreets("[{0} & {1}]"));

            return sb.ToString();
        }
        /*************************************    new over  *************************************************/

        public static string GetArea(this ListingInfo l)
        {
            string neighborhood = l.Neighborhood;
            if (!string.IsNullOrEmpty(neighborhood) && (string.Compare(neighborhood, "stuyvesant town", true) == 0 || string.Compare(neighborhood, "peter cooper village", true) == 0))
                neighborhood = "Gramercy Park";
            if (!string.IsNullOrEmpty(neighborhood))
                return neighborhood;
            else
            {
                var b = l.GetBuilding();
                if (b != null)
                {
                    return string.Format("{0}, {1}", b.City, b.State);
                }
            }
            return string.Empty;
        }


        public static string GetCrossStreets(this ListingInfo l, string format)
        {
            if (l.ShowAddress)
                if (!string.IsNullOrEmpty(l.CrossStreet1) && !string.IsNullOrEmpty(l.CrossStreet2))
                {
                    if (string.IsNullOrEmpty(format))
                        format = "{0} and {1}";

                    return string.Format(format, l.CrossStreet1, l.CrossStreet2);
                }
            return string.Empty;
        }

        public static string GetDetailLink(this ListingInfo l)
        {
            if (l != null)
                return string.Format("detail.aspx?id={0}", l.ListingID);
            return string.Empty;
        }

        public static string GetFullDetailLink(this ListingInfo l)
        {
            return string.Format("http://{0}/detail.aspx?id={1}", HttpContext.Current.Request.Url.Host, l.ListingID);
        }

        public static string GetPrintLink(this ListingInfo l)
        {
            // return string.Format("print.aspx?id={0}", l.ListingID);
            if (l.HasPhoto() || l.HasFloorplan())
                return string.Format("PrintPlan.aspx?id={0}", l.ListingID);
            else
                return string.Format("print.aspx?id={0}", l.ListingID);
        }

        public static string GetMapLink(this ListingInfo l)
        {
            return string.Format("map.aspx?id={0}", l.ListingID);
        }

        public static string GetSimilarListingsLink(this ListingInfo l)
        {
            return string.Format("search.aspx?type=similar&lid={0}", l.ListingID);
        }

        public static string GetEmailFriendLink(this ListingInfo l)
        {
            return string.Format("email_to_a_friend.aspx?id={0}", l.ListingID);
        }

        public static string GetMortgageCalculatorLink(this ListingInfo l)
        {
            return string.Format("mortgage_calculator.aspx?id={0}", l.ListingID);
        }

        public static string GetPDFLink(this ListingInfo l)
        {
            string url = "#";
            var m = l.GetMedia().OtherFiles.FirstOrDefault(x =>
                x.MediaUrl.EndsWith("pdf", StringComparison.InvariantCultureIgnoreCase));
            if (m != null)
                url = m.MediaUrl;
            return url;
        }

        public static bool ShowMortgageCalculator(this ListingInfo l)
        {
            return l.Price > 0;
        }

        public static string GetAllPhotoLink(this ListingInfo l)
        {
            return string.Format("photo2.aspx?id={0}", l.ListingID);
        }

        public static string GetFloorplanLink(this ListingInfo l)
        {
            return string.Format("floorplan.aspx?id={0}", l.ListingID);
        }

        public static string GetVideoLink(this ListingInfo l)
        {
            string url = string.Empty;

            var videos = l.GetMedia().Videos;
            if (videos.Count > 0)
            {
                if (videos.Any(x => x.IsEmbeddedVideo == true))
                    url = string.Format("video.aspx?id={0}", l.ListingID);
                else
                    url = videos[0].MediaUrl;
            }

            if (!string.IsNullOrEmpty(url) && url.Contains("http://https://"))
            {
                url = url.Replace("http://https://", "https://");
            }

            return url;
        }

        public static string GetOLRMaxScript(this ListingInfo l)
        {
            string script = string.Empty;
            if (l.OLRMaxAlbumID > 0)
            {
                TagBuilder t = new TagBuilder("script");
                t.Attributes.Add("src", string.Format("http://www.olrdigital.com/olrmax/{0}-{1}-netmore-29-34", l.ListingID, l.OLRMaxAlbumID));
                t.Attributes.Add("language", "javascript");
                t.Attributes.Add("type", "text/javascript");
                script = t.ToString(TagRenderMode.Normal);
            }
            return script;
        }

        public static bool HasPhoto(this ListingInfo l)
        {
            return l.GetMedia().Photos.Count > 0;
        }

        public static bool HasFloorplan(this ListingInfo l)
        {
            return l.GetMedia().Floorplans.Count > 0;
        }

        public static bool HasVirtualTour(this ListingInfo l)
        {
            return l.GetMedia().VirtualTours.Count > 0;
        }

        public static bool HasVideo(this ListingInfo l)
        {
            return l.GetMedia().Videos.Count > 0;
        }

        public static bool HasPDF(this ListingInfo l)
        {
            return l.GetMedia().OtherFiles.Any(x =>
                x.MediaUrl.EndsWith("pdf", StringComparison.InvariantCultureIgnoreCase));
        }

        public static bool HasOLRMax(this ListingInfo l)
        {
            return l.OLRMaxAlbumID > 0;
        }

        #region Media

        public static string GetDisplayPhoto(this ListingInfo l)
        {
            if (l != null)
                return l.GetDisplayPhoto(Resources.Media.NoListingPhoto);
            else
                return string.Format(Resources.Media.NoListingPhoto);
        }

        public static string GetDisplayPhoto(this ListingInfo l, string defaultPhoto)
        {
            string url = defaultPhoto;
            var media = ServiceLocator.GetRepository().GetListingMedia(l.ListingID);
            if (media.Photos.Count > 0)
                url = media.Photos[0].MediaUrl;
            else if (l.ShowBuildingPhoto && !string.IsNullOrEmpty(l.BuildingPhotoUrl))
                url = l.BuildingPhotoUrl;
            return url;
        }
        public static string DisplayBuildingName(this ListingInfo l)
        {
            if (l.BuildingID != 0)
            {
                BuildingInfo b = ServiceLocator.GetRepository().GetBuildingInfoByID(l.BuildingID);
                if (!string.IsNullOrEmpty(b.BuildingName) || !b.HideAddress)
                    return b.BuildingName;
                else
                    return string.Empty;
            }
            else
                return string.Empty;
        }
        public static Media GetMedia(this ListingInfo l)
        {
            return ServiceLocator.GetRepository()
                                 .GetListingMedia(l.ListingID);
        }

        #endregion

        #region Contacts

        public static Brokers GetBrokers(this ListingInfo l)
        {
            Brokers brokers = new Brokers();
            IRepository db = ServiceLocator.GetRepository();
            if (l.ListingAgentID > 0)
            {
                var la = db.GetBrokerByID(l.ListingAgentID);
                if (la != null && la.CompanyID > 0)
                    brokers.Add(la);
            }
            if (l.CoExAgentID > 0 && l.ListingAgentID != l.CoExAgentID)
            {
                var co = db.GetBrokerByID(l.CoExAgentID);
                if (co != null && co.CompanyID > 0)
                    brokers.Add(co);
            }
            if (l.ThirdAgentID > 0 && l.ListingAgentID != l.ThirdAgentID && l.CoExAgentID != l.ThirdAgentID)
            {
                var third = db.GetBrokerByID(l.ThirdAgentID);
                if (third != null && third.CompanyID > 0)
                    brokers.Add(third);
            }
            return brokers;
        }

        #endregion

        public static OpenHouses GetOpenHouses(this ListingInfo l)
        {
            return ServiceLocator.GetRepository().GetOpenHouses(l.ListingID);
        }

        public static string GetOpenHouseString(this ListingInfo l)
        {
            string result = string.Empty;
            var openhouses = l.GetOpenHouses();
            if (openhouses.Count > 0)
            {
                int count = 0;

                foreach (var o in openhouses)
                {
                    if (o.ByAppointment)
                        result += string.Format(" {0},  {1}-{2} &nbsp;&nbsp;By Appointment Only&nbsp;| ", SiteHelper.FormatDate((DateTime)o.OpenHouseDate), o.StartTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim(), o.EndTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim());
                    else
                        result += string.Format(" {0},  {1}-{2} | ", SiteHelper.FormatDate((DateTime)o.OpenHouseDate), o.StartTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim(), o.EndTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim());
                    count++;
                    if (count >= 2)
                        break;
                }
                if (!string.IsNullOrWhiteSpace(result))
                {
                    result = result.Remove(result.LastIndexOf("|"));
                    if (count > 1)
                        result = string.Format(@"<span class=""openhouse"">Open Houses:&nbsp;&nbsp;{0}</span>", result);
                    else
                        result = string.Format(@"<span class=""openhouse"">Open House:&nbsp;&nbsp;{0}</span>", result);
                }
            }
            return result;
        }
        public static string GetOpenHouseString(this ListingInfo l, bool isDefault)
        {
            string result = string.Empty;
            var openhouses = l.GetOpenHouses();
            if (openhouses.Count > 0)
            {
                int count = 0;
                if (isDefault)
                    foreach (var o in openhouses)
                    {
                        result += string.Format(" {0},  {1}-{2} <br/>", SiteHelper.FormatDate((DateTime)o.OpenHouseDate), o.StartTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim(), o.EndTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim());
                        count++;
                        if (count >= 2)
                            break;
                    }
                else
                    foreach (var o in openhouses)
                    {
                        result += string.Format(" {0},  {1}-{2} | ", SiteHelper.FormatDate((DateTime)o.OpenHouseDate), o.StartTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim(), o.EndTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim());
                        count++;
                        if (count >= 2)
                            break;
                    }
                if (!string.IsNullOrWhiteSpace(result))
                {
                    result = result.Remove(result.LastIndexOf("|"));
                    if (count > 1)
                        result = string.Format(@"<span class=""openhouse"">Open Houses:&nbsp;&nbsp;{0}</span>", result);
                    else
                        result = string.Format(@"<span class=""openhouse"">Open House:&nbsp;&nbsp;{0}</span>", result);
                }
            }
            return result;
        }
        public static SearchCriteria GetSimilarListingCriteria(this ListingInfo l)
        {
            SearchCriteria sc = new SearchCriteria();
            int priceDiff = Convert.ToInt32(((double)l.Price * .20));
            sc.SearchType = "sale";
            if (l.Rent > 0 || l.FurnishedRent > 0)
            {
                sc.SearchType = "rental";
                if (l.Rent > 0)
                    priceDiff = Convert.ToInt32(((double)l.Rent * .20));
                else
                    priceDiff = Convert.ToInt32(((double)l.FurnishedRent * .20));
            }
            sc.SearchParam.Add(new SearchStructs { SearchProperty = "PriceMinimum", SearchValue = string.Format("{0}", l.Price - priceDiff) });
            sc.SearchParam.Add(new SearchStructs { SearchProperty = "PriceMaximum", SearchValue = string.Format("{0}", l.Price + priceDiff) });
            sc.SearchParam.Add(new SearchStructs { SearchProperty = "Beds", SearchValue = string.Format("{0}", l.Bedrooms) });
            return sc;
        }

        public static string BuildOpenHouses(this ListingInfo l)
        {
            string result = string.Empty;
            var openhouses = l.GetOpenHouses();
            if (openhouses.Count > 0)
            {
                int count = 0;

                foreach (var o in openhouses)
                {
                    result += string.Format(" {0},  {1}-{2} | ", SiteHelper.FormatDate(o.OpenHouseDate), o.StartTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim(), o.EndTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim());
                    count++;
                    if (count >= 2)
                        break;
                }
                if (!string.IsNullOrWhiteSpace(result))
                {
                    result = result.Remove(result.LastIndexOf("|"));
                    result = @"Open House<br />" + result;
                }
            }
            return result;
        }
        public static string BuildOpenHouses(this ListingInfo l, bool isDefaultPage)
        {
            string result = string.Empty;
            var openhouses = l.GetOpenHouses();
            if (openhouses.Count > 0)
            {
                int count = 0;

                foreach (var o in openhouses)
                {
                    if (isDefaultPage)
                        result += string.Format(" {0},  {1}-{2}<br/> ", SiteHelper.FormatDate(o.OpenHouseDate), o.StartTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim(), o.EndTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim());
                    else
                        result += string.Format(" {0},  {1}-{2} |", SiteHelper.FormatDate(o.OpenHouseDate), o.StartTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim(), o.EndTime.Replace("PM", string.Empty).Replace("AM", string.Empty).Trim());
                    count++;
                    if (count >= 2)
                        break;
                }
                if (!string.IsNullOrWhiteSpace(result))
                {
                    if (!isDefaultPage)
                        result = result.Remove(result.LastIndexOf("|"));
                    else
                        result = result.Remove(result.LastIndexOf("<br/>"), 5);
                    if (!isDefaultPage)
                        if (openhouses.Count == 1)
                            result = @"Open House<br />" + result;
                        else
                            result = @"Open Houses<br />" + result;
                }
            }
            return result;
        }
        public static string BuildIsNoFee(this ListingInfo l)
        {
            string result = string.Empty;
            if (l.IsNoFee)
                result = "<span style='color:#dd5931;'>NO FEE</span>";
            return result;
        }

        public static string GetDescriptionForPrintPage(this ListingInfo l)
        {
            string des = Regex.Replace(l.Description, "<script.*?</script>", "", RegexOptions.Singleline | RegexOptions.IgnoreCase); ;
            return des;
        }
    }
}