﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using BrokerTemplate.Core;
using System.Text;

namespace BrokerTemplate.Web
{
    /// <summary>
    /// Summary description for BrokerInfoExtensions
    /// </summary>
    public static class BrokerInfoExtensions
    {
        public static string GetVCardLink(this BrokerInfo b)
        {
            return string.Format("vcard.aspx?id=" + b.ID);
        }

        public static string GetDetailLink(this BrokerInfo b)
        {
            return string.Format("agent_detail.aspx?agentid=" + b.ID);
        }

        public static string GetEmailMeLink(this BrokerInfo b)
        {
            return string.Format("email_agent.aspx?id=" + b.ID);
        }

        public static string GetEmailMeLink(this BrokerInfo b, ListingInfo l)
        {
            return string.Format("email_agent.aspx?id={0}&listingid={1}", b.ID, l.ListingID);
        }

        public static string GetScheduleViewingLink(this BrokerInfo b, ListingInfo l)
        {
            return string.Format("scheduleviewing.aspx?lid={0}&agentid={1}", l.ListingID, b.ID);
        }

        public static Listings GetCommercialListings(this BrokerInfo b, SearchOptions options, out int total)
        {
            Listings list = new Listings();
            options.AgentID = b.ID;
            list = ServiceLocator.GetComercialSearchService().RunSearch(options, out total);
            //if (total == 0)
            //{
            //    if (options.PropertyTypes.Contains(PropertyType.Sale))
            //    {
            //        options.PropertyTypes.Remove(PropertyType.Sale);
            //        list = ServiceLocator.GetComercialSearchService().RunSearch(options, out total);
            //    }
            //    else
            //    {
            //        options.PropertyTypes.Add(PropertyType.Sale);
            //        list = ServiceLocator.GetComercialSearchService().RunSearch(options, out total);
            //    }
            //}

            return list;
        }

        public static Listings GetListings(this BrokerInfo b, SearchOptions options, out int total)
        {
            Listings list = new Listings();
            options.AgentID = b.ID;
            if (options.PropertyTypes.Contains(PropertyType.Commercial))
                list = GetCommercialListings(b, options, out total);
            else
                list = ServiceLocator.GetSearchService().RunSearch(options, out total);
            return list;
        }
        public static string GetTitle(this BrokerInfo b)
        {
            string str;
            if (!string.IsNullOrEmpty(b.Title))
                str = b.Title.Replace(",", "<br/>");
            else
                str = "<br/>";
            return str;
        }
        public static CompanyNewsList GetPress(this BrokerInfo b)
        {
            PagingOptions options = new PagingOptions();
            options.PageSize = 10;
            int total = 0;
            var list = ServiceLocator.GetRepository().GetAgentPress(options, b.ID, out total);
            return list;
        }
        public static CompanyNewsList GetTestimonial(this BrokerInfo b)
        {
            PagingOptions options = new PagingOptions();
            options.PageSize = 10;
            int total = 0;
            var list = ServiceLocator.GetRepository().GetAgentTestimonials(options, b.ID, out total);
            return list;
        }
        public static string GenVCard(this BrokerInfo c)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("BEGIN:VCARD");
            sb.AppendLine("VERSION:2.1");
            sb.AppendLine(string.Format("N;LANGUAGE=en-us:{0};{1}", c.LastName, c.FirstName));
            sb.AppendLine(string.Format("FN:{0} {1}", c.FirstName, c.LastName));
            sb.AppendLine(string.Format("TITLE:{0}", c.Title));
            sb.AppendLine(string.Format("TEL;CELL;VOICE:{0}", c.MobilePhone));
            sb.AppendLine(string.Format("TEL;WORK;VOICE:{0}", c.WorkPhone));

            var company = new Repository().GetCompanyInfoByID(c.CompanyID);
            if (company != null)
            {
                sb.AppendLine(string.Format("ORG: {0}", company.CompanyName));
                sb.AppendLine(string.Format("LABEL;WORK;PREF;ENCODING=QUOTED-PRINTABLE:{0} =0A{1} {2} {3}", company.Address, company.City, company.State, company.ZipCode));
            }

            sb.AppendLine(string.Format("EMAIL;PREF;INTERNET: {0}", c.Email));
            sb.AppendLine(string.Format("REV:{0}{1}{2}T050100Z", DateTime.Today.Year, DateTime.Now.Month, DateTime.Now.Day));
            sb.AppendLine("END:VCARD");
            return sb.ToString();
        }

    }
}