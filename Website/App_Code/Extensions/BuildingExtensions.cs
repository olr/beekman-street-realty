﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BrokerTemplate.Core;

namespace BrokerTemplate.Web
{
    /// <summary>
    /// Summary description for BuildingExtensions
    /// </summary>
    public static class BuildingExtensions
    {
        public static string GetNewDevelopmentLink(this BuildingInfo b)
        {
            return string.Format("search.aspx?buid={0}", b.ID);
        }
        public static IList<KeyValuePair<string, string>> GetBuildingDetail(this BuildingInfo info)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
            if (!string.IsNullOrEmpty(info.OwnershipType))
                list.Add(new KeyValuePair<string,string>("Ownership",info.OwnershipType));
            if (!string.IsNullOrEmpty(info.BuildingType))
            { 
                if(!string.IsNullOrEmpty(info.BuildingAge))
                    list.Add(new KeyValuePair<string, string>("BuildingType",info.BuildingAge+" "+ info.BuildingType));
                else
                    list.Add(new KeyValuePair<string, string>("BuildingType", info.BuildingType));
            }
            if (info.FloorCount>0 )
                list.Add(new KeyValuePair<string, string>("Floors | Apts", info.FloorCount+ " | "+ info.ApartmentCount));
            if (!string.IsNullOrEmpty(info.PetPolice))
                list.Add(new KeyValuePair<string, string>("Pet Policy", info.PetPolice));
            if(!string.IsNullOrEmpty(info.ServiceLevelType))
                list.Add(new KeyValuePair<string, string>("Service Level", info.ServiceLevelType));
            return list;
        }
        public static IList<string> GetBuildingFeatures(this BuildingInfo info)
        {
            List<string> list = new List<string>();
            if (info != null)
                list = info.Amenities.ToList();
            return list;
        }
        public static string GetBuildingPhotos(this BuildingInfo b)
        {
            string url = Resources.Media.NoListingPhoto;
            var media = ServiceLocator.GetRepository().GetBuildingMedia(b.ID.ToString());
            if (media.Photos.Count > 0)
                url = media.Photos[0].MediaUrl;
            else
                if (!string.IsNullOrEmpty(b.PhotoUrl))
                    url = b.PhotoUrl;
            return url;
        }
        public static Media GetMedia(this BuildingInfo b)
        {
            var media = ServiceLocator.GetRepository().GetBuildingMedia(b.ID.ToString());
            return media;
        }
    }
}