﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="about.aspx.cs" Inherits="about" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages clearfix">
        <div class="titles-global" style="margin-bottom: 20px;">
            Our Company
        </div>
        <div class="text clearfix">
        <div class="about-image left">
            <img src="images/our_company.jpg" />
        </div>
        
            <p>
                <span>Rent an Apartment with Beekman Street Realty: Our Edge</span>
            </p>
            <p>
                Beekman Street Realty is street-wise! We are New York real estate with an edge.
                We specialize in providing high-quality residential real estate services within
                the New York City area. In addition to all the open listings in New York, we have
                exclusive access to thousands of apartment listings not available to other New York
                City brokerage firms. Our rental properties span across Manhattan, Brooklyn, Queens,
                the Bronx, Yonkers and Scarsdale. From a quaint brownstone to a luxury townhouse,
                we know New York because we are New York!
            </p>
            <p>
                Our talented, experienced and committed rental agents are working FOR YOU. If you
                would like to learn more about our agents, visit our <a href="agents.aspx">agent profiles</a> page.
            </p>
            <p>
                Begin your apartment search now. To find out which area or neighborhood is most
                suitable for you, read our informative <a href="neighborhood_guide.aspx">neighborhood profiles</a>. Also, read our <a href="renters_guide.aspx">apartment
                rental guidelines</a> for New York City to get a feel for the process. You’ll see that
                Beekman Street Realty is the real deal in New York real estate, ensuring you a smooth
                experience from beginning to end in your apartment search.
            </p>
        </div>
    </div>
</asp:Content>
