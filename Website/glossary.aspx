﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="glossary.aspx.cs" Inherits="glossary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages clearfix">
        <div class="titles-global" style="margin-bottom: 20px;">
            Glossary & Terms
        </div>
        <div class="all-alpha-title clearfix">
            <ul class="clearfix">
                <li id="A-button" class="letter"><span class="cursor">a</span> </li>
                <li id="B-button" class="letter"><span class="cursor">b</span> </li>
                <li id="C-button" class="letter"><span class="cursor">c</span> </li>
                <li id="D-button" class="letter"><span class="cursor">d</span> </li>
                <li id="E-button" class="letter"><span class="cursor">e</span> </li>
                <li id="F-button" class="letter"><span class="cursor">f</span> </li>
                <li id="G-button" class="letter"><span class="cursor">g</span> </li>
                <li id="H-button" class="letter"><span class="cursor">h</span> </li>
                <li id="I-button" class="letter"><span class="cursor">i</span> </li>
                <li id="J-button" class="letter"><span class="cursor">j</span> </li>
                <li id="K-button" class="letter"><span class="cursor">k</span> </li>
                <li id="L-button" class="letter"><span class="cursor">l</span> </li>
                <li id="M-button" class="letter"><span class="cursor">m</span> </li>
                <li id="N-button" class="letter"><span class="cursor">n</span> </li>
                <li id="O-button" class="letter"><span class="cursor">o</span> </li>
                <li id="P-button" class="letter"><span class="cursor">p</span> </li>
                <li id="Q-button" class="letter"><span class="cursor">q</span> </li>
                <li id="R-button" class="letter"><span class="cursor">r</span> </li>
                <li id="S-button" class="letter"><span class="cursor">s</span> </li>
                <li id="T-button" class="letter"><span class="cursor">t</span> </li>
                <li id="U-button" class="letter"><span class="cursor">u</span> </li>
                <li id="V-button" class="letter"><span class="cursor">v</span> </li>
                <li id="W-button" class="letter"><span class="cursor">w</span> </li>
                <li id="X-button" class="letter"><span class="cursor">x</span> </li>
                <li id="Y-button" class="letter"><span class="cursor">y</span> </li>
                <li id="Z-button" class="letter"><span class="cursor">z</span> </li>
            </ul>
            <!--A list ends-->
            <div id="A-list">
                <div class="clearfix">
                    <div class="alpha-title">
                        a</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--A list ends-->
            <!--B list here-->
            <div id="B-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        b</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--B list ends-->
            <!--C list here-->
            <div id="C-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        c</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--C list ends-->
            <!--D list here-->
            <div id="D-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        d</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--D list ends-->
            <!--E list here-->
            <div id="E-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        e</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--E list ends-->
            <!--F list here-->
            <div id="F-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        f</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--F list ends-->
            <!--G list here-->
            <div id="G-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        g</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--G list ends-->
            <!--H list here-->
            <div id="H-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        h</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--H list ends-->
            <!--I list here-->
            <div id="I-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        i</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--I list ends-->
            <!--J list here-->
            <div id="J-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        j</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--J list ends-->
            <!--K list here-->
            <div id="K-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        k</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--K list ends-->
            <!--L list here-->
            <div id="L-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        l</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--L list ends-->
            <!--M list here-->
            <div id="M-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        m</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--M list ends-->
            <!--N list here-->
            <div id="N-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        n</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--N list ends-->
            <!--O list here-->
            <div id="O-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        o</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--O list ends-->
            <!--P list here-->
            <div id="P-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        p</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--P list ends-->
            <!--Q list here-->
            <div id="Q-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        q</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--Q list ends-->
            <!--R list here-->
            <div id="R-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        r</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--R list ends-->
            <!--S list here-->
            <div id="S-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        s</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--S list ends-->
            <!--T list here-->
            <div id="T-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        t</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--T list ends-->
            <!--U list here-->
            <div id="U-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        u</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--U list ends-->
            <!--V list here-->
            <div id="V-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        v</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--V list ends-->
            <!--W list here-->
            <div id="W-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        w</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--W list ends-->
            <!--X list here-->
            <div id="X-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        x</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--X list ends-->
            <!--Y list here-->
            <div id="Y-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        y</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--Y list ends-->
            <!--Z list here-->
            <div id="Z-list" style="display: none;">
                <div class="clearfix">
                    <div class="alpha-title">
                        z</div>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                    <p class="terms-title">
                        Lorem Ipsum</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tincidunt iaculis.
                        Pellentesque tristique luctus dui, vitae dictum odio commodo sit amet.</p>
                </div>
            </div>
            <!--Z list ends-->
        </div>
    </div>
</asp:Content>
