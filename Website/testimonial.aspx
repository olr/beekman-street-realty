﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="testimonial.aspx.cs" Inherits="testimonial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages clearfix">
        <div class="titles-global" style="margin-bottom: 20px;">
            testimonials
        </div>
        <div class="testimonial-wrapper">
         <olr:CompanyTestimonial runat="server" ID="CompanyTestimonial1" />
        <olr:OLRPageNav ID="PageNav2" runat="server" UsePostback="false" PrevImageUrl="images/icons/arrow_left.png"
            NextImageUrl="images/icons/arrow_right.png"  TotalInfoFormat="testimonial"/>
        </div>
    </div>
</asp:Content>
