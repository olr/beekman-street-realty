﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Configuration;
using System.Net.Mail;

public partial class registration : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        submitButton.Click += new ImageClickEventHandler(SubmitButton_Click);
    }

    void SubmitButton_Click(object sender, ImageClickEventArgs e)
    {
        if (Page.IsValid)
        {

            StringBuilder sb = new StringBuilder();


            sb.AppendLine("Customer Information<br />");
            sb.AppendLine("----------------------------------------------------------------------------------------------------<br />");
            sb.AppendFormat("I Am Looking To: {0}<br />\n", CustomerType.SelectedValue);

            string buildingAmenities = Request["BuildingAmenities"];

            sb.AppendFormat("Desired Neighborhoods: {0}<br />\n", Neighborhoods.Text);
            sb.AppendFormat("# Beds: {0}<br />\n", Beds.Text);
            sb.AppendFormat("# Baths: {0}<br />\n", Baths.Text);
            sb.AppendFormat("Min Price: {0}<br />\n", MinPrice.Text);
            sb.AppendFormat("Max Price: {0}<br />\n", MaxPrice.Text);
            sb.AppendFormat("Desired Building Amenities: {0}<br /><br />\n", buildingAmenities);
            sb.AppendFormat("First Name: {0}<br />\n", FirstName.Text);
            sb.AppendFormat("Last Name: {0}<br />\n", LastName.Text);
            sb.AppendFormat("Email: {0}<br />\n", Email.Text);
            sb.AppendFormat("Phone: {0}<br />\n", Phone.Text);
            sb.AppendFormat("Preferred Contact Method: {0}<br />\n", ContactByPhone.Checked ? "By Phone" : "By Email");
            sb.AppendFormat("Additional Comments: <br />{0}<br />\n", Comments.Text);
            sb.AppendLine("----------------------------------------------------------------------------------------------------<br />");

            string toEmail = ConfigurationManager.AppSettings["defaultEmail"];

            if (!string.IsNullOrEmpty(toEmail))
            {
                MailMessage message = new MailMessage("noreply@olr.com", toEmail);
                message.ReplyToList.Add(Email.Text);
                message.Subject = "Customer Registration";
                message.IsBodyHtml = true;
                message.Body = sb.ToString();

                try
                {
                    using (var client = new SmtpClient())
                    {
                        client.Send(message);
                    }
                }
                catch (Exception)
                {
                }
            }
            formPanel.Visible = false;
            messagePanel.Visible = true;
        }
    }
}