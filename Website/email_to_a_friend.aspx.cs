﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net.Mail;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Configuration;

public partial class email_to_a_friend : System.Web.UI.Page
{
    string siteName = ConfigurationManager.AppSettings["SiteName"];
    protected ListingInfo Model = new ListingInfo();
    protected Exception exception;

    protected override void OnInit(EventArgs e)
    {
        sendEmail.Click += new EventHandler(Send_Email);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Model = ServiceLocator.GetRepository().GetListingInfoByID(Request.QueryString["id"]);
            if (Model != null)
            {
                this.Controls.DataBind<ListingInfo>(Model);
                this.DataBind(false);
            }
        }
    }
    
    void Send_Email(object sender, EventArgs e)
    {
        int id = fn.Get<int>(Request.QueryString["aid"]);
        Model = ServiceLocator.GetRepository().GetListingInfoByID(Request.QueryString["id"]);
        if (IsValid && Model != null)
        {
            string url = string.Format("http://{0}/{1}", Request.Url.Host, Model.GetDetailLink());
            string msg = string.Format(@"Hi {2}, <br/> I found this listing on {0}. <a href=""{1}"">Click here to view this listing.</a>", siteName, url, recepientName.Text);
            msg = string.Format("<p>{0}</p><p>{1}</p><p>{2}</p>", msg, messageText.Text, senderName.Text);

            StringBuilder body = new StringBuilder();
            body.AppendLine("<div class='overlay_container' style='background-color: #ffffff; color: #2D2D2D; font-family: Arial; font-size: 12px;line-height: 1.2em;'>");
            body.AppendFormat("<img src='http://{0}/images/PRINT-Logo.jpg' style='width:176px; border:none;' />", Request.Url.Host);
            body.AppendLine("<div>");
            body.AppendFormat(msg);
            body.AppendLine("</div>");
            body.AppendLine("</div>");

            MailMessage messageToAgent = new MailMessage("noreply@olr.com", Model.GetBrokers()[0].Email);
            messageToAgent.Subject = "Website lead from http://www.beekmanstreetrealty.com - your listing ID#" + id + " has been shared.";
            messageToAgent.Body = body.ToString();
            messageToAgent.IsBodyHtml = true;
            MailMessage message = new MailMessage(senderEmail.Text, recepientEmail.Text);
            message.Subject = string.Format(Resources.Email.SubjectEmailToFriend, siteName);
            message.Body = body.ToString();
            message.IsBodyHtml = true; 
            SmtpClient client = new SmtpClient();

            try
            {
                client.Send(message);
                client.Send(messageToAgent);
                formpanel.Visible = false;
                sentpanel.Visible = true;
            }
            catch (Exception ex)
            {
                exception = ex;
                formpanel.Visible = false;
                sentfailed.Visible = true;
            }

        }
    }
}
