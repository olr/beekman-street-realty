﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Xml.Linq;


public partial class search : BasePage<search, SearchOptions>
{
    protected string type = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        Model = new SearchOptions();

        if (string.Equals(Request.QueryString["type"], "rent") || string.Equals(Request.QueryString["type"], "rental"))
        {
            Model.PropertyTypes.Add(PropertyType.Rental);
            type = "rent";
        }
        else if (!string.IsNullOrEmpty(Request.QueryString["address"]))
        {
            string buildingid = ServiceLocator.GetSearchService().GetBuildingIDByAddress(Request.QueryString["address"]);
            int id = Convert.ToInt32(buildingid);
            Model.BuildingID = id;
            Model.PropertyTypes.Add(PropertyType.Sale);
            Model.PropertyTypes.Add(PropertyType.Rental);
            TempStorage.Add("SearchOptions", Model);
            Response.Redirect("~/result.aspx?transfer=1");
        }
        else if (!string.IsNullOrEmpty(Request.QueryString["nb"]))
        {
            Model.PropertyTypes.Add(PropertyType.Rental);
            Model.PropertyTypes.Add(PropertyType.Sale);
            Model.Neighborhoods.Add(Convert.ToInt32(Request.QueryString["nb"]));
            TempStorage.Add("SearchOptions", Model);
            Response.Redirect("~/result.aspx");
        }
        else if (!string.IsNullOrEmpty(Request.QueryString["nofee"]))
        {
            Model.PropertyTypes.Add(PropertyType.Rental);
            Model.ApartmentFeatures.Add(ApartmentFeature.NoFee);
            TempStorage.Add("SearchOptions", Model);
            Response.Redirect("~/result.aspx?nofee=1");
        }
        else if (!string.IsNullOrEmpty(Request.QueryString["buid"]))
        {
            int buildingid = fn.Get<int>(Request.QueryString["buid"]);
            Model.PropertyTypes.Add(PropertyType.Sale);
            Model.PropertyTypes.Add(PropertyType.Rental);
            Model.BuildingID = buildingid;
            TempStorage.Add("SearchOptions", Model);
            Response.Redirect("~/result.aspx?transfer=1");
        }
        else if (!string.IsNullOrEmpty(Request.QueryString["all"]))
        {
            if (Request.QueryString["all"] == "rentals")
                Model.PropertyTypes.Add(PropertyType.Rental);
            else
                Model.PropertyTypes.Add(PropertyType.Sale);

            TempStorage.Add("SearchOptions", Model);
            Response.Redirect("~/result.aspx");
        }
        else
        {
            Model.PropertyTypes.Add(PropertyType.Sale);
            type = "sale";
        }
        if (!string.IsNullOrEmpty(Request.QueryString["neighborhood"]))
        {
            string value;
            var xml = XElement.Load(Server.MapPath("~/App_Data/NeighborhoodData.xml"));
            var node = xml.Elements("neighborhood").Where(x => x.Attribute("name").Value == Request.QueryString["neighborhood"].ToLower()).FirstOrDefault();
            if (node != null)
                value = node.Attribute("value").Value;
            else
                value = "0";
            Model.Neighborhoods = GetArea(value);
            TempStorage.Add("SearchOptions", Model);
            Response.Redirect("~/result.aspx");
        }
        if (string.Equals(Request.QueryString["refine"], "true"))
        {
            SearchOptions options;
            if (TempStorage.TryGet<SearchOptions>("SearchOptions", out options))
                Model = options;
        }
    }

    private List<int> GetArea(string number)
    {
        List<int> list = new List<int>();
        if (!string.IsNullOrEmpty(number))
            Array.ForEach<string>(number.Split(new char[] { ',' }), x => list.Add(Convert.ToInt32(x)));
        return list;
    }
}