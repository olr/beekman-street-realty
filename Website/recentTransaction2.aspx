﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true"
    CodeFile="recentTransaction2.aspx.cs" Inherits="recentTransaction2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript">
        $(function () {
            $("input:radio").change(function () {
                if ($("input:radio:checked").val() == "rent") {
                    window.location.replace("recenttransaction2.aspx?type=rent");
                }
                else {
                    window.location.replace("recenttransaction2.aspx?type=sale");
                }
            });
            <% if(pType=="sale"){ %>
                $('input#saleRadio').attr("checked","true");
            <%} else{ %>
                $('input#rentRadio').attr("checked","true");
            <%} %>
            $('.column-photo a').click(function () { return false; })
                                .css('cursor','default');
            $('.column-location a').click(function () { return false; })
                                .css('cursor', 'default');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="result-page">
    <div class="titles-global left" style="padding-top:10px;">recent transactions</div>
        <div class="recenttransaction_search_container">
            <div class="recent-transaction-title">
                Recently Transaction Type</div>
            <div class="refine_search_text">
                Sales</div>
            <div style="margin: 5px 5px 0 0;">
                    <input id="saleRadio" name="propertyType" type="radio" value="sale" />
            </div>
            <div class="refine_search_text">
                <span>Rentals</span></div>
            <div style="margin: 5px 5px 0 0;">
                    <input id="rentRadio" name="propertyType" type="radio" value="rent" />
            </div>
            <div class="clear">
            </div>
        </div>
        <div class="result-container">
            <olr:OLRPageNav ID="PageNav1" runat="server" UsePostback="false" PrevImageUrl="images/icons/prev.png"
                NextImageUrl="images/icons/next.png" />
            <olr:ShortForm runat="server" ID="ShortForm1" />
            <olr:OLRPageNav ID="PageNav2" runat="server" UsePostback="false" PrevImageUrl="images/icons/prev.png"
                NextImageUrl="images/icons/next.png" />
        </div>
    </div>
</asp:Content>
