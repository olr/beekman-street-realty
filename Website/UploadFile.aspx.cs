﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class UploadFile : System.Web.UI.Page
{
    protected int categoryID;
    protected void Page_Load(object sender, EventArgs e)
    {
        categoryID = fn.Get<int>(Request.QueryString["categoryID"]);
    }
    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        if (uploadFile.PostedFile != null && uploadFile.PostedFile.ContentLength > 0)
        {
            var file = uploadFile.PostedFile;
            int agentid = Convert.ToInt32(Request.QueryString["agentid"]);
            int result = CustomFile.AddFile(categoryID, file,agentid);
            if (result > 0)
            {
                Response.Redirect(Request.RawUrl);
            }
            else
            {
                //do something else
            }
        }
    }

    
}