﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="press.aspx.cs" Inherits="press" EnableEventValidation="false"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
 <script type="text/javascript">
     $(function () {
         $("span.pageinfo").each(function () {
             var str = $(this).text();
             if (str.indexOf('Listing')) {
                 $(this).text(str.replace('Listing', 'Presse'));
             }
         })
     })
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="news-page">

        <div class="titles-global" style="margin-bottom:20px;">press</div>
        <olr:CompanyPress runat="server" ID="CompanyPress1" />
        <olr:OLRPageNav ID="PageNav2" runat="server" UsePostback="false" PrevImageUrl="images/icons/arrow_left.png"
            NextImageUrl="images/icons/arrow_right.png" TotalInfoFormat="press" />
    </div>

</asp:Content>
