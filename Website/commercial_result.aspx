﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="commercial_result.aspx.cs" Inherits="commercial_result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
<div class="result-page">
    <div class="titles-global">commercial</div>
    <olr:OLRPageNav ID="PageNav1" runat="server" UsePostback="false" PrevImageUrl="images/icons/prev.png" NextImageUrl="images/icons/next.png" />
    <%if (total != 0)
      { %>
    <olr:Commercial_List runat="server" ID="Commercial_List1" />
    <%}
      else
      { %>
    <p class="no-results">If the listings below do not match your criteria please complete the following <a href="commercial_email.aspx" class="modal {width:480,height:470}"><u>registration form</u></a></p>
    <%} %>
    <olr:OLRPageNav ID="PageNav2" runat="server" UsePostback="false" PrevImageUrl="images/icons/prev.png" NextImageUrl="images/icons/next.png" />
</div>

</asp:Content>
