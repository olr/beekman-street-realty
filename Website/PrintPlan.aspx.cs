﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PrintPlan : System.Web.UI.Page
{
    protected bool hasFloorPlan;
    protected bool hasPhoto;
    protected string currency;
    protected ListingInfo l;
    protected void Page_Load(object sender, EventArgs e)
    {
        string id = Request.QueryString["id"];
        l = ServiceLocator.GetRepository().GetListingInfoByID(id);
        if (l.GetMedia().Floorplans.Count == 0)
            hasFloorPlan = false;
        else
            hasFloorPlan = true;
        if (l.GetMedia().Photos.Count == 0)
            hasPhoto = false;
        else
            hasPhoto = true;
        if (string.IsNullOrEmpty(Request.QueryString["currency"]))
            currency = "USD";
        else
            currency = Request.QueryString["currency"];
    }
}