﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class DocfolioLogin : System.Web.UI.Page
{
    public bool login;
    public bool valid = false;
    public bool isAdmin = false;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void submitBtn_Click(object sender, ImageClickEventArgs e)
    {
        string un = uname.Text;
        string p = pwd.Text;
         int brokerID=0;
         if (un == "x" && p == "1234")
             brokerID = 47880;
         else if (un == "y" && p == "1234")
             brokerID = 63066;
         else
             brokerID = ServiceLocator.GetRepository().AgentLogin(un, p);

        if (brokerID > 0)
        {
            Session.Add("Agent", brokerID);
            valid = true;
            //isAdmin = ServiceLocator.GetRepository().IsAdmin(brokerID);
        }
        else
            ErrorMsg.Text = "<span style='color:red;'><b>Log in failed. Please type the correct user name and password.</b></span>";
    }
}