﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using OLRDataLayer;


public partial class _default : BasePage<_default, SearchOptions>
{
    protected ListingInfo saleListing = null;
    protected ListingInfo rentListing = null;
    protected List<OLRData.Listing> listings;
    protected FeaturedPropertyGroup openhouses;
    protected override void OnInit(EventArgs e)
    {
        Model = new SearchOptions();
        Model.PropertyTypes.Add(PropertyType.Sale);
        listings = getFeaturedListing();
        saleListing = Convert((from x in listings where x.Price>0 select x).FirstOrDefault());
        rentListing = Convert((from x in listings where x.Rent2 > 0 select x).FirstOrDefault());
        openhouses = getOpenHouses();
        Session.Remove("Agent");
    }
    /*  this is for one featured sale and one featured rent  source from Citysites
    protected FeaturedPropertyGroup featuredproperties;
    protected FeaturedPropertyGroup openhouses;
    protected override void OnInit(EventArgs e)
    {
        Model = new SearchOptions();
        Model.PropertyTypes.Add(PropertyType.Sale);
        featuredproperties = getFeaturedProperties();
        openhouses = getOpenHouses();
    }
     */
    protected FeaturedPropertyGroup getOpenHouses()
    {
        SearchOptions data = new SearchOptions();
        data.PropertyTypes.Add(PropertyType.Sale);
        data.OpenHouse = true;
        int total = 0;
        Listings salelist = ServiceLocator.GetSearchService().RunSearch(data, out total);
        if (salelist != null && salelist.Count > 1)
            salelist.Sort(OpenHouseComparer);

        data.PropertyTypes.Remove(PropertyType.Sale);
        data.PropertyTypes.Add(PropertyType.Rental);
        total = 0;
        Listings rentallist = ServiceLocator.GetSearchService().RunSearch(data, out total);
        if (rentallist != null && rentallist.Count > 1)
            rentallist.Sort(OpenHouseComparer);

        FeaturedPropertyGroup group = new FeaturedPropertyGroup(salelist, rentallist);
        return group;
    }

    private int OpenHouseComparer(ListingInfo a, ListingInfo b)
    {
        if (a != null && b != null)
        {
            var o1 = a.GetOpenHouses().OrderByDescending(x => x.OpenHouseDate).FirstOrDefault();
            var o2 = b.GetOpenHouses().OrderByDescending(x => x.OpenHouseDate).FirstOrDefault();
            if (o1 != null && o2 != null)
                return o1.OpenHouseDate.CompareTo(o2.OpenHouseDate);
        }
        return 0;
    }
    protected List<OLRData.Listing> getFeaturedListing()
    {
        DataLayer db = new DataLayer();
        List<OLRData.Listing> ls=(from x in db.GetActiveListingsAsQueryable()
                                 where (x.FeaturedListing==true)  
                                  select x ).ToList();
        return ls;
    }

    private ListingInfo Convert(OLRData.Listing listing)
    {
        ListingInfo info = new ListingInfo();
        if (listing != null)
        {
            info.ListingID = listing.ListingID.ToString();
            info.BuildingPhotoUrl = Resources.Media.BuildingPhotoRepository + listing.Building.PictureName;
            info.ShowBuildingPhoto = listing.ShowBuildingPhoto.GetValueOrDefault();
            info.Bathrooms = listing.Baths.GetValueOrDefault();
            info.Bedrooms = listing.Bedrooms.GetValueOrDefault();
            info.Neighborhood = listing.Building.NeighborhoodName;
            info.Rent = listing.Rent.GetValueOrDefault();
            info.FurnishedRent = listing.FurnishRent.GetValueOrDefault();
        }
        return info;
    }
}