﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="detail.aspx.cs" Inherits="detail" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/jquery.ad-gallery.css" rel="stylesheet" type="text/css" />
    <link href="css/detailslideshow.css?v=1" rel="stylesheet" type="text/css" />
    <script src="js/jquery.ad-gallery.js" type="text/javascript"></script>
    <script src="js/jquery.oauth.js" type="text/javascript"></script>
    <script src="js/sha1.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(function () {
            //alert($('option:selected').attr('class'));
            $('.poi').click(function () {
                $('.scroll').animate({ left: '-1100px' }, 500, 'swing');
                //isPoiVisible = true;
            });
            $('.googlemap').click(function () {
                $('.scroll').animate({ left: '0px' }, 500, 'swing');
                //isPoiVisible = false;
            });
            $('.streetview').click(function () {
                $('.scroll').animate({ left: '-550px' }, 500, 'swing');
                //isPoiVisible = false;
            });
            $('a.afshowAll').click(function () {
                $('div.afrest').attr('style', 'display:block; margin-top:-12px;');
                $(this).attr('style', 'display:none');
                $('a.afshow10').show();
                return false;
            })
            $('a.afshow10').click(function () {
                $('div.afrest').attr('style', 'display:none;');
                $(this).attr('style', 'display:none');
                $('a.afshowAll').show();
                return false;
            })
            $('a.bfshowAll').click(function () {
                $('div.bfrest').attr('style', 'display:block; margin-top:-12px;');
                $(this).attr('style', 'display:none');
                $('a.bfshow10').show();
                return false;
            })
            $('a.bfshow10').click(function () {
                $('div.bfrest').attr('style', 'display:none;');
                $(this).attr('style', 'display:none');
                $('a.bfshowAll').show();
                return false;
            })
            var price = getValue($('span.p').text());
            var priceMaintenance = getValue($('span.maintenance').text());
            var priceCommonCharge = getValue($('span.commonCharge').text());
            var priceRETax = getValue($('span.RETax').text());
            var pricePPSF = getValue($('span.PPSF').text());
            var priceFurnished = getValue($('span.Furnished').text());
            $('select.currency').change(function () {
                var symbol = $('.currency option:selected').attr('class');
                buildPrintLink();
                var rate = $(this).val();
                var textPrice = $().number_format(price * rate, { numberOfDecimals: 0 });
                var textpriceMaintenance = $().number_format(priceMaintenance * rate, { numberOfDecimals: 0 });
                var textpriceCommonCharge = $().number_format(priceCommonCharge * rate, { numberOfDecimals: 0 });
                var textpriceRETax = $().number_format(priceRETax * rate, { numberOfDecimals: 0 });
                var textpricePPSF = $().number_format(pricePPSF * rate, { numberOfDecimals: 0 });
                var textpriceFurnished = $().number_format(priceFurnished * rate, { numberOfDecimals: 0 });
                if (symbol == 'USD') {
                    $('span.p').text("").text(textPrice);
                    $('span.maintenance').text("").text(priceMaintenance);
                    $('span.commonCharge').text("").text(textpriceCommonCharge);
                    $('span.RETax').text("").text(textpriceRETax);
                    $('span.PPSF').text("").text(textpricePPSF);
                    $('span.Furnished').text("").text(textpriceFurnished);
                }
                else {
                    $('span.p').text("").text(textPrice + ' [$' + $().number_format(price, { numberOfDecimals: 0 }) + ' US]');
                    $('span.maintenance').text("").text(textpriceMaintenance + ' [$' + $().number_format(priceMaintenance, { numberOfDecimals: 0 }) + ' US]');
                    $('span.commonCharge').text("").text(textpriceCommonCharge + ' [$' + $().number_format(priceCommonCharge, { numberOfDecimals: 0 }) + ' US]');
                    $('span.RETax').text("").text(textpriceRETax + ' [$' + $().number_format(priceRETax, { numberOfDecimals: 0 }) + ' US]');
                    $('span.PPSF').text("").text(textpricePPSF + ' [$' + $().number_format(pricePPSF, { numberOfDecimals: 0 }) + ' US]');
                    $('span.Furnished').text("").text(textpriceFurnished + ' [$' + $().number_format(priceFurnished, { numberOfDecimals: 0 }) + ' US]');
                }


                var symbol = $('.currency option:selected').attr('class');
                switch (symbol) {
                    case 'USD': $('.symbol').text('').text('$');
                        break;
                    case 'AUD': $('.symbol').text('').text('$');
                        break;
                    case 'CAD': $('.symbol').text('').text('$');
                        break;
                    case 'HKD': $('.symbol').text('').text('$');
                        break;
                    case 'MXN': $('.symbol').text('').text('$');
                        break;
                    case 'NZD': $('.symbol').text('').text('$');
                        break;
                    case 'BRL': $('.symbol').text('').text('R$');
                        break;
                    case 'CHF': $('.symbol').text('').text('CHF');
                        break;
                    case 'CNY': $('.symbol').text('').text('¥');
                        break;
                    case 'JPY': $('.symbol').text('').text('¥');
                        break;
                    case 'DKK': $('.symbol').text('').text('kr');
                        break;
                    case 'NOK': $('.symbol').text('').text('kr');
                        break;
                    case 'SEK': $('.symbol').text('').text('kr');
                        break;
                    case 'EUR': $('.symbol').text('').text('€ ');
                        break;
                    case 'GBP': $('.symbol').text('').text('£');
                        break;
                    case 'TWD': $('.symbol').text('').text('NT$');
                        break;
                    case 'ZAR': $('.symbol').text('').text('R');
                        break;
                    case 'INR': $('.symbol').text('').text('INR');
                        break;
                }
            });
        });

        function getValue(str) {
            var val = str.replace(/[^0-9]+/g, '');
            return val;
        }

        function buildPrintLink() {
            var currency = $('select.currency option:selected').attr('class');
            //console.log(currency);
            var Printlink = $('a.printbutton').attr('href');
            if (Printlink.indexOf('currency') == -1) {
                $('a.printbutton').attr('href', Printlink + '&currency=' + currency);
                //console.log('no currency');
            }
            else {
                var pat = /currency=(\w+)/;
                Printlink = Printlink.replace(pat, "currency=" + currency);
                $('a.printbutton').attr('href', Printlink);
                //console.log('modify currency');
            }
        } 
            
    </script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAsw1rDRNIFqU6G5hduNJzcLdiH2mac174&sensor=false">
    </script>
    <script type="text/javascript">
      var geocoder;
  var infowindow = new google.maps.InfoWindow();
  var marker;
  var map;
  var panorama;
  var mapcenter=new google.maps.LatLng(<%=Latitude%>,<%=Longitude%>);
        $(function () {
            var options = {
                center:mapcenter ,
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
                //streetViewControl:false
            };
            map = new google.maps.Map(document.getElementById("map_canvas"),
            options);
            var lat = <%=Latitude%>;
            var lng = <%=Longitude%>;

            var True=true;
            var False=false;
            var panoramaOptions = {
                    position: mapcenter,
                    pov: {
                        heading: 34,
                        pitch: 10,
                        zoom: 0
                     }
                    //visible:true
            };
            panorama = new  google.maps.StreetViewPanorama(document.getElementById("pano"), panoramaOptions);

            if(<%=showAddress %>){
             geocoder = new google.maps.Geocoder();
            var lat = <%=Latitude%>;
             var lng = <%=Longitude%>;

            var latlng = new google.maps.LatLng(lat, lng);

            geocoder.geocode( { 'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          map.setZoom(14);
          marker = new google.maps.Marker({
              position: latlng,
              map: map
          });
         // infowindow.setContent(results[1].formatted_address);
          infowindow.open(map, marker);
        }
      } else {
        console.log('failed'+ google.maps.GeocoderStatus);
      }

    });
    }// if is excusive and showaddress
        })
    </script>
 <script type="text/javascript">
        $(function () {
            $('div.listingNote.en').attr('style', 'display:block');
            $('select.P').change(function () {
                var language = $(this).val();
                $('div.listingNote').attr('style', 'display:none');
                $('div.listingNote.' + language).attr('style', 'display:block');
            });
            $('div.buildingNote.en').attr('style', 'display:block');
            $('select.U').change(function () {
                var language = $(this).val();
                $('div.buildingNote').attr('style', 'display:none');
                $('div.buildingNote.' + language).attr('style', 'display:block');
            });
        })
    </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#label1").click(function () {
                    $("#detail-window").animate({ left: '0' }, 500, function () {
                        $(".desc-language").show();
                    });
                });
                $("#label2").click(function () {
                    $(".desc-language").hide();
                    $("#detail-window").animate({ left: '-550' }, 500);
                });
                $("#label3").click(function () {
                    $(".desc-language").hide();
                    $("#detail-window").animate({ left: '-1100' }, 500);
                });
                $("#label4").click(function () {
                    $("#media-window").animate({ left: '0' }, 500);
                });
                $("#label5").click(function () {
                    $("#media-window").animate({ left: '-550' }, 500);
                });
                $("#label6").click(function () {
                    $("#media-window").animate({ left: '-1100' }, 500);
                });
                $("#label7").click(function () {
                    $("#media-window").animate({ left: '-1650' }, 500);
                });

                $thumbcount = $(".m-slider-thumb");
                var current = 1;
                var count = $thumbcount.size();
                $(".previmage").click(function () {
                    if (current <= count && current > 1) {
                        current = current - 1;
                        $("#m-slider-thumb" + current).click();
                    }
                    //console.log(current);
                    $('.nextimage').animate({ opacity: 1 });
                    if (current == 1)
                    { $('.previmage').animate({ opacity: 0 }); }
                });

                $(".nextimage").click(function () {
                    if (current < count && current >= 1) {
                        current = current + 1;
                        $("#m-slider-thumb" + current).click();
                    }
                    // console.log(current);
                    $('.previmage').animate({ opacity: 1 });
                    if (current == count)
                    { $('.nextimage').animate({ opacity: 0 }); }
                });
                $('.m-images-mask').mouseenter(function () {
                    if (current == 1) { $('.nextimage').animate({ opacity: 1 }); }
                    else if (current == count) { $('.previmage').animate({ opacity: 1 }); }
                    else {
                        $('.previmage').animate({ opacity: 1 });
                        $('.nextimage').animate({ opacity: 1 });
                    }
                });
                $('.m-images-mask').mouseleave(function () {
                    if (current == 1) { $('.nextimage').animate({ opacity: 0 }); }
                    else if (current == count) { $('.previmage').animate({ opacity: 0 }); }
                    else {
                        $('.previmage').animate({ opacity: 0 });
                        $('.nextimage').animate({ opacity: 0 });
                    }
                });
                if ($thumbcount.size() == 1) {
                    $('div.previmage').hide();
                    $('div.nextimage').hide();
                    $('div.media-slider').hide();

                }
                var pageIndex = 0;
                var pageSize = 5;
                var numThumbnails = $thumbcount.size();
                var numPages = Math.ceil(numThumbnails / pageSize);
                if (numThumbnails == 0)
                    numPages = 1;

                if (numThumbnails > pageSize) {
                    $("#m-slider-right").css('background', 'url(images/right.png)');
                    $("#m-slider-right").click(function () {
                        if ((pageIndex + 1) >= numPages)
                            return;
                        var offset = -508 * (pageIndex + 1);
                        pageIndex++;
                        $("#m-slider-window").animate({ left: offset }, 500);

                        //hide right arrow when we reach the last page
                        if ((pageIndex + 1) >= numPages)
                            $("#m-slider-right").css('background', 'url(images/r-off.png)');
                        if (pageIndex >= 1)
                            $("#m-slider-left").css('background', 'url(images/left.png)');
                    });
                    $("#m-slider-left").click(function () {
                        if (pageIndex == 0)
                            return;
                        var offset = -508 * (pageIndex - 1);
                        if (pageIndex == 1)
                            offset = 0;
                        $("#m-slider-window").animate({ left: offset }, 500);
                        pageIndex--;

                        if ((pageIndex + 1) < numPages)
                            $("#m-slider-right").css('background', 'url(images/right.png)');
                        if (pageIndex == 0)
                            $("#m-slider-left").css('background', 'url(images/l-off.png)');
                        else
                            $("#m-slider-left").css('background', 'url(images/left.png)');
                    });
                }

                $("#m-slider-thumb1").hover(function () {
                    current = 1;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '0' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb2").hover(function () {
                    current = 2;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-550' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb3").hover(function () {
                    current = 3;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-1100' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb4").hover(function () {
                    current = 4;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-1650' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb5").hover(function () {
                    current = 5;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-2200' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb6").hover(function () {
                    current = 6;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-2750' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb7").hover(function () {
                    current = 7;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-3300' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb8").hover(function () {
                    current = 8;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-3850' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb9").hover(function () {
                    current = 9;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-4400' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb10").hover(function () {
                    current = 10;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-4950' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb11").hover(function () {
                    current = 11;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-5500' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb12").hover(function () {
                    current = 12;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-6050' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb13").hover(function () {
                    current = 13;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-6600' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb14").hover(function () {
                    current = 14;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-7150' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb15").hover(function () {
                    current = 15;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-7700' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb16").hover(function () {
                    current = 16;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-8250' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb17").hover(function () {
                    current = 17;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-8800' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb18").hover(function () {
                    current = 18;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-9350' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb19").hover(function () {
                    current = 19;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-9900' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb20").hover(function () {
                    current = 20;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-10450' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb21").hover(function () {
                    current = 21;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-11000' }, 500);
                    thumbClick(current, count);
                });

                $("#m-slider-thumb1").click(function () {
                    current = 1;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '0' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb2").click(function () {
                    current = 2;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-550' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb3").click(function () {
                    current = 3;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-1100' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb4").click(function () {
                    current = 4;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-1650' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb5").click(function () {
                    current = 5;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-2200' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb6").click(function () {
                    current = 6;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-2750' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb7").click(function () {
                    current = 7;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-3300' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb8").click(function () {
                    current = 8;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-3850' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb9").click(function () {
                    current = 9;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-4400' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb10").click(function () {
                    current = 10;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-4950' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb11").click(function () {
                    current = 11;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-5500' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb12").click(function () {
                    current = 12;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-6050' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb13").click(function () {
                    current = 13;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-6600' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb14").click(function () {
                    current = 14;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-7150' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb15").click(function () {
                    current = 15;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-7700' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb16").click(function () {
                    current = 16;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-8250' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb17").click(function () {
                    current = 17;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-8800' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb18").click(function () {
                    current = 18;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-9350' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb19").click(function () {
                    current = 19;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-9900' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb20").click(function () {
                    current = 20;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-10450' }, 500);
                    thumbClick(current, count);
                });
                $("#m-slider-thumb21").click(function () {
                    current = 21;
                    $("#m-images-window").stop(true, true);
                    $("#m-images-window").animate({ left: '-11000' }, 500);
                    thumbClick(current, count);
                });

            });
            function thumbClick(current,count) {
                if (current == 1) {
                    $('.previmage').animate({ opacity: 0 });
                }
                else if (current == count) {
                    $('.nextimage').animate({ opacity: 0 });
                } 
            };
        </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <%--2 Columns layout--%>
    <div class="detail-page">
        <div class="prepend-1 top-detail clearfix">
            <div class="column name-column">
                <h2 class="display-address">
                    <%=Model.GetShowAddress(out withStreetAddress)%>
                    <%if (Model.GetShowAddress(out withStreetAddress) != Model.Neighborhood)
                      {%>
                    -
                    <%=Model.GetArea()%><%} %></h2>
              <%--  <div class="buidlingname">
                    <%=Model.DisplayBuildingName()%></div>--%>
                <%if (!withStreetAddress)
                  { %>
                <div class="cross-streets">
                    <%=Model.GetCrossStreets("[{0} & {1}]")%></div>
                <%} %>
                <div class="detail-open-house">
                    <%=Model.GetOpenHouseString()%></div>
                <div class='detail-highlight'>
                    <%if (Model.Foreclosure)
                      { %>
                    Foreclosure Date:&nbsp;&nbsp;
                    <%=Model.ForeclosureDate.Month + "/" + Model.ForeclosureDate.Day + "/" + Model.ForeclosureDate.Year%>
                    <%} %>
                    <%if (Model.AvailAtAuction)
                      { %>
                    Auction Date:&nbsp;&nbsp;
                    <%=Model.AuctionDate.Month+"/" +Model.AuctionDate.Day+"/"+Model.AuctionDate.Year%>
                    <%} %>
                    <%if (Model.ShortSale)
                      { %>
                    Short Sale:&nbsp;&nbsp;
                    <%=Model.ShortSaleDate.Month + "/" + Model.ShortSaleDate.Day + "/" + Model.ShortSaleDate.Year%>
                    <%} %>
                </div>
            </div>
            <div class="column right detail-pager">
                <div>
                    <h2 class="webid">
                        Listing ID
                        <%=Model.ListingID%></h2>
                </div>
                <div class="detail-paging">
                    <olr:Paging ID="Paging2" runat="server" />
                </div>
            </div>
        </div>
        <div class="layout-detail clearfix">
            <div class="image-viewer left">
                <olr:ImageViewer ID="ImageViewer1" runat="server" />
                <div class="toolbar-space">
                    <%=Html.RenderPartial("cntrls/Details/DetailToolbar",Model) %>
                </div>
               <div class="section listing-description">
                    <div class="">
                        <% if (!string.Equals(Model.DisplayAddress, Model.GetDisplayAddress()))
                           { %>
                        <div class="page-titles">
                            <%=Model.DisplayAddress%></div>
                        <% } %>
                        <%if (notes.Count != 0)
                          {
                              if (listingNotesNum > 0)
                              {%><div class="page-titles">
                                  Listing Description&nbsp;&nbsp;<%if (listingNotesNum > 1)
                                                                   { %>
                                  <%=GetLanguageDropDown("P", listingNoteLanguageArray)%><%} %></div>
                        <asp:ListView ID="Notelist" runat="server">
                            <LayoutTemplate>
                                <asp:Literal runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <div align="justify" class='listingNote <%#(Container.DataItem as ListingBrokerNoteItem).Culture %>'
                                    style="display: none;">
                                    <%#SiteHelper.FormatWeb((Container.DataItem as ListingBrokerNoteItem).Note)%>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                        <br />
                        <%} if (buildingNotesNum > 0)
                              {%>
                        <div class="page-titles">
                            Building Description
                            <%if (buildingNotesNum > 1)
                              { %><%=GetLanguageDropDown("U", listingNoteLanguageArray)%><%} %></div>
                        <asp:ListView ID="BuildingNoteList" runat="server">
                            <LayoutTemplate>
                                <asp:Literal runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <div align="justify" class='buildingNote <%#(Container.DataItem as ListingBrokerNoteItem).Culture%>'
                                    style="display: none;">
                                    <%#SiteHelper.FormatWeb((Container.DataItem as ListingBrokerNoteItem).Note)%>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>

                        <%}
                          }
                          else if (!string.IsNullOrEmpty(Model.Description))
                          { %>
                          <div class="description page-titles">Description</div>
                          <%=SiteHelper.FormatWeb(Model.Description)%>
                        <%}%>
                    </div>
                </div>
                <!--Comment out-->
                <%-- <div class="map-container">
                    <div id="map_canvas">
                    </div>
                </div>--%>
                <div class="top">
                    <div class="nav page-titles clearfix">
                        <ul>
                            <li class="left first corners"><a class="googlemap cursor">Map</a> |&nbsp;</li>
                             <li class="left corners">
                            <a class="streetview cursor">&nbsp;Street View&nbsp;</a></li>
                             <li class="left last corners">
                            | <a class="poi cursor">Points of Interest</a></li>
                        </ul>
          
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="poi-container corners">
                    <div class="scroll-container">
                        <div class="scroll">
                            <div class="Gmap map-container-detail">
                               <div id="map_canvas" style="width:528px; height: 250px;">
</div>
                            </div>
                            <div class="streetview-container corners left">
                        <div id="pano" class="detail-street-view-window">bkjhk
    </div>
                    </div>
                            <div class="yelp-container">
                                <%=Html.RenderPartial("cntrls/Details/yelp",Model)%>
                            </div>
                        </div>
                    </div>
                </div>
                <%-- <div id="1" class="detail-street-view-window">
                </div>
                <olr:StreetView ID="streetview" runat="server" />
                <div class="streetview-wrapper">
                    <div class="page-titles">
                        Street View</div>
                    <div class="streetview-container corners left">
                        <div id="Div1" class="detail-street-view-window">
    </div>
                    </div>
                </div>--%>
            </div>
            <div class="listing-details clearfix right">
                <%=Html.RenderPartial("cntrls/Details/GeneralDetails", Model)%>
                <%=Html.RenderPartial("cntrls/Details/IncomePerAnnum", Model)%>
                <%=Html.RenderPartial("cntrls/Details/ExpensesPerAnnum", Model)%>
                <%=Html.RenderPartial("cntrls/Details/ApartmentFeatures", Model)%>
                <%=Html.RenderPartial("cntrls/Details/BuildingFeatures", Model)%>
                <%-- <%=Html.RenderPartial("cntrls/Details/FloorDescriptions", Model)%>--%>
                <div class="detail-container corners">
                    <div class="page-titles">
                        Contact
                    </div>
                    <%=Html.RenderPartial("cntrls/Details/ContactInfo", Model)%>
                </div>
                <%=Html.RenderPartial("cntrls/Details/IDXLogo", Model)%>
            </div>
        </div>
       <%-- <div class="recentlyviewed-wrapper">
            <div class="page-titles">
                Recently Viewed</div>
            <%=Html.RenderPartial("Cntrls/Details/recentViewed",recentlist) %>
        </div>--%>
    </div>
    <!--2 Columns Layout end-->
    <%--3 Columns layout--%>
    <%--    <div class="detail-page">
        <div class="prepend-1 top-detail clearfix">
            <div class="column name-column">
                <h2 class="display-address">
                    <%=Model.GetShowAddress(out withStreetAddress)%>
                    <%if (Model.GetShowAddress(out withStreetAddress) != Model.Neighborhood)
                      {%>
                    -
                    <%=Model.GetArea()%><%} %></h2>
                <%if (!withStreetAddress)
                  { %>
                <div class="cross-streets">
                    <%=Model.GetCrossStreets("[{0} & {1}]")%></div>
                <%} %>
                <div class="detail-open-house">
                    <%=Model.GetOpenHouseString()%></div>
            </div>
            <div class="column right detail-pager">
                <div>
                    <h2 class="webid">
                        Listing ID
                        <%=Model.ListingID%></h2>
                </div>
                <br>
                <div class="detail-paging">
                    <olr:Paging ID="Paging1" runat="server" />
                </div>
            </div>
        </div>
        <div class="layout-detail clearfix">
            <div class="image-viewer left">
                <olr:ImageViewer ID="ImageViewer2" runat="server" />
                <div class="toolbar-space">
                    <%=Html.RenderPartial("cntrls/Details/DetailToolbar",Model) %>
                </div>
                <div class="section listing-description">
                    <% if (!string.Equals(Model.DisplayAddress, Model.GetDisplayAddress()))
                       { %>
                    <div>
                        <b><%=Model.DisplayAddress%></b></div>
                        <br>
                    <% } %>
                    <%if (!string.IsNullOrEmpty(Model.Description))
                      { %>
                    <div class="page-titles">
                        Listing Description</div>
                    <div align="justify">
                        <%=SiteHelper.FormatWeb(Model.Description)%>
                    </div>
                    <%} %>
                </div>
                <%--<div class="map-container">
                    <div id="map_canvas">
                    </div>
                </div>--%>
    <%--                <div class="top">
                    <div class="nav page-titles">
                        <a class="streetview cursor">Street View</a> | <a class="googlemap cursor">Map</a> | <a class="poi cursor">Points of Interest</a>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="poi-container corners">
                    <div class="scroll-container">
                        <div class="scroll">
                            <div class="Gmap map-container-detail">
                                <%=Html.RenderPartial("cntrls/Details/Map", Model) %>
                            </div>
                            <div class="yelp-container">
                                <%=Html.RenderPartial("cntrls/Details/yelp",Model)%>
                            </div>
                        </div>
                    </div>
                </div>
    --%>
    <%-- <div id="panos" class="detail-street-view-window">
                </div>--%>
    <%--<olr:StreetView ID="streetview" runat="server" />--%>
    <%--                <div class="streetview-wrapper">
                    <div class="page-titles">
                        Street View</div>
                    <div class="streetview-container corners">
                        <div id="panos" class="detail-street-view-window">
                        </div>
                    </div>
                </div>
            </div>
            <div class="listing-details clearfix left">
                <%=Html.RenderPartial("cntrls/Details/GeneralDetails", Model)%>
                <%=Html.RenderPartial("cntrls/Details/IncomePerAnnum", Model)%>
                <%=Html.RenderPartial("cntrls/Details/ExpensesPerAnnum", Model)%>
                <%=Html.RenderPartial("cntrls/Details/ApartmentFeatures", Model)%>
                <%=Html.RenderPartial("cntrls/Details/BuildingFeatures", Model)%>--%>
    <%-- <%=Html.RenderPartial("cntrls/Details/FloorDescriptions", Model)%>--%>
    <%--            </div>
            <div class="listing-details detail-contact clearfix right">
            <div class="page-titles">
                Contact
            </div>
            <div class="detail-container corners">
                    <%=Html.RenderPartial("cntrls/Details/ContactInfo", Model)%>
                </div>
                <%=Html.RenderPartial("cntrls/Details/IDXLogo", Model)%>
             </div>   
        </div>
        <div class="recentlyviewed-wrapper">
            <div class="page-titles">
                Recently Viewed</div>
            <%=Html.RenderPartial("Cntrls/Details/recentViewed",recentlist) %>
        </div>
    </div>
    --%>
    <!--3 Column Layout ends-->
</asp:Content>
