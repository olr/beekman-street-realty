﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class listingsInBuilding : BasePage<listingsInBuilding, SearchOptions>
{
    protected string pType;
    protected string buildingID;
    protected override void OnInit(EventArgs e)
    {
        this.Controls.InitSortables(SortClicked);
        base.OnInit(e);
    }

    void SortClicked(object sender, CommandEventArgs e)
    {
        SearchOptions opts;
        if (TempStorage.TryGet<SearchOptions>("SearchOptions", out opts))
        {
            opts.OrderBy = fn.StringToEnum<SortOrder>(e.CommandName);
            opts.IsDescending = ((SortDirection)e.CommandArgument).Equals(SortDirection.Descending);
            opts.PageIndex = 0;
            RunSearch(opts);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        pType = Request.QueryString["type"];
        buildingID = Request.QueryString["buildingid"];
        if (!IsPostBack)
        {
            Model = new SearchOptions();

            if (Request.QueryString["type"] == "sale")
                Model.PropertyTypes.Add(PropertyType.Sale);
            else
                Model.PropertyTypes.Add(PropertyType.Rental);

            Model.BuildingID = Convert.ToInt32(buildingID);

            if (Model != null)
            {

                if (Request.QueryString["page"] != null)
                {
                    int page = fn.Get<int>(Request.QueryString["page"]);
                    if (page > 0)
                        Model.PageIndex = page - 1;
                }
                RunSearch(Model);
            }
            else
                Response.Redirect("~/search.aspx");

        }
    }

    void ReSort(SearchOptions options, string sortOrder)
    {
        var lastSortOrder = options.OrderBy;
        switch (sortOrder)
        {
            case "price": options.OrderBy = SortOrder.Price; break;
            case "size": options.OrderBy = SortOrder.Size; break;
            case "location": options.OrderBy = SortOrder.Address; break;
        }
        if (lastSortOrder.Equals(options.OrderBy))
            //reverse sort order
            options.IsDescending = !options.IsDescending;
        options.PageIndex = 0;
        TempStorage.Add("SearchOptions", options);
        Response.Redirect("~/listingsinbuilding.aspx");
    }

    void RunSearch(SearchOptions options)
    {
        if (options != null)
        {
            int _total = 0;
            Listings listings = null;
            if (options.PropertyTypes.Contains(PropertyType.Commercial))
                listings = ServiceLocator.GetComercialSearchService().RunSearch(options, out _total);
            else
                listings = ServiceLocator.GetSearchService().RunSearch(options, out _total);
            if (listings != null)
            {
                this.Controls.SetPagers(options.PageIndex + 1, options.PageSize, _total);
                this.Controls.DataBind<Listings>(listings);
                listings.CreatePagingSession(); //save paging info

            }
            TempStorage.Add("SearchOptions", options);
            this.Controls.DataBind<SearchOptions>(options);
        }
    }
}
