﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class newlistings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            SearchOptions options = new SearchOptions();
            options.PropertyTypes.Add(PropertyType.Sale);
            options.DateOnMarket = DateTime.Today.AddDays(-7);
            int total = 0;
            var newSales = ServiceLocator.GetSearchService().RunSearch(options, out total);
            ShortForm1.DataBind(newSales);
        }
    }
}