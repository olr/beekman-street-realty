﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="management.aspx.cs" Inherits="management" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="outside-shadow container-wrapper clearfix">
        <div class="agent-page">
            <div class="titles-global" style="margin-bottom: 20px;">
                Management
            </div>
            <div class="agent_result_container">
                <asp:ListView runat="server" ID="ManagerList">
                    <LayoutTemplate>
                        <ul class="agent-results">
                            <asp:Literal runat="server" ID="itemPlaceHolder" />
                        </ul>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li>
                            <div class="agent-container clearfix">
                                <div class="agent-thumb left">
                                    <a href="<%# GetDetail((Container.DataItem as BrokerInfo).ID)%>">
                                        <img class="agent_thumb_image" src='<%# Eval("PhotoUrl")%>' style="height: 147px;
                                            width: 105px;" alt="" />
                                    </a>
                                </div>
                                <div class="left">
                                    <div class="agent-info">
                                        <div>
                                            <a class="agent-name" href="<%# GetDetail((Container.DataItem as BrokerInfo).ID)%>">
                                                <%#Eval("FirstName") %></a>
                                        </div>
                                        <div class="agent-title">
                                            <%# Eval("Title")%>
                                        </div>
                                        <div class="agentphone">
                                            <%# Eval("WorkPhone")%>
                                        </div>
                                        <div class="agentphone">
                                            <%# Eval("MobilePhone")%>
                                        </div>
                                    </div>
                                    <div class="agent-tools">
                                        <div>
                                            <a class='email-me' href="<%# GetEmail((Container.DataItem as BrokerInfo).ID)%>">Email Me </a>
                                        </div>
                                        <div>
                                            <a class="vcard" href="<%# GetVcard((Container.DataItem as BrokerInfo).ID)%>">Download Contact </a>
                                        </div>
                                        <div>
                                            <a class="mylistings" href="<%# GetDetail((Container.DataItem as BrokerInfo).ID)%>">View My Listings
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </div>
    </div>
</asp:Content>
