﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net.Mail;

public partial class career_form : System.Web.UI.Page
{
    protected string sendSuccessful;
    protected Exception exception;
    //protected override void OnInit(EventArgs e)
    //{
    //    Submit.Click += new ImageClickEventHandler(Submit_Click);
    //}
   protected void Submit_Click(object sender, ImageClickEventArgs e)
    {
        if (Page.IsValid)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Job Application<br />");
            sb.AppendLine("----------------------------------------------------------------------------------------------------<br />");
            //sb.AppendFormat("I'm a: {0}<br />\n", relationship.SelectedValue);
            sb.AppendFormat("Name: {0}<br />\n", name.Text);
            sb.AppendFormat("Day Phone: {0}<br />\n", dphone.Text);
            sb.AppendFormat("Evening Phone: {0}<br />\n", ephone.Text);
            sb.AppendFormat("Email: {0}<br />\n", Email.Text);
            sb.AppendFormat("Current Occupation: {0} <br />\n", occupation.Text);
            sb.AppendFormat("License Status: {0}<br />\n", licence_status.SelectedValue);
           
            sb.AppendFormat("Message: <br />{0}<br />\n", message.Text);
            sb.AppendLine("----------------------------------------------------------------------------------------------------<br />");

            string toEmail = ConfigurationManager.AppSettings["defaultEmail"];
            if (!string.IsNullOrEmpty(toEmail))
            {
                MailMessage msg = new MailMessage("noreply@olr.com", toEmail);
                msg.ReplyToList.Add(toEmail);
                msg.Subject = "Website lead from http://www.olr.com - Career / Candidate inquiry.";
                msg.IsBodyHtml = true; 
                msg.Body = sb.ToString();
                try
                {
                    using (var client = new SmtpClient())
                    {
                        client.Send(msg);
                        formpanel.Visible = false;
                        sentpanel.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    exception = ex;
                    formpanel.Visible = false;
                    sentfailed.Visible = true;
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        sendSuccessful = "false";
    }
}