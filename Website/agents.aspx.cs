﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class agents : BasePage<agents, Brokers>
{
    protected override void OnInit(EventArgs e)
    {
        AgentSearchOptions options = new AgentSearchOptions();
        if (!string.IsNullOrEmpty(Request.QueryString["fn"]))
            options.FirstName = Request.QueryString["fn"];
        if (!string.IsNullOrEmpty(Request.QueryString["ln"]))
            options.LastName = Request.QueryString["ln"];
        if (!string.IsNullOrEmpty(Request.QueryString["agentname"]))
        {
            options.Name = Request.QueryString["agentname"];
        }
        if (!string.IsNullOrEmpty(Request.QueryString["language"]))
        {
            long l = Convert.ToUInt32(Request.QueryString["language"]);
            options.Languages.Add((LanguageType)l);
        }
        options.PageSize = 100;
        options.GetAll = true;
        options.OrderBy = SortOrder.LastName;
        int total;
        var agents = ServiceLocator.GetRepository()
                                   .GetBrokers(options, out total);
        if (agents != null)
        {
            Model = new Brokers();
            foreach (var a in agents)
                Model.Add(a);
        }
        base.OnInit(e);
    }
}