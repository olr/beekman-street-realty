﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class floorplan : BasePage<floorplan, ListingInfo>
{
    protected string print;
    protected void Page_Load(object sender, EventArgs e)
    {
        Model = this.GetListing() ?? new ListingInfo();
        if (!string.IsNullOrEmpty(Request.QueryString["print"]))
            if (Request.QueryString["print"] == "t")
                print = "true";
            else
                print = "false";

    }


}