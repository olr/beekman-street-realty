﻿<%@ Page Language="C#" MasterPageFile="~/Popup.master" AutoEventWireup="true" CodeFile="scheduleviewing.aspx.cs" Inherits="scheduleviewing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/overlay.css" rel="stylesheet" type="text/css" />
    <link href="css/calendrical.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="js/jquery.calendrical.js" type="text/javascript"></script>
    <style type="text/css">
        .date { width: 160px !important; margin-right: 10px; }
        .time { width: 106px !important; }
        .time-capsule { position: relative; }
        .time-capsule label { position: absolute; left: 5px; }
        .calendricalDatePopup { z-index: 100; }
        .calendricalTimePopup { z-index: 100; }
    </style>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $('input, textarea').bind('focus', function () {
                $(this).siblings('label').hide();
            });
            $('input, textarea').bind('blur', function () {
                if ($(this).val().length == 0)
                    $(this).siblings('label').show();
            });
            $('input, textarea').each(function (i) {
                if ($(this).val().length == 0)
                    $(this).siblings('label').show();
                else
                    $(this).siblings('label').hide();
            });

            $('.date').calendricalDate({usa:true});
            $('.time').calendricalTime();
        });
    </script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="overlay">
    <h1 class="title">Schedule A Viewing</h1>

    <div id="formpanel" runat="server">
        <form id="Form1" runat="server">
            <div class="w_popup_body">
                <h2>To: <%=Broker.FirstName %> <%=Broker.LastName%></h2>
                <h3>
                    <%if (Model.ShowAddress)
                      { %><%=Model.Address%><%}
                      else
                      { %><%=Model.Neighborhood%><%} %> | Listing ID <%=Model.ListingID %></h3>
                      <%--<div class="listingid"><%=Model.ListingID %></div>--%>
                <fieldset class="form">
                    <div class="section">
                        <label>Your Name</label>
                        <asp:TextBox ID="yourName" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="yourName" runat="server" ErrorMessage="*Please enter your name." CssClass="error-message-text" />
                    </div>
                    <div class="section">
                        <label>Your Email</label>
                        <asp:TextBox ID="yourEmail" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="yourEmail" runat="server" ErrorMessage="*Please enter your email." CssClass="error-message-text" />
                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="yourEmail" EnableClientScript="true"
                    ID="RegularExpressionValidator2" runat="server" ErrorMessage="*Please enter a valid email address." CssClass="invalid-message-text" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
       
                    </div>
                    <div class="section">
                        <label>Your Phone</label>
                        <asp:TextBox ID="yourPhone" runat="server" />
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="yourPhone" runat="server" ErrorMessage="*Please enter your phone number." CssClass="error-message-text"/>
                         <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="yourPhone"
                    EnableClientScript="true" ID="RegularExpressionValidator1" runat="server" ErrorMessage="*Please enter your correct phone number" CssClass="invalid-message-text"
                    ValidationExpression="1?\s*\W?\s*([2-9][0-8][0-9])\s*\W?\s*([2-9][0-9]{2})\s*\W?\s*([0-9]{4})(\se?x?t?(\d*))?" />
                    </div>
                    <div class="section">
                        <label>Your Move-In Date</label>
                        <asp:TextBox ID="moveinDate" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="moveinDate" runat="server" ErrorMessage="*Please enter your preferred move-in date." CssClass="error-message-text"/>
                    </div>
                    <div class="section">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <label>Preferred Date 1</label>
                                    <asp:TextBox ID="preferredDate1" runat="server" CssClass="date" />
                                </td>
                                <td>
                                    <div class="time-capsule">
                                        <label>Time</label>
                                        <asp:TextBox ID="preferredTime1" runat="server" CssClass="time" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="section">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <label>Preferred Date 2</label>
                                    <asp:TextBox ID="preferredDate2" runat="server" CssClass="date" />
                                </td>
                                <td>
                                    <div class="time-capsule">
                                        <label>Time</label>
                                        <asp:TextBox ID="preferredTime2" runat="server" CssClass="time" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="section">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <label>Preferred Date 3</label>
                                    <asp:TextBox ID="preferredDate3" runat="server" CssClass="date" />
                                </td>
                                <td>
                                    <div class="time-capsule">
                                        <label>Time</label>
                                        <asp:TextBox ID="preferredTime3" runat="server" CssClass="time" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="section">
                        <label>Your Message</label>
                        <asp:TextBox ID="messageText" runat="server" CssClass="textBox" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </fieldset>
                <div class="buttons">
                    <asp:Button ID="sendEmail" runat="server" Text="Send Email" />
                </div>
            </div>
        </form>
    </div>

    <div id="sentpanel" runat="server" visible="false">
            <div class="footer">
               <p>Thank You. Agent will be contact with you shortly to schedule a viewing.</p>
            </div>
        </div>
        <div id="sentfailed" runat="server" visible="false">
            <div class="footer">
                <p>
                    Sorry, your email sent failed.</p>
                <p>
                    <%=exception.ToString() %></p>
            </div>
        </div>

    </div>

</asp:Content>

