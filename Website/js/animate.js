﻿$(document).ready(function () {
    $(function () {
        var index = 0;
        $('#fwd').click(function () {
            if (index < 2) {
                $('.agent-photos').animate({ left: '-=942px' }, 2800, 'easeOutElastic');
                index++;
            }
        });
        $('#rwd').click(function () {
            if (index > 0) {
                $('.agent-photos').animate({ left: '+=942px' }, 2800, 'easeOutElastic');
                index--;
            }
        });
    })
});