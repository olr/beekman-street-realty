﻿/*
* jQuery hover map plugin v0.01
* Copyright (c) 2011 Erwin Tenebro
*/

(function ($) {
    $.fn.hovermap = function (options, arg) {

        if (options && typeof (options) == 'object') {
            options = $.extend({}, $.hovermap.defaults, options);
        }

        return this.each(function () {
            new $.hovermap(this, options, arg);
        });
    }; //end $.fn.hovermap

    $.hovermap = function (elem, options, arg) {
        var target = elem;
        var $target = $(target);
        var $area = $target.find('map area');

        if (options && typeof (options) == 'string') {
            var area = getArea(arg);
            if (area) { area.trigger(options); }
            return;
        }

        function getArea(regionName) {
            return $target.find("map area[alt=" + regionName + "]");
        }

        if ($area) {

            $target.css('position', 'relative');

            if (options.backgroundImage)
                $target.css('background', 'url(' + options.backgroundImage + ') no-repeat');

            //set width & height
            if (options.width > 0 && options.height > 0) {
                $target.css({ width: options.width + 'px', height: options.height + 'px' });
            }

            //set z-index
            $target.find('img.map').css({ position: 'relative', 'z-index': '30' })

            //hide images
            $target.find('.region').css({ display: 'none', position: 'absolute', 'z-index': '20' });

            $area.each(function (i, e) {
                var $this = $(this);
                var data = $this.metadata();
                if (data.hoverImage) {
                    var $html = $("<img src='images/clear.png' border='0' class='region' />")
                    $html.attr('src', data.hoverImage);
                    $html.css({ position: 'absolute', left: data.left + 'px', top: data.top + 'px' });
                    $html.addClass($this.attr('id') + options.mapClassExt).hide();
                    $html.addClass("{hoverImage:'" + data.hoverImage + "', selectedImage:'" + data.selectedImage + "'}");
                    $target.append($html);
                }

                var regionCheckbox = '.' + $(this).attr('id') + options.cbClassExt;
                $(regionCheckbox).bind('click', function () {
                    var IsChecked = $(this).attr('checked');
                    if (IsChecked) {
                        $this.trigger('select');
                    } else {
                        $this.trigger('unselect');
                    }
                });
                $(regionCheckbox).bind('toggle', function (event, checked) {
                    if (checked) {
                        $this.trigger('select');
                    } else {
                        $this.trigger('unselect');
                    }
                });
            });

            $(options.allManhattanCheckboxSelector).click(function () {
                var IsChecked = $(this).attr('checked');
                $('input.manhattan').trigger('toggle', [IsChecked]);
            });

            $(options.allBrooklynCheckboxSelector).click(function () {
                var IsChecked = $(this).attr('checked');
                $('input.brooklyn').trigger('toggle', [IsChecked]);
            });
            $area.mouseover(function () {
                var regionMap = '.' + $(this).attr('id') + options.mapClassExt;
                $(regionMap).fadeIn();
            }).mouseout(function () {
                var regionMap = '.' + $(this).attr('id') + options.mapClassExt;
                if (!$(regionMap).hasClass(options.selectedClass)) {
                    $(regionMap).fadeOut();
                }
            }).click(function () {
                var regionMap = '.' + $(this).attr('id') + options.mapClassExt;
                var regionCheckbox = '.' + $(this).attr('id') + options.cbClassExt;
                var data = $(this).metadata();
                if ($(regionMap).hasClass(options.selectedClass)) {
                    $(regionMap).attr('src', data.hoverImage);
                    $(regionMap).removeClass(options.selectedClass).fadeOut();
                    $(regionCheckbox).attr('checked', false);
                    $(options.allManhattanCheckboxSelector).attr('checked', false);
                    $(options.allBrooklynCheckboxSelector).attr('checked', false);
                } else {
                    $(regionMap).addClass(options.selectedClass).fadeIn();
                    $(regionMap).attr('src', data.selectedImage);
                    $(regionCheckbox).attr('checked', true);
                }
                return false;
            });

            $area.bind('select', function () {
                var regionMap = '.' + $(this).attr('id') + options.mapClassExt;
                var regionCheckbox = '.' + $(this).attr('id') + options.cbClassExt;
                var data = $(this).metadata();
                $(regionMap).addClass(options.selectedClass).fadeIn();
                $(regionMap).attr('src', data.selectedImage);
                $(regionCheckbox).attr('checked', true);
            });

            $area.bind('unselect', function () {
                var regionCheckbox = '.' + $(this).attr('id') + options.cbClassExt;
                var regionMap = '.' + $(this).attr('id') + options.mapClassExt;
                var data = $(this).metadata();
                $(regionMap).attr('src', data.hoverImage);
                $(regionMap).removeClass(options.selectedClass).fadeOut();
                $(regionCheckbox).attr('checked', false);
                $(options.allManhattanCheckboxSelector).attr('checked', false);
                $(options.allBrooklynCheckboxSelector).attr('checked', false);
            });

        } //end if (area)

    };

    /// default options
    $.hovermap.defaults = {
        mapClassExt: '-map',
        cbClassExt: '-checkbox',
        selectedClass: 'selected',
        backgroundImage: false,
        allManhattanCheckboxSelector: '.all-checkbox',
        allBrooklynCheckboxSelector: '.brooklyn-all-checkbox',
        width: 0,
        heigth: 0
    };

})(jQuery);

