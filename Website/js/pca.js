﻿// JavaScript Document
var LoanAmt;
var Rate;
var Period;
var Payment;
var DwnPay;
var Mort;
var Price;
var Taxes;
var MaintCC, MOP;

function stripChars(pattern, str) {
    return str.replace(pattern, "");
}

function CalcPayment(form) {

    var DisplayField = function() {
        form.txtPrice.value = FormatCurrency(Price.toString(), 0);
        form.txtMortgage.value = FormatCurrency(Mort.toString(), 0);
        form.txtTaxes.value = FormatCurrency(Taxes.toString(), 0);
        form.txtMaintCC.value = FormatCurrency(MaintCC.toString(), 0);
        form.txtMOP.value = FormatCurrency(MOP.toString(), 0);
    };

    // This calculates the payment amount from the other three values

    // Assign the values from the form


    DwnPay = stripChars(/\%|,/g, form.txtDown.value);

    Price = stripChars(/\$|,/g, form.txtPrice.value);

    Mort = parseInt(Price * (1 - (DwnPay / 100)));


    //LoanAmt = stripChars(/\$|,/g, form.txtMortgage.value);
    LoanAmt = Mort;

    Rate = parseFloat(form.txtMortgageInterestRate.value);

    Period = parseInt(form.txtMortgageLength.value) * 12;

    // Validate the values that are coming from the form
    if (isNaN(LoanAmt)) { alert("Missing Loan Amount"); return; }
    if (isNaN(Rate)) { alert("Missing Interest Rate."); form.txtMortgageInterestRate.focus(); return; }
    if (isNaN(Period)) { alert("Missing Period Value"); return; }

    // The Rate is the yearly interest rate -- adjust to make a monthly rate.
    //   Also, the rate is a percentage, so divide by 100 to make the usable
    //   amount;
    if (Rate >= 1) { Rate /= 100; }
    Rate /= 12;

    // Calculate the payment based on the other three amounts
    Payment = LoanAmt *
               (Rate * Math.pow(1 + Rate, Period)) /
                (Math.pow(1 + Rate, Period) - 1);

    // Format the value by multiplying by 100, rounding the number, then
    //   dividing that result to get two decimal places
    Payment = Math.ceil(Payment * 100) / 100;
    form.txtMonthlyMortgage.value = FormatCurrency(Payment.toString(), 2);

    Taxes = stripChars(/\$|,/g, form.txtTaxes.value) * 1;
    MaintCC = stripChars(/\$|,/g, form.txtMaintCC.value) * 1;



    if (Taxes == "")
        Taxes = 0;
    if (MaintCC == "")
        MaintCC = 0;

    MOP = Payment + Taxes + MaintCC;



    var nMaintCCDeduct = form.txtDeductionPercent.value / 100 * MaintCC;

    var nAdjMortgageInterest = 0;
    if (Mort > 1000000)
        nAdjMortgageInterest = Rate * 1000000;  //only the first $1M of the mortgage may be tax deductible



    var nLostInterest = parseInt(Price * (0 / 100)) * Rate;

    //var nTaxBracket = document.all.txtTaxBracket.value;
    var nTaxBracket = stripChars(/\%|,/g, form.txtTaxBracket.value);

    var nMortgageInterest = Mort * Rate;


    if (nAdjMortgageInterest == 0)
        nTotalTaxReductions = nMortgageInterest + nMaintCCDeduct + Taxes;
    else
        nTotalTaxReductions = nAdjMortgageInterest + nMaintCCDeduct + Taxes;


    nTotalReduction = nTotalTaxReductions + nLostInterest;

    if (form.txtTaxBracket.value != "")  //Added by guang 2/26/99
        form.txtMonthlyAfterTax.value = FormatCurrency(nTaxBracket * nTotalReduction / 100, 2);

    form.txtNetCost.value = FormatCurrency(MOP - (nTaxBracket * nTotalReduction / 100), 2)


    DisplayField();

    return;

}


function FormatCurrency(num, dec) {
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
        num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();
    if (cents < 10)
        cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + ',' +
num.substring(num.length - (4 * i + 3));
    if (dec > 0)
        return (((sign) ? '' : '-') + '$' + num + '.' + cents);
    else
        return (((sign) ? '' : '-') + '$' + num);
}

function ValToMoney(Val, ConverMethod) {
    // Format the passed value as a float with two decimal places
    var FmtVal;
    var DecPos;

    // If the conversion method is 1, use the ceil() method to convert
    //   the value and not the round()
    if (ConverMethod == 1) {
        FmtVal = (Math.ceil(Val * 100) / 100).toString();
    }
    else {
        FmtVal = (Math.round(Val * 100) / 100).toString();
    }

    // Make sure it has two decimal places after the decimal point
    DecPos = FmtVal.indexOf('.');
    if (DecPos == -1) { FmtVal += ''; }
    else if (DecPos == FmtVal.length - 1) { FmtVal += '00'; }
    else if (DecPos == FmtVal.length - 2) { FmtVal += '0'; }

    return FmtVal;
}
