﻿$(function () {
    $('input[name=PropertyType]').bind('click', toggleSearchType);
});

var toggleSearchType = function (e) {
    var show = $(e.currentTarget).attr('value');
    //update the price range dropdown
    var proxy = new ServiceProxy('Service.svc/');
    proxy.invoke('RenderPriceRange', { saleRental: show }, function (result) {
        $('#dynamic-price-range').html(result.Content);
    });
    proxy.invoke('RenderOwnership', { saleRental: show }, function (result) {
        $('#dynamic-ownership').html(result.Content);
    });
    proxy.invoke('RenderApartmentFeatures', { saleRental: show }, function (result) {
        $('#dynamic-aptfeatures').html(result.Content);
    });
    proxy.invoke('NoFee', { saleRental: show }, function (result) {
        $('#dynamic-nofee').html(result.Content);
    });
    //proxy.invoke('RenderCommercialForm', { saleRental: show }, function (result) {
        //$('#dynamic-commercial-options').html(result.Content);
    //});

};
