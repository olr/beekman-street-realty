﻿$(function () {
    $('input[name=PropertyType]').bind('click', toggleSearchType);
});

var toggleSearchType = function (e) {
    var show = $(e.currentTarget).attr('value');
    //update the price range dropdown
    var proxy = new ServiceProxy('Service.svc/');
    proxy.invoke('RenderPriceRange', { saleRental: show }, function (result) {
        $('#pricerange').html(result.Content);
        $('div.page-titles').attr('style', "display:none");
    });
};
