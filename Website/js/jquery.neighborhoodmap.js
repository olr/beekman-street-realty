﻿/*
* jQuery hover map plugin v0.01
* Copyright (c) 2011 Erwin Tenebro
*/

(function ($) {
    $.fn.neighborhoodmap = function (options, arg) {

        if (options && typeof (options) == 'object') {
            options = $.extend({}, $.neighborhoodmap.defaults, options);
        }

        return this.each(function () {
            new $.neighborhoodmap(this, options, arg);
        });
    }; //end $.fn.neighborhoodmap


    $.neighborhoodmap = function (elem, options, arg) {
        var target = elem;
        var $target = $(target);
        var $area = $target.find('map area');

        if (options && typeof (options) == 'string') {
            var area = getArea(arg);
            if (area) { area.trigger(options); }
            return;
        }

        function getArea(regionName) {
            return $target.find("map area[alt=" + regionName + "]");
        }

        if ($area) {

            $target.css('position', 'relative');

            if (options.backgroundImage)
                $target.css('background', 'url(' + options.backgroundImage + ') no-repeat');

            //set width & height
            if (options.width > 0 && options.height > 0) {
                $target.css({ width: options.width + 'px', height: options.height + 'px' });
            }

            //set z-index
            $target.find('img.map').css({ position: 'relative', 'z-index': '100' })

            //hide images
            $target.find('.region').css({ display: 'none', position: 'absolute', 'z-index': '20' });

            $area.each(function (i, e) {
                var $this = $(this);
                var data = $this.metadata();
                if (data.hoverImage) {
                    var $html = $("<img src='img/map/clear.png' border='0' class='region' />")
                    $html.attr('src', data.hoverImage);
                    $html.css({ position: 'absolute', left: data.left + 'px', top: data.top + 'px' });
                    $html.addClass($this.attr('id') + options.mapClassExt).hide();
                    $html.addClass("{hoverImage:'" + data.hoverImage + "', selectedImage:'" + data.selectedImage + "'}");
                    $target.append($html);
                }
            });

            $area.mouseover(function () {
                var regionMap = '.' + $(this).attr('id') + options.mapClassExt;
                $(regionMap).css('display', 'inline');
            }).mouseout(function () {
                var regionMap = '.' + $(this).attr('id') + options.mapClassExt;
                if (!$(regionMap).hasClass(options.selectedClass)) {
                    $(regionMap).css('display', 'none');
                }
            })
            /*** start of added by Steve ***/
            $area.click(function () {
                //alert("clicked");
                var regionMap = '.' + $(this).attr('id') + options.mapClassExt;
                //alert(regionMap);
                $area.each(function () {
                    var areaMap = '.' + $(this).attr('id') + options.mapClassExt;
                    if ($(areaMap).hasClass(options.selectedClass)) {
                        $(areaMap).removeClass(options.selectedClass);
                        $(areaMap).css('display', 'none');    
                    }
                });                
                $(regionMap).addClass(options.selectedClass);
                $(regionMap).css('display', 'inline');
            });
            /**** end of added by Steve ***/

            $area.bind('unselect', function () {
                var regionMap = '.' + $(this).attr('id') + options.mapClassExt;
                var data = $(this).metadata();
                $(regionMap).attr('src', data.hoverImage);
                $(regionMap).removeClass(options.selectedClass).css('display', 'none');
            });

        } //end if (area)

    };

    /// default options
    $.neighborhoodmap.defaults = {
        mapClassExt: '-map',
        cbClassExt: '-checkbox',
        selectedClass: 'selected',
        backgroundImage: false,
        width: 0,
        heigth: 0
    };

})(jQuery);

