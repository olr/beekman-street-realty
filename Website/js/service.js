﻿function ServiceProxy(serviceUrl) {
    var _I = this;
    this.serviceUrl = serviceUrl;
    // *** Call a wrapped object
    this.invoke = function (method, data, callback, error, bare) {
        // *** Convert input data into JSON - REQUIRES Json2.js
        var json = JSON.stringify(data);
        // *** The service endpoint URL        
        var url = _I.serviceUrl + method;
        $.ajax({
            url: url,
            data: json,
            type: "POST",
            processData: true,
            contentType: "application/json",
            timeout: 10000,
            dataType: "json",
            success:
                    function (result) {
                        if (!callback) return;
                        if (bare)
                        { callback(result); return; }
                        for (var property in result) {
                            callback(result[property]);
                            break;
                        }
                    },
            error: function (xhr) {
                if (!error) return;
                if (xhr.responseText) {
                    var err = JSON.parse(xhr.responseText);
                    if (err)
                        error(err);
                    else
                        error({ Message: "Unknown server error." })
                }
                return;
            }
        });
    }
}