﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class docfolio : BasePage<docfolio, UserCategories>
{
    protected BrokerInfo broker;
    protected UserCategories categories;
    protected bool isAdmin = false;
    protected string searchTitle = "";
    protected int brokerID=0;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!string.IsNullOrEmpty(Request.QueryString["admin"]))
            isAdmin = true;
        if (Session["Agent"] == null)
        {
            Response.Redirect("default.aspx");
        }
        else
        {
            brokerID = Convert.ToInt32(Session["Agent"]);
            //brokerID = 47880;
            isAdmin =ServiceLocator.GetRepository().IsAdmin(brokerID);
            broker = ServiceLocator.GetRepository().GetBrokerByID(brokerID);
            searchTitle = Request.QueryString["title"];
            //if (!string.IsNullOrEmpty(searchTitle))
            //    categories = ServiceLocator.GetRepository().SearchCategories(searchTitle);
            //else
            //    categories = ServiceLocator.GetRepository().GetAllCategories();
        }
    }
}