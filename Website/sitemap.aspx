﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="sitemap.aspx.cs" Inherits="sitemap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages sitemap guides">
        <div class="titles-global" style="margin-bottom: 20px;">
            site map
        </div>
        <div class="sitemap-column left">
            <ul>
                <li class="main-title"><a target="_blank" href="default.aspx">home</a></li>
            </ul>
            <ul>
                <li class="main-title">search</li>
                <li><a target="_blank" href="search.aspx">sales</a></li>
                <li><a target="_blank" href="search.aspx?type=rent">rentals</a></li>
                <li><a class="vow" target="_blank" href="vow_popup.aspx">vip search</a></li>
                <li><a target="_blank" href="vow.aspx">VOW</a></li>
                <li><a target="_blank" href="commercial_result.aspx">commercial</a></li>
                <li><a target="_blank" href="alllistings.aspx">all listings</a></li>
            </ul>
            <ul>
                <li class="main-title">open houses</li>
                <li><a target="_blank" href="open_house.aspx?type=sale">sales</a></li>
                <li><a target="_blank" href="open_house.aspx?type=rent">rentals</a></li>
            </ul>
            <ul>
                <li class="main-title">featured listings</li>
                <li><a target="_blank" href="featured_listings.aspx">sales</a></li>
                <li><a target="_blank" href="featured_listings.aspx?type=rent">rentals</a></li>
            </ul>
            <ul>
                <li class="main-title">new developments</li>
                <li><a target="_blank" href="FeaturedBuildings.aspx">featured buildings</a></li>
                <li><a target="_blank" href="buildingdetail.aspx?id=15107">featured buildings detail</a></li>
            </ul>
            <ul>
                <li class="main-title">agents</li>
                <li><a target="_blank" href="agents.aspx">agents search</a></li>
                <li><a target="_blank" href="agent_detail.aspx?agentid=47880">agents detail</a></li>
            </ul>
            <ul>
                <li class="main-title">recent transaction</li>
                <li><a target="_blank" href="recenttransaction2.aspx?type=sale">sales</a></li>
                <li><a target="_blank" href="recenttransaction2.aspx?type=rent">rentals</a></li>
            </ul>
        </div>
        <div class="sitemap-column left">
           <ul>
                <li class="main-title">about</li>
                <li><a target="_blank" href="about.aspx">our company</a></li>
                <li><a target="_blank" href="career_form.aspx" class="career-form">careers</a></li>
            </ul>
            <ul>
                <li class="main-title">resources</li>
                <li><a target="_blank" href="buyers_guide.aspx">buyer's guide</a></li>
                <li><a target="_blank" href="renters_guide.aspx">renter's guide</a></li>
                <li><a target="_blank" href="international.aspx">foreign investors</a></li>
                <li><a target="_blank" href="neighborhood_guide.aspx">neighborhood guide (option 1)</a></li>
                <li><a target="_blank" href="neighborhood_guide2.aspx">neighborhood guide (option 2)</a></li>
                <li><a target="_blank" href="glossary.aspx">glossay & terms</a></li>
            </ul>
            <ul>
                <li class="main-title"><a target="_blank" class="listwithus-pop" href="list_with_us.aspx">list with us</a></li>
            </ul>
            <ul>
                <li class="main-title"><a target="_blank" href="contact.aspx">contact</a></li>
            </ul>
            <ul>
                <li class="main-title"><a target="_blank" href="press.aspx">press</a></li>
            </ul>
            <ul>
                <li class="main-title"><a target="_blank" href="testimonial.aspx">testimonials</a></li>
            </ul>
            <ul>
                <li class="main-title"><a target="_blank" href="relocation.aspx">relocation</a></li>
            </ul>
            <ul>
                <li class="main-title"><a target="_blank" class="agent-login" href="docfolioLogin.aspx">docfolio</a></li>
            </ul>
             <ul>
                <li class="main-title"><a target="_blank" href="customer_registration.aspx">customer registration</a></li>
            </ul>
        </div>
        <div class="sitemap-column left">
            <ul>
                <li class="main-title">listings detail tools</li>
                <li><a class="" href="detail.aspx?id=965976">photo</a></li>
                <li><a class="" href="detail.aspx?id=965976">OLR Max photo</a></li>
                <li><a class="" href="detail.aspx?id=965976">floorplan</a></li>
                <li><a class="" href="detail.aspx?id=965976">print listing</a></li>
                <li><a class="" href="detail.aspx?id=965976">video</a></li>
                <li><a class="emailfriend tip" href="email_to_a_friend.aspx">email listing / email to a friend</a></li>
                <li><a class="" href="detail.aspx?id=965976">map</a></li>
                <li><a class="resize tip" href="mortgage_calculator.aspx">mortgage calculator</a></li>
                <li><a class="" href="detail.aspx?id=965976">share this</a></li>
                <li><a class="" href="detail.aspx?id=965976">download contact</a></li>
            </ul>
            <ul>
                <li class="main-title">all popup forms</li>
                <li><a class="agent-login tip" href="agent_login_popup.aspx">agents login</a></li>
                <li><a class="email-me" href="email_agent.aspx">email agent (option 1)</a></li>
                <li><a class="emailform" href="email_form.aspx">email agent (option 2)</a></li>
                <li><a class="emailfriend tip" href="email_to_a_friend.aspx">email listing / email to a friend</a></li>
                <li><a class="" href="detail.aspx?id=965976">schedule a viewing</a></li>
                <li><a class="career-form" href="career_form.aspx">career form</a></li>
            </ul>
            <ul>
                <li class="main-title">2 columns layout</li>
                <li><a target="_blank" href="detail.aspx?id=1045501">sales</a></li>
                <li><a target="_blank" href="detail.aspx?id=965985">rentals</a></li>
                <li><a target="_blank" href="detail.aspx?id=42806TH">townhouse</a></li>
            </ul>
            <ul>
                <li class="main-title">3 columns layout</li>
                <li><a target="_blank" href="http://base3.olrdev.com/detail.aspx?id=1045501">sales</a></li>
                <li><a target="_blank" href="http://base3.olrdev.com/detail.aspx?id=965985">rentals</a></li>
                <li><a target="_blank" href="http://base3.olrdev.com/detail.aspx?id=42806TH">townhouse</a></li>
            </ul>
        </div>
    </div>
</asp:Content>
