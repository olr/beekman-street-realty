﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="contact.aspx.cs" Inherits="contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("img#nav6").attr("src", "images/menu/menu_selected_07.png");
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages clearfix">
        <div class="titles-global" style="margin-bottom:20px;">
       contact us
    </div>
        <div class="company-info-container span-8">
            <div class="contact-text">
            <div class="company-title">Beekman <span>Street</span> Realty</div>
            <p>70 West 93rd Street, Suite 2</p>
            <p> New York, NY 10025</p>
            <br>
            <p>P: 212-624-6714</p>
            <p>F: 212-624-6712</p>
            <p><b>info@beekmanstreetrealty.com
</b></p>
<%--            <p>
                <%=company.Address %>
            </p>
            <p>
                <%=showPhone() %><br />
                <%=showFax() %><br />
                <%=showEmail() %>
            </p>--%>
            </div>
<%--            <div>
                <img src="images/contact-img.jpg" width="250" />
            </div>--%>
            <div class="contact-map corners">
           <img src="images/contact-map.png"/>
        </div>
        </div>

        <div class="contact-container span-16 last">
            <olr:ContactUs id="ContactUs1" runat="server" />
        </div>

    </div>
</asp:Content>
