﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net.Mail;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Configuration;

public partial class email_agent : System.Web.UI.Page
{
    protected BrokerInfo Model = new BrokerInfo();
    protected Exception exception;
    protected ListingInfo listing;

    protected override void OnInit(EventArgs e)
    {
        sendEmail.Click += new EventHandler(Send_Email);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int id = fn.Get<int>(Request.QueryString["id"]);
            if (id > 0)
            {
                Model = ServiceLocator.GetRepository().GetBrokerByID(id);
            }
            else
            {
                Model = new BrokerInfo();
                Model.FirstName = ConfigurationManager.AppSettings["SiteName"];
                Model.Email = ConfigurationManager.AppSettings["defaultEmail"];
            }
            if (Model != null)
            {
                this.Controls.DataBind<BrokerInfo>(Model);
                listing = GetListing();
                if (listing != null)
                {
                    //subjectEmail.Text = "Request more info: " + listing.GetDisplayAddress();
                    messageText.Text = "Please contact me about the following Web ID: " + listing.ListingID;
                }

                this.DataBind(true);
            }
        }
    }

    ListingInfo GetListing()
    {
        ListingInfo l = null;
        string lid = Request.QueryString["listingid"];
        if (!string.IsNullOrEmpty(lid))
            l = ServiceLocator.GetRepository().GetListingInfoByID(lid);
        return l;
    }

    void Send_Email(object sender, EventArgs e)
    {
        int id = fn.Get<int>(Request.QueryString["id"]);
        Model = ServiceLocator.GetRepository().GetBrokerByID(id);
        if (IsValid && Model != null &&  !string.IsNullOrEmpty(Model.Email))
        {
            StringBuilder body = new StringBuilder();

            body.AppendLine("<div class='overlay_container' style='background-color: #ffffff; color: #2D2D2D; font-family: Arial; font-size: 12px;line-height: 1.2em;'>");
            body.AppendFormat("<img src='http://{0}/images/PRINT-Logo.jpg' style='width:176px; border:none;' />", Request.Url.Host);
            body.AppendFormat("<div>{0}: {1} {2}</div>", Resources.Email.ToLabel, Model.FirstName, Model.LastName);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientNameEmailLabel, yourName.Text);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientEmailEmailLabel, yourEmail.Text);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientPhoneEmailLabel, yourPhone.Text);
            body.AppendFormat("<div>My Move-In Date: {0}</div>", moveinDate.Text);
            //body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.SubjectLabel, subjectEmail.Text);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientMessageEmailLabel, messageText.Text);
            if (!string.IsNullOrEmpty(Request.QueryString["listingid"]))
                body.AppendFormat("<a href='http://{1}/detail.aspx?id={0}'>Click here to view this listing on your website.</a>", Request.QueryString["listingid"], Request.Url.Host);
            body.AppendLine("</div>");

            MailMessage message = new MailMessage(yourEmail.Text, Model.Email);
            if (listing != null && !string.IsNullOrEmpty(listing.ListingID))
                message.Subject = "Website lead from http://www.beekmanstreetrealty.com - Listing ID#" + listing.ListingID;
            else
                message.Subject = "Website lead from http://www.beekmanstreetrealty.com";
            message.Body = body.ToString();
            message.IsBodyHtml = true; 
            SmtpClient client = new SmtpClient();
            try
            {
                client.Send(message);
                formpanel.Visible = false;
                sentpanel.Visible = true;
            }
            catch (Exception ex)
            {
                exception = ex;
                formpanel.Visible = false;
                sentfailed.Visible = true;
            }
        }
    }

}
