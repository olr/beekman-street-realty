﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Configuration;
using System.Text;

public partial class detail : BasePage<detail, ListingInfo>
{
    public bool withStreetAddress = false;
    protected string Address { get; set; }
    protected string APIKey { get; set; }
    protected double Longitude { get; set; }
    protected double Latitude { get; set; }
    protected bool isExclusive = false;
    protected bool showAddress = false;
    protected ListingBrokerNotes notes;
    protected List<RecentlyViewListings> recentlist;
    protected string[] listingNoteLanguageArray;
    protected int listingNotesNum = 0, buildingNotesNum = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] == null && Request.QueryString["textfield"] == null)
            Response.Redirect("search.aspx");


        if (!TempStorage.TryGet<List<RecentlyViewListings>>("RecentlyViewed", out recentlist))
            recentlist = new List<RecentlyViewListings>();

        if (!Page.IsPostBack)
        {
            string id = Request.QueryString["id"] ?? Request.QueryString["textfield"];
            notes = ServiceLocator.GetRepository().GetNotesByID(id);
            var nList = (from note in notes
                         where note.NoteType == 'P'
                         orderby note.NoteType
                         select note).ToList();
            listingNoteLanguageArray = (from note in nList
                                        select note.Culture).ToArray();
            Notelist.DataSource = nList;
            Notelist.DataBind();
            var buildingList = (from note in notes
                                where note.NoteType == 'U'
                                select note).ToList();
            string[] buildingNoteLanguageArray = (from note in buildingList
                                                  select note.Culture).ToArray();
            listingNotesNum = nList.Count;
            buildingNotesNum = buildingList.Count;
            BuildingNoteList.DataSource = buildingList;
            BuildingNoteList.DataBind();
            Model = ServiceLocator.GetRepository().GetListingInfoByID(id);
            if (Model != null)
            {
                this.Controls.DataBind<ListingInfo>(Model);
                if (recentlist.Find(r => r.ListingId == Model.ListingID) == null)
                    recentlist.Add(new RecentlyViewListings { ListingId = Model.ListingID, BuildingId = Model.BuildingID });
                TempStorage.Add("RecentlyViewed", recentlist);
                if (!Model.IsCommercialListing)
                {
                    isExclusive = Model.IsExclusive;
                    showAddress = Model.ShowAddress;
                    var building = Model.GetBuilding();
                    if (building != null)
                    {
                        BrokerTemplate.Web.LocationPoint l = SiteHelper.GeocodeAddress(Model.BuildingID);
                        if (l.Longitude != 0)
                        {
                            Latitude = l.Latitude;
                            Longitude = l.Longitude;
                        }
                        else
                        {
                             l = new LocationPoint { Address = string.Format("{0}, {1}, {2} {3}", Model.Address.Replace(' ', '-'), Model.City.Replace(' ', '-'), Model.State.Replace(' ', '-'), Model.Zip) };

                            if (SiteHelper.GeocodeAddress(l))
                            {
                                Latitude = l.Latitude;
                                Longitude = l.Longitude;
                            }
                        }
                    }
                }
                else
                {
                    CommercialInfo cl = (CommercialInfo)Model;
                    isExclusive = cl.IsExclusive;
                    showAddress = cl.ShowAddress;
                    LocationPoint l = new LocationPoint { Address = string.Format("{0}, {1}, {2} {3}", cl.Address.Replace(' ', '-'), cl.City.Replace(' ', '-'), cl.State.Replace(' ', '-'), cl.Zip) };

                    if (SiteHelper.GeocodeAddress(l))
                    {
                        Latitude = l.Latitude;
                        Longitude = l.Longitude;
                    }
                }
            }
            else
                Response.Redirect("~/default.aspx");
        }
    }
    protected string GetLanguageDropDown(string noteType, string[] values)
    {
        StringBuilder sb = new StringBuilder("<select class='" + noteType + "'>");
        if (values.Contains("en"))
            sb.AppendFormat("<option value='{0}' >{1}</option>", "en", SiteHelper.GetCulture("en"));
        foreach (var value in values)
        {
            if (value != "en")
                sb.AppendFormat("<option value='{0}' >{1}</option>", value, SiteHelper.GetCulture(value));
        }
        sb.Append("</select>");
        return sb.ToString();
    }
}
