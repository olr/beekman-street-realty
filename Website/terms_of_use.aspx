﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="terms_of_use.aspx.cs" Inherits="terms_of_use" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages clearfix">
        <div class="titles-global" style="margin-bottom: 20px;">
            Terms and Conditions of Use
        </div>
        <div class="text clearfix">
            <p>
                <span>WHAT YOU SHOULD KNOW </span>
            </p>
            <p>
                Welcome to the beekmanstreetrealty.com web site (the "Web Site"). The goal of this
                Web Site is to provide you with access to the most comprehensive network of residential
                real estate products/services and related links to meet your needs (the “Content”).
                Please read our Terms of Use (the "Terms") carefully before continuing on with your
                use of this Web Site. These Terms shall govern the use of the Web Site and apply
                to all Internet traffic visiting the Web Site. By accessing or using this Web Site,
                you agree to the Terms. The Terms are meant to protect all of our Web Site visitors
                and your use of this Web Site signifies your agreement with these Terms.<b> IF YOU
                DO NOT AGREE WITH THESE TERMS, DO NOT USE THIS WEB SITE. Beekman Street Realty LLC</b>
                ( “Beekman Street Realty LLC”, “We”, “Us”, “Our”) reserves the right, in its sole
                discretion, to modify, alter or otherwise update these Terms at any time. Such modifications
                shall be effective immediately upon posting. By using this Web Site after we have
                posted notice of such modifications, alterations or updates you agree to be bound
                by such revised Terms.
            </p>
            <p>
                In accordance with our goals, this Web Site may permit you to link to many other
                web sites, that may or may not be affiliated with this Web Site and/or Us, and that
                may have terms of use that differ from, or contain terms in addition to, the terms
                specified here. Your access to such web sites through links provided on this Web
                Site is governed by the terms of use and policies of those sites, not this Web Site.
            </p>
            <p>
                <span>TRADEMARKS, COPYRIGHTS AND RESTRICTIONS</span></p>
            <p>
                This Web Site is controlled and operated by Beekman Street Realty LLC, 70 West 93rd
                Street, Ground Floor, New York, New York, 10025, (212) 624-6714. All content on
                this Web Site, including, but not limited to text, images, illustrations, audio
                clips, and video clips, is protected by copyrights, trademarks, service marks, and/or
                other intellectual property rights (which are governed by U.S. and worldwide copyright
                laws and treaty provisions, privacy and publicity laws, and communication regulations
                and statutes), and are owned and controlled by Us or its affiliates, or by third
                party content providers, merchants, sponsors and licensors (collectively "Providers")
                that have licensed their content or the right to market their products and/or services
                to Us. Content on this Web Site or any web site owned, operated, licensed or controlled
                by the Providers is solely for your personal, non-commercial use. You may print
                a copy of the Content and/or information contained herein for your personal, non-commercial
                use only, but you may not copy, reproduce, republish, upload, post, transmit, distribute,
                and/or exploit the Content or information in any way (including by e-mail or other
                electronic means) for commercial use without the prior written consent of Us or
                the Providers. You may request consent by faxing a request to Beekman Street Realty
                LLC Legal Department at 212-624-6712. Without the prior written consent of Us or
                the Providers, your modification of the Content, use of the Content on any other
                web site or networked computer environment, or use of the Content for any purpose
                other than personal, non-commercial use, violates the rights of the owners of beekmanstreetrealty.com
                and/or the Provider copyrights, trademarks or service marks and other proprietary
                rights, and is prohibited. As a condition to your use of this Web Site, you warrant
                to Us that you will not use our Web Site for any purpose that is unlawful or prohibited
                by these Terms, including without limitation the posting or transmitting any threatening,
                libelous, defamatory, obscene, scandalous, inflammatory, pornographic, or profane
                material. If you violate any of these Terms, your permission to use our Web Site
                immediately terminates without the necessity of any notice. Us retains the right
                to deny access to anyone at its discretion for any reason, including for violation
                of these Terms. You may not use on your web site any trademarks, service marks or
                copyrighted materials appearing on this Web Site, including but not limited to any
                logos or characters, without the express written consent of the owner of the mark
                or copyright. You may not frame or otherwise incorporate into another web site any
                of the Content or other materials on this Web Site without prior written consent
                of Us.
            </p>
            <p>
                <span>PROHIBITED ACTIVITIES </span>
            </p>
            <p>
                You are specifically prohibited from any use of this Web Site, and You agree not
                to use or permit others to use this Web Site, for any of the following: (a) take
                any action that imposes an unreasonable or disproportionately large load on the
                Web Site's infrastructure, including but not limited to "spam" or other such unsolicited
                mass e-mailing techniques; (b) disclose to, or share with, the assigned confirmation
                numbers and/or passwords with any unauthorized third parties or using the assigned
                confirmation numbers and/or passwords for any unauthorized purpose; (c) attempt
                to decipher, decompile, disassemble or reverse engineer any of the software or HTML
                code comprising or in any way making up a part of this Web Site; (d) upload, post,
                emailing or otherwise transmitting any information, Content, or proprietary rights
                that You do not have a right to transmit under any law or under contractual or fiduciary
                relationships; (e) violating any applicable local, state, national or international
                law, including, but not limited to, any regulations having the force of law; and,
                (f) using any robot, spider, intelligent agent, other automatic device, or manual
                process to search, monitor or copy Our Web pages, or the Content without Our prior
                written permission, provided that generally available third party Web browser such
                as Netscape Navigator® and Microsoft Internet Explorer® may be used without such
                permission.
            </p>
            <p>
                <span>LINKS </span>
            </p>
            <p>
                This Web Site may contain links to other web sites ("Linked Sites"). The Linked
                Sites are provided for your convenience and information only and, as such, you access
                them at your own risk. The content of any Linked Sites are not under Our control,
                and We are not responsible for, and do not endorse, such content, whether or not
                We are affiliated with the owners of such Linked Sites. You may not establish a
                hyperlink to this Web Site or provide any links that state or imply any sponsorship
                or endorsement of your web site by Us, or our affiliates or Providers.
            </p>
            <p>
                <span>DISCLAIMER OF WARRANTIES AND LIABILITY </span>
            </p>
            <p>
                ALL CONTENT ON THIS WEB SITE IS PROVIDED "AS IS" AND WITHOUT WARRANTIES OF ANY KIND
                EITHER EXPRESS OR IMPLIED. OTHER THAN THOSE WARRANTIES WHICH, UNDER THE U.S. LAWS
                APPLICABLE TO THESE TERMS, ARE IMPLIED BY LAW AND ARE INCAPABLE OF EXCLUSION, RESTRICTION,
                OR MODIFICATION, Beekman Street Realty LLC DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS
                OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY
                AND FITNESS FOR A PARTICULAR PURPOSE. NEITHER Beekman Street Realty LLC, ITS AFFILIATED
                OR RELATED ENTITIES, NOR THE PROVIDERS, NOR ANY PERSON INVOLVED IN THE CREATION,
                PRODUCTION, AND DISTRIBUTION OF THIS WEB SITE WARRANT THAT THE FUNCTIONS CONTAINED
                IN THIS WEB SITE WILL BE UNINTERRUPTED OR ERROR-FREE, THAT DEFECTS WILL BE CORRECTED,
                OR THAT THE SERVER THAT MAKES THE CONTENT AVAILABLE WILL BE FREE OF VIRUSES OR OTHER
                HARMFUL COMPONENTS. THE CONTENT THAT YOU ACCESS ON THIS WEB SITE IS PROVIDED SOLELY
                FOR YOUR CONVENIENCE AND INFORMATION ONLY. Beekman Street Realty LLC DOES NOT WARRANT
                OR MAKE ANY REPRESENTATIONS REGARDING THE RESULTS THAT MAY BE OBTAINED FROM THE
                USE OF THIS WEB SITE, OR AS TO THE RELIABILITY, ACCURACY OR CURRENCY OF ANY INFORMATION
                CONTENT, SERVICE AND/OR MERCHANDISE ACQUIRED PURSUANT TO YOUR USE OF THIS WEB SITE.
            </p>
            <p>
                YOU EXPRESSLY AGREE THAT USE OF THIS WEB SITE IS AT YOUR SOLE RISK. YOU (AND NOT
                US) ASSUME THE ENTIRE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION OF YOUR
                SYSTEM. YOU EXPRESSLY AGREE THAT NEITHER US, NOR OUR AFFILIATED OR RELATED ENTITIES
                (INCLUDING OUR PROVIDERS), NOR ANY OF THEIR RESPECTIVE EMPLOYEES, OR AGENTS, NOR
                ANY PERSON OR ENTITY INVOLVED IN THE CREATION, PRODUCTION AND DISTRIBUTION OF THIS
                WEB SITE, IS RESPONSIBLE OR LIABLE TO ANY PERSON OR ENTITY WHATSOEVER FOR ANY LOSS,
                DAMAGE (WHETHER ACTUAL, CONSEQUENTIAL, PUNITIVE OR OTHERWISE), INJURY, CLAIM, LIABILITY
                OR OTHER CAUSE OF ANY KIND OR CHARACTER WHATSOEVER BASED UPON OR RESULTING FROM
                THE USE OR ATTEMPTED USE OF THIS WEB SITE OR ANY OTHER LINKED SITE. BY WAY OF EXAMPLE,
                AND WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, WE AND OUR RELATED PERSONS
                AND ENTITIES SHALL NOT BE RESPONSIBLE OR LIABLE FOR ANY CLAIM OR DAMAGE ARISING
                FROM FAILURE OF PERFORMANCE, ERROR, OMISSION, INTERRUPTION, DELETION, DEFECT, DELAY
                IN OPERATION, COMPUTER VIRUS, THEFT, DESTRUCTION, UNAUTHORIZED ACCESS TO OR ALTERATION
                OF PERSONAL RECORDS, OR THE RELIANCE UPON OR USE OF DATA, INFORMATION, OPINIONS
                OR OTHER MATERIALS APPEARING ON THIS WEB SITE. YOU EXPRESSLY ACKNOWLEDGE AND AGREE
                THAT WE ARE NOT LIABLE OR RESPONSIBLE FOR ANY DEFAMATORY, OFFENSIVE OR ILLEGAL CONDUCT
                OF OTHER SUBSCRIBERS OR THIRD PARTIES. SOME JURISDICTIONS MAY NOT ALLOW THE EXCLUSION
                OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES. IN SUCH JURISDICTIONS,
                OUR LIABILITY IS LIMITED TO THE GREATEST EXTENT PERMITTED BY LAW.
            </p>
            <p>
                <span>INDEMNIFICATION </span>
            </p>
            <p>
                You agree to indemnify, defend, and hold harmless Beekman Street Realty LLC and
                the Providers, its and their officers, directors, employees, affiliates, agents,
                licensors, and suppliers from and against all losses, expenses, damages and costs,
                including reasonable attorneys' fees, resulting from any violation by you of these
                Terms
            </p>
            <p>
                <span>THIRD PARTY RIGHTS </span>
            </p>
            <p>
                These Terms are for the benefit of Beekman Street Realty LLC and its Providers,
                its and their officers, directors, employees, affiliates, agents, licensors, and
                suppliers. Each of these individuals or entities shall have the right to assert
                and enforce these Terms directly against you on its or their own behalf.
            </p>
            <p>
                <span>JURISDICTIONAL ISSUES </span>
            </p>
            <p>
                Unless otherwise specified, the Content contained in this Web Site is presented
                solely for your convenience and/or information. This Web Site is controlled and
                operated by Beekman Street Realty LLC from its offices within New York, New York.
                Beekman Street Realty LLC makes no representation that Content in its Web Site is
                appropriate or available for use in other locations. Those who choose to access
                this Web Site from other locations do so on their own initiative and are responsible
                for compliance with local laws, if and to the extent local laws are applicable.
                You may not use or export the materials in this Web Site in violation of U.S. export
                laws and regulations. These Terms shall be governed by, construed and enforced in
                accordance with the laws of the State of New Jersey, as they are applied to agreements
                entered into and to be performed entirely within such State. Any action you, any
                third party or Beekman Street Realty LLC brings to enforce these Terms, or in connection
                with any matters related to this Web Site, shall be brought only in either the state
                or Federal courts located in New York City, New York and you expressly consent to
                the jurisdiction of said courts. If any provision of these Terms shall be unlawful,
                void or for any reason unenforceable, then that provision shall be deemed sever
                able from these Terms and shall not affect the validity and enforceability of any
                remaining provisions.
            </p>
            <p>
                <span>ENTIRE AGREEMENT </span>
            </p>
            <p>
                The provisions and conditions of these Terms, and each obligation referenced herein,
                represent the entire Agreement between Beekman Street Realty LLC, its affiliated
                or related entities, and you, and supersede any prior agreements or understandings
                not incorporated herein. In the event that any inconsistencies exist between these
                Terms and any future published terms of use or understanding, the last published
                Terms or terms of use or understanding shall prevail.
            </p>
            <p>
                <b>ANY RIGHTS NOT EXPRESSLY GRANTED HEREIN ARE RESERVED BY BEEKMAN STREET REALTY LLC
                </b>
            </p>
        </div>
        <div class="titles-global" style="margin-bottom: 20px;">
            Privacy Policy
        </div>
        <div class="text clearfix">
            <p>
                We at <b>Beekman Street Realty LLC</b> (referred to as "<b>Beekman Street Realty LLC," "We,"
                "Us," or "Our"</b>) recognize the importance of protecting the privacy of certain information
                collected about the <b>users and visitors</b> of Our Web Site (referred to as <b>"User," "You"
                or "Your"</b>). This Privacy Policy discloses what information We gather, how We use
                it, and how to correct or change it. At Beekman Street Realty LLC, it is Our intention
                to give You an understanding about how We collect Your information and the use We
                make of it in the course of Our business.
            </p>
            <p>
                To that end, Beekman Street Realty LLC gathers two types of information about You:
                1) data that Users provide through optional, voluntary registration on Our Web Site,
                and 2) data that Beekman Street Realty LLC gathers through aggregated tracking,
                mainly by tallying Web pages that are viewed by Users throughout Our Web Site.
            </p>
            <p>
                However, be assured that Beekman Street Realty LLC will not disclose Personal Information
                (as defined herein) to third parties without Your consent, except as may be noted
                in this Privacy Policy or disclosed to You when the information is submitted.
            </p>
            <p>
                <span>How Does Beekman Street Realty LLC Collect Information? </span>
            </p>
            <p>
                Optional Registration We collect information by specifically requesting it from
                You. Generally this occurs when You register at the Web Site or request information
                or a service. Usually, We and Our business partners use Your information to operate
                the Web Site and to deliver services. Beekman Street Realty LLC also uses Your information
                to inform You about other products and/or services available from Beekman Street
                Realty LLC and Beekman Street Realty LLC-affiliates.
            </p>
            <p>
                <u>Use of Tracking:</u> Use of Internet Protocol ("IP") Addresses:
            </p>
            <p>
                An IP address is a unique number that is automatically assigned to Your computer
                whenever You are surfing the Internet so that Your computer can be identified by
                the main computers, known as "Web servers," that "serve up" Web pages. This allows
                Us to identify and gather general information and data about You, such as the Web
                pages You view.
            </p>
            <p>
                Beekman Street Realty LLC collects IP addresses for the purposes of: helping Us
                diagnose problems with Our main computers, for system administration, to report
                aggregated information to Our business partners, and to audit the use of Our Web
                Site. When Users request Web pages from Our Web Site, Our Web servers log the User's
                IP address. We do not normally link IP addresses to anything personally identifiable,
                which means that a User’s session will be logged, but the User will remain anonymous
                to Us. For example, We collect and/or track the home server domain name, the type
                of computer, and the type of Web browser used by You to access this Web Site. Such
                collection and tracking information is gathered by Us as You navigate through this
                Web Site, and shall be used by Us for Our business purposes only.
            </p>
            <p>
                We can, and will, use IP addresses to identify You when We feel it is necessary
                to enforce compliance with Our Web Site Terms of Use or to protect Our service,
                site, Users, or others.
            </p>
            <p>
                <u>Use Of Cookies:</u> What are cookies? Cookies are small pieces of information that a
                Web site sends to Your computer for record-keeping purposes, which information is
                stored in a file on Your computer’s hard drive. Cookies make Web surfing easier
                for You by saving Your preferences so that We can use the saved information to facilitate
                Your use of Our Web Site when You return to the Web Site. Cookies do not tell Us
                Your individual identity unless You have chosen to provide it to Us. We never save
                passwords or credit card information in cookies. The use of cookies is an industry
                standard, and as such, You will find that most major Web sites use them.
            </p>
            <p>
                By showing how and when Users use the Web Site, cookies help Us see which areas
                of the Web Site are popular and which are not. Many improvements and updates to
                the Web Site are based on such data as total number of visitors and pages viewed.
                This information is most easily tracked with cookies.
            </p>
            <p>
                Most cookies expire after a defined period of time, or You can delete Your cookie
                file at any time. Most Web browsers are initially set up to accept cookies. You
                can reset Your Web browser to refuse cookies or to indicate when a cookie is being
                sent. However, note that some parts of Beekman Street Realty LLC and/or Beekman
                Street Realty LLC-affiliates' services will not function properly or may be considerably
                slower if You refuse cookies. For example, without cookies, You will not be able
                to set personalized preferences, and/or may have difficulty completing marketing
                transactions.
            </p>
            <p>
                Beekman Street Realty LLC and/or Beekman Street Realty LLC-affiliates have two (2)
                primary uses for cookies. First, We use them to specify a User’s preferences. Second,
                We use cookies to track Web site usage trends and patterns. This helps Us understand
                Our Users’ needs better and improve areas of Our Web Site. While both of these activities
                depend on the use of cookies, You have the option of disabling (refusing) the cookies
                via Your Web browser preferences.
            </p>
            <p>
                You may occasionally get cookies from Our business partners. Beekman Street Realty
                LLC and/or Beekman Street Realty LLC-affiliates do not control these cookies. The
                use of advertising cookies sent by such third-party Web servers is standard in the
                Internet industry.
            </p>
            <p>
                <span>With Whom Does Beekman Street Realty LLC Share Information? </span>
            </p>
            <p>
                As a general rule, Beekman Street Realty LLC will not disclose any specific personal
                information that You submit to Us about You (<b>"Personal Information"</b>) in a manner
                that connects You with the Personal Information, except when We have Your permission
                or under special circumstances, such as when We believe in good faith that the law
                requires it or under the circumstances described below. The following describes
                some of the ways that Your Personal Information may be disclosed.
            </p>
            <p>
                <u>Business Partners & Sponsors:</u> Beekman Street Realty LLC may disclose some or all
                of Your Personal Information to business partners or sponsors, but this use will
                be specifically described to You prior to data collection or prior to transferring
                the data. Many promotions offer opportunities to request additional information
                from sponsors. By requesting more information, You give Beekman Street Realty LLC
                permission to transfer Your Personal Information to the sponsor so they can fulfill
                Your request. In many instances, only Your e-mail address will be shared. If more
                Personal Information will be shared with the sponsor, You will be notified prior
                to the transfer.
            </p>
            <p>
                <u>Third Party & Aggregate Data:</u> From time to time, You may be offered the opportunity
                to receive materials or special offers from third parties. If You opt to receive
                information from these third parties, Beekman Street Realty LLC will (with Your
                permission) share Your name and e-mail address with such third parties. Under confidentiality
                agreements, Beekman Street Realty LLC may match Personal Information with third
                party data. Also, Beekman Street Realty LLC may keep track of what portions of the
                Web Site Users are visiting and other User statistics, and aggregate this information
                to help Us create a better experience for Users of Our Web Site. This is all done
                without knowing Your name or other Personal Information in order to describe Our
                viewers and services to prospective partners, advertisers, and other third parties,
                and for other lawful purposes.
            </p>
            <p>
                <u>Other:</u> Beekman Street Realty LLC may also disclose account information in special
                cases when We have reason to believe that disclosing this information is necessary
                to identify, contact or bring legal action against someone who may be violating
                Our Terms of Use, or may be causing injury to or interference (either intentionally
                or unintentionally) with any of Beekman Street Realty LLC’ rights or property, other
                Users, or anyone else. Beekman Street Realty LLC may disclose or access account
                information when We believe in good faith that the law requires it and for administrative
                and other purposes that We deem necessary to maintain, service, and improve Our
                products and services.
            </p>
            <p>
                <span>How Can You Control The Usage Of Information? </span>
            </p>
            <p>
                In case You change Your mind or some Personal Information changes (such as Your
                zip code or e-mail address), We will try to provide a way to correct, update or
                remove the Personal Information that You give us.
            </p>
            <p>
                <span>What Happens When You Link To A Third Party Site? </span>
            </p>
            <p>
                You should be aware that when You are on Beekman Street Realty LLC’ and/or Beekman
                Street Realty LLC-affiliates’ Web site You can hyperlink, or be directed to, other
                Web sites that are beyond Our control and/or outside Our service. For example, if
                You "click" on a banner advertisement or a Beekman Street Realty LLC search result,
                the "click" may transfer You off Our Web Site. These other Web sites may include
                sites of advertisers, sponsors, and partners that may use Our and/or Beekman Street
                Realty LLC-affiliates’ logo as part of a co-branding agreement. These other Web
                sites may send their own cookies to Users, collect data, or solicit Information.
            </p>
            <p>
                Beekman Street Realty LLC and Beekman Street Realty LLC-affiliates do not control
                such Web sites, and, therefore, are not responsible for their contents or the hyperlinks
                or advertising they choose to place on such Web sites. Beekman Street Realty LLC
                and/or Beekman Street Realty LLC-affiliates’ inclusion of hyperlinks to these Web
                sites do not imply any endorsement of the material on such Web sites or any association
                with their operators. Our Privacy Policy does not extend to anything that is inherent
                in the operation of the Internet, which is beyond Beekman Street Realty LLC and/or
                Beekman Street Realty LLC-affiliates' control.
            </p>
            <p>
                Please keep in mind that whenever You give out information on-line (for example,
                via message boards or forums) that information can be collected and used by people
                You do not know. While Beekman Street Realty LLC and Beekman Street Realty LLC-affiliates
                strive to protect Users’ information and privacy, We cannot guarantee the security
                of any information You disclose on-line, and You disclose such information at Your
                own risk.
            </p>
            <p>
                <span>Is Information Secure From Others Using The Beekman Street Realty LLC Web Site?
                </span>
            </p>
            <p>
                The security of all information associated with Our Users is an important concern
                to Us. We exercise care in providing secure transmission of Your information from
                Your computer to Our servers. Unfortunately, no data transmission over the Internet
                can be guaranteed to be 100% secure. As a result, while We strive to protect Your
                information, Beekman Street Realty LLC and Beekman Street Realty LLC-affiliates
                can't ensure or warrant the security of any information You transmit to Us or from
                Our on-line products or services, and You do so at Your own risk. Once We receive
                Your transmission, Beekman Street Realty LLC uses industry standard efforts to safeguard
                the confidentiality of Your information, such as firewalls and Secure Socket Layers
                (SSL). However, "perfect security" does not exist on the Internet.
            </p>
            <p>
                <span>YOUR ACCEPTANCE OF THE TERMS OF THIS PRIVACY POLICY </span>
            </p>
            <p>
                By using Our Web Site, You signify Your agreement to Beekman Street Realty LLC’
                Privacy Policy.<b> IF YOU DO NOT AGREE WITH THIS PRIVACY POLICY, PLEASE DO NOT USE
                THIS WEB SITE.</b> Your continued use of Beekman Street Realty LLC and/or Beekman Street
                Realty LLC-affiliates’ Web sites following the posting of changes to these terms
                will mean You accept those changes. <b>TRADEMARKS, COPYRIGHTS AND RESTRICTIONS</b>
            </p>
        </div>
    </div>
</asp:Content>
