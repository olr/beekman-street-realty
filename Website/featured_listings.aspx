﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="featured_listings.aspx.cs" Inherits="featured_listings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   <script type="text/javascript">
       $(document).ready(function () {
           $("input:radio").change(function () {
               //alert("radio changed");
               if ($("input:radio:checked").val() == "rental") {
                   //alert("rent");
                   window.location.replace("featured_listings.aspx?type=rent");
               }
               else {
                   //alert("sale");
                   window.location.replace("featured_listings.aspx?type=sale");
               }
           });
       });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="result-wrapper">
        <div class="result-content-container">
        <div class="featured-listings-type-container">
                <div class="open-house-title">
                    Featured Listing Type</div>
                <div style="margin: 3px 5px 0 0; float:left;">
        <%if (pType == "rent")
          {%>
                <input type="radio" id="saleRadio" value="sale" name="PropertyType" class="newPropertyType"  />
            Sales&nbsp; 
                <input type="radio" id="rentalRadio" value="rental" name="PropertyType" class="newPropertyType" checked="true" />
            Rentals&nbsp;
            <%}
          else
          { %>
            
                <input type="radio" id="Radio1" value="sale" name="PropertyType" class="newPropertyType"  checked="true" />
            Sales&nbsp; 
                <input type="radio" id="Radio2" value="rental" name="PropertyType" class="newPropertyType"  />
            Rentals&nbsp;
            <%} %>
            </div>
            </div>
            <br />
            <br />
            <div class="result-page">
                <h2 class="titles-global" style="padding-top:0px; margin-top:-15px;">
                    Featured Listings</h2>
                <olr:ShortForm runat="server" ID="ShortForm1" />
               
            </div>
        </div>
    </div>
</asp:Content>
