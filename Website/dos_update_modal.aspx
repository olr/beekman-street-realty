﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dos_update_modal.aspx.cs" Inherits="dos_update_modal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title><style>
        .dos-update * {
                font-family: "futura-pt", Arial, Helvetica, sans-serif;
                line-height: 1.2;
        }

        .dos-update h3 {
            font-size: 24px; font-weight: bold;
        }

        .dos-update-wrapper {box-sizing: border-box; padding: 15px 50px;}
    </style>
</head>
<body>
    <form id="form1" runat="server" class="dos-update">
    <div class="dos-update-wrapper dos-update">
        <h3 >Broker's Operating Procedures</h3>
        <p>Please be advised that prior to showing a purchaser a property:</p>
        <p>We (do or do not) require identification from a prospective purchaser.</p>
        <p>We (do or do not) require a purchaser to sign an exclusive brokerage agreement.</p>
        <p>We (do or do not) require a pre-approval for a mortgage loan in order to show a purchaser any properties.</p>
        <p>*Although Broker may not require such information, a seller of real estate may require this information prior to showing the property and/or as part of any purchase offer.</p>
        <p style="font-size: 12px">Updated as of Mar 22, 2022 8:06 AM</p>
    </div>
    </form>
</body>
</html>
