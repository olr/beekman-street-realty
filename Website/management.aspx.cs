﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using BrokerTemplate.Core;

public partial class management : System.Web.UI.Page
{
    private Brokers getManagers()
    {
        Brokers bs = new Brokers();
        string path = Server.MapPath("~/App_Data/ExecutiveTeam.xml");
        XElement root = XElement.Load(path);
        var elements = root.Elements("officer");
        foreach (var e in elements)
        {
            string id = e.Element("ID").Value;
            string name = e.Element("name").Value;
            string title = e.Element("title").Value;
            string ophone = e.Element("officephone").Value;
            string cphone = e.Element("cellphone").Value;
            //string email = e.Element("email").Value;
            string photo = e.Element("photo").Value;
            string url = e.Element("url").Value;
            string vcf = e.Element("vcf").Value;
            bs.Add(new BrokerInfo
            {
                ID = string.IsNullOrEmpty(id) ? 0 : Convert.ToInt32(id),
                FirstName = name,
                Title = title,
                WorkPhone = !string.IsNullOrEmpty(ophone) ? "O: " + ophone : string.Empty,
                MobilePhone = !string.IsNullOrEmpty(cphone) ? "M: " + cphone : string.Empty,
                PhotoUrl = photo,
                PrivateUrl = url,
                FacebookUrl = vcf
            });
        }
        return bs;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Brokers bs = getManagers();
        ManagerList.DataSource = bs;
        ManagerList.DataBind();
    }

    protected string GetDetail(int id)
    {
        if (id == 0)
            return "#";
        else
            return "agent_detail.aspx?agentid=" + id;
    }
    protected string GetEmail(int id)
    {
        if (id == 0)
            return "#";
        else
            return "email_agent.aspx?id=" + id;
    }
    protected string GetVcard(int id)
    {
        if (id == 0)
            return "#";
        else
            return "vcard.aspx?id=" + id;
    }
}