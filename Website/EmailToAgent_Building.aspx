﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.master" AutoEventWireup="true" CodeFile="EmailToAgent_Building.aspx.cs" Inherits="EmailToAgent_Building" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<title>Email Agent</title>
    <link href="css/calendrical.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="js/jquery.calendrical.js" type="text/javascript"></script>
    <style type="text/css">
        .date { width: 357px !important; margin-right: 10px; }
        .time { width: 106px !important; }
        .time-capsule { position: relative; }
        .time-capsule label { position: absolute; left: 5px; }
        .calendricalDatePopup { z-index: 100; }
        .calendricalTimePopup { z-index: 100; }
    </style>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $('input, textarea').bind('focus', function () {
                $(this).siblings('label').hide();
            });
            $('input, textarea').bind('blur', function () {
                if ($(this).val().length == 0)
                    $(this).siblings('label').show();
            });
            $('input, textarea').each(function (i) {
                if ($(this).val().length == 0)
                    $(this).siblings('label').show();
                else
                    $(this).siblings('label').hide();
            });
            $('.date').calendricalDate({ usa: true });
          
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
  <h1 class="title">Email Agent</h1>
    <form id="Form1" runat="server">
        <div class="w_popup_body">
            <div id="formpanel" runat="server">
                <h3><%=bi.Address %></h3>
                <fieldset class="form">
                    <div class="section">
                        <label>Your Name</label>
                        <asp:TextBox ID="yourName" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="yourName" runat="server" ErrorMessage="*Please enter your name." CssClass="error-message-text"/>
                    </div>
                    <div class="section">
                        <label>Your Email</label>
                        <asp:TextBox ID="yourEmail" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="yourEmail" runat="server" ErrorMessage="*Please enter your email address." CssClass="error-message-text"/>
                         <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="yourEmail" ID="RegularExpressionValidator1" runat="server" ErrorMessage="*Please enter a valid email address." CssClass="invalid-message-text" ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$" />
                    </div>
                    <div class="section" id="calendarfield">
                        <label>Your Phone Number</label>
                        <asp:TextBox ID="yourPhone" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="yourPhone" runat="server" ErrorMessage="*Please enter your phone number." CssClass="error-message-text"/>
                         <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="yourPhone"
                    EnableClientScript="true" ID="RegularExpressionValidator2" runat="server" ErrorMessage="*Please enter a valid phone number." CssClass="invalid-message-text"
                    ValidationExpression="1?\s*\W?\s*([2-9][0-8][0-9])\s*\W?\s*([2-9][0-9]{2})\s*\W?\s*([0-9]{4})(\se?x?t?(\d*))?" />
                    </div>
                   
                    <div class="section">
                        <label>Your Message</label>
                        <asp:TextBox ID="messageText" runat="server" CssClass="textBox" TextMode="MultiLine" Height="113px"></asp:TextBox>
                    </div>
                </fieldset>
                <div class="buttons">
                    <asp:Button ID="sendEmail" runat="server" Text="Send Email" />
                </div>
            </div>
            <div id="sentpanel" runat="server" visible="false">
            <div class="footer">
                <p>
                    Thank you. Your message has been sent, a representative will contact you shortly.</p>
            </div>
        </div>
        <div id="sentfailed" runat="server" visible="false">
            <div class="footer">
                <p>
                    Sorry, your email sent failed.</p>
                <p>
                    <%=exception.ToString() %></p>
            </div>
        </div>
        </div>
    </form>
</asp:Content>

