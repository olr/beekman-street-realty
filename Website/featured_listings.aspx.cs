﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class featured_listings : BasePage<featured_listings, SearchOptions>
{
    protected string title;
    public string pType = "rent";
    protected override void OnInit(EventArgs e)
    {
        this.Controls.InitSortables(SortClicked);
        base.OnInit(e);
    }

    void SortClicked(object sender, CommandEventArgs e)
    {
        SearchOptions opts;
        if (TempStorage.TryGet<SearchOptions>("featuredListings", out opts))
        {
            opts.OrderBy = fn.StringToEnum<SortOrder>(e.CommandName);
            opts.IsDescending = ((SortDirection)e.CommandArgument).Equals(SortDirection.Descending);
            opts.PageIndex = 0;
            RunSearch(opts);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        pType = Request.QueryString["type"];
        //SearchOptions options; 
        int total = 0;
        if (!Page.IsPostBack)
        {
            //if (TempStorage.TryGet<SearchOptions>("featuredListings", out options))
            //{
            //    Model = options;
            //}
            //else

            {
                InitModel();
            }
            if (pType == "sale")
            {
                title = "featured sale listings";
            }
            else
            {
                title = "featured rental listings";
            }
            var newListings = ServiceLocator.GetSearchService().RunSearch(Model, out total);
            ShortForm1.DataBind(newListings);

            if (!string.IsNullOrEmpty(Request.QueryString["sort"]))
            {
                ReSort(Model, Request.QueryString["sort"]);
                return;
            }
            if (Request.QueryString["page"] != null)
            {
                int page = fn.Get<int>(Request.QueryString["page"]);
                if (page > 0)
                    Model.PageIndex = page - 1;
            }
            RunSearch(Model);
        }
    }
    void InitModel()
    {
        Model = new SearchOptions();
        if (pType == "rent")
            Model.PropertyTypes.Add(PropertyType.Rental);
        else
            Model.PropertyTypes.Add(PropertyType.Sale);

        Model.FeaturedListings = true;
        Model.OrderBy = SortOrder.DateOnMarket;
        Model.PageSize = 10;
    }
    void RunSearch(SearchOptions options)
    {
        if (options != null)
        {
            int _total = 0;
            Listings listings = null;
            if (options.PropertyTypes.Contains(PropertyType.Commercial))
                listings = ServiceLocator.GetComercialSearchService().RunSearch(options, out _total);
            else
                listings = ServiceLocator.GetSearchService().RunSearch(options, out _total);
            if (listings != null)
            {
                this.Controls.SetPagers(options.PageIndex + 1, options.PageSize, _total);
                this.Controls.DataBind<Listings>(listings);
                listings.CreatePagingSession(); //save paging info

            }
            TempStorage.Add("featuredListings", options);
            this.Controls.DataBind<SearchOptions>(options);
            ShowVowLink = lastPage(options.PageIndex, options.PageSize, _total);
        }
    }

    void ReSort(SearchOptions options, string sortOrder)
    {
        var lastSortOrder = options.OrderBy;
        switch (sortOrder)
        {
            case "price": options.OrderBy = SortOrder.Price; break;
            case "size": options.OrderBy = SortOrder.Size; break;
            case "location": options.OrderBy = SortOrder.Address; break;
        }
        if (lastSortOrder.Equals(options.OrderBy))
            //reverse sort order
            options.IsDescending = !options.IsDescending;
        options.PageIndex = 0;
        TempStorage.Add("featuredListings", options);
        Response.Redirect("~/new_listings.aspx");
    }

    protected bool ShowVowLink { get; set; }
    private bool lastPage(int pageIndex, int pageSize, int totalItems)
    {
        int numPages = (int)Math.Ceiling(totalItems / (double)pageSize);
        numPages = numPages > 0 ? numPages : 1;
        return pageIndex + 1 == numPages;
    }

}
