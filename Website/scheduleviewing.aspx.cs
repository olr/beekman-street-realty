﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;
using System.Text;
using System.Net.Mail;

public partial class scheduleviewing : BasePage<scheduleviewing, ListingInfo>
{
    protected BrokerInfo Broker { get; set; }
    protected Exception exception;

    protected override void OnInit(EventArgs e)
    {
        sendEmail.Click += new EventHandler(sendEmail_Click);

        Model = this.GetListing();
        Broker = this.GetBroker();
    }

    protected override void OnLoad(EventArgs e)
    {
        messageText.Text = string.Format(@"Please contact me regarding scheduling a viewing of this listing - ID: {0} .", Model.ListingID);
    }

    void sendEmail_Click(object sender, EventArgs e)
    {
        if (IsValid && Model != null && Broker != null)
        {
            StringBuilder body = new StringBuilder();

            body.AppendLine("<div class='overlay_container' style='background-color: #ffffff; color: #2D2D2D; font-family: Arial; font-size: 12px;line-height: 1.2em;'>");
            body.AppendFormat("<img src='http://{0}/images/PRINT-Logo.jpg' style='width:176px; border:none;' />", Request.Url.Host);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientNameEmailLabel, yourName.Text);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientEmailEmailLabel, yourEmail.Text);
            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientPhoneEmailLabel, yourPhone.Text);
            if (!string.IsNullOrEmpty(Request.QueryString["lid"]))
                body.AppendFormat("You can <a href='http://{1}/detail.aspx?id={0}'>view the listing by clicking here</a>", Request.QueryString["lid"], Request.Url.Host);
            body.AppendFormat("<div>My Move-In Date: {0}</div>", moveinDate.Text);
            if (!string.IsNullOrEmpty(preferredDate1.Text))
                body.AppendFormat("<div>Preferred Date 1: {0} {1}</div>", preferredDate1.Text, preferredTime1.Text);
            if (!string.IsNullOrEmpty(preferredDate2.Text))
                body.AppendFormat("<div>Preferred Date 2: {0} {1}</div>", preferredDate2.Text, preferredTime2.Text);
            if (!string.IsNullOrEmpty(preferredDate3.Text))
                body.AppendFormat("<div>Preferred Date 3: {0} {1}</div>", preferredDate3.Text, preferredTime3.Text);

            body.AppendFormat("<div>{0}: {1}</div>", Resources.Email.ClientMessageEmailLabel, messageText.Text);
            body.AppendLine("</div>");

            MailMessage message = new MailMessage("base@email.com", Broker.Email);
            message.Subject = "Website lead from http://www.beekmanstreetrealty.com - Listing ID# " + Model.ListingID + "- Schedule a Viewing ";
            message.Body = body.ToString();
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient();
            try
            {
                client.Send(message);
                formpanel.Visible = false;
                sentpanel.Visible = true;
            }
            catch (Exception ex)
            {
                exception = ex;
                formpanel.Visible = false;
                sentfailed.Visible = true;
            }
        }
    }

}