﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class video : System.Web.UI.Page
{
    protected ListingInfo listing;
    protected void Page_Load(object sender, EventArgs e)
    {
        listing = this.GetListing();
        if (listing != null)
        {
            var video = listing.GetMedia()
                               .Videos
                               .Where(x => x.IsEmbeddedVideo == true)
                               .FirstOrDefault();
            if (video != null)
                embedCode.Text = video.EmbedCode;
        }
    }
}