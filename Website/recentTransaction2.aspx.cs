﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class recentTransaction2 : BasePage<recentTransaction2, SearchOptions>
{
    protected string pType;
    protected override void OnInit(EventArgs e)
    {
        this.Controls.InitSortables(SortClicked);
        base.OnInit(e);
    }

    void SortClicked(object sender, CommandEventArgs e)
    {
        SearchOptions opts;
        if (TempStorage.TryGet<SearchOptions>("SearchOptions", out opts))
        {
            opts.OrderBy = fn.StringToEnum<SortOrder>(e.CommandName);
            opts.IsDescending = ((SortDirection)e.CommandArgument).Equals(SortDirection.Descending);
            opts.PageIndex = 0;
            RunSearch(opts);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            pType = Request.QueryString["type"];
            Model = new SearchOptions();
            if (pType == "rent" || pType == "rental")
                Model.PropertyTypes.Add(PropertyType.Rental);
            else
                Model.PropertyTypes.Add(PropertyType.Sale);
            Model.RecentTransactions = true;

            if (!string.IsNullOrEmpty(Request.QueryString["sort"]))
            {
                ReSort(Model, Request.QueryString["sort"]);
                return;
            }
            if (Request.QueryString["page"] != null)
            {
                int page = fn.Get<int>(Request.QueryString["page"]);
                if (page > 0)
                    Model.PageIndex = page - 1;
            }
            RunSearch(Model);
        }
    }

    void ReSort(SearchOptions options, string sortOrder)
    {
        var lastSortOrder = options.OrderBy;
        switch (sortOrder)
        {
            case "price": options.OrderBy = SortOrder.Price; break;
            case "size": options.OrderBy = SortOrder.Size; break;
            case "location": options.OrderBy = SortOrder.Address; break;
        }
        if (lastSortOrder.Equals(options.OrderBy))
            //reverse sort order
            options.IsDescending = !options.IsDescending;
        options.PageIndex = 0;
        TempStorage.Add("SearchOptions", options);
        Response.Redirect("~/open_house.aspx");
    }

    void RunSearch(SearchOptions options)
    {
        if (options != null)
        {
            int _total = 0;
            Listings listings = null;
            if (options.PropertyTypes.Contains(PropertyType.Commercial))
                listings = ServiceLocator.GetComercialSearchService().RunSearch(options, out _total);
            else
                listings = ServiceLocator.GetSearchService().RunSearch(options, out _total);
            if (listings != null)
            {
                this.Controls.SetPagers(options.PageIndex + 1, options.PageSize, _total);
                this.Controls.DataBind<Listings>(listings);
                listings.CreatePagingSession(); //save paging info

            }
            TempStorage.Add("SearchOptions", options);
            this.Controls.DataBind<SearchOptions>(options);
        }
    }
}
