﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Core;
using BrokerTemplate.Web;

public partial class FeaturedBuildings : BasePage<FeaturedBuildings, Buildings>
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Model = ServiceLocator.GetSearchService().GetFeaturedBuildings();
    }
}