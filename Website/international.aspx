﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="international.aspx.cs" Inherits="international" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="static-pages guides foreign">
        <div class="titles-global" style="margin-bottom: 20px;">
          foreign investors
        </div>
<%--        <span>WHAT ARE THE MAIN STEPS OF SALE – PURCHASE TRANSACTIONS IN NEW YORK CITY?</span>
        <p>
            <u>STEP 1: PREPARATION</u><br />
            Before you begin your journey into New York Real Estate, it will be helpful to get
            prepared so that when you find what you’re looking for, you can move quickly and
            secure the property of your dreams.
        </p>
        <ul>
            <li>If you are getting financing, you should speak to a mortgage broker or a banker
                and get pre-approved for a loan before you start your search. This will not only
                help you learn how much you can spend but also make you a more attractive buyer
                and help you negotiate a better price. </li>
            <li>It is a requirement in New York to have a lawyer who can review your contract and
                protect your interests during the transaction. If you do not have one, we will be
                glad to recommend several to choose from including those whom speak your language.
                It is needless to say how important it is to select a lawyer who you trust and who
                you are comfortable with. </li>
            <li>It is very useful to speak to a good tax advisor in order to help you decide on
                the optimal purchase structure for your deal. Tax liability varies for US residents
                and non-residents, as well as whether you buy a home or investment property. As
                a result, it is smart to consult with a good tax specialist, fiscal attorney or
                an accountant. </li>
            <li>Finally, you need to prepare your finances and especially a 10% deposit for your
                purchase so that it is readily available when you need it. You may consider opening
                a US bank account if you do not have one or otherwise transfer money to your attorney’s
                escrow account. </li>
        </ul>
        <p>
            <u>STEP 2: PROPERTY SEARCH AND THE OFFER</u><br />
            Every purchase starts with the selection process. After viewing the properties and
            selecting the one you really like, you should make an offer to buy it. Such an offer
            is not binding and you can make more than one offer to find the best deal. New luxury
            property developers (sponsors) however are less likely to negotiate the price and
            usually expect to sell their properties at asking price. I will always be on your
            side and do my best to get you the lowest price and the best deal! Once the offer
            is accepted by the seller, the seller’s agent or lawyer send the contract and bylaws
            of the building (or an offering plan for a new development).
        </p>
        <p>
            <u>STEP 3: CONTRACT NEGOTIATION AND SIGNING</u><br />
            The usual time for your lawyer to review the contract is 5 to 10 business days (the
            seller’s agent of the property you choose will advise you on such time limits).
            Pease note until such contract is signed, the seller may often choose to continue
            showing the property and accept a higher offer. Terms of the contract are very important
            and regulate all the details of the future purchase. You should carefully go over
            the contract with you lawyer to understand the risks and obligations you are taking
            upon yourself. Upon signing the contract, 10% deposit of the property price is payable.
            Therefore, you have to make sure that you have the funds readily available in the
            US. One way to do it is to wire the funds to your attorney in the US. You may also
            open a US bank account if you wish. The deposit is kept in the seller’s attorney-designated
            escrow account. It is usually non-refundable if a buyer doesn’t go ahead with the
            deal. However you can negotiate certain conditions under which you can get the deposit
            back. One of such conditional is a mortgage contingency - a clause in the contract
            that guarantees the refund of the deposit if a buyer cannot secure financing at
            certain terms. It is up to your lawyer to negotiate the clause with the seller,
            although few sellers agree to include it.
        </p>
        <p>
            <u>STEP 4: CO-OP OR CONDO BOARD APPROVAL</u><br />
            Board approval is probably the most unpleasant part of a property purchase in New
            York. It is something that cannot be avoided even if you are buying an apartment
            in the friendliest of all condo buildings. You will have to put together a very
            thorough package with a lot of personal and financial information, including tax
            returns, bank statements, personal and business references, etc. Coop board approval
            is a rigorous process that will usually include a personal interview. Condo board
            approvals follow the same procedure and require the same amount of documentation.
            However, it is usually an easier deal.
        </p>
        <p>
            <u>STEP 5: PREPARATION FOR THE CLOSING DAY</u>
            <br />
            After the contract is signed, you have additional time to secure financing and perform
            additional research on the property’s legal status and condition. This is the time
            to do the survey and appraisal, which is necessary for the mortgage. Your lawyer
            will be reviewing the documents, doing the title search, looking for liens and violations.
            If you apply for a mortgage, your bank will check all the required documents on
            your income and assets as well as the building financials. Once everything is in
            order, your bank will wire the entire amount of the loan usually to your attorney’s
            escrow account. At the same time your down payment and final closing costs will
            also be wired to the escrow account with your attorney acting as an escrow agent
            until the closing. The day before your closing you should do a final walk-through
            at your new property to make sure its condition is as agreed and as expected.
        </p>
        <p>
            <u>STEP 6: CLOSING</u><br />
            A closing is an actual sale-purchase transaction, which usually takes place several
            weeks after signing the contract. This is the time when all the parties in the transaction
            (seller(s) and buyer(s), seller’s and buyer’s attorney, bank attorney, title insurance
            representative, etc) gather together to sign a final set of documents and to make
            all the payments. At the moment of the closing, your attorney will distribute all
            the payments including the payment to the seller, taxes, fees and title insurance
            and other. In exchange, a buyer will receive the new title along with the title
            insurance and, of course, keys to the new investment property. <b style="font-weight: 800;">
                Congratulations!</b>
        </p>
        <ol class="alpha">
            <li><u>WHAT WILL MY EXPENSE BE AS AN OWNER OF REAL ESTATE IN NEW YORK CITY?</u>
                <br />
                <br />
                As an owner of real estate property in the US, you will have to pay real estate
                taxes and monthly common charges for a condo and a monthly maintenance fee for a
                co-op. Depending on the type and the size of the property, taxes may run from a
                few hundred dollars a year to several thousand dollars per year. The same is true
                for common charges and maintenance fees – these range from a couple hundred a month
                to a few thousand a month and depend on the size and type of property as well as
                on location and amenities available. The more additional services the building offers,
                the higher the monthly payments. Be prepared o spend over $1 dollar per square foot
                per month for your property in New York. As an owner of your home, you can use some
                of these expenses to lower your tax bill. Real estate taxes can be deducted from
                taxable income by the owner (the person who actually paid real estate tax) within
                the same year. Owners of a co-op can deduct part of their monthly maintenance because
                it consists of, among other things, the interest payment on the co-op building mortgage.
                If you are an investor, most of your expenses can be deducted in the year they were
                incurred. Both common charges and real estate tax are treated as a business expense
                in the year incurred and can be deductible. Please consult your tax advisor if this
                applies to you. You can also see our tax page for more details. </li>
            <li><u>HOW DO I CHECK THE PROPERTY TITLE TO MAKE SURE THAT MY PURCHASE IS LEGAL AND
                SAFE?</u>
                <br />
                <br />
                To structure the sale-purchase transaction, both the seller and the buyer employ
                attorneys, who negotiate the contract (preliminary agreement, under which a deposit
                is paid), arrange for the title search and insurance, check for liens and violations
                on the building, etc. Other professionals that might be needed include appraisers,
                who determine the fair market value for the property; surveys, who check the condition
                of the property and its technical elements; architects, who determine the exact
                size of the property and make measurements; and mortgage brokers, who help secure
                a mortgage loan. </li>
            <li><u>WHAT ARE THE TAXES ON SALE PROCEEDS FOR A NON-RESIDENT?</u>
                <br />
                <br />
                There is a significant difference in tax liability for foreign and US tax residents.
                Federal tax on long term (property held more than a year) capital gain for US residents
                is 15%. The same tax for foreign residents is 30%. Moreover, to insure payment of
                the US tax, non-residents are subject to the Foreign investment in Real Property
                Tax Act of 1980 (FIRPTA) income tax withholding immediately upon sale of a property
                by a foreign resident. The Internal Revenue Service (IRS) withholds 10% of the gross
                sale price; New York State withholds an additional tax of approximately 6.85% of
                the Estimated Gain. An IRS form must be filed by the seller/buyer – Statement of
                Withholding on Dispositions by Foreign Persons of US Real Property Interests. For
                New York State, an additional form must be filed - Non-resident Real Property Estimated
                Income Tax Payment Form (Complex form, please consult with a tax professional for
                completion). To avoid this complexity, in some cases it makes sense for a foreign
                buyers to own real estate through a domestic Limited Liability Company or LLC.
            </li>
        </ol>
        <ol class="numeric">
            <li><u>WHAT ARE THE BUYERS EXPENSES AT CLOSING? (CLOSING COSTS)</u><br />
                <br />
                Closing costs are additional expense that both a buyer and a seller occur during
                a sale-purchase transaction. Closing costs in New York may comprise 1-8% of a purchase
                amount.*Buyer’s expenses include legal and registration fees as well as federal,
                state and city taxes. These costs have to be covered before or at the closing will
                be an addition to your down payment. Please include these costs in preparation for
                your purchase. These expenses are described in detail below. *If you are buying
                a newly built condo (in NYC it is called “buying from a sponsor”), you might have
                to pay: New York City real property transfer tax, which equals 1%for properties
                below $499,999 and 1.425% for properties above $499,999. Another tax payable to
                the state is New York state transfer tax, which is 0.4% of the sale price. Additionally,
                you will have to cover the seller’s attorney fee which is usually $1,500-$2,500.*Title
                insurance is also required to protect your ownership right from possible third party
                claims for the entire length of your ownership. It costs approximately $450 per
                $100,000 of the property price. *If your property costs more than $1,000,000, you
                will also have to pay a Mansion Tax of 1% of the property price. *Many building
                collect additional fees such as an application fee of $200 and up; a managing agent
                fee of $250-$500, and a move-in deposit of $500-$1500. *You will also have to hire
                an attorney to review all the documents. This amount can range from approximately
                $2,500-$5,000 depending on the complexity of the purchase structure, plus recording
                expenses, starting at $500. *If you are taking on a mortgage to buy your property,
                more cash is needed for closing. Mortgage loan origination fees or points range
                from 0.5% to 3% of value of the loan. Such mortgage related expenses include a lien
                search of around $300-$400, various bank fees (such as tax escrow, homeowners’ insurance)
                of $400-$1200, mortgage title insurance, which costs $200 per $100,000 of property
                value, and a mortgage tax of 1.8% of the mortgage loan for loans under $499,999
                and 1.925% of mortgage loans above $499,999. Additionally, you will have to pay
                $500 and up for an appraisal, $30-$100 per applicant’s credit report, another $500
                and up for the loan application. There is also usually a bank attorney fee of approximately
                $1000-$1500. </li>
            <li><u>WHAT IS A CONDO BUILDING?</u><br />
                <br />
                Around 25% of the residential buildings in Manhattan are condos, and their number
                is rapidly growing. Condominium buildings (condos) are multiple family buildings
                where you own an apartment and a corresponding share of common areas- a regular
                form of real estate ownership. Unlike in Co-ops, there are minimal restrictions
                for who can own a condo as well as on usage of a condo property. Condos welcome
                foreign investors, allow pied-a-terres (part-time stay) and using it as an investment
                property, which means you can rent it out for as many years as you’d like.<br />
                <br />
                To manage a building, condominiums usually also have a Board – an elective organ
                of owners that makes and approves important decisions on use, repair and internal
                rules that are mandatory for all owners and tenants in the building. There is also
                a formal approval process of each potential purchaser performed by the Board. However,
                such approval is a formality. Condos are priced higher than co-ops, mainly because
                of the higher liquidity of this type of investment. Simply put, condos are easier
                to buy and easier to sell than co-ops. As a result, condos are a more attractive
                option for buyers from abroad. There has been a lot of condo construction in New
                York over the recent few years. Many older buildings are undergoing renovation and
                are being converted to condos as well. In addition, most newly constructed condos
                offer tax relief for the first 10 to 25 years, which results in significant tax
                savings. Moreover, many newly constructed buildings boast sleek modern amenities,
                floor to ceiling windows, mint renovation and superb services. </li>
            <li><u>HOW PROPERTIES RIGHTS ARE REGISTERED</u><br />
                <br />
                A recording of the title with the new owner’s information is part of the closing,
                and is usually carried out by the buyer’s attorney, who submits the documents at
                the New York City Register. At the moment of purchase, all title documents are photographed,
                photocopied and filed so that they can be found and examined by everyone who wants
                to see them.</li>
            <li><u>WHAT IS FIRPTA?</u><br />
                <br />
                Foreign Investment in Real Property Tax Act, a statute that requires that a seller
                who is a foreign person permit a withholding of a part of the selling price against
                the United States gains taxes that the foreign person will owe on capital gains
                earned by the sale of real Unites States real property.</li>
            <li><u>WHAT CONSTITUTES A “FOREIGN PERSON” UNDER FIRPTA?</u><br />
                <br />
                A foreign person for federal income tax purposes is generally any person who is
                not either a resident alien (i.e. a holder of a green card) or a United States citizen.
                If an owner of real property is an entity formed outside of the United States, it
                is also deemed “foreign person” under the IRS Code (example: British Virgin Islands
                or Channel Islands Corporations).</li>
            <li><u>WHO HAS THE RESPONSIBILITY TO WITHHOLD?</u><br />
                <br />
                The IRS gives the buyer the obligation to withhold ten (10%) percent of the purchase
                price at closing and deliver it to the IRS when the seller of real property is a
                “foreign person”; a failure to withhold becomes the responsibility of the buyer
                to who m the IRS will look for payment in the event withholding is not made. The
                seller must obtain a Tax ID number in order to meet the buyer’s obligations to the
                IRS.</li>
            <li><u>WHAT IF CAPITAL GAINS TAX IS MORE THAN FIRPTA WITHHOLDING?</u><br />
                <u>WHAT IF CAPITAL GAINS TAX IS LESS THAN FIRPTA WITHHOLDING?</u><br />
                <br />
                Remember that FIRPTA is a “withholding” statute and is not the seller’s payment
                of the required gains taxes for which the seller is absolutely liable. By providing
                the 10%, the buyer has fulfilled all of buyer’s withholding requirements and has
                no further obligations in the regard regardless of the correct amount of capital
                gains tax to be paid. The seller is required to pay the actual gains tax which may
                be credited toward the withholding made at closing. This is true whether the tax
                due is larger or smaller than the withheld amount. One must file a 1040NR (non-resident
                income tax return), 1120F (foreign corporation) or other entity income tax return
                together with the Evidence of Receipt, on or before April 15 of the year subsequent
                to the closing. If the withholding exceeds the tax due, the seller will be entitled
                to a rebate of the difference between the tax due and the withholding. If the withholding
                is less than the tax due, the balance is to be paid with the filing of the 1040NR
                or the 1120F.8 </li>
            <li><u>HOW SHOULD OVERSEAS INVESTORS TAKE TITLE?</u><br />
                <br />
                Long term capital gains rates (reduced from ordinary income tax rates) are available
                for individuals or entities that are taxed as individuals (e.g. an individually
                owned LLC) after ownership of one (1) year. Corporations are taxed at the ordinary
                income rate regardless of the length of ownership. Currently, the long term capital
                gain rate is 15% (of the net gain, as defined below) and ordinary income rate varies
                but is approximately 35% of the net gain. Thus, in determining how best to take
                title, one must first determine the tax effect of the title taken. Also, if a corporation
                is the owner of the property, a foreign (including sister state) entity must pay
                NY State corporation franchise taxes and NY City general business taxes regardless
                of the place of incorporation (these taxes are business taxes and are NOT income
                taxes, which are treated separately).</li>
            <li><u>DOES A FOREIGN PERSON HAVE AN INCOME TAX OBLIGATION TO NEW YORK STATE?</u><br />
                <br />
                Yes. New York State Law requires that any “non-resident” of the State of New York
                must file and have a percent of the gain withheld at closing and paid to NY State
                before a deed can be filed IT-2663 or IT-2664 for cooperative apartments. The percent
                withheld varies; for 2008 the amount is 6.85% of the net gain.</li>
            <li><u>CAN OVERSEAS INVESTORS TAKE ADVANTAGE OF LIKE KIND TRANSFERS?</u><br />
                <br />
                Yes. But the rules are slightly different. According to 1031Vest, a qualified intermediary:
                “Basically, because the exchange or is a foreign person or entity, a Notice of Non-Recognition
                Transfer needs to be delivered at the first closing, but only if the exchange or
                has already identified the replacement property. If the property has not been identified,
                the exchange or can apply to the IRS for a Withholding Certificate, which the IRS
                must act in within 90 days. While waiting for the IRS, the 10% withholding is held
                in escrow by the transferee, rather than being sent directly to the IRS within the
                usual timeline of 20 days after the closing.</li>
            <li><u>IF A BUYER OR SELLER IS AN OVERSEAS INVESTOR AND IS UT OF THE COUNTRY, CAN
                THE DEAL GO TO CLOSING? IF SO, WHAT IS NEEDED?</u><br />
                <br />
                Yes. No one, whether an individual or entity, whether domestic, or overseas investor,
                is required to be personally at a closing of property which that person(s) or entity
                is purchasing or selling. Indeed, it is common that the client does not go to closing
                and from time to time, the client may actually never be in New York during the process.
                This is true even where financing is being undertaken. In my practice, we commonly
                use a power of attorney that is specifically linked to the real estate or cooperative
                apartment which the client is purchasing or selling. Where financing is taking place,
                the lender often required a specific reference to the loan in the power of attorney.
                The balance of a purchase price is usually wired into your attorney’s escrow account
                so that it is available no later than the close of business day before the actual
                closing.</li>
        </ol>
--%>        <br /><br />
    </div>
</asp:Content>
