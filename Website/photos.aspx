﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popup.master" AutoEventWireup="true" CodeFile="photos.aspx.cs" Inherits="photos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery.colorbox-min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('a.print').click(function () {
                parent.$.fn.colorbox.close();
            });
        })
        </script>
        <style type="text/css">
            .print-icon {padding-top:16px; background:url(images/icon-sprite.png) no-repeat; background-position: -203px 0px; width:29px; height:31px;}
.print-icon:hover {background:url(images/icon-sprite.png) no-repeat; background-position: -203px -31px; width:29px; height:31px;}
.print-icon img{width:29px; height:31px}
        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
 <hr />
    <h2>
        <%= Model.GetDisplayAddress() %>&nbsp; &nbsp;&nbsp;Listing ID:&nbsp;
        <%=Model.ListingID %></h2>
<hr />

<asp:ListView runat="server" ID="list">
    <LayoutTemplate>
        <div class="galleryContainer">
            <ul class="tabular clearfix gallery">
                <li runat="server" id="itemPlaceHolder" />
            </ul>
        </div>
    </LayoutTemplate>
    <ItemTemplate>
        <li>
            <img src="<%# Eval("MediaUrl")%>" alt="" />
        </li>
    </ItemTemplate>
</asp:ListView>

 <a href="photoPrint.aspx?id=<%=Model.ListingID %>" target="_blank" class="print-icon"><img src="images/clear.png" alt="print"/></a>

<%-- <a href="photoPrint.aspx?id=<%=Model.ListingID %>" target="_blank" class="print"><img src="images/detail_tool_print.png" alt="print"/></a>
--%></asp:Content>

