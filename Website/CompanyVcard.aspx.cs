﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerTemplate.Web;
using BrokerTemplate.Core;
using System.Text;
using System.Configuration;

public partial class CompanyVcard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string companyID = ConfigurationManager.AppSettings["CompanyID"];
            int ID = Convert.ToInt32(companyID);
            CompanyInfo com= ServiceLocator.GetRepository().GetCompanyInfoByID(ID);
            Response.BufferOutput = true;
            Response.Clear();
            Response.ContentType = "text/x-vcard";
            Response.AddHeader("Content-Disposition", "inline; filename=contact.vcf");
            Response.AddHeader("Content-Description", "vCard");
            Response.Write(GenVCard(com));
            Response.End();
        }

    }

    private string GenVCard(CompanyInfo c)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("BEGIN:VCARD");
        sb.AppendLine("VERSION:3.0");
        sb.AppendLine(string.Format("ORG: {0}", c.CompanyName));
        sb.AppendLine(string.Format("LABEL;WORK;PREF;ENCODING=QUOTED-PRINTABLE:{0} =0A{1} {2} {3}", c.Address, c.City, c.State, c.ZipCode));
        sb.AppendLine(string.Format("TEL;WORK;VOICE: {0}", "212-953-1099"));
        sb.AppendLine(string.Format("PHOTO;VALUE=URL;TYPE=GIF:http://www.netmorerealty.com/images/mls_idx.gif"));
        sb.AppendLine(string.Format("EMAIL;PREF;INTERNET: {0}", "info@netmorerealty.com"));
        sb.AppendLine("END:VCARD");
        return sb.ToString();
    }

}