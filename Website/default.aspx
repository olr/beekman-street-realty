﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="false"
    CodeFile="default.aspx.cs" Inherits="_default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="Server">
    <script type="text/javascript">
        $(function () {
            // $('#sale').click();
            $('div.page-titles').attr('style', "display:none");
        })
    </script>
      <script type="text/javascript">
         var featuredSales = [];
         var featuredRentals = [];
         var currentSalesIndex = 0;
         var currentRentalsIndex = 0;
         $(function () {
             var proxy = new ServiceProxy("Service.svc/")
             proxy.invoke("GetRandomSaleFeaturedListings", null, function (result) {
                 featuredSales = result;
                 //featuredSales = null;
                 if (featuredSales != null && featuredSales.length > 0) {
                     loadSales(currentSalesIndex);
                     $('#salepre').addClass('disabled');
                     $('#sprebutton').attr('src', 'images/back-button-disable.png');
                     if (featuredSales.length == 1) {
                         $('#sprebutton').hide();
                         $('#snextbutton').hide();
                     }
                 }
                 else {
                     $('#sprebutton').hide();
                     $('#snextbutton').hide();
                 }
                 // console.log('index_sale   ' + currentSalesIndex);
             });
             proxy.invoke("GetRandomRentalFeaturedListings", null, function (result) {
                 featuredRentals = result;
                 if (featuredRentals.length > 0) {
                     loadRentals(currentRentalsIndex);
                     $('#rentpre').addClass('disabled');
                     $('#rprebutton').attr('src', 'images/back-button-disable.png');
                     if (featuredRentals.length == 1) {
                         $('#rprebutton').hide();
                         $('#rnextbutton').hide();
                     }
                 }
                 else {
                     $('#rprebutton').hide();
                     $('#rnextbutton').hide();
                 }
                 //console.log('index_rent   ' + currentRentalsIndex);
             });
             //add handlers
             /**************************************sale next*********************************************************/
             $('#salenext').click(function () {
                 if ($(this).hasClass('disabled'))
                     return false;
                 if ($('#salepre').hasClass('disabled')) {
                     $('#salepre').removeClass('disabled');
                     $('#sprebutton').attr('src', 'images/back-button.png');
                 }
                 if ($('#sale'))
                     $('#s2-content').html("<div class='loading'>&nbsp;</div>");
                 if (featuredSales.length > currentSalesIndex + 1) {
                     loadSales(++currentSalesIndex);
                     if (featuredSales.length == currentSalesIndex + 1) {
                         $(this).addClass('disabled');
                         $('#snextbutton').attr('src', 'images/forward-button-disable.png');
                     }
                 }
                 // console.log('index_sale   ' + currentSalesIndex);
                 return false;
                 // console.log('index_sale   ' + currentSalesIndex);
             });
             /**************************************sale previous*********************************************************/
             $('#salepre').click(function () {
                 if ($(this).hasClass('disabled'))
                     return false;
                 if ($('#salenext').hasClass('disabled')) {
                     $('#salenext').removeClass('disabled');
                     $('#snextbutton').attr('src', 'images/forward-button.png');
                 }
                 $('#s2-content').html("<div class='loading'>&nbsp;</div>");
                 if (currentSalesIndex > 0) {
                     loadSales(--currentSalesIndex);
                     if (currentSalesIndex == 0) {
                         $(this).addClass('disabled');
                         $('#sprebutton').attr('src', 'images/back-button-disable.png')
                     }
                 }
                 return false;
                 //console.log('index_sale   ' + currentSalesIndex);
             });
             /**************************************rent next*********************************************************/
             $('#rentnext').click(function () {
                 if ($(this).hasClass('disabled'))
                     return false;
                 if ($('#rentpre').hasClass('disabled')) {
                     $('#rentpre').removeClass('disabled');
                     $('#rprebutton').attr('src', 'images/back-button.png');
                 }
                 $('#s3-content').html("<div class='loading'>&nbsp;</div>");
                 if (featuredRentals.length > currentRentalsIndex + 1) {
                     loadRentals(++currentRentalsIndex);
                     if (featuredRentals.length == currentRentalsIndex + 1) {
                         $(this).addClass('disabled');
                         $('#rnextbutton').attr('src', 'images/forward-button-disable.png')
                     }
                 }
                 return false;
             });
             /**************************************rent previous*********************************************************/

             $('#rentpre').click(function () {
                 if ($(this).hasClass('disabled'))
                     return false;
                 if ($('#rentnext').hasClass('disabled')) {
                     $('#rentnext').removeClass('disabled');
                     $('#rnextbutton').attr('src', 'images/forward-button.png');
                 }
                 $('#s3-content').html("<div class='loading'>&nbsp;</div>");
                 if (currentRentalsIndex > 0) {
                     loadRentals(--currentRentalsIndex);
                     if (currentRentalsIndex == 0) {
                         $(this).addClass('disabled');
                         $('#rprebutton').attr('src', 'images/back-button-disable.png')
                     }
                 }
                 return false;
             });
         })

         function loadSales(index) {
             if (featuredSales.length > 0) {
                 var proxy = new ServiceProxy("Service.svc/")
                 proxy.invoke("RenderSaleFeaturedListing", { id: featuredSales[index] }, function (result) {
                     //console.log(featuredSales[index]);
                     $('#s2-content').html(result.Content);
                 });
             }
         }
         function loadRentals(index) {
             //console.log("rent length: "+featuredRentals.length);
             if (featuredRentals.length > 0) {
                 var proxy = new ServiceProxy("Service.svc/")
                 proxy.invoke("RenderRentalFeaturedListing", { id: featuredRentals[index] }, function (result) {
                     //console.log(featuredRentals[index]);
                     $('#s3-content').html(result.Content);
                 });
             }
         }
    </script>
    <script type="text/javascript">
   $(window).load(function () {
            $('#slider').nivoSlider({
                effect: 'fade',
                controlNavThumbs:false,
                controlNavThumbsFromRel:true,
                controlNavThumbsSearch: '.jpg',
                controlNavThumbsReplace: '_thumb.jpg',
                directionNav:false,
                pauseOnHover: true,
                captionOpacity: 0.6,
            });
        });
    </script>
    <script type="text/javascript" src="js/default.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="main-wrapper clearfix">
        <!--MAIN SLIDESHOW-->
        <div class="main-image left">
            <div id="slider-wrapper" class="corners">
                <olr:DynamicSlideShow ID="slideshow" runat="server" />
                <div id="htmlcaption" class="nivo-html-caption">
                    <strong>This</strong> is an example of a <em>HTML</em> caption with <a href="#">a link</a>.
                </div>
            </div>
        </div>

      
        <div class="bottom-content right clearfix">
            <div class="featured-col clearfix corners relative">
                <div class="home-title">
                    Featured Sale
                </div>
                <div class="home-featured-button right">
                   <%-- <div class="prev-button button-image left">
                        <a id="salepre" href="#">
                            <img src="images/clear.png" /></a></div>
                    <div class="next-button button-image left">
                        <a id="salenext" href="#">
                            <img src="images/clear.png" /></a></div>--%>
                            <div class="featured-button right">
                            <a id="salepre" href="#">
                                <img id="sprebutton" src="images/back-button.png" /></a> <a id="salenext" href="#">
                                    <img id="snextbutton" src="images/forward-button.png" /></a>
                        </div>
                </div>
                <div id="s2-content">
                        <div class="loading">&nbsp;</div>
                    </div>
                <%--<%= Html.RenderPartial("Cntrls/Default/sale_cntrl", saleListing)%>--%>
            </div>
            <div class="featured-col two clearfix corners relative">
                <div class="home-title">
                    Featured Rental
                </div>
                <div class="home-featured-button right">
                   <%-- <div class="prev-button button-image left">
                        <a id="rentpre" href="#">
                            <img src="images/clear.png" /></a></div>
                    <div class="next-button button-image left">
                        <a id="rentnext" href="#">
                            <img src="images/clear.png" /></a></div>--%>
                            <div class="featured-button right">
                            <a id="rentpre" href="#">
                                <img id="rprebutton" src="images/back-button.png" /></a> <a id="rentnext" href="#">
                                    <img id="rnextbutton" src="images/forward-button.png" /></a>
                        </div>
                </div>
                <div id="s3-content">
                        <div class="loading">&nbsp;</div>
        </div>
               <%-- <%= Html.RenderPartial("Cntrls/Default/rental_cntrl", rentListing)%>--%>
            </div>
            <%--<div class="open-house-col right clearfix corners relative">
             <div class="title">
                    Featured Open House
                </div>
                <%=Html.RenderPartial("Cntrls/Default/featured_openhouse",openhouses) %>
            </div>--%>
        </div>
    </div>
</asp:Content>
